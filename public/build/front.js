(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["front"],{

/***/ "./assets/js/front.js":
/*!****************************!*\
  !*** ./assets/js/front.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _front_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./front/forms */ "./assets/js/front/forms.js");
/* harmony import */ var _front_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./front/common */ "./assets/js/front/common.js");
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you require will output into a single css file (app.css in this case)
__webpack_require__(/*! ../scss/front.scss */ "./assets/scss/front.scss"); // Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');


console.log('Hello Webpack Encore! Edit me in assets/js/app.js');



/***/ }),

/***/ "./assets/js/front/common.js":
/*!***********************************!*\
  !*** ./assets/js/front/common.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_parse_int__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.parse-int */ "./node_modules/core-js/modules/es.parse-int.js");
/* harmony import */ var core_js_modules_es_parse_int__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_int__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ion-rangeslider */ "./node_modules/ion-rangeslider/js/ion.rangeSlider.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ion-rangeslider/css/ion.rangeSlider.css */ "./node_modules/ion-rangeslider/css/ion.rangeSlider.css");
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var magnific_popup__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! magnific-popup */ "./node_modules/magnific-popup/dist/jquery.magnific-popup.js");
/* harmony import */ var magnific_popup__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(magnific_popup__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var magnific_popup_dist_magnific_popup_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! magnific-popup/dist/magnific-popup.css */ "./node_modules/magnific-popup/dist/magnific-popup.css");
/* harmony import */ var magnific_popup_dist_magnific_popup_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(magnific_popup_dist_magnific_popup_css__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var smoothscroll__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! smoothscroll */ "./node_modules/smoothscroll/smoothscroll.js");
/* harmony import */ var smoothscroll__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(smoothscroll__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var mobile_detect_mobile_detect_min__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! mobile-detect/mobile-detect.min */ "./node_modules/mobile-detect/mobile-detect.min.js");
/* harmony import */ var mobile_detect_mobile_detect_min__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(mobile_detect_mobile_detect_min__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var lightbox2__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lightbox2 */ "./node_modules/lightbox2/dist/js/lightbox.js");
/* harmony import */ var lightbox2__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lightbox2__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var lightbox2_dist_css_lightbox_css__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! lightbox2/dist/css/lightbox.css */ "./node_modules/lightbox2/dist/css/lightbox.css");
/* harmony import */ var lightbox2_dist_css_lightbox_css__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(lightbox2_dist_css_lightbox_css__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _masonry_pkgd_min__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./masonry.pkgd.min */ "./assets/js/front/masonry.pkgd.min.js");
/* harmony import */ var _masonry_pkgd_min__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_masonry_pkgd_min__WEBPACK_IMPORTED_MODULE_17__);










function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }






 //import 'jquery-colorbox';
//import 'jquery-colorbox/example2/colorbox.css';





var common = function () {
  return {
    init: function init() {
      //let md = new MobileDetect(window.navigator.userAgent);
      common.initMap();
      common.initSlider();
      common.initCalc();
      common.initPopups();
      common.initSmoothScroll();
      common.initMobileMenu();
      common.initStickyHeader();
      common.initAntiSpam(); //common.initColorBox();
      //common.initMasonry();
    },
    initMap: function initMap() {
      if (typeof ymaps !== 'undefined') {
        ymaps.ready(function () {
          var center = [59.984458, 30.360640];
          var myMap = new ymaps.Map('map', {
            center: center,
            zoom: 16,
            controls: []
          }, {
            searchControlProvider: 'yandex#search'
          });
          var marker = new ymaps.Placemark([59.984458, 30.360640], {
            hintContent: '195197, Россия, Санкт-Петербург, Полюстровский проспект, 72Б , оф. 108',
            balloonContent: '195197, Россия, Санкт-Петербург, Полюстровский проспект, 72Б , оф. 108'
          }, {
            iconLayout: 'default#image',
            iconImageHref: 'build/img/map.svg',
            iconImageSize: [41, 48],
            iconImageOffset: [-5, -38]
          });
          myMap.behaviors.disable('scrollZoom');
          myMap.geoObjects.add(marker);
        });
      }
    },
    initSlider: function initSlider() {
      $(".js-slider").ionRangeSlider({
        min: 10,
        max: 150,
        from: 50,
        skin: "round"
      });
    },
    initCalc: function initCalc() {
      $('.js-calc__next').click(function (e) {
        e.preventDefault();
        var $calc = $(this).closest('.js-calc'),
            step = parseInt($calc.attr('data-step')),
            nextStepId = step + 1,
            $nextStep = $calc.find('[data-id="' + String(nextStepId) + '"]');
        $('.js-step').removeClass('active');
        $nextStep.addClass('active');

        if (nextStepId > 5) {
          $('.js-calc__nav').hide();
        }

        $('.js-calc__back').removeClass('disabled');
        $('.js-step-current').text(nextStepId);
        $calc.attr('data-step', nextStepId);
      });
      $('.js-calc__back').click(function (e) {
        e.preventDefault();
        var $calc = $(this).closest('.js-calc'),
            step = parseInt($calc.attr('data-step')),
            prevStepId = step - 1,
            $prevStep = $calc.find('[data-id="' + String(prevStepId) + '"]');

        if (step === 1) {
          return;
        } else if (step === 2) {
          $('.js-calc__back').addClass('disabled');
        }

        $('.js-step').removeClass('active');
        $prevStep.addClass('active');
        $calc.attr('data-step', prevStepId);
        $('.js-step-current').text(prevStepId);

        if (step <= 6) {
          $('.js-calc__nav').show();
        }
      });
      $('.js-calc-form').on('form_success', function () {
        $('.js-form__content').hide();
        $(this).find('.js-form__success').show();
        $('.js-form-success-phone').text($(this).find('.js-phone').val());
      });
    },
    initPopups: function initPopups() {
      $('.js-popup').click(function (e) {
        e.preventDefault();
        var src = $(this).attr('href');

        if (!(_typeof($(this).data('form')) === undefined)) {
          $('.js-form-name').attr('value', $(this).data('form'));
        }

        $.magnificPopup.open({
          items: {
            src: src,
            // can be a HTML string, jQuery object, or CSS selector
            type: 'inline'
          }
        });
      });
      $('.js-mfp-close').click(function () {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
      });
      $('.js-popup-back').click(function (e) {
        e.preventDefault();
        var $form = $(this).closest('.js-form'),
            $success = $form.find('.js-form__success'),
            $content = $form.find('.js-form__content');
        $success.hide();
        $content.show();
      });
      $('.js-popup__form').on('form_success', function () {
        $(this).find('.js-form__content').hide();
        $(this).find('.js-form__success').show();
        $('.js-form-success-phone').text($(this).find('.js-phone').val());
      });
    },
    initSmoothScroll: function initSmoothScroll() {//var scroll = new SmoothScroll('a[href*="#"]');
    },
    initMobileMenu: function initMobileMenu() {
      $('.js-menu-toggler').click(function (e) {
        e.preventDefault();
        $('.js-mobile-menu').toggleClass('open');
      });
      $('.js-mobile-menu__link').click(function (e) {
        $('.js-mobile-menu').toggleClass('open');
      });
    },
    initStickyHeader: function initStickyHeader() {
      var nav = $('.js-header');
      var lastScrollTop;
      window.addEventListener("scroll", function () {
        // listen for the scroll
        var st = window.pageYOffset || document.documentElement.scrollTop;

        if (st > lastScrollTop) {
          nav.css('opacity', '0'); // hide the nav-bar when going down

          nav.removeClass('sticky');
        } else {
          nav.css('opacity', 1); // display the nav-bar when going up

          nav.addClass('sticky');
        }

        lastScrollTop = st;

        if (st < 10) {
          nav.removeClass('sticky');
        }
      }, false);
    },
    initAntiSpam: function initAntiSpam() {
      $(document).ready(function () {
        $('.js-antispam').val('no-bots-antispam');
      });
    },
    initColorBox: function initColorBox() {
      $('.js-colorbox').colorbox({
        rel: 'gal'
      });
    },
    initMasonry: function initMasonry() {
      $('.js-masonry-grid').masonry({
        // options
        itemSelector: '.js-grid-item',
        columnWidth: 200
      });
    }
  };
}();

common.init();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/front/forms.js":
/*!**********************************!*\
  !*** ./assets/js/front/forms.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.object.keys */ "./node_modules/core-js/modules/es.object.keys.js");
/* harmony import */ var core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_parse_float__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.parse-float */ "./node_modules/core-js/modules/es.parse-float.js");
/* harmony import */ var core_js_modules_es_parse_float__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_float__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var jquery_maskedinput_src_jquery_maskedinput__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! jquery.maskedinput/src/jquery.maskedinput */ "./node_modules/jquery.maskedinput/src/jquery.maskedinput.js");
/* harmony import */ var jquery_maskedinput_src_jquery_maskedinput__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(jquery_maskedinput_src_jquery_maskedinput__WEBPACK_IMPORTED_MODULE_6__);








var forms = function () {
  var emailRe = /^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i;
  var phoneRe = /^[- +()0-9]+$/i;
  return {
    init: function init() {
      forms.phoneMask();
      forms.createNumberType();
      forms.placeholderMove();
      forms.inputChange();
      forms.submit();
      forms.placeholderInit();
      forms.processing();
    },

    /**
     * Добавляет маску на инпуты с типом phone
     */
    phoneMask: function phoneMask() {
      var $phoneInput = $(".js-phone");

      if ($phoneInput.length) {
        $phoneInput.mask("+7 (999) 999-99-99", {
          autoclear: false
        });
      }
    },

    /**
     * Инициализирует инпуты с типом Number
     */
    createNumberType: function createNumberType() {
      $('.js-form__number').each(function () {
        var $input = $(this),
            $plus = $input.siblings('.js-form__number-plus'),
            $minus = $input.siblings('.js-form__number-minus'),
            min = parseFloat($input.attr('min')),
            max = parseFloat($input.attr('max')),
            step = $input.attr('step') ? parseFloat($input.attr('step')) : 1,
            oldValue = parseFloat($input.val());
        $input.data('start-value', $input.val());
        $input.data('prev-value', $input.val());

        if (oldValue >= max) {
          $input.val(max);
          $plus.addClass('maxed');
        } else if (oldValue <= min) {
          $input.val(min);
          $minus.addClass('maxed');
        }

        $input.change(function () {
          oldValue = parseFloat($(this).val());

          if (oldValue >= max) {
            $input.val(max);
            $plus.addClass('maxed');
            $minus.removeClass('maxed');
            oldValue = max;
          } else if (oldValue <= min) {
            $input.val(min);
            $minus.addClass('maxed');
            $plus.removeClass('maxed');
            oldValue = min;
          }

          oldValue = $(this).val() - $(this).data('prev-value');
          $(this).data('prev-value', $(this).val());
        });
        $plus.click(function () {
          oldValue = parseFloat($input.val());
          $minus.removeClass('maxed');

          if (oldValue >= max) {
            $input.val(oldValue);
            $plus.addClass('maxed');
          } else {
            $plus.removeClass('maxed');
            $input.val(oldValue + step);
            $input.data('prev-value', +$input.data('prev-value') + step);

            if (oldValue + step >= max) {
              $plus.addClass('maxed');
            }

            $input.trigger('change');
          }
        });
        $minus.click(function () {
          oldValue = parseFloat($input.val());
          $plus.removeClass('maxed');

          if (oldValue <= min) {
            $input.val(oldValue);
            $minus.addClass('maxed');
          } else {
            $minus.removeClass('maxed');
            $input.val(oldValue - step);
            $input.data('prev-value', +$input.data('prev-value') - step);

            if (oldValue - step <= min) {
              $minus.addClass('maxed');
            }

            $input.trigger('change');
          }
        });
      });
    },
    placeholderInit: function placeholderInit() {
      $('.js-input').each(function () {
        var $input = $(this);

        if ($input.val() !== '') {
          $input.addClass('active');
        } else {
          $input.removeClass('active valid');
        }
      });
    },

    /**
     * Сдвигает плейсхолдер при фокусе добавляя класс
     */
    placeholderMove: function placeholderMove() {
      $('.js-input').change(function () {
        if ($(this).val() !== '') {
          $(this).addClass('active');
        } else {
          $(this).removeClass('active valid');
        }
      });
    },

    /**
     * Убирает класс ошибки при фокусе и
     * инициализирует валидацию на событие change
     */
    inputChange: function inputChange() {
      var $inputs = $('.js-input');
      $inputs.focus(function () {
        $(this).removeClass('error');
      });
      $inputs.on('change', function () {
        if ($(this).hasClass('js-required') || $(this).hasClass('js-validate')) {
          forms.inputValidate($(this));
        }
      });
    },

    /**
     * Валидация инпута
     */
    inputValidate: function inputValidate($element) {
      var value = $element.val(),
          valid = false,
          $parent = $($element).parent('.form__text-input-b, .form__checkbox-input-b');

      if (value.length === 0 && !$element.hasClass('js-required')) {
        //console.log($element);
        $element.removeClass('error').removeClass('valid');

        if ($parent.length) {
          $parent.removeClass('valid');
        }

        return;
      }

      switch ($element.attr('type')) {
        case 'email':
          valid = emailRe.test(value);
          break;

        case 'tel':
          valid = phoneRe.test(value);
          break;

        case 'checkbox':
          valid = $element.prop('checked');
          break;

        default:
          valid = value !== "";
          break;
      }

      if (valid) {
        $element.removeClass('error').addClass('valid');

        if ($parent.length) {
          $parent.addClass('valid').removeClass('error');
        }
      } else if ($element.hasClass('js-required') || $element.hasClass('js-validate')) {
        $element.removeClass('valid').addClass('error');

        if ($parent.length) {
          //console.log('parent error')
          $parent.removeClass('valid').addClass('error');
        }
      }
    },

    /**
     * Обработать ответ страницы
     */
    isErorrsReturnAndRender: function isErorrsReturnAndRender($form, response) {
      var errorMessage,
          hasErorrs = false;

      if (!response['errors'] || !Object.keys(response['errors']).length) {
        $form.find('.js-input').each(function () {
          var $input = $(this);
          var $parent = $input.parent();
          $input.removeClass('error').addClass('valid');
          $parent.addClass('valid');
        });
        var $success = $form.find('.js-form__success'),
            $content = $form.find('.js-form__content');

        if ($success.length) {
          $success.addClass('active');
        }

        if ($content.length) {
          $content.hide();
        }
      } else if (response['errors']['all']) {
        errorMessage = response['errors']['all'];
        forms.showErrorMessage($form, errorMessage);
        hasErorrs = true;
      } else if (response['errors']) {
        $form.find('.js-input').each(function () {
          var $input = $(this);
          var $parent = $input.parent();

          if (response['errors'][$input.attr('name')]) {
            errorMessage = response['errors'][$input.attr('name')];
            $parent.removeClass('valid');
            $input.addClass('error');
            forms.showErrorMessage($parent, errorMessage);
          } else if (response['errors']['all-nomessage']) {
            $parent.removeClass('valid');
            $input.removeClass('valid');
            $input.addClass('error');
          } else {
            $input.removeClass('error');
          }
        });
        hasErorrs = true;
      }

      return hasErorrs;
    },

    /**
     * Отправка формы
     */
    submit: function submit() {
      $('.js-form').submit(function (event) {
        event.preventDefault();
        var $form = $(this),
            dataCorrect = true,
            method,
            contentType,
            processData,
            data;
        $form.find('.js-input.js-required').each(function () {
          //:visible
          forms.inputValidate($(this));
        });
        $form.find('.js-required').each(function () {
          if ($(this).hasClass('error')) {
            dataCorrect = false;
          }
        });

        if (dataCorrect) {
          $form.trigger('form_start');

          if ($form.hasClass('js-login-form')) {
            data = JSON.stringify({
              username: $('.js-username').val(),
              password: $('.js-password').val(),
              token: $('.js-token').val()
            });
            contentType = 'application/json';
            processData = true;
          } else {
            data = new FormData($form[0]);
            contentType = false;
            processData = false;
          }

          method = $form.attr('method');
          $.ajax({
            type: method ? method : 'POST',
            url: $form.attr('action'),
            data: data,
            dataType: "json",
            contentType: contentType,
            processData: processData,
            success: function success(resp) {
              if (!forms.isErorrsReturnAndRender($form, resp)) {
                $form.trigger('form_success', resp);
              } else {
                $form.trigger('form_error', resp);
              }
            },
            error: function error() {
              forms.showErrorMessage($form, [$form.data('error-msg')]);
              $form.trigger('form_error');
            }
          });
        } else {
          return false;
        }
      });
      $('.js-submit').click(function (e) {
        e.preventDefault();
        $(this).closest('.js-form').submit();
      });
    },

    /**
     * Вывести сообщение об ошибке в форме
     */
    showErrorMessage: function showErrorMessage($element, $messages) {
      for (var $key in $messages) {
        var $error = $('<div class="form__send-fail" style="opacity: 0">' + $messages[$key] + '</div>'); //if ($element.data('error-msg')) {

        $element.append($error);
        $error.fadeTo("fast", 1, function () {
          setTimeout(function () {
            $error.first().fadeOut("fast", function () {
              $element.find('.form__send-fail').remove();
            });
          }, 1500);
        }); //}
      }
    },
    processing: function processing() {
      $('.js-form').on('form_start', function () {
        $(this).addClass('processing');
      });
      $('.js-form').on('form_success form_error', function () {
        $(this).removeClass('processing');
      });
      $('.js-login-form').on('form_success', function (e, resp) {
        window.location.replace($(this).data('redirect'));
      });
    }
  };
}();

forms.init();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/front/masonry.pkgd.min.js":
/*!*********************************************!*\
  !*** ./assets/js/front/masonry.pkgd.min.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_1__factory, __WEBPACK_LOCAL_MODULE_1__module;var __WEBPACK_LOCAL_MODULE_2__, __WEBPACK_LOCAL_MODULE_2__factory, __WEBPACK_LOCAL_MODULE_2__module;var __WEBPACK_LOCAL_MODULE_3__, __WEBPACK_LOCAL_MODULE_3__factory, __WEBPACK_LOCAL_MODULE_3__module;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_4__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_5__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_6__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");

__webpack_require__(/*! core-js/modules/es.array.filter */ "./node_modules/core-js/modules/es.array.filter.js");

__webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");

__webpack_require__(/*! core-js/modules/es.array.is-array */ "./node_modules/core-js/modules/es.array.is-array.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.array.splice */ "./node_modules/core-js/modules/es.array.splice.js");

__webpack_require__(/*! core-js/modules/es.object.create */ "./node_modules/core-js/modules/es.object.create.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.parse-float */ "./node_modules/core-js/modules/es.parse-float.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/es.string.match */ "./node_modules/core-js/modules/es.string.match.js");

__webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * Masonry PACKAGED v4.2.2
 * Cascading grid layout library
 * https://masonry.desandro.com
 * MIT License
 * by David DeSandro
 */
!function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (i) {
    return e(t, i);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(window, function (t, e) {
  "use strict";

  function i(i, r, a) {
    function h(t, e, n) {
      var o,
          r = "$()." + i + '("' + e + '")';
      return t.each(function (t, h) {
        var u = a.data(h, i);
        if (!u) return void s(i + " not initialized. Cannot call methods, i.e. " + r);
        var d = u[e];
        if (!d || "_" == e.charAt(0)) return void s(r + " is not a valid method");
        var l = d.apply(u, n);
        o = void 0 === o ? l : o;
      }), void 0 !== o ? o : t;
    }

    function u(t, e) {
      t.each(function (t, n) {
        var o = a.data(n, i);
        o ? (o.option(e), o._init()) : (o = new r(n, e), a.data(n, i, o));
      });
    }

    a = a || e || t.jQuery, a && (r.prototype.option || (r.prototype.option = function (t) {
      a.isPlainObject(t) && (this.options = a.extend(!0, this.options, t));
    }), a.fn[i] = function (t) {
      if ("string" == typeof t) {
        var e = o.call(arguments, 1);
        return h(this, t, e);
      }

      return u(this, t), this;
    }, n(a));
  }

  function n(t) {
    !t || t && t.bridget || (t.bridget = i);
  }

  var o = Array.prototype.slice,
      r = t.console,
      s = "undefined" == typeof r ? function () {} : function (t) {
    r.error(t);
  };
  return n(e || t.jQuery), i;
}), function (t, e) {
   true ? !(__WEBPACK_LOCAL_MODULE_1__factory = (e), (__WEBPACK_LOCAL_MODULE_1__module = { id: "ev-emitter/ev-emitter", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_1__ = (typeof __WEBPACK_LOCAL_MODULE_1__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_1__factory.call(__WEBPACK_LOCAL_MODULE_1__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_1__module.exports, __WEBPACK_LOCAL_MODULE_1__module)) : __WEBPACK_LOCAL_MODULE_1__factory), (__WEBPACK_LOCAL_MODULE_1__module.loaded = true), __WEBPACK_LOCAL_MODULE_1__ === undefined && (__WEBPACK_LOCAL_MODULE_1__ = __WEBPACK_LOCAL_MODULE_1__module.exports)) : undefined;
}("undefined" != typeof window ? window : this, function () {
  function t() {}

  var e = t.prototype;
  return e.on = function (t, e) {
    if (t && e) {
      var i = this._events = this._events || {},
          n = i[t] = i[t] || [];
      return -1 == n.indexOf(e) && n.push(e), this;
    }
  }, e.once = function (t, e) {
    if (t && e) {
      this.on(t, e);
      var i = this._onceEvents = this._onceEvents || {},
          n = i[t] = i[t] || {};
      return n[e] = !0, this;
    }
  }, e.off = function (t, e) {
    var i = this._events && this._events[t];

    if (i && i.length) {
      var n = i.indexOf(e);
      return -1 != n && i.splice(n, 1), this;
    }
  }, e.emitEvent = function (t, e) {
    var i = this._events && this._events[t];

    if (i && i.length) {
      i = i.slice(0), e = e || [];

      for (var n = this._onceEvents && this._onceEvents[t], o = 0; o < i.length; o++) {
        var r = i[o],
            s = n && n[r];
        s && (this.off(t, r), delete n[r]), r.apply(this, e);
      }

      return this;
    }
  }, e.allOff = function () {
    delete this._events, delete this._onceEvents;
  }, t;
}), function (t, e) {
   true ? !(__WEBPACK_LOCAL_MODULE_2__factory = (e), (__WEBPACK_LOCAL_MODULE_2__module = { id: "get-size/get-size", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_2__ = (typeof __WEBPACK_LOCAL_MODULE_2__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_2__factory.call(__WEBPACK_LOCAL_MODULE_2__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_2__module.exports, __WEBPACK_LOCAL_MODULE_2__module)) : __WEBPACK_LOCAL_MODULE_2__factory), (__WEBPACK_LOCAL_MODULE_2__module.loaded = true), __WEBPACK_LOCAL_MODULE_2__ === undefined && (__WEBPACK_LOCAL_MODULE_2__ = __WEBPACK_LOCAL_MODULE_2__module.exports)) : undefined;
}(window, function () {
  "use strict";

  function t(t) {
    var e = parseFloat(t),
        i = -1 == t.indexOf("%") && !isNaN(e);
    return i && e;
  }

  function e() {}

  function i() {
    for (var t = {
      width: 0,
      height: 0,
      innerWidth: 0,
      innerHeight: 0,
      outerWidth: 0,
      outerHeight: 0
    }, e = 0; u > e; e++) {
      var i = h[e];
      t[i] = 0;
    }

    return t;
  }

  function n(t) {
    var e = getComputedStyle(t);
    return e || a("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See https://bit.ly/getsizebug1"), e;
  }

  function o() {
    if (!d) {
      d = !0;
      var e = document.createElement("div");
      e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style.boxSizing = "border-box";
      var i = document.body || document.documentElement;
      i.appendChild(e);
      var o = n(e);
      s = 200 == Math.round(t(o.width)), r.isBoxSizeOuter = s, i.removeChild(e);
    }
  }

  function r(e) {
    if (o(), "string" == typeof e && (e = document.querySelector(e)), e && "object" == _typeof(e) && e.nodeType) {
      var r = n(e);
      if ("none" == r.display) return i();
      var a = {};
      a.width = e.offsetWidth, a.height = e.offsetHeight;

      for (var d = a.isBorderBox = "border-box" == r.boxSizing, l = 0; u > l; l++) {
        var c = h[l],
            f = r[c],
            m = parseFloat(f);
        a[c] = isNaN(m) ? 0 : m;
      }

      var p = a.paddingLeft + a.paddingRight,
          g = a.paddingTop + a.paddingBottom,
          y = a.marginLeft + a.marginRight,
          v = a.marginTop + a.marginBottom,
          _ = a.borderLeftWidth + a.borderRightWidth,
          z = a.borderTopWidth + a.borderBottomWidth,
          E = d && s,
          b = t(r.width);

      b !== !1 && (a.width = b + (E ? 0 : p + _));
      var x = t(r.height);
      return x !== !1 && (a.height = x + (E ? 0 : g + z)), a.innerWidth = a.width - (p + _), a.innerHeight = a.height - (g + z), a.outerWidth = a.width + y, a.outerHeight = a.height + v, a;
    }
  }

  var s,
      a = "undefined" == typeof console ? e : function (t) {
    console.error(t);
  },
      h = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
      u = h.length,
      d = !1;
  return r;
}), function (t, e) {
  "use strict";

   true ? !(__WEBPACK_LOCAL_MODULE_3__factory = (e), (__WEBPACK_LOCAL_MODULE_3__module = { id: "desandro-matches-selector/matches-selector", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_3__ = (typeof __WEBPACK_LOCAL_MODULE_3__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_3__factory.call(__WEBPACK_LOCAL_MODULE_3__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_3__module.exports, __WEBPACK_LOCAL_MODULE_3__module)) : __WEBPACK_LOCAL_MODULE_3__factory), (__WEBPACK_LOCAL_MODULE_3__module.loaded = true), __WEBPACK_LOCAL_MODULE_3__ === undefined && (__WEBPACK_LOCAL_MODULE_3__ = __WEBPACK_LOCAL_MODULE_3__module.exports)) : undefined;
}(window, function () {
  "use strict";

  var t = function () {
    var t = window.Element.prototype;
    if (t.matches) return "matches";
    if (t.matchesSelector) return "matchesSelector";

    for (var e = ["webkit", "moz", "ms", "o"], i = 0; i < e.length; i++) {
      var n = e[i],
          o = n + "MatchesSelector";
      if (t[o]) return o;
    }
  }();

  return function (e, i) {
    return e[t](i);
  };
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_3__], __WEBPACK_LOCAL_MODULE_4__ = ((function (i) {
    return e(t, i);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__))) : undefined;
}(window, function (t, e) {
  var i = {};
  i.extend = function (t, e) {
    for (var i in e) {
      t[i] = e[i];
    }

    return t;
  }, i.modulo = function (t, e) {
    return (t % e + e) % e;
  };
  var n = Array.prototype.slice;
  i.makeArray = function (t) {
    if (Array.isArray(t)) return t;
    if (null === t || void 0 === t) return [];
    var e = "object" == _typeof(t) && "number" == typeof t.length;
    return e ? n.call(t) : [t];
  }, i.removeFrom = function (t, e) {
    var i = t.indexOf(e);
    -1 != i && t.splice(i, 1);
  }, i.getParent = function (t, i) {
    for (; t.parentNode && t != document.body;) {
      if (t = t.parentNode, e(t, i)) return t;
    }
  }, i.getQueryElement = function (t) {
    return "string" == typeof t ? document.querySelector(t) : t;
  }, i.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t);
  }, i.filterFindElements = function (t, n) {
    t = i.makeArray(t);
    var o = [];
    return t.forEach(function (t) {
      if (t instanceof HTMLElement) {
        if (!n) return void o.push(t);
        e(t, n) && o.push(t);

        for (var i = t.querySelectorAll(n), r = 0; r < i.length; r++) {
          o.push(i[r]);
        }
      }
    }), o;
  }, i.debounceMethod = function (t, e, i) {
    i = i || 100;
    var n = t.prototype[e],
        o = e + "Timeout";

    t.prototype[e] = function () {
      var t = this[o];
      clearTimeout(t);
      var e = arguments,
          r = this;
      this[o] = setTimeout(function () {
        n.apply(r, e), delete r[o];
      }, i);
    };
  }, i.docReady = function (t) {
    var e = document.readyState;
    "complete" == e || "interactive" == e ? setTimeout(t) : document.addEventListener("DOMContentLoaded", t);
  }, i.toDashed = function (t) {
    return t.replace(/(.)([A-Z])/g, function (t, e, i) {
      return e + "-" + i;
    }).toLowerCase();
  };
  var o = t.console;
  return i.htmlInit = function (e, n) {
    i.docReady(function () {
      var r = i.toDashed(n),
          s = "data-" + r,
          a = document.querySelectorAll("[" + s + "]"),
          h = document.querySelectorAll(".js-" + r),
          u = i.makeArray(a).concat(i.makeArray(h)),
          d = s + "-options",
          l = t.jQuery;
      u.forEach(function (t) {
        var i,
            r = t.getAttribute(s) || t.getAttribute(d);

        try {
          i = r && JSON.parse(r);
        } catch (a) {
          return void (o && o.error("Error parsing " + s + " on " + t.className + ": " + a));
        }

        var h = new e(t, i);
        l && l.data(t, n, h);
      });
    });
  }, i;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_2__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_5__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t, e) {
  "use strict";

  function i(t) {
    for (var e in t) {
      return !1;
    }

    return e = null, !0;
  }

  function n(t, e) {
    t && (this.element = t, this.layout = e, this.position = {
      x: 0,
      y: 0
    }, this._create());
  }

  function o(t) {
    return t.replace(/([A-Z])/g, function (t) {
      return "-" + t.toLowerCase();
    });
  }

  var r = document.documentElement.style,
      s = "string" == typeof r.transition ? "transition" : "WebkitTransition",
      a = "string" == typeof r.transform ? "transform" : "WebkitTransform",
      h = {
    WebkitTransition: "webkitTransitionEnd",
    transition: "transitionend"
  }[s],
      u = {
    transform: a,
    transition: s,
    transitionDuration: s + "Duration",
    transitionProperty: s + "Property",
    transitionDelay: s + "Delay"
  },
      d = n.prototype = Object.create(t.prototype);
  d.constructor = n, d._create = function () {
    this._transn = {
      ingProperties: {},
      clean: {},
      onEnd: {}
    }, this.css({
      position: "absolute"
    });
  }, d.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t);
  }, d.getSize = function () {
    this.size = e(this.element);
  }, d.css = function (t) {
    var e = this.element.style;

    for (var i in t) {
      var n = u[i] || i;
      e[n] = t[i];
    }
  }, d.getPosition = function () {
    var t = getComputedStyle(this.element),
        e = this.layout._getOption("originLeft"),
        i = this.layout._getOption("originTop"),
        n = t[e ? "left" : "right"],
        o = t[i ? "top" : "bottom"],
        r = parseFloat(n),
        s = parseFloat(o),
        a = this.layout.size;

    -1 != n.indexOf("%") && (r = r / 100 * a.width), -1 != o.indexOf("%") && (s = s / 100 * a.height), r = isNaN(r) ? 0 : r, s = isNaN(s) ? 0 : s, r -= e ? a.paddingLeft : a.paddingRight, s -= i ? a.paddingTop : a.paddingBottom, this.position.x = r, this.position.y = s;
  }, d.layoutPosition = function () {
    var t = this.layout.size,
        e = {},
        i = this.layout._getOption("originLeft"),
        n = this.layout._getOption("originTop"),
        o = i ? "paddingLeft" : "paddingRight",
        r = i ? "left" : "right",
        s = i ? "right" : "left",
        a = this.position.x + t[o];

    e[r] = this.getXValue(a), e[s] = "";
    var h = n ? "paddingTop" : "paddingBottom",
        u = n ? "top" : "bottom",
        d = n ? "bottom" : "top",
        l = this.position.y + t[h];
    e[u] = this.getYValue(l), e[d] = "", this.css(e), this.emitEvent("layout", [this]);
  }, d.getXValue = function (t) {
    var e = this.layout._getOption("horizontal");

    return this.layout.options.percentPosition && !e ? t / this.layout.size.width * 100 + "%" : t + "px";
  }, d.getYValue = function (t) {
    var e = this.layout._getOption("horizontal");

    return this.layout.options.percentPosition && e ? t / this.layout.size.height * 100 + "%" : t + "px";
  }, d._transitionTo = function (t, e) {
    this.getPosition();
    var i = this.position.x,
        n = this.position.y,
        o = t == this.position.x && e == this.position.y;
    if (this.setPosition(t, e), o && !this.isTransitioning) return void this.layoutPosition();
    var r = t - i,
        s = e - n,
        a = {};
    a.transform = this.getTranslate(r, s), this.transition({
      to: a,
      onTransitionEnd: {
        transform: this.layoutPosition
      },
      isCleaning: !0
    });
  }, d.getTranslate = function (t, e) {
    var i = this.layout._getOption("originLeft"),
        n = this.layout._getOption("originTop");

    return t = i ? t : -t, e = n ? e : -e, "translate3d(" + t + "px, " + e + "px, 0)";
  }, d.goTo = function (t, e) {
    this.setPosition(t, e), this.layoutPosition();
  }, d.moveTo = d._transitionTo, d.setPosition = function (t, e) {
    this.position.x = parseFloat(t), this.position.y = parseFloat(e);
  }, d._nonTransition = function (t) {
    this.css(t.to), t.isCleaning && this._removeStyles(t.to);

    for (var e in t.onTransitionEnd) {
      t.onTransitionEnd[e].call(this);
    }
  }, d.transition = function (t) {
    if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(t);
    var e = this._transn;

    for (var i in t.onTransitionEnd) {
      e.onEnd[i] = t.onTransitionEnd[i];
    }

    for (i in t.to) {
      e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
    }

    if (t.from) {
      this.css(t.from);
      var n = this.element.offsetHeight;
      n = null;
    }

    this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0;
  };
  var l = "opacity," + o(a);
  d.enableTransition = function () {
    if (!this.isTransitioning) {
      var t = this.layout.options.transitionDuration;
      t = "number" == typeof t ? t + "ms" : t, this.css({
        transitionProperty: l,
        transitionDuration: t,
        transitionDelay: this.staggerDelay || 0
      }), this.element.addEventListener(h, this, !1);
    }
  }, d.onwebkitTransitionEnd = function (t) {
    this.ontransitionend(t);
  }, d.onotransitionend = function (t) {
    this.ontransitionend(t);
  };
  var c = {
    "-webkit-transform": "transform"
  };
  d.ontransitionend = function (t) {
    if (t.target === this.element) {
      var e = this._transn,
          n = c[t.propertyName] || t.propertyName;

      if (delete e.ingProperties[n], i(e.ingProperties) && this.disableTransition(), n in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[n]), n in e.onEnd) {
        var o = e.onEnd[n];
        o.call(this), delete e.onEnd[n];
      }

      this.emitEvent("transitionEnd", [this]);
    }
  }, d.disableTransition = function () {
    this.removeTransitionStyles(), this.element.removeEventListener(h, this, !1), this.isTransitioning = !1;
  }, d._removeStyles = function (t) {
    var e = {};

    for (var i in t) {
      e[i] = "";
    }

    this.css(e);
  };
  var f = {
    transitionProperty: "",
    transitionDuration: "",
    transitionDelay: ""
  };
  return d.removeTransitionStyles = function () {
    this.css(f);
  }, d.stagger = function (t) {
    t = isNaN(t) ? 0 : t, this.staggerDelay = t + "ms";
  }, d.removeElem = function () {
    this.element.parentNode.removeChild(this.element), this.css({
      display: ""
    }), this.emitEvent("remove", [this]);
  }, d.remove = function () {
    return s && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function () {
      this.removeElem();
    }), void this.hide()) : void this.removeElem();
  }, d.reveal = function () {
    delete this.isHidden, this.css({
      display: ""
    });
    var t = this.layout.options,
        e = {},
        i = this.getHideRevealTransitionEndProperty("visibleStyle");
    e[i] = this.onRevealTransitionEnd, this.transition({
      from: t.hiddenStyle,
      to: t.visibleStyle,
      isCleaning: !0,
      onTransitionEnd: e
    });
  }, d.onRevealTransitionEnd = function () {
    this.isHidden || this.emitEvent("reveal");
  }, d.getHideRevealTransitionEndProperty = function (t) {
    var e = this.layout.options[t];
    if (e.opacity) return "opacity";

    for (var i in e) {
      return i;
    }
  }, d.hide = function () {
    this.isHidden = !0, this.css({
      display: ""
    });
    var t = this.layout.options,
        e = {},
        i = this.getHideRevealTransitionEndProperty("hiddenStyle");
    e[i] = this.onHideTransitionEnd, this.transition({
      from: t.visibleStyle,
      to: t.hiddenStyle,
      isCleaning: !0,
      onTransitionEnd: e
    });
  }, d.onHideTransitionEnd = function () {
    this.isHidden && (this.css({
      display: "none"
    }), this.emitEvent("hide"));
  }, d.destroy = function () {
    this.css({
      position: "",
      left: "",
      right: "",
      top: "",
      bottom: "",
      transition: "",
      transform: ""
    });
  }, n;
}), function (t, e) {
  "use strict";

   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_2__, __WEBPACK_LOCAL_MODULE_4__, __WEBPACK_LOCAL_MODULE_5__], __WEBPACK_LOCAL_MODULE_6__ = ((function (i, n, o, r) {
    return e(t, i, n, o, r);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__))) : undefined;
}(window, function (t, e, i, n, o) {
  "use strict";

  function r(t, e) {
    var i = n.getQueryElement(t);
    if (!i) return void (h && h.error("Bad element for " + this.constructor.namespace + ": " + (i || t)));
    this.element = i, u && (this.$element = u(this.element)), this.options = n.extend({}, this.constructor.defaults), this.option(e);
    var o = ++l;
    this.element.outlayerGUID = o, c[o] = this, this._create();

    var r = this._getOption("initLayout");

    r && this.layout();
  }

  function s(t) {
    function e() {
      t.apply(this, arguments);
    }

    return e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e;
  }

  function a(t) {
    if ("number" == typeof t) return t;
    var e = t.match(/(^\d*\.?\d*)(\w*)/),
        i = e && e[1],
        n = e && e[2];
    if (!i.length) return 0;
    i = parseFloat(i);
    var o = m[n] || 1;
    return i * o;
  }

  var h = t.console,
      u = t.jQuery,
      d = function d() {},
      l = 0,
      c = {};

  r.namespace = "outlayer", r.Item = o, r.defaults = {
    containerStyle: {
      position: "relative"
    },
    initLayout: !0,
    originLeft: !0,
    originTop: !0,
    resize: !0,
    resizeContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
      opacity: 0,
      transform: "scale(0.001)"
    },
    visibleStyle: {
      opacity: 1,
      transform: "scale(1)"
    }
  };
  var f = r.prototype;
  n.extend(f, e.prototype), f.option = function (t) {
    n.extend(this.options, t);
  }, f._getOption = function (t) {
    var e = this.constructor.compatOptions[t];
    return e && void 0 !== this.options[e] ? this.options[e] : this.options[t];
  }, r.compatOptions = {
    initLayout: "isInitLayout",
    horizontal: "isHorizontal",
    layoutInstant: "isLayoutInstant",
    originLeft: "isOriginLeft",
    originTop: "isOriginTop",
    resize: "isResizeBound",
    resizeContainer: "isResizingContainer"
  }, f._create = function () {
    this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), n.extend(this.element.style, this.options.containerStyle);

    var t = this._getOption("resize");

    t && this.bindResize();
  }, f.reloadItems = function () {
    this.items = this._itemize(this.element.children);
  }, f._itemize = function (t) {
    for (var e = this._filterFindItemElements(t), i = this.constructor.Item, n = [], o = 0; o < e.length; o++) {
      var r = e[o],
          s = new i(r, this);
      n.push(s);
    }

    return n;
  }, f._filterFindItemElements = function (t) {
    return n.filterFindElements(t, this.options.itemSelector);
  }, f.getItemElements = function () {
    return this.items.map(function (t) {
      return t.element;
    });
  }, f.layout = function () {
    this._resetLayout(), this._manageStamps();

    var t = this._getOption("layoutInstant"),
        e = void 0 !== t ? t : !this._isLayoutInited;

    this.layoutItems(this.items, e), this._isLayoutInited = !0;
  }, f._init = f.layout, f._resetLayout = function () {
    this.getSize();
  }, f.getSize = function () {
    this.size = i(this.element);
  }, f._getMeasurement = function (t, e) {
    var n,
        o = this.options[t];
    o ? ("string" == typeof o ? n = this.element.querySelector(o) : o instanceof HTMLElement && (n = o), this[t] = n ? i(n)[e] : o) : this[t] = 0;
  }, f.layoutItems = function (t, e) {
    t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout();
  }, f._getItemsForLayout = function (t) {
    return t.filter(function (t) {
      return !t.isIgnored;
    });
  }, f._layoutItems = function (t, e) {
    if (this._emitCompleteOnItems("layout", t), t && t.length) {
      var i = [];
      t.forEach(function (t) {
        var n = this._getItemLayoutPosition(t);

        n.item = t, n.isInstant = e || t.isLayoutInstant, i.push(n);
      }, this), this._processLayoutQueue(i);
    }
  }, f._getItemLayoutPosition = function () {
    return {
      x: 0,
      y: 0
    };
  }, f._processLayoutQueue = function (t) {
    this.updateStagger(), t.forEach(function (t, e) {
      this._positionItem(t.item, t.x, t.y, t.isInstant, e);
    }, this);
  }, f.updateStagger = function () {
    var t = this.options.stagger;
    return null === t || void 0 === t ? void (this.stagger = 0) : (this.stagger = a(t), this.stagger);
  }, f._positionItem = function (t, e, i, n, o) {
    n ? t.goTo(e, i) : (t.stagger(o * this.stagger), t.moveTo(e, i));
  }, f._postLayout = function () {
    this.resizeContainer();
  }, f.resizeContainer = function () {
    var t = this._getOption("resizeContainer");

    if (t) {
      var e = this._getContainerSize();

      e && (this._setContainerMeasure(e.width, !0), this._setContainerMeasure(e.height, !1));
    }
  }, f._getContainerSize = d, f._setContainerMeasure = function (t, e) {
    if (void 0 !== t) {
      var i = this.size;
      i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px";
    }
  }, f._emitCompleteOnItems = function (t, e) {
    function i() {
      o.dispatchEvent(t + "Complete", null, [e]);
    }

    function n() {
      s++, s == r && i();
    }

    var o = this,
        r = e.length;
    if (!e || !r) return void i();
    var s = 0;
    e.forEach(function (e) {
      e.once(t, n);
    });
  }, f.dispatchEvent = function (t, e, i) {
    var n = e ? [e].concat(i) : i;
    if (this.emitEvent(t, n), u) if (this.$element = this.$element || u(this.element), e) {
      var o = u.Event(e);
      o.type = t, this.$element.trigger(o, i);
    } else this.$element.trigger(t, i);
  }, f.ignore = function (t) {
    var e = this.getItem(t);
    e && (e.isIgnored = !0);
  }, f.unignore = function (t) {
    var e = this.getItem(t);
    e && delete e.isIgnored;
  }, f.stamp = function (t) {
    t = this._find(t), t && (this.stamps = this.stamps.concat(t), t.forEach(this.ignore, this));
  }, f.unstamp = function (t) {
    t = this._find(t), t && t.forEach(function (t) {
      n.removeFrom(this.stamps, t), this.unignore(t);
    }, this);
  }, f._find = function (t) {
    return t ? ("string" == typeof t && (t = this.element.querySelectorAll(t)), t = n.makeArray(t)) : void 0;
  }, f._manageStamps = function () {
    this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this));
  }, f._getBoundingRect = function () {
    var t = this.element.getBoundingClientRect(),
        e = this.size;
    this._boundingRect = {
      left: t.left + e.paddingLeft + e.borderLeftWidth,
      top: t.top + e.paddingTop + e.borderTopWidth,
      right: t.right - (e.paddingRight + e.borderRightWidth),
      bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
    };
  }, f._manageStamp = d, f._getElementOffset = function (t) {
    var e = t.getBoundingClientRect(),
        n = this._boundingRect,
        o = i(t),
        r = {
      left: e.left - n.left - o.marginLeft,
      top: e.top - n.top - o.marginTop,
      right: n.right - e.right - o.marginRight,
      bottom: n.bottom - e.bottom - o.marginBottom
    };
    return r;
  }, f.handleEvent = n.handleEvent, f.bindResize = function () {
    t.addEventListener("resize", this), this.isResizeBound = !0;
  }, f.unbindResize = function () {
    t.removeEventListener("resize", this), this.isResizeBound = !1;
  }, f.onresize = function () {
    this.resize();
  }, n.debounceMethod(r, "onresize", 100), f.resize = function () {
    this.isResizeBound && this.needsResizeLayout() && this.layout();
  }, f.needsResizeLayout = function () {
    var t = i(this.element),
        e = this.size && t;
    return e && t.innerWidth !== this.size.innerWidth;
  }, f.addItems = function (t) {
    var e = this._itemize(t);

    return e.length && (this.items = this.items.concat(e)), e;
  }, f.appended = function (t) {
    var e = this.addItems(t);
    e.length && (this.layoutItems(e, !0), this.reveal(e));
  }, f.prepended = function (t) {
    var e = this._itemize(t);

    if (e.length) {
      var i = this.items.slice(0);
      this.items = e.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i);
    }
  }, f.reveal = function (t) {
    if (this._emitCompleteOnItems("reveal", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function (t, i) {
        t.stagger(i * e), t.reveal();
      });
    }
  }, f.hide = function (t) {
    if (this._emitCompleteOnItems("hide", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function (t, i) {
        t.stagger(i * e), t.hide();
      });
    }
  }, f.revealItemElements = function (t) {
    var e = this.getItems(t);
    this.reveal(e);
  }, f.hideItemElements = function (t) {
    var e = this.getItems(t);
    this.hide(e);
  }, f.getItem = function (t) {
    for (var e = 0; e < this.items.length; e++) {
      var i = this.items[e];
      if (i.element == t) return i;
    }
  }, f.getItems = function (t) {
    t = n.makeArray(t);
    var e = [];
    return t.forEach(function (t) {
      var i = this.getItem(t);
      i && e.push(i);
    }, this), e;
  }, f.remove = function (t) {
    var e = this.getItems(t);
    this._emitCompleteOnItems("remove", e), e && e.length && e.forEach(function (t) {
      t.remove(), n.removeFrom(this.items, t);
    }, this);
  }, f.destroy = function () {
    var t = this.element.style;
    t.height = "", t.position = "", t.width = "", this.items.forEach(function (t) {
      t.destroy();
    }), this.unbindResize();
    var e = this.element.outlayerGUID;
    delete c[e], delete this.element.outlayerGUID, u && u.removeData(this.element, this.constructor.namespace);
  }, r.data = function (t) {
    t = n.getQueryElement(t);
    var e = t && t.outlayerGUID;
    return e && c[e];
  }, r.create = function (t, e) {
    var i = s(r);
    return i.defaults = n.extend({}, r.defaults), n.extend(i.defaults, e), i.compatOptions = n.extend({}, r.compatOptions), i.namespace = t, i.data = r.data, i.Item = s(o), n.htmlInit(i, t), u && u.bridget && u.bridget(t, i), i;
  };
  var m = {
    ms: 1,
    s: 1e3
  };
  return r.Item = o, r;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_6__, __WEBPACK_LOCAL_MODULE_2__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(window, function (t, e) {
  var i = t.create("masonry");
  i.compatOptions.fitWidth = "isFitWidth";
  var n = i.prototype;
  return n._resetLayout = function () {
    this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];

    for (var t = 0; t < this.cols; t++) {
      this.colYs.push(0);
    }

    this.maxY = 0, this.horizontalColIndex = 0;
  }, n.measureColumns = function () {
    if (this.getContainerWidth(), !this.columnWidth) {
      var t = this.items[0],
          i = t && t.element;
      this.columnWidth = i && e(i).outerWidth || this.containerWidth;
    }

    var n = this.columnWidth += this.gutter,
        o = this.containerWidth + this.gutter,
        r = o / n,
        s = n - o % n,
        a = s && 1 > s ? "round" : "floor";
    r = Math[a](r), this.cols = Math.max(r, 1);
  }, n.getContainerWidth = function () {
    var t = this._getOption("fitWidth"),
        i = t ? this.element.parentNode : this.element,
        n = e(i);

    this.containerWidth = n && n.innerWidth;
  }, n._getItemLayoutPosition = function (t) {
    t.getSize();
    var e = t.size.outerWidth % this.columnWidth,
        i = e && 1 > e ? "round" : "ceil",
        n = Math[i](t.size.outerWidth / this.columnWidth);
    n = Math.min(n, this.cols);

    for (var o = this.options.horizontalOrder ? "_getHorizontalColPosition" : "_getTopColPosition", r = this[o](n, t), s = {
      x: this.columnWidth * r.col,
      y: r.y
    }, a = r.y + t.size.outerHeight, h = n + r.col, u = r.col; h > u; u++) {
      this.colYs[u] = a;
    }

    return s;
  }, n._getTopColPosition = function (t) {
    var e = this._getTopColGroup(t),
        i = Math.min.apply(Math, e);

    return {
      col: e.indexOf(i),
      y: i
    };
  }, n._getTopColGroup = function (t) {
    if (2 > t) return this.colYs;

    for (var e = [], i = this.cols + 1 - t, n = 0; i > n; n++) {
      e[n] = this._getColGroupY(n, t);
    }

    return e;
  }, n._getColGroupY = function (t, e) {
    if (2 > e) return this.colYs[t];
    var i = this.colYs.slice(t, t + e);
    return Math.max.apply(Math, i);
  }, n._getHorizontalColPosition = function (t, e) {
    var i = this.horizontalColIndex % this.cols,
        n = t > 1 && i + t > this.cols;
    i = n ? 0 : i;
    var o = e.size.outerWidth && e.size.outerHeight;
    return this.horizontalColIndex = o ? i + t : this.horizontalColIndex, {
      col: i,
      y: this._getColGroupY(i, t)
    };
  }, n._manageStamp = function (t) {
    var i = e(t),
        n = this._getElementOffset(t),
        o = this._getOption("originLeft"),
        r = o ? n.left : n.right,
        s = r + i.outerWidth,
        a = Math.floor(r / this.columnWidth);

    a = Math.max(0, a);
    var h = Math.floor(s / this.columnWidth);
    h -= s % this.columnWidth ? 0 : 1, h = Math.min(this.cols - 1, h);

    for (var u = this._getOption("originTop"), d = (u ? n.top : n.bottom) + i.outerHeight, l = a; h >= l; l++) {
      this.colYs[l] = Math.max(d, this.colYs[l]);
    }
  }, n._getContainerSize = function () {
    this.maxY = Math.max.apply(Math, this.colYs);
    var t = {
      height: this.maxY
    };
    return this._getOption("fitWidth") && (t.width = this._getContainerFitWidth()), t;
  }, n._getContainerFitWidth = function () {
    for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) {
      t++;
    }

    return (this.cols - t) * this.columnWidth - this.gutter;
  }, n.needsResizeLayout = function () {
    var t = this.containerWidth;
    return this.getContainerWidth(), t != this.containerWidth;
  }, i;
});

/***/ }),

/***/ "./assets/scss/front.scss":
/*!********************************!*\
  !*** ./assets/scss/front.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/js/front.js","runtime","vendors~admin~front","vendors~front"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvZnJvbnQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2Zyb250L2NvbW1vbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvZnJvbnQvZm9ybXMuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2Zyb250L21hc29ucnkucGtnZC5taW4uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Njc3MvZnJvbnQuc2NzcyJdLCJuYW1lcyI6WyJyZXF1aXJlIiwiY29uc29sZSIsImxvZyIsImNvbW1vbiIsImluaXQiLCJpbml0TWFwIiwiaW5pdFNsaWRlciIsImluaXRDYWxjIiwiaW5pdFBvcHVwcyIsImluaXRTbW9vdGhTY3JvbGwiLCJpbml0TW9iaWxlTWVudSIsImluaXRTdGlja3lIZWFkZXIiLCJpbml0QW50aVNwYW0iLCJ5bWFwcyIsInJlYWR5IiwiY2VudGVyIiwibXlNYXAiLCJNYXAiLCJ6b29tIiwiY29udHJvbHMiLCJzZWFyY2hDb250cm9sUHJvdmlkZXIiLCJtYXJrZXIiLCJQbGFjZW1hcmsiLCJoaW50Q29udGVudCIsImJhbGxvb25Db250ZW50IiwiaWNvbkxheW91dCIsImljb25JbWFnZUhyZWYiLCJpY29uSW1hZ2VTaXplIiwiaWNvbkltYWdlT2Zmc2V0IiwiYmVoYXZpb3JzIiwiZGlzYWJsZSIsImdlb09iamVjdHMiLCJhZGQiLCIkIiwiaW9uUmFuZ2VTbGlkZXIiLCJtaW4iLCJtYXgiLCJmcm9tIiwic2tpbiIsImNsaWNrIiwiZSIsInByZXZlbnREZWZhdWx0IiwiJGNhbGMiLCJjbG9zZXN0Iiwic3RlcCIsInBhcnNlSW50IiwiYXR0ciIsIm5leHRTdGVwSWQiLCIkbmV4dFN0ZXAiLCJmaW5kIiwiU3RyaW5nIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsImhpZGUiLCJ0ZXh0IiwicHJldlN0ZXBJZCIsIiRwcmV2U3RlcCIsInNob3ciLCJvbiIsInZhbCIsInNyYyIsImRhdGEiLCJ1bmRlZmluZWQiLCJtYWduaWZpY1BvcHVwIiwib3BlbiIsIml0ZW1zIiwidHlwZSIsImluc3RhbmNlIiwiY2xvc2UiLCIkZm9ybSIsIiRzdWNjZXNzIiwiJGNvbnRlbnQiLCJ0b2dnbGVDbGFzcyIsIm5hdiIsImxhc3RTY3JvbGxUb3AiLCJ3aW5kb3ciLCJhZGRFdmVudExpc3RlbmVyIiwic3QiLCJwYWdlWU9mZnNldCIsImRvY3VtZW50IiwiZG9jdW1lbnRFbGVtZW50Iiwic2Nyb2xsVG9wIiwiY3NzIiwiaW5pdENvbG9yQm94IiwiY29sb3Jib3giLCJyZWwiLCJpbml0TWFzb25yeSIsIm1hc29ucnkiLCJpdGVtU2VsZWN0b3IiLCJjb2x1bW5XaWR0aCIsImZvcm1zIiwiZW1haWxSZSIsInBob25lUmUiLCJwaG9uZU1hc2siLCJjcmVhdGVOdW1iZXJUeXBlIiwicGxhY2Vob2xkZXJNb3ZlIiwiaW5wdXRDaGFuZ2UiLCJzdWJtaXQiLCJwbGFjZWhvbGRlckluaXQiLCJwcm9jZXNzaW5nIiwiJHBob25lSW5wdXQiLCJsZW5ndGgiLCJtYXNrIiwiYXV0b2NsZWFyIiwiZWFjaCIsIiRpbnB1dCIsIiRwbHVzIiwic2libGluZ3MiLCIkbWludXMiLCJwYXJzZUZsb2F0Iiwib2xkVmFsdWUiLCJjaGFuZ2UiLCJ0cmlnZ2VyIiwiJGlucHV0cyIsImZvY3VzIiwiaGFzQ2xhc3MiLCJpbnB1dFZhbGlkYXRlIiwiJGVsZW1lbnQiLCJ2YWx1ZSIsInZhbGlkIiwiJHBhcmVudCIsInBhcmVudCIsInRlc3QiLCJwcm9wIiwiaXNFcm9ycnNSZXR1cm5BbmRSZW5kZXIiLCJyZXNwb25zZSIsImVycm9yTWVzc2FnZSIsImhhc0Vyb3JycyIsIk9iamVjdCIsImtleXMiLCJzaG93RXJyb3JNZXNzYWdlIiwiZXZlbnQiLCJkYXRhQ29ycmVjdCIsIm1ldGhvZCIsImNvbnRlbnRUeXBlIiwicHJvY2Vzc0RhdGEiLCJKU09OIiwic3RyaW5naWZ5IiwidXNlcm5hbWUiLCJwYXNzd29yZCIsInRva2VuIiwiRm9ybURhdGEiLCJhamF4IiwidXJsIiwiZGF0YVR5cGUiLCJzdWNjZXNzIiwicmVzcCIsImVycm9yIiwiJG1lc3NhZ2VzIiwiJGtleSIsIiRlcnJvciIsImFwcGVuZCIsImZhZGVUbyIsInNldFRpbWVvdXQiLCJmaXJzdCIsImZhZGVPdXQiLCJyZW1vdmUiLCJsb2NhdGlvbiIsInJlcGxhY2UiLCJ0IiwiZGVmaW5lIiwiaSIsInIiLCJhIiwiaCIsIm4iLCJvIiwidSIsInMiLCJkIiwiY2hhckF0IiwibCIsImFwcGx5Iiwib3B0aW9uIiwiX2luaXQiLCJqUXVlcnkiLCJwcm90b3R5cGUiLCJpc1BsYWluT2JqZWN0Iiwib3B0aW9ucyIsImV4dGVuZCIsImZuIiwiY2FsbCIsImFyZ3VtZW50cyIsImJyaWRnZXQiLCJBcnJheSIsInNsaWNlIiwiX2V2ZW50cyIsImluZGV4T2YiLCJwdXNoIiwib25jZSIsIl9vbmNlRXZlbnRzIiwib2ZmIiwic3BsaWNlIiwiZW1pdEV2ZW50IiwiYWxsT2ZmIiwiaXNOYU4iLCJ3aWR0aCIsImhlaWdodCIsImlubmVyV2lkdGgiLCJpbm5lckhlaWdodCIsIm91dGVyV2lkdGgiLCJvdXRlckhlaWdodCIsImdldENvbXB1dGVkU3R5bGUiLCJjcmVhdGVFbGVtZW50Iiwic3R5bGUiLCJwYWRkaW5nIiwiYm9yZGVyU3R5bGUiLCJib3JkZXJXaWR0aCIsImJveFNpemluZyIsImJvZHkiLCJhcHBlbmRDaGlsZCIsIk1hdGgiLCJyb3VuZCIsImlzQm94U2l6ZU91dGVyIiwicmVtb3ZlQ2hpbGQiLCJxdWVyeVNlbGVjdG9yIiwibm9kZVR5cGUiLCJkaXNwbGF5Iiwib2Zmc2V0V2lkdGgiLCJvZmZzZXRIZWlnaHQiLCJpc0JvcmRlckJveCIsImMiLCJmIiwibSIsInAiLCJwYWRkaW5nTGVmdCIsInBhZGRpbmdSaWdodCIsImciLCJwYWRkaW5nVG9wIiwicGFkZGluZ0JvdHRvbSIsInkiLCJtYXJnaW5MZWZ0IiwibWFyZ2luUmlnaHQiLCJ2IiwibWFyZ2luVG9wIiwibWFyZ2luQm90dG9tIiwiXyIsImJvcmRlckxlZnRXaWR0aCIsImJvcmRlclJpZ2h0V2lkdGgiLCJ6IiwiYm9yZGVyVG9wV2lkdGgiLCJib3JkZXJCb3R0b21XaWR0aCIsIkUiLCJiIiwieCIsIkVsZW1lbnQiLCJtYXRjaGVzIiwibWF0Y2hlc1NlbGVjdG9yIiwibW9kdWxvIiwibWFrZUFycmF5IiwiaXNBcnJheSIsInJlbW92ZUZyb20iLCJnZXRQYXJlbnQiLCJwYXJlbnROb2RlIiwiZ2V0UXVlcnlFbGVtZW50IiwiaGFuZGxlRXZlbnQiLCJmaWx0ZXJGaW5kRWxlbWVudHMiLCJmb3JFYWNoIiwiSFRNTEVsZW1lbnQiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZGVib3VuY2VNZXRob2QiLCJjbGVhclRpbWVvdXQiLCJkb2NSZWFkeSIsInJlYWR5U3RhdGUiLCJ0b0Rhc2hlZCIsInRvTG93ZXJDYXNlIiwiaHRtbEluaXQiLCJjb25jYXQiLCJnZXRBdHRyaWJ1dGUiLCJwYXJzZSIsImNsYXNzTmFtZSIsImVsZW1lbnQiLCJsYXlvdXQiLCJwb3NpdGlvbiIsIl9jcmVhdGUiLCJ0cmFuc2l0aW9uIiwidHJhbnNmb3JtIiwiV2Via2l0VHJhbnNpdGlvbiIsInRyYW5zaXRpb25EdXJhdGlvbiIsInRyYW5zaXRpb25Qcm9wZXJ0eSIsInRyYW5zaXRpb25EZWxheSIsImNyZWF0ZSIsImNvbnN0cnVjdG9yIiwiX3RyYW5zbiIsImluZ1Byb3BlcnRpZXMiLCJjbGVhbiIsIm9uRW5kIiwiZ2V0U2l6ZSIsInNpemUiLCJnZXRQb3NpdGlvbiIsIl9nZXRPcHRpb24iLCJsYXlvdXRQb3NpdGlvbiIsImdldFhWYWx1ZSIsImdldFlWYWx1ZSIsInBlcmNlbnRQb3NpdGlvbiIsIl90cmFuc2l0aW9uVG8iLCJzZXRQb3NpdGlvbiIsImlzVHJhbnNpdGlvbmluZyIsImdldFRyYW5zbGF0ZSIsInRvIiwib25UcmFuc2l0aW9uRW5kIiwiaXNDbGVhbmluZyIsImdvVG8iLCJtb3ZlVG8iLCJfbm9uVHJhbnNpdGlvbiIsIl9yZW1vdmVTdHlsZXMiLCJlbmFibGVUcmFuc2l0aW9uIiwic3RhZ2dlckRlbGF5Iiwib253ZWJraXRUcmFuc2l0aW9uRW5kIiwib250cmFuc2l0aW9uZW5kIiwib25vdHJhbnNpdGlvbmVuZCIsInRhcmdldCIsInByb3BlcnR5TmFtZSIsImRpc2FibGVUcmFuc2l0aW9uIiwicmVtb3ZlVHJhbnNpdGlvblN0eWxlcyIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJzdGFnZ2VyIiwicmVtb3ZlRWxlbSIsInJldmVhbCIsImlzSGlkZGVuIiwiZ2V0SGlkZVJldmVhbFRyYW5zaXRpb25FbmRQcm9wZXJ0eSIsIm9uUmV2ZWFsVHJhbnNpdGlvbkVuZCIsImhpZGRlblN0eWxlIiwidmlzaWJsZVN0eWxlIiwib3BhY2l0eSIsIm9uSGlkZVRyYW5zaXRpb25FbmQiLCJkZXN0cm95IiwibGVmdCIsInJpZ2h0IiwidG9wIiwiYm90dG9tIiwibmFtZXNwYWNlIiwiZGVmYXVsdHMiLCJvdXRsYXllckdVSUQiLCJtYXRjaCIsIkl0ZW0iLCJjb250YWluZXJTdHlsZSIsImluaXRMYXlvdXQiLCJvcmlnaW5MZWZ0Iiwib3JpZ2luVG9wIiwicmVzaXplIiwicmVzaXplQ29udGFpbmVyIiwiY29tcGF0T3B0aW9ucyIsImhvcml6b250YWwiLCJsYXlvdXRJbnN0YW50IiwicmVsb2FkSXRlbXMiLCJzdGFtcHMiLCJzdGFtcCIsImJpbmRSZXNpemUiLCJfaXRlbWl6ZSIsImNoaWxkcmVuIiwiX2ZpbHRlckZpbmRJdGVtRWxlbWVudHMiLCJnZXRJdGVtRWxlbWVudHMiLCJtYXAiLCJfcmVzZXRMYXlvdXQiLCJfbWFuYWdlU3RhbXBzIiwiX2lzTGF5b3V0SW5pdGVkIiwibGF5b3V0SXRlbXMiLCJfZ2V0TWVhc3VyZW1lbnQiLCJfZ2V0SXRlbXNGb3JMYXlvdXQiLCJfbGF5b3V0SXRlbXMiLCJfcG9zdExheW91dCIsImZpbHRlciIsImlzSWdub3JlZCIsIl9lbWl0Q29tcGxldGVPbkl0ZW1zIiwiX2dldEl0ZW1MYXlvdXRQb3NpdGlvbiIsIml0ZW0iLCJpc0luc3RhbnQiLCJpc0xheW91dEluc3RhbnQiLCJfcHJvY2Vzc0xheW91dFF1ZXVlIiwidXBkYXRlU3RhZ2dlciIsIl9wb3NpdGlvbkl0ZW0iLCJfZ2V0Q29udGFpbmVyU2l6ZSIsIl9zZXRDb250YWluZXJNZWFzdXJlIiwiZGlzcGF0Y2hFdmVudCIsIkV2ZW50IiwiaWdub3JlIiwiZ2V0SXRlbSIsInVuaWdub3JlIiwiX2ZpbmQiLCJ1bnN0YW1wIiwiX2dldEJvdW5kaW5nUmVjdCIsIl9tYW5hZ2VTdGFtcCIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsIl9ib3VuZGluZ1JlY3QiLCJfZ2V0RWxlbWVudE9mZnNldCIsImlzUmVzaXplQm91bmQiLCJ1bmJpbmRSZXNpemUiLCJvbnJlc2l6ZSIsIm5lZWRzUmVzaXplTGF5b3V0IiwiYWRkSXRlbXMiLCJhcHBlbmRlZCIsInByZXBlbmRlZCIsInJldmVhbEl0ZW1FbGVtZW50cyIsImdldEl0ZW1zIiwiaGlkZUl0ZW1FbGVtZW50cyIsInJlbW92ZURhdGEiLCJtcyIsImZpdFdpZHRoIiwibWVhc3VyZUNvbHVtbnMiLCJjb2xZcyIsImNvbHMiLCJtYXhZIiwiaG9yaXpvbnRhbENvbEluZGV4IiwiZ2V0Q29udGFpbmVyV2lkdGgiLCJjb250YWluZXJXaWR0aCIsImd1dHRlciIsImhvcml6b250YWxPcmRlciIsImNvbCIsIl9nZXRUb3BDb2xQb3NpdGlvbiIsIl9nZXRUb3BDb2xHcm91cCIsIl9nZXRDb2xHcm91cFkiLCJfZ2V0SG9yaXpvbnRhbENvbFBvc2l0aW9uIiwiZmxvb3IiLCJfZ2V0Q29udGFpbmVyRml0V2lkdGgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7O0FBT0E7QUFDQUEsbUJBQU8sQ0FBQyxvREFBRCxDQUFQLEMsQ0FFQTtBQUNBOzs7QUFFQUMsT0FBTyxDQUFDQyxHQUFSLENBQVksbURBQVo7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0NBRUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBSUMsTUFBTSxHQUFJLFlBQVk7QUFDdEIsU0FBTztBQUNIQyxRQUFJLEVBQUUsZ0JBQVk7QUFDZDtBQUNBRCxZQUFNLENBQUNFLE9BQVA7QUFDQUYsWUFBTSxDQUFDRyxVQUFQO0FBQ0FILFlBQU0sQ0FBQ0ksUUFBUDtBQUNBSixZQUFNLENBQUNLLFVBQVA7QUFDQUwsWUFBTSxDQUFDTSxnQkFBUDtBQUNBTixZQUFNLENBQUNPLGNBQVA7QUFDQVAsWUFBTSxDQUFDUSxnQkFBUDtBQUNBUixZQUFNLENBQUNTLFlBQVAsR0FUYyxDQVVkO0FBQ0E7QUFDSCxLQWJFO0FBY0hQLFdBQU8sRUFBRSxtQkFBWTtBQUdqQixVQUFHLE9BQU9RLEtBQVAsS0FBa0IsV0FBckIsRUFBaUM7QUFDN0JBLGFBQUssQ0FBQ0MsS0FBTixDQUFZLFlBQVk7QUFDcEIsY0FBSUMsTUFBTSxHQUFHLENBQUMsU0FBRCxFQUFZLFNBQVosQ0FBYjtBQUVBLGNBQUlDLEtBQUssR0FBRyxJQUFJSCxLQUFLLENBQUNJLEdBQVYsQ0FBYyxLQUFkLEVBQXFCO0FBQzdCRixrQkFBTSxFQUFFQSxNQURxQjtBQUU3QkcsZ0JBQUksRUFBRSxFQUZ1QjtBQUc3QkMsb0JBQVEsRUFBRTtBQUhtQixXQUFyQixFQUlUO0FBQ0NDLGlDQUFxQixFQUFFO0FBRHhCLFdBSlMsQ0FBWjtBQU9BLGNBQUlDLE1BQU0sR0FBRyxJQUFJUixLQUFLLENBQUNTLFNBQVYsQ0FBb0IsQ0FBQyxTQUFELEVBQVksU0FBWixDQUFwQixFQUE0QztBQUNyREMsdUJBQVcsRUFBRSx3RUFEd0M7QUFFckRDLDBCQUFjLEVBQUU7QUFGcUMsV0FBNUMsRUFHVjtBQUNDQyxzQkFBVSxFQUFFLGVBRGI7QUFFQ0MseUJBQWEsRUFBRSxtQkFGaEI7QUFHQ0MseUJBQWEsRUFBRSxDQUFDLEVBQUQsRUFBSyxFQUFMLENBSGhCO0FBSUNDLDJCQUFlLEVBQUUsQ0FBQyxDQUFDLENBQUYsRUFBSyxDQUFDLEVBQU47QUFKbEIsV0FIVSxDQUFiO0FBVUFaLGVBQUssQ0FBQ2EsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0IsWUFBeEI7QUFDQWQsZUFBSyxDQUFDZSxVQUFOLENBQWlCQyxHQUFqQixDQUFxQlgsTUFBckI7QUFFSCxTQXZCRDtBQXdCSDtBQUVKLEtBNUNFO0FBOENIZixjQUFVLEVBQUUsc0JBQVk7QUFDcEIyQixPQUFDLENBQUMsWUFBRCxDQUFELENBQWdCQyxjQUFoQixDQUErQjtBQUMzQkMsV0FBRyxFQUFFLEVBRHNCO0FBRTNCQyxXQUFHLEVBQUUsR0FGc0I7QUFHM0JDLFlBQUksRUFBRSxFQUhxQjtBQUkzQkMsWUFBSSxFQUFFO0FBSnFCLE9BQS9CO0FBTUgsS0FyREU7QUF1REgvQixZQUFRLEVBQUUsb0JBQVk7QUFDbEIwQixPQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQk0sS0FBcEIsQ0FBMEIsVUFBVUMsQ0FBVixFQUFhO0FBQ25DQSxTQUFDLENBQUNDLGNBQUY7QUFDQSxZQUFJQyxLQUFLLEdBQUdULENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVUsT0FBUixDQUFnQixVQUFoQixDQUFaO0FBQUEsWUFDSUMsSUFBSSxHQUFHQyxRQUFRLENBQUNILEtBQUssQ0FBQ0ksSUFBTixDQUFXLFdBQVgsQ0FBRCxDQURuQjtBQUFBLFlBRUlDLFVBQVUsR0FBR0gsSUFBSSxHQUFHLENBRnhCO0FBQUEsWUFHSUksU0FBUyxHQUFHTixLQUFLLENBQUNPLElBQU4sQ0FBVyxlQUFhQyxNQUFNLENBQUNILFVBQUQsQ0FBbkIsR0FBZ0MsSUFBM0MsQ0FIaEI7QUFLQWQsU0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFja0IsV0FBZCxDQUEwQixRQUExQjtBQUVBSCxpQkFBUyxDQUFDSSxRQUFWLENBQW1CLFFBQW5COztBQUNBLFlBQUdMLFVBQVUsR0FBRyxDQUFoQixFQUFrQjtBQUNkZCxXQUFDLENBQUMsZUFBRCxDQUFELENBQW1Cb0IsSUFBbkI7QUFDSDs7QUFDRHBCLFNBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9Ca0IsV0FBcEIsQ0FBZ0MsVUFBaEM7QUFDQWxCLFNBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCcUIsSUFBdEIsQ0FBMkJQLFVBQTNCO0FBQ0FMLGFBQUssQ0FBQ0ksSUFBTixDQUFXLFdBQVgsRUFBdUJDLFVBQXZCO0FBQ0gsT0FoQkQ7QUFrQkFkLE9BQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CTSxLQUFwQixDQUEwQixVQUFVQyxDQUFWLEVBQWE7QUFDbkNBLFNBQUMsQ0FBQ0MsY0FBRjtBQUVBLFlBQUlDLEtBQUssR0FBR1QsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxPQUFSLENBQWdCLFVBQWhCLENBQVo7QUFBQSxZQUNJQyxJQUFJLEdBQUdDLFFBQVEsQ0FBQ0gsS0FBSyxDQUFDSSxJQUFOLENBQVcsV0FBWCxDQUFELENBRG5CO0FBQUEsWUFFSVMsVUFBVSxHQUFHWCxJQUFJLEdBQUcsQ0FGeEI7QUFBQSxZQUdJWSxTQUFTLEdBQUdkLEtBQUssQ0FBQ08sSUFBTixDQUFXLGVBQWFDLE1BQU0sQ0FBQ0ssVUFBRCxDQUFuQixHQUFnQyxJQUEzQyxDQUhoQjs7QUFLQSxZQUFHWCxJQUFJLEtBQUcsQ0FBVixFQUFZO0FBQ1I7QUFDSCxTQUZELE1BRU8sSUFBSUEsSUFBSSxLQUFHLENBQVgsRUFBYTtBQUNoQlgsV0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JtQixRQUFwQixDQUE2QixVQUE3QjtBQUNIOztBQUNEbkIsU0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFja0IsV0FBZCxDQUEwQixRQUExQjtBQUNBSyxpQkFBUyxDQUFDSixRQUFWLENBQW1CLFFBQW5CO0FBQ0FWLGFBQUssQ0FBQ0ksSUFBTixDQUFXLFdBQVgsRUFBdUJTLFVBQXZCO0FBQ0F0QixTQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQnFCLElBQXRCLENBQTJCQyxVQUEzQjs7QUFDQSxZQUFHWCxJQUFJLElBQUksQ0FBWCxFQUFhO0FBQ1RYLFdBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJ3QixJQUFuQjtBQUNIO0FBQ0osT0FwQkQ7QUFzQkF4QixPQUFDLENBQUMsZUFBRCxDQUFELENBQW1CeUIsRUFBbkIsQ0FBc0IsY0FBdEIsRUFBc0MsWUFBVztBQUM3Q3pCLFNBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCb0IsSUFBdkI7QUFDQXBCLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdCLElBQVIsQ0FBYSxtQkFBYixFQUFrQ1EsSUFBbEM7QUFDQXhCLFNBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCcUIsSUFBNUIsQ0FBaUNyQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFnQixJQUFSLENBQWEsV0FBYixFQUEwQlUsR0FBMUIsRUFBakM7QUFDSCxPQUpEO0FBS0gsS0FyR0U7QUF1R0huRCxjQUFVLEVBQUUsc0JBQVk7QUFFcEJ5QixPQUFDLENBQUMsV0FBRCxDQUFELENBQWVNLEtBQWYsQ0FBcUIsVUFBVUMsQ0FBVixFQUFhO0FBQzlCQSxTQUFDLENBQUNDLGNBQUY7QUFDQSxZQUFJbUIsR0FBRyxHQUFHM0IsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRYSxJQUFSLENBQWEsTUFBYixDQUFWOztBQUNBLFlBQUcsRUFBRSxRQUFRYixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE0QixJQUFSLENBQWEsTUFBYixDQUFSLE1BQWlDQyxTQUFuQyxDQUFILEVBQWlEO0FBQzdDN0IsV0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQmEsSUFBbkIsQ0FBd0IsT0FBeEIsRUFBZ0NiLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTRCLElBQVIsQ0FBYSxNQUFiLENBQWhDO0FBQ0g7O0FBQ0Q1QixTQUFDLENBQUM4QixhQUFGLENBQWdCQyxJQUFoQixDQUFxQjtBQUNqQkMsZUFBSyxFQUFFO0FBQ0hMLGVBQUcsRUFBRUEsR0FERjtBQUNPO0FBQ1ZNLGdCQUFJLEVBQUU7QUFGSDtBQURVLFNBQXJCO0FBTUgsT0FaRDtBQWNBakMsT0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQk0sS0FBbkIsQ0FBeUIsWUFBWTtBQUNqQyxZQUFJd0IsYUFBYSxHQUFHOUIsQ0FBQyxDQUFDOEIsYUFBRixDQUFnQkksUUFBcEM7QUFDQUoscUJBQWEsQ0FBQ0ssS0FBZDtBQUNILE9BSEQ7QUFLQW5DLE9BQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CTSxLQUFwQixDQUEwQixVQUFVQyxDQUFWLEVBQWE7QUFDbkNBLFNBQUMsQ0FBQ0MsY0FBRjtBQUNBLFlBQUk0QixLQUFLLEdBQUdwQyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLE9BQVIsQ0FBZ0IsVUFBaEIsQ0FBWjtBQUFBLFlBQ0kyQixRQUFRLEdBQUdELEtBQUssQ0FBQ3BCLElBQU4sQ0FBVyxtQkFBWCxDQURmO0FBQUEsWUFFSXNCLFFBQVEsR0FBR0YsS0FBSyxDQUFDcEIsSUFBTixDQUFXLG1CQUFYLENBRmY7QUFHQXFCLGdCQUFRLENBQUNqQixJQUFUO0FBQ0FrQixnQkFBUSxDQUFDZCxJQUFUO0FBQ0gsT0FQRDtBQVNBeEIsT0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ5QixFQUFyQixDQUF3QixjQUF4QixFQUF3QyxZQUFXO0FBQy9DekIsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0IsSUFBUixDQUFhLG1CQUFiLEVBQWtDSSxJQUFsQztBQUNBcEIsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0IsSUFBUixDQUFhLG1CQUFiLEVBQWtDUSxJQUFsQztBQUNBeEIsU0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJxQixJQUE1QixDQUFpQ3JCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdCLElBQVIsQ0FBYSxXQUFiLEVBQTBCVSxHQUExQixFQUFqQztBQUNILE9BSkQ7QUFNSCxLQTNJRTtBQTZJSGxELG9CQUFnQixFQUFFLDRCQUFZLENBQzFCO0FBQ0gsS0EvSUU7QUFnSkhDLGtCQUFjLEVBQUUsMEJBQVk7QUFDeEJ1QixPQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQk0sS0FBdEIsQ0FBNEIsVUFBVUMsQ0FBVixFQUFhO0FBQ3JDQSxTQUFDLENBQUNDLGNBQUY7QUFDQVIsU0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ1QyxXQUFyQixDQUFpQyxNQUFqQztBQUNILE9BSEQ7QUFJQXZDLE9BQUMsQ0FBQyx1QkFBRCxDQUFELENBQTJCTSxLQUEzQixDQUFpQyxVQUFVQyxDQUFWLEVBQWE7QUFDMUNQLFNBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCdUMsV0FBckIsQ0FBaUMsTUFBakM7QUFFSCxPQUhEO0FBSUgsS0F6SkU7QUEySkg3RCxvQkFBZ0IsRUFBRSw0QkFBVztBQUN6QixVQUFJOEQsR0FBRyxHQUFHeEMsQ0FBQyxDQUFDLFlBQUQsQ0FBWDtBQUNBLFVBQUl5QyxhQUFKO0FBQ0FDLFlBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBVztBQUFHO0FBQzVDLFlBQUlDLEVBQUUsR0FBR0YsTUFBTSxDQUFDRyxXQUFQLElBQXNCQyxRQUFRLENBQUNDLGVBQVQsQ0FBeUJDLFNBQXhEOztBQUNBLFlBQUlKLEVBQUUsR0FBR0gsYUFBVCxFQUF1QjtBQUNuQkQsYUFBRyxDQUFDUyxHQUFKLENBQVEsU0FBUixFQUFrQixHQUFsQixFQURtQixDQUNLOztBQUN4QlQsYUFBRyxDQUFDdEIsV0FBSixDQUFnQixRQUFoQjtBQUNILFNBSEQsTUFLSztBQUNEc0IsYUFBRyxDQUFDUyxHQUFKLENBQVEsU0FBUixFQUFtQixDQUFuQixFQURDLENBQ3NCOztBQUN2QlQsYUFBRyxDQUFDckIsUUFBSixDQUFhLFFBQWI7QUFDSDs7QUFDRHNCLHFCQUFhLEdBQUdHLEVBQWhCOztBQUNBLFlBQUdBLEVBQUUsR0FBRyxFQUFSLEVBQVc7QUFDUEosYUFBRyxDQUFDdEIsV0FBSixDQUFnQixRQUFoQjtBQUNIO0FBQ0osT0FmRCxFQWVHLEtBZkg7QUFnQkgsS0E5S0U7QUFnTEh2QyxnQkFBWSxFQUFFLHdCQUFZO0FBQ3RCcUIsT0FBQyxDQUFDOEMsUUFBRCxDQUFELENBQVlqRSxLQUFaLENBQWtCLFlBQVk7QUFDMUJtQixTQUFDLENBQUMsY0FBRCxDQUFELENBQWtCMEIsR0FBbEIsQ0FBc0Isa0JBQXRCO0FBQ0gsT0FGRDtBQUdILEtBcExFO0FBc0xId0IsZ0JBQVksRUFBRSx3QkFBWTtBQUMvQmxELE9BQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JtRCxRQUFsQixDQUEyQjtBQUFDQyxXQUFHLEVBQUM7QUFBTCxPQUEzQjtBQUNBLEtBeExRO0FBeUxUQyxlQUFXLEVBQUUsdUJBQVk7QUFDeEJyRCxPQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQnNELE9BQXRCLENBQThCO0FBQzdCO0FBQ0FDLG9CQUFZLEVBQUUsZUFGZTtBQUc3QkMsbUJBQVcsRUFBRTtBQUhnQixPQUE5QjtBQUtBO0FBL0xRLEdBQVA7QUFpTUgsQ0FsTVksRUFBYjs7QUFtTUF0RixNQUFNLENBQUNDLElBQVAsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9NQTs7QUFDQSxJQUFJc0YsS0FBSyxHQUFJLFlBQVk7QUFDckIsTUFBSUMsT0FBTyxHQUFHLHNEQUFkO0FBQ0EsTUFBSUMsT0FBTyxHQUFHLGdCQUFkO0FBQ0EsU0FBTztBQUNIeEYsUUFBSSxFQUFFLGdCQUFZO0FBQ2RzRixXQUFLLENBQUNHLFNBQU47QUFDQUgsV0FBSyxDQUFDSSxnQkFBTjtBQUNBSixXQUFLLENBQUNLLGVBQU47QUFDQUwsV0FBSyxDQUFDTSxXQUFOO0FBQ0FOLFdBQUssQ0FBQ08sTUFBTjtBQUNBUCxXQUFLLENBQUNRLGVBQU47QUFDQVIsV0FBSyxDQUFDUyxVQUFOO0FBQ0gsS0FURTs7QUFXSDs7O0FBR0FOLGFBQVMsRUFBRSxxQkFBWTtBQUNuQixVQUFJTyxXQUFXLEdBQUduRSxDQUFDLENBQUMsV0FBRCxDQUFuQjs7QUFDQSxVQUFJbUUsV0FBVyxDQUFDQyxNQUFoQixFQUF3QjtBQUNwQkQsbUJBQVcsQ0FBQ0UsSUFBWixDQUFpQixvQkFBakIsRUFBdUM7QUFBQ0MsbUJBQVMsRUFBQztBQUFYLFNBQXZDO0FBQ0g7QUFDSixLQW5CRTs7QUFxQkg7OztBQUdBVCxvQkFBZ0IsRUFBRSw0QkFBWTtBQUMxQjdELE9BQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCdUUsSUFBdEIsQ0FBMkIsWUFBWTtBQUNuQyxZQUFJQyxNQUFNLEdBQUd4RSxDQUFDLENBQUMsSUFBRCxDQUFkO0FBQUEsWUFDSXlFLEtBQUssR0FBR0QsTUFBTSxDQUFDRSxRQUFQLENBQWdCLHVCQUFoQixDQURaO0FBQUEsWUFFSUMsTUFBTSxHQUFHSCxNQUFNLENBQUNFLFFBQVAsQ0FBZ0Isd0JBQWhCLENBRmI7QUFBQSxZQUdJeEUsR0FBRyxHQUFHMEUsVUFBVSxDQUFDSixNQUFNLENBQUMzRCxJQUFQLENBQVksS0FBWixDQUFELENBSHBCO0FBQUEsWUFJSVYsR0FBRyxHQUFHeUUsVUFBVSxDQUFDSixNQUFNLENBQUMzRCxJQUFQLENBQVksS0FBWixDQUFELENBSnBCO0FBQUEsWUFLSUYsSUFBSSxHQUFHNkQsTUFBTSxDQUFDM0QsSUFBUCxDQUFZLE1BQVosSUFBc0IrRCxVQUFVLENBQUNKLE1BQU0sQ0FBQzNELElBQVAsQ0FBWSxNQUFaLENBQUQsQ0FBaEMsR0FBd0QsQ0FMbkU7QUFBQSxZQU1JZ0UsUUFBUSxHQUFHRCxVQUFVLENBQUNKLE1BQU0sQ0FBQzlDLEdBQVAsRUFBRCxDQU56QjtBQU9BOEMsY0FBTSxDQUFDNUMsSUFBUCxDQUFZLGFBQVosRUFBMkI0QyxNQUFNLENBQUM5QyxHQUFQLEVBQTNCO0FBQ0E4QyxjQUFNLENBQUM1QyxJQUFQLENBQVksWUFBWixFQUEwQjRDLE1BQU0sQ0FBQzlDLEdBQVAsRUFBMUI7O0FBQ0EsWUFBSW1ELFFBQVEsSUFBSTFFLEdBQWhCLEVBQXFCO0FBQ2pCcUUsZ0JBQU0sQ0FBQzlDLEdBQVAsQ0FBV3ZCLEdBQVg7QUFDQXNFLGVBQUssQ0FBQ3RELFFBQU4sQ0FBZSxPQUFmO0FBQ0gsU0FIRCxNQUdPLElBQUkwRCxRQUFRLElBQUkzRSxHQUFoQixFQUFxQjtBQUN4QnNFLGdCQUFNLENBQUM5QyxHQUFQLENBQVd4QixHQUFYO0FBQ0F5RSxnQkFBTSxDQUFDeEQsUUFBUCxDQUFnQixPQUFoQjtBQUNIOztBQUNEcUQsY0FBTSxDQUFDTSxNQUFQLENBQWMsWUFBWTtBQUN0QkQsa0JBQVEsR0FBR0QsVUFBVSxDQUFDNUUsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMEIsR0FBUixFQUFELENBQXJCOztBQUNBLGNBQUltRCxRQUFRLElBQUkxRSxHQUFoQixFQUFxQjtBQUNqQnFFLGtCQUFNLENBQUM5QyxHQUFQLENBQVd2QixHQUFYO0FBQ0FzRSxpQkFBSyxDQUFDdEQsUUFBTixDQUFlLE9BQWY7QUFDQXdELGtCQUFNLENBQUN6RCxXQUFQLENBQW1CLE9BQW5CO0FBQ0EyRCxvQkFBUSxHQUFHMUUsR0FBWDtBQUVILFdBTkQsTUFNTyxJQUFJMEUsUUFBUSxJQUFJM0UsR0FBaEIsRUFBcUI7QUFDeEJzRSxrQkFBTSxDQUFDOUMsR0FBUCxDQUFXeEIsR0FBWDtBQUNBeUUsa0JBQU0sQ0FBQ3hELFFBQVAsQ0FBZ0IsT0FBaEI7QUFDQXNELGlCQUFLLENBQUN2RCxXQUFOLENBQWtCLE9BQWxCO0FBQ0EyRCxvQkFBUSxHQUFHM0UsR0FBWDtBQUNIOztBQUNEMkUsa0JBQVEsR0FBSzdFLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBCLEdBQVIsS0FBZ0IxQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE0QixJQUFSLENBQWEsWUFBYixDQUE3QjtBQUNBNUIsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNEIsSUFBUixDQUFhLFlBQWIsRUFBMkI1QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVEwQixHQUFSLEVBQTNCO0FBQ0gsU0FoQkQ7QUFpQkErQyxhQUFLLENBQUNuRSxLQUFOLENBQVksWUFBWTtBQUNwQnVFLGtCQUFRLEdBQUdELFVBQVUsQ0FBQ0osTUFBTSxDQUFDOUMsR0FBUCxFQUFELENBQXJCO0FBQ0FpRCxnQkFBTSxDQUFDekQsV0FBUCxDQUFtQixPQUFuQjs7QUFDQSxjQUFJMkQsUUFBUSxJQUFJMUUsR0FBaEIsRUFBcUI7QUFDakJxRSxrQkFBTSxDQUFDOUMsR0FBUCxDQUFXbUQsUUFBWDtBQUNBSixpQkFBSyxDQUFDdEQsUUFBTixDQUFlLE9BQWY7QUFDSCxXQUhELE1BR087QUFDSHNELGlCQUFLLENBQUN2RCxXQUFOLENBQWtCLE9BQWxCO0FBQ0FzRCxrQkFBTSxDQUFDOUMsR0FBUCxDQUFXbUQsUUFBUSxHQUFHbEUsSUFBdEI7QUFDQTZELGtCQUFNLENBQUM1QyxJQUFQLENBQVksWUFBWixFQUEwQixDQUFDNEMsTUFBTSxDQUFDNUMsSUFBUCxDQUFZLFlBQVosQ0FBRCxHQUE2QmpCLElBQXZEOztBQUNBLGdCQUFJa0UsUUFBUSxHQUFHbEUsSUFBWCxJQUFtQlIsR0FBdkIsRUFBNEI7QUFDeEJzRSxtQkFBSyxDQUFDdEQsUUFBTixDQUFlLE9BQWY7QUFDSDs7QUFDRHFELGtCQUFNLENBQUNPLE9BQVAsQ0FBZSxRQUFmO0FBQ0g7QUFDSixTQWZEO0FBZ0JBSixjQUFNLENBQUNyRSxLQUFQLENBQWEsWUFBWTtBQUNyQnVFLGtCQUFRLEdBQUdELFVBQVUsQ0FBQ0osTUFBTSxDQUFDOUMsR0FBUCxFQUFELENBQXJCO0FBQ0ErQyxlQUFLLENBQUN2RCxXQUFOLENBQWtCLE9BQWxCOztBQUNBLGNBQUkyRCxRQUFRLElBQUkzRSxHQUFoQixFQUFxQjtBQUNqQnNFLGtCQUFNLENBQUM5QyxHQUFQLENBQVdtRCxRQUFYO0FBQ0FGLGtCQUFNLENBQUN4RCxRQUFQLENBQWdCLE9BQWhCO0FBQ0gsV0FIRCxNQUdPO0FBQ0h3RCxrQkFBTSxDQUFDekQsV0FBUCxDQUFtQixPQUFuQjtBQUNBc0Qsa0JBQU0sQ0FBQzlDLEdBQVAsQ0FBV21ELFFBQVEsR0FBR2xFLElBQXRCO0FBQ0E2RCxrQkFBTSxDQUFDNUMsSUFBUCxDQUFZLFlBQVosRUFBMEIsQ0FBQzRDLE1BQU0sQ0FBQzVDLElBQVAsQ0FBWSxZQUFaLENBQUQsR0FBNkJqQixJQUF2RDs7QUFDQSxnQkFBSWtFLFFBQVEsR0FBR2xFLElBQVgsSUFBbUJULEdBQXZCLEVBQTRCO0FBQ3hCeUUsb0JBQU0sQ0FBQ3hELFFBQVAsQ0FBZ0IsT0FBaEI7QUFDSDs7QUFDRHFELGtCQUFNLENBQUNPLE9BQVAsQ0FBZSxRQUFmO0FBQ0g7QUFDSixTQWZEO0FBZ0JILE9BbEVEO0FBbUVILEtBNUZFO0FBOEZIZCxtQkFBZSxFQUFFLDJCQUFXO0FBQ3hCakUsT0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFldUUsSUFBZixDQUFvQixZQUFXO0FBQzNCLFlBQUlDLE1BQU0sR0FBR3hFLENBQUMsQ0FBQyxJQUFELENBQWQ7O0FBRUEsWUFBSXdFLE1BQU0sQ0FBQzlDLEdBQVAsT0FBaUIsRUFBckIsRUFBeUI7QUFDckI4QyxnQkFBTSxDQUFDckQsUUFBUCxDQUFnQixRQUFoQjtBQUNILFNBRkQsTUFFTztBQUNIcUQsZ0JBQU0sQ0FBQ3RELFdBQVAsQ0FBbUIsY0FBbkI7QUFDSDtBQUNKLE9BUkQ7QUFTSCxLQXhHRTs7QUEwR0g7OztBQUdBNEMsbUJBQWUsRUFBRSwyQkFBWTtBQUN6QjlELE9BQUMsQ0FBQyxXQUFELENBQUQsQ0FBZThFLE1BQWYsQ0FBc0IsWUFBWTtBQUM5QixZQUFJOUUsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMEIsR0FBUixPQUFrQixFQUF0QixFQUEwQjtBQUN0QjFCLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW1CLFFBQVIsQ0FBaUIsUUFBakI7QUFDSCxTQUZELE1BRU87QUFDSG5CLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWtCLFdBQVIsQ0FBb0IsY0FBcEI7QUFDSDtBQUNKLE9BTkQ7QUFPSCxLQXJIRTs7QUF1SEg7Ozs7QUFJQTZDLGVBQVcsRUFBRSx1QkFBWTtBQUNyQixVQUFJaUIsT0FBTyxHQUFHaEYsQ0FBQyxDQUFDLFdBQUQsQ0FBZjtBQUNBZ0YsYUFBTyxDQUFDQyxLQUFSLENBQWMsWUFBWTtBQUN0QmpGLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWtCLFdBQVIsQ0FBb0IsT0FBcEI7QUFDSCxPQUZEO0FBR0E4RCxhQUFPLENBQUN2RCxFQUFSLENBQVcsUUFBWCxFQUFxQixZQUFZO0FBQzdCLFlBQUl6QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFrRixRQUFSLENBQWlCLGFBQWpCLEtBQW1DbEYsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRa0YsUUFBUixDQUFpQixhQUFqQixDQUF2QyxFQUF3RTtBQUNwRXpCLGVBQUssQ0FBQzBCLGFBQU4sQ0FBb0JuRixDQUFDLENBQUMsSUFBRCxDQUFyQjtBQUNIO0FBQ0osT0FKRDtBQUtILEtBcklFOztBQXVJSDs7O0FBR0FtRixpQkFBYSxFQUFFLHVCQUFVQyxRQUFWLEVBQW9CO0FBQy9CLFVBQUlDLEtBQUssR0FBR0QsUUFBUSxDQUFDMUQsR0FBVCxFQUFaO0FBQUEsVUFDSTRELEtBQUssR0FBRyxLQURaO0FBQUEsVUFFSUMsT0FBTyxHQUFHdkYsQ0FBQyxDQUFDb0YsUUFBRCxDQUFELENBQVlJLE1BQVosQ0FBbUIsOENBQW5CLENBRmQ7O0FBR0EsVUFBSUgsS0FBSyxDQUFDakIsTUFBTixLQUFpQixDQUFqQixJQUFzQixDQUFDZ0IsUUFBUSxDQUFDRixRQUFULENBQWtCLGFBQWxCLENBQTNCLEVBQTZEO0FBQ3pEO0FBQ0FFLGdCQUFRLENBQUNsRSxXQUFULENBQXFCLE9BQXJCLEVBQThCQSxXQUE5QixDQUEwQyxPQUExQzs7QUFDQSxZQUFJcUUsT0FBTyxDQUFDbkIsTUFBWixFQUFvQjtBQUNoQm1CLGlCQUFPLENBQUNyRSxXQUFSLENBQW9CLE9BQXBCO0FBQ0g7O0FBQ0Q7QUFDSDs7QUFDRCxjQUFRa0UsUUFBUSxDQUFDdkUsSUFBVCxDQUFjLE1BQWQsQ0FBUjtBQUNJLGFBQUssT0FBTDtBQUNJeUUsZUFBSyxHQUFHNUIsT0FBTyxDQUFDK0IsSUFBUixDQUFhSixLQUFiLENBQVI7QUFDQTs7QUFDSixhQUFLLEtBQUw7QUFDSUMsZUFBSyxHQUFHM0IsT0FBTyxDQUFDOEIsSUFBUixDQUFhSixLQUFiLENBQVI7QUFDQTs7QUFDSixhQUFLLFVBQUw7QUFDSUMsZUFBSyxHQUFHRixRQUFRLENBQUNNLElBQVQsQ0FBYyxTQUFkLENBQVI7QUFDQTs7QUFDSjtBQUNJSixlQUFLLEdBQUlELEtBQUssS0FBSyxFQUFuQjtBQUNBO0FBWlI7O0FBY0EsVUFBSUMsS0FBSixFQUFXO0FBQ1BGLGdCQUFRLENBQUNsRSxXQUFULENBQXFCLE9BQXJCLEVBQThCQyxRQUE5QixDQUF1QyxPQUF2Qzs7QUFDQSxZQUFJb0UsT0FBTyxDQUFDbkIsTUFBWixFQUFvQjtBQUNoQm1CLGlCQUFPLENBQUNwRSxRQUFSLENBQWlCLE9BQWpCLEVBQTBCRCxXQUExQixDQUFzQyxPQUF0QztBQUNIO0FBQ0osT0FMRCxNQUtPLElBQUlrRSxRQUFRLENBQUNGLFFBQVQsQ0FBa0IsYUFBbEIsS0FBb0NFLFFBQVEsQ0FBQ0YsUUFBVCxDQUFrQixhQUFsQixDQUF4QyxFQUEwRTtBQUM3RUUsZ0JBQVEsQ0FBQ2xFLFdBQVQsQ0FBcUIsT0FBckIsRUFBOEJDLFFBQTlCLENBQXVDLE9BQXZDOztBQUNBLFlBQUlvRSxPQUFPLENBQUNuQixNQUFaLEVBQW9CO0FBQ2hCO0FBQ0FtQixpQkFBTyxDQUFDckUsV0FBUixDQUFvQixPQUFwQixFQUE2QkMsUUFBN0IsQ0FBc0MsT0FBdEM7QUFDSDtBQUNKO0FBQ0osS0FoTEU7O0FBa0xIOzs7QUFHQXdFLDJCQUF1QixFQUFFLGlDQUFTdkQsS0FBVCxFQUFnQndELFFBQWhCLEVBQTBCO0FBQy9DLFVBQUlDLFlBQUo7QUFBQSxVQUNJQyxTQUFTLEdBQUcsS0FEaEI7O0FBR0EsVUFBSSxDQUFDRixRQUFRLENBQUMsUUFBRCxDQUFULElBQXVCLENBQUNHLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZSixRQUFRLENBQUMsUUFBRCxDQUFwQixFQUFnQ3hCLE1BQTVELEVBQW9FO0FBQ2hFaEMsYUFBSyxDQUFDcEIsSUFBTixDQUFXLFdBQVgsRUFBd0J1RCxJQUF4QixDQUE2QixZQUFXO0FBQ3BDLGNBQUlDLE1BQU0sR0FBR3hFLENBQUMsQ0FBQyxJQUFELENBQWQ7QUFDQSxjQUFJdUYsT0FBTyxHQUFHZixNQUFNLENBQUNnQixNQUFQLEVBQWQ7QUFFQWhCLGdCQUFNLENBQUN0RCxXQUFQLENBQW1CLE9BQW5CLEVBQTRCQyxRQUE1QixDQUFxQyxPQUFyQztBQUNBb0UsaUJBQU8sQ0FBQ3BFLFFBQVIsQ0FBaUIsT0FBakI7QUFDSCxTQU5EO0FBUUEsWUFBSWtCLFFBQVEsR0FBR0QsS0FBSyxDQUFDcEIsSUFBTixDQUFXLG1CQUFYLENBQWY7QUFBQSxZQUNJc0IsUUFBUSxHQUFHRixLQUFLLENBQUNwQixJQUFOLENBQVcsbUJBQVgsQ0FEZjs7QUFFQSxZQUFJcUIsUUFBUSxDQUFDK0IsTUFBYixFQUFxQjtBQUNqQi9CLGtCQUFRLENBQUNsQixRQUFULENBQWtCLFFBQWxCO0FBQ0g7O0FBQ0QsWUFBSW1CLFFBQVEsQ0FBQzhCLE1BQWIsRUFBcUI7QUFDakI5QixrQkFBUSxDQUFDbEIsSUFBVDtBQUNIO0FBQ0osT0FqQkQsTUFrQkssSUFBR3dFLFFBQVEsQ0FBQyxRQUFELENBQVIsQ0FBbUIsS0FBbkIsQ0FBSCxFQUE4QjtBQUMvQkMsb0JBQVksR0FBR0QsUUFBUSxDQUFDLFFBQUQsQ0FBUixDQUFtQixLQUFuQixDQUFmO0FBQ0FuQyxhQUFLLENBQUN3QyxnQkFBTixDQUF1QjdELEtBQXZCLEVBQThCeUQsWUFBOUI7QUFDQUMsaUJBQVMsR0FBRyxJQUFaO0FBQ0gsT0FKSSxNQUtBLElBQUdGLFFBQVEsQ0FBQyxRQUFELENBQVgsRUFBdUI7QUFDeEJ4RCxhQUFLLENBQUNwQixJQUFOLENBQVcsV0FBWCxFQUF3QnVELElBQXhCLENBQTZCLFlBQVc7QUFDcEMsY0FBSUMsTUFBTSxHQUFHeEUsQ0FBQyxDQUFDLElBQUQsQ0FBZDtBQUNBLGNBQUl1RixPQUFPLEdBQUdmLE1BQU0sQ0FBQ2dCLE1BQVAsRUFBZDs7QUFFQSxjQUFJSSxRQUFRLENBQUMsUUFBRCxDQUFSLENBQW1CcEIsTUFBTSxDQUFDM0QsSUFBUCxDQUFZLE1BQVosQ0FBbkIsQ0FBSixFQUE2QztBQUN6Q2dGLHdCQUFZLEdBQUdELFFBQVEsQ0FBQyxRQUFELENBQVIsQ0FBbUJwQixNQUFNLENBQUMzRCxJQUFQLENBQVksTUFBWixDQUFuQixDQUFmO0FBQ0EwRSxtQkFBTyxDQUFDckUsV0FBUixDQUFvQixPQUFwQjtBQUNBc0Qsa0JBQU0sQ0FBQ3JELFFBQVAsQ0FBZ0IsT0FBaEI7QUFDQXNDLGlCQUFLLENBQUN3QyxnQkFBTixDQUF1QlYsT0FBdkIsRUFBZ0NNLFlBQWhDO0FBQ0gsV0FMRCxNQU1LLElBQUdELFFBQVEsQ0FBQyxRQUFELENBQVIsQ0FBbUIsZUFBbkIsQ0FBSCxFQUF3QztBQUN6Q0wsbUJBQU8sQ0FBQ3JFLFdBQVIsQ0FBb0IsT0FBcEI7QUFDQXNELGtCQUFNLENBQUN0RCxXQUFQLENBQW1CLE9BQW5CO0FBQ0FzRCxrQkFBTSxDQUFDckQsUUFBUCxDQUFnQixPQUFoQjtBQUNILFdBSkksTUFLQTtBQUNEcUQsa0JBQU0sQ0FBQ3RELFdBQVAsQ0FBbUIsT0FBbkI7QUFDSDtBQUNKLFNBbEJEO0FBb0JBNEUsaUJBQVMsR0FBRyxJQUFaO0FBQ0g7O0FBQ0QsYUFBT0EsU0FBUDtBQUNILEtBeE9FOztBQTBPSDs7O0FBR0E5QixVQUFNLEVBQUUsa0JBQVk7QUFDaEJoRSxPQUFDLENBQUMsVUFBRCxDQUFELENBQWNnRSxNQUFkLENBQXFCLFVBQVVrQyxLQUFWLEVBQWlCO0FBQ2xDQSxhQUFLLENBQUMxRixjQUFOO0FBRUEsWUFBSTRCLEtBQUssR0FBR3BDLENBQUMsQ0FBQyxJQUFELENBQWI7QUFBQSxZQUNJbUcsV0FBVyxHQUFHLElBRGxCO0FBQUEsWUFFSUMsTUFGSjtBQUFBLFlBR0lDLFdBSEo7QUFBQSxZQUlJQyxXQUpKO0FBQUEsWUFLSTFFLElBTEo7QUFNQVEsYUFBSyxDQUFDcEIsSUFBTixDQUFXLHVCQUFYLEVBQW9DdUQsSUFBcEMsQ0FBeUMsWUFBWTtBQUFFO0FBQ25EZCxlQUFLLENBQUMwQixhQUFOLENBQW9CbkYsQ0FBQyxDQUFDLElBQUQsQ0FBckI7QUFDSCxTQUZEO0FBR0FvQyxhQUFLLENBQUNwQixJQUFOLENBQVcsY0FBWCxFQUEyQnVELElBQTNCLENBQWdDLFlBQVk7QUFDeEMsY0FBSXZFLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWtGLFFBQVIsQ0FBaUIsT0FBakIsQ0FBSixFQUErQjtBQUMzQmlCLHVCQUFXLEdBQUcsS0FBZDtBQUNIO0FBQ0osU0FKRDs7QUFLQSxZQUFJQSxXQUFKLEVBQWlCO0FBQ2IvRCxlQUFLLENBQUMyQyxPQUFOLENBQWMsWUFBZDs7QUFDQSxjQUFHM0MsS0FBSyxDQUFDOEMsUUFBTixDQUFlLGVBQWYsQ0FBSCxFQUFtQztBQUMvQnRELGdCQUFJLEdBQUcyRSxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUNsQkMsc0JBQVEsRUFBRXpHLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0IwQixHQUFsQixFQURRO0FBRWxCZ0Ysc0JBQVEsRUFBRTFHLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0IwQixHQUFsQixFQUZRO0FBR2xCaUYsbUJBQUssRUFBRTNHLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZTBCLEdBQWY7QUFIVyxhQUFmLENBQVA7QUFLQTJFLHVCQUFXLEdBQUcsa0JBQWQ7QUFDQUMsdUJBQVcsR0FBRyxJQUFkO0FBQ0gsV0FSRCxNQVFNO0FBQ0YxRSxnQkFBSSxHQUFHLElBQUlnRixRQUFKLENBQWF4RSxLQUFLLENBQUMsQ0FBRCxDQUFsQixDQUFQO0FBQ0FpRSx1QkFBVyxHQUFHLEtBQWQ7QUFDQUMsdUJBQVcsR0FBRyxLQUFkO0FBQ0g7O0FBRURGLGdCQUFNLEdBQUdoRSxLQUFLLENBQUN2QixJQUFOLENBQVcsUUFBWCxDQUFUO0FBQ0FiLFdBQUMsQ0FBQzZHLElBQUYsQ0FBTztBQUNINUUsZ0JBQUksRUFBRW1FLE1BQU0sR0FBR0EsTUFBSCxHQUFZLE1BRHJCO0FBRUhVLGVBQUcsRUFBRTFFLEtBQUssQ0FBQ3ZCLElBQU4sQ0FBVyxRQUFYLENBRkY7QUFHSGUsZ0JBQUksRUFBRUEsSUFISDtBQUlIbUYsb0JBQVEsRUFBRSxNQUpQO0FBS0hWLHVCQUFXLEVBQUVBLFdBTFY7QUFNSEMsdUJBQVcsRUFBRUEsV0FOVjtBQU9IVSxtQkFBTyxFQUFFLGlCQUFTQyxJQUFULEVBQWU7QUFDcEIsa0JBQUcsQ0FBQ3hELEtBQUssQ0FBQ2tDLHVCQUFOLENBQThCdkQsS0FBOUIsRUFBcUM2RSxJQUFyQyxDQUFKLEVBQWdEO0FBQzVDN0UscUJBQUssQ0FBQzJDLE9BQU4sQ0FBYyxjQUFkLEVBQThCa0MsSUFBOUI7QUFDSCxlQUZELE1BR0s7QUFDRDdFLHFCQUFLLENBQUMyQyxPQUFOLENBQWMsWUFBZCxFQUE0QmtDLElBQTVCO0FBQ0g7QUFDSixhQWRFO0FBZUhDLGlCQUFLLEVBQUUsaUJBQVc7QUFDZHpELG1CQUFLLENBQUN3QyxnQkFBTixDQUF1QjdELEtBQXZCLEVBQThCLENBQUNBLEtBQUssQ0FBQ1IsSUFBTixDQUFXLFdBQVgsQ0FBRCxDQUE5QjtBQUNBUSxtQkFBSyxDQUFDMkMsT0FBTixDQUFjLFlBQWQ7QUFDSDtBQWxCRSxXQUFQO0FBb0JILFNBckNELE1BcUNPO0FBQ0gsaUJBQU8sS0FBUDtBQUNIO0FBQ0osT0F6REQ7QUEwREEvRSxPQUFDLENBQUMsWUFBRCxDQUFELENBQWdCTSxLQUFoQixDQUFzQixVQUFVQyxDQUFWLEVBQWE7QUFDL0JBLFNBQUMsQ0FBQ0MsY0FBRjtBQUNBUixTQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLE9BQVIsQ0FBZ0IsVUFBaEIsRUFBNEJzRCxNQUE1QjtBQUNILE9BSEQ7QUFJSCxLQTVTRTs7QUE4U0g7OztBQUdBaUMsb0JBQWdCLEVBQUUsMEJBQVNiLFFBQVQsRUFBbUIrQixTQUFuQixFQUE4QjtBQUM1QyxXQUFJLElBQUlDLElBQVIsSUFBZ0JELFNBQWhCLEVBQTJCO0FBQ3ZCLFlBQUlFLE1BQU0sR0FBR3JILENBQUMsQ0FBQyxxREFBcURtSCxTQUFTLENBQUNDLElBQUQsQ0FBOUQsR0FBdUUsUUFBeEUsQ0FBZCxDQUR1QixDQUV2Qjs7QUFDQWhDLGdCQUFRLENBQUNrQyxNQUFULENBQWdCRCxNQUFoQjtBQUNBQSxjQUFNLENBQUNFLE1BQVAsQ0FBYyxNQUFkLEVBQXVCLENBQXZCLEVBQTBCLFlBQVc7QUFDakNDLG9CQUFVLENBQUMsWUFBVTtBQUNqQkgsa0JBQU0sQ0FBQ0ksS0FBUCxHQUFlQyxPQUFmLENBQXVCLE1BQXZCLEVBQStCLFlBQVc7QUFDdEN0QyxzQkFBUSxDQUFDcEUsSUFBVCxDQUFjLGtCQUFkLEVBQWtDMkcsTUFBbEM7QUFDSCxhQUZEO0FBR0gsV0FKUyxFQUlQLElBSk8sQ0FBVjtBQUtILFNBTkQsRUFKdUIsQ0FXdkI7QUFDSDtBQUNKLEtBL1RFO0FBaVVIekQsY0FBVSxFQUFFLHNCQUFXO0FBQ25CbEUsT0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjeUIsRUFBZCxDQUFpQixZQUFqQixFQUErQixZQUFXO0FBQ3RDekIsU0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRbUIsUUFBUixDQUFpQixZQUFqQjtBQUNILE9BRkQ7QUFJQW5CLE9BQUMsQ0FBQyxVQUFELENBQUQsQ0FBY3lCLEVBQWQsQ0FBaUIseUJBQWpCLEVBQTRDLFlBQVc7QUFDbkR6QixTQUFDLENBQUMsSUFBRCxDQUFELENBQVFrQixXQUFSLENBQW9CLFlBQXBCO0FBQ0gsT0FGRDtBQUlBbEIsT0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0J5QixFQUFwQixDQUF1QixjQUF2QixFQUF1QyxVQUFTbEIsQ0FBVCxFQUFXMEcsSUFBWCxFQUFpQjtBQUNwRHZFLGNBQU0sQ0FBQ2tGLFFBQVAsQ0FBZ0JDLE9BQWhCLENBQXdCN0gsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNEIsSUFBUixDQUFhLFVBQWIsQ0FBeEI7QUFDSCxPQUZEO0FBR0g7QUE3VUUsR0FBUDtBQStVSCxDQWxWVyxFQUFaOztBQW1WQTZCLEtBQUssQ0FBQ3RGLElBQU4sRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BWQTs7Ozs7OztBQVFBLENBQUMsVUFBUzJKLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFVBQXNDd0gsaUNBQXVDLENBQUMseUVBQUQsQ0FBakMsbUNBQTRDLFVBQVNDLENBQVQsRUFBVztBQUFDLFdBQU96SCxDQUFDLENBQUN1SCxDQUFELEVBQUdFLENBQUgsQ0FBUjtBQUFjLEdBQXRFO0FBQUEsb0dBQTVDLEdBQW9ILFNBQXBIO0FBQWdPLENBQTlPLENBQStPdEYsTUFBL08sRUFBc1AsVUFBU29GLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDOztBQUFhLFdBQVN5SCxDQUFULENBQVdBLENBQVgsRUFBYUMsQ0FBYixFQUFlQyxDQUFmLEVBQWlCO0FBQUMsYUFBU0MsQ0FBVCxDQUFXTCxDQUFYLEVBQWF2SCxDQUFiLEVBQWU2SCxDQUFmLEVBQWlCO0FBQUMsVUFBSUMsQ0FBSjtBQUFBLFVBQU1KLENBQUMsR0FBQyxTQUFPRCxDQUFQLEdBQVMsSUFBVCxHQUFjekgsQ0FBZCxHQUFnQixJQUF4QjtBQUE2QixhQUFPdUgsQ0FBQyxDQUFDdkQsSUFBRixDQUFPLFVBQVN1RCxDQUFULEVBQVdLLENBQVgsRUFBYTtBQUFDLFlBQUlHLENBQUMsR0FBQ0osQ0FBQyxDQUFDdEcsSUFBRixDQUFPdUcsQ0FBUCxFQUFTSCxDQUFULENBQU47QUFBa0IsWUFBRyxDQUFDTSxDQUFKLEVBQU0sT0FBTyxLQUFLQyxDQUFDLENBQUNQLENBQUMsR0FBQyw4Q0FBRixHQUFpREMsQ0FBbEQsQ0FBYjtBQUFrRSxZQUFJTyxDQUFDLEdBQUNGLENBQUMsQ0FBQy9ILENBQUQsQ0FBUDtBQUFXLFlBQUcsQ0FBQ2lJLENBQUQsSUFBSSxPQUFLakksQ0FBQyxDQUFDa0ksTUFBRixDQUFTLENBQVQsQ0FBWixFQUF3QixPQUFPLEtBQUtGLENBQUMsQ0FBQ04sQ0FBQyxHQUFDLHdCQUFILENBQWI7QUFBMEMsWUFBSVMsQ0FBQyxHQUFDRixDQUFDLENBQUNHLEtBQUYsQ0FBUUwsQ0FBUixFQUFVRixDQUFWLENBQU47QUFBbUJDLFNBQUMsR0FBQyxLQUFLLENBQUwsS0FBU0EsQ0FBVCxHQUFXSyxDQUFYLEdBQWFMLENBQWY7QUFBaUIsT0FBaE8sR0FBa08sS0FBSyxDQUFMLEtBQVNBLENBQVQsR0FBV0EsQ0FBWCxHQUFhUCxDQUF0UDtBQUF3UDs7QUFBQSxhQUFTUSxDQUFULENBQVdSLENBQVgsRUFBYXZILENBQWIsRUFBZTtBQUFDdUgsT0FBQyxDQUFDdkQsSUFBRixDQUFPLFVBQVN1RCxDQUFULEVBQVdNLENBQVgsRUFBYTtBQUFDLFlBQUlDLENBQUMsR0FBQ0gsQ0FBQyxDQUFDdEcsSUFBRixDQUFPd0csQ0FBUCxFQUFTSixDQUFULENBQU47QUFBa0JLLFNBQUMsSUFBRUEsQ0FBQyxDQUFDTyxNQUFGLENBQVNySSxDQUFULEdBQVk4SCxDQUFDLENBQUNRLEtBQUYsRUFBZCxLQUEwQlIsQ0FBQyxHQUFDLElBQUlKLENBQUosQ0FBTUcsQ0FBTixFQUFRN0gsQ0FBUixDQUFGLEVBQWEySCxDQUFDLENBQUN0RyxJQUFGLENBQU93RyxDQUFQLEVBQVNKLENBQVQsRUFBV0ssQ0FBWCxDQUF2QyxDQUFEO0FBQXVELE9BQTlGO0FBQWdHOztBQUFBSCxLQUFDLEdBQUNBLENBQUMsSUFBRTNILENBQUgsSUFBTXVILENBQUMsQ0FBQ2dCLE1BQVYsRUFBaUJaLENBQUMsS0FBR0QsQ0FBQyxDQUFDYyxTQUFGLENBQVlILE1BQVosS0FBcUJYLENBQUMsQ0FBQ2MsU0FBRixDQUFZSCxNQUFaLEdBQW1CLFVBQVNkLENBQVQsRUFBVztBQUFDSSxPQUFDLENBQUNjLGFBQUYsQ0FBZ0JsQixDQUFoQixNQUFxQixLQUFLbUIsT0FBTCxHQUFhZixDQUFDLENBQUNnQixNQUFGLENBQVMsQ0FBQyxDQUFWLEVBQVksS0FBS0QsT0FBakIsRUFBeUJuQixDQUF6QixDQUFsQztBQUErRCxLQUFuSCxHQUFxSEksQ0FBQyxDQUFDaUIsRUFBRixDQUFLbkIsQ0FBTCxJQUFRLFVBQVNGLENBQVQsRUFBVztBQUFDLFVBQUcsWUFBVSxPQUFPQSxDQUFwQixFQUFzQjtBQUFDLFlBQUl2SCxDQUFDLEdBQUM4SCxDQUFDLENBQUNlLElBQUYsQ0FBT0MsU0FBUCxFQUFpQixDQUFqQixDQUFOO0FBQTBCLGVBQU9sQixDQUFDLENBQUMsSUFBRCxFQUFNTCxDQUFOLEVBQVF2SCxDQUFSLENBQVI7QUFBbUI7O0FBQUEsYUFBTytILENBQUMsQ0FBQyxJQUFELEVBQU1SLENBQU4sQ0FBRCxFQUFVLElBQWpCO0FBQXNCLEtBQW5PLEVBQW9PTSxDQUFDLENBQUNGLENBQUQsQ0FBeE8sQ0FBbEI7QUFBK1A7O0FBQUEsV0FBU0UsQ0FBVCxDQUFXTixDQUFYLEVBQWE7QUFBQyxLQUFDQSxDQUFELElBQUlBLENBQUMsSUFBRUEsQ0FBQyxDQUFDd0IsT0FBVCxLQUFtQnhCLENBQUMsQ0FBQ3dCLE9BQUYsR0FBVXRCLENBQTdCO0FBQWdDOztBQUFBLE1BQUlLLENBQUMsR0FBQ2tCLEtBQUssQ0FBQ1IsU0FBTixDQUFnQlMsS0FBdEI7QUFBQSxNQUE0QnZCLENBQUMsR0FBQ0gsQ0FBQyxDQUFDOUosT0FBaEM7QUFBQSxNQUF3Q3VLLENBQUMsR0FBQyxlQUFhLE9BQU9OLENBQXBCLEdBQXNCLFlBQVUsQ0FBRSxDQUFsQyxHQUFtQyxVQUFTSCxDQUFULEVBQVc7QUFBQ0csS0FBQyxDQUFDZixLQUFGLENBQVFZLENBQVI7QUFBVyxHQUFwRztBQUFxRyxTQUFPTSxDQUFDLENBQUM3SCxDQUFDLElBQUV1SCxDQUFDLENBQUNnQixNQUFOLENBQUQsRUFBZWQsQ0FBdEI7QUFBd0IsQ0FBcG1DLENBQUQsRUFBdW1DLFVBQVNGLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFVBQXNDd0gsdUNBQStCeEgsQ0FBekIsNmpCQUE1QyxHQUF3RSxTQUF4RTtBQUFtSixDQUFqSyxDQUFrSyxlQUFhLE9BQU9tQyxNQUFwQixHQUEyQkEsTUFBM0IsR0FBa0MsSUFBcE0sRUFBeU0sWUFBVTtBQUFDLFdBQVNvRixDQUFULEdBQVksQ0FBRTs7QUFBQSxNQUFJdkgsQ0FBQyxHQUFDdUgsQ0FBQyxDQUFDaUIsU0FBUjtBQUFrQixTQUFPeEksQ0FBQyxDQUFDa0IsRUFBRixHQUFLLFVBQVNxRyxDQUFULEVBQVd2SCxDQUFYLEVBQWE7QUFBQyxRQUFHdUgsQ0FBQyxJQUFFdkgsQ0FBTixFQUFRO0FBQUMsVUFBSXlILENBQUMsR0FBQyxLQUFLeUIsT0FBTCxHQUFhLEtBQUtBLE9BQUwsSUFBYyxFQUFqQztBQUFBLFVBQW9DckIsQ0FBQyxHQUFDSixDQUFDLENBQUNGLENBQUQsQ0FBRCxHQUFLRSxDQUFDLENBQUNGLENBQUQsQ0FBRCxJQUFNLEVBQWpEO0FBQW9ELGFBQU0sQ0FBQyxDQUFELElBQUlNLENBQUMsQ0FBQ3NCLE9BQUYsQ0FBVW5KLENBQVYsQ0FBSixJQUFrQjZILENBQUMsQ0FBQ3VCLElBQUYsQ0FBT3BKLENBQVAsQ0FBbEIsRUFBNEIsSUFBbEM7QUFBdUM7QUFBQyxHQUF4SCxFQUF5SEEsQ0FBQyxDQUFDcUosSUFBRixHQUFPLFVBQVM5QixDQUFULEVBQVd2SCxDQUFYLEVBQWE7QUFBQyxRQUFHdUgsQ0FBQyxJQUFFdkgsQ0FBTixFQUFRO0FBQUMsV0FBS2tCLEVBQUwsQ0FBUXFHLENBQVIsRUFBVXZILENBQVY7QUFBYSxVQUFJeUgsQ0FBQyxHQUFDLEtBQUs2QixXQUFMLEdBQWlCLEtBQUtBLFdBQUwsSUFBa0IsRUFBekM7QUFBQSxVQUE0Q3pCLENBQUMsR0FBQ0osQ0FBQyxDQUFDRixDQUFELENBQUQsR0FBS0UsQ0FBQyxDQUFDRixDQUFELENBQUQsSUFBTSxFQUF6RDtBQUE0RCxhQUFPTSxDQUFDLENBQUM3SCxDQUFELENBQUQsR0FBSyxDQUFDLENBQU4sRUFBUSxJQUFmO0FBQW9CO0FBQUMsR0FBclAsRUFBc1BBLENBQUMsQ0FBQ3VKLEdBQUYsR0FBTSxVQUFTaEMsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsUUFBSXlILENBQUMsR0FBQyxLQUFLeUIsT0FBTCxJQUFjLEtBQUtBLE9BQUwsQ0FBYTNCLENBQWIsQ0FBcEI7O0FBQW9DLFFBQUdFLENBQUMsSUFBRUEsQ0FBQyxDQUFDNUQsTUFBUixFQUFlO0FBQUMsVUFBSWdFLENBQUMsR0FBQ0osQ0FBQyxDQUFDMEIsT0FBRixDQUFVbkosQ0FBVixDQUFOO0FBQW1CLGFBQU0sQ0FBQyxDQUFELElBQUk2SCxDQUFKLElBQU9KLENBQUMsQ0FBQytCLE1BQUYsQ0FBUzNCLENBQVQsRUFBVyxDQUFYLENBQVAsRUFBcUIsSUFBM0I7QUFBZ0M7QUFBQyxHQUFsWCxFQUFtWDdILENBQUMsQ0FBQ3lKLFNBQUYsR0FBWSxVQUFTbEMsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsUUFBSXlILENBQUMsR0FBQyxLQUFLeUIsT0FBTCxJQUFjLEtBQUtBLE9BQUwsQ0FBYTNCLENBQWIsQ0FBcEI7O0FBQW9DLFFBQUdFLENBQUMsSUFBRUEsQ0FBQyxDQUFDNUQsTUFBUixFQUFlO0FBQUM0RCxPQUFDLEdBQUNBLENBQUMsQ0FBQ3dCLEtBQUYsQ0FBUSxDQUFSLENBQUYsRUFBYWpKLENBQUMsR0FBQ0EsQ0FBQyxJQUFFLEVBQWxCOztBQUFxQixXQUFJLElBQUk2SCxDQUFDLEdBQUMsS0FBS3lCLFdBQUwsSUFBa0IsS0FBS0EsV0FBTCxDQUFpQi9CLENBQWpCLENBQXhCLEVBQTRDTyxDQUFDLEdBQUMsQ0FBbEQsRUFBb0RBLENBQUMsR0FBQ0wsQ0FBQyxDQUFDNUQsTUFBeEQsRUFBK0RpRSxDQUFDLEVBQWhFLEVBQW1FO0FBQUMsWUFBSUosQ0FBQyxHQUFDRCxDQUFDLENBQUNLLENBQUQsQ0FBUDtBQUFBLFlBQVdFLENBQUMsR0FBQ0gsQ0FBQyxJQUFFQSxDQUFDLENBQUNILENBQUQsQ0FBakI7QUFBcUJNLFNBQUMsS0FBRyxLQUFLdUIsR0FBTCxDQUFTaEMsQ0FBVCxFQUFXRyxDQUFYLEdBQWMsT0FBT0csQ0FBQyxDQUFDSCxDQUFELENBQXpCLENBQUQsRUFBK0JBLENBQUMsQ0FBQ1UsS0FBRixDQUFRLElBQVIsRUFBYXBJLENBQWIsQ0FBL0I7QUFBK0M7O0FBQUEsYUFBTyxJQUFQO0FBQVk7QUFBQyxHQUEzbUIsRUFBNG1CQSxDQUFDLENBQUMwSixNQUFGLEdBQVMsWUFBVTtBQUFDLFdBQU8sS0FBS1IsT0FBWixFQUFvQixPQUFPLEtBQUtJLFdBQWhDO0FBQTRDLEdBQTVxQixFQUE2cUIvQixDQUFwckI7QUFBc3JCLENBQTE2QixDQUF2bUMsRUFBbWhFLFVBQVNBLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFVBQXNDd0gsdUNBQTJCeEgsQ0FBckIseWpCQUE1QyxHQUFvRSxTQUFwRTtBQUE2SSxDQUEzSixDQUE0Sm1DLE1BQTVKLEVBQW1LLFlBQVU7QUFBQzs7QUFBYSxXQUFTb0YsQ0FBVCxDQUFXQSxDQUFYLEVBQWE7QUFBQyxRQUFJdkgsQ0FBQyxHQUFDcUUsVUFBVSxDQUFDa0QsQ0FBRCxDQUFoQjtBQUFBLFFBQW9CRSxDQUFDLEdBQUMsQ0FBQyxDQUFELElBQUlGLENBQUMsQ0FBQzRCLE9BQUYsQ0FBVSxHQUFWLENBQUosSUFBb0IsQ0FBQ1EsS0FBSyxDQUFDM0osQ0FBRCxDQUFoRDtBQUFvRCxXQUFPeUgsQ0FBQyxJQUFFekgsQ0FBVjtBQUFZOztBQUFBLFdBQVNBLENBQVQsR0FBWSxDQUFFOztBQUFBLFdBQVN5SCxDQUFULEdBQVk7QUFBQyxTQUFJLElBQUlGLENBQUMsR0FBQztBQUFDcUMsV0FBSyxFQUFDLENBQVA7QUFBU0MsWUFBTSxFQUFDLENBQWhCO0FBQWtCQyxnQkFBVSxFQUFDLENBQTdCO0FBQStCQyxpQkFBVyxFQUFDLENBQTNDO0FBQTZDQyxnQkFBVSxFQUFDLENBQXhEO0FBQTBEQyxpQkFBVyxFQUFDO0FBQXRFLEtBQU4sRUFBK0VqSyxDQUFDLEdBQUMsQ0FBckYsRUFBdUYrSCxDQUFDLEdBQUMvSCxDQUF6RixFQUEyRkEsQ0FBQyxFQUE1RixFQUErRjtBQUFDLFVBQUl5SCxDQUFDLEdBQUNHLENBQUMsQ0FBQzVILENBQUQsQ0FBUDtBQUFXdUgsT0FBQyxDQUFDRSxDQUFELENBQUQsR0FBSyxDQUFMO0FBQU87O0FBQUEsV0FBT0YsQ0FBUDtBQUFTOztBQUFBLFdBQVNNLENBQVQsQ0FBV04sQ0FBWCxFQUFhO0FBQUMsUUFBSXZILENBQUMsR0FBQ2tLLGdCQUFnQixDQUFDM0MsQ0FBRCxDQUF0QjtBQUEwQixXQUFPdkgsQ0FBQyxJQUFFMkgsQ0FBQyxDQUFDLG9CQUFrQjNILENBQWxCLEdBQW9CLDJGQUFyQixDQUFKLEVBQXNIQSxDQUE3SDtBQUErSDs7QUFBQSxXQUFTOEgsQ0FBVCxHQUFZO0FBQUMsUUFBRyxDQUFDRyxDQUFKLEVBQU07QUFBQ0EsT0FBQyxHQUFDLENBQUMsQ0FBSDtBQUFLLFVBQUlqSSxDQUFDLEdBQUN1QyxRQUFRLENBQUM0SCxhQUFULENBQXVCLEtBQXZCLENBQU47QUFBb0NuSyxPQUFDLENBQUNvSyxLQUFGLENBQVFSLEtBQVIsR0FBYyxPQUFkLEVBQXNCNUosQ0FBQyxDQUFDb0ssS0FBRixDQUFRQyxPQUFSLEdBQWdCLGlCQUF0QyxFQUF3RHJLLENBQUMsQ0FBQ29LLEtBQUYsQ0FBUUUsV0FBUixHQUFvQixPQUE1RSxFQUFvRnRLLENBQUMsQ0FBQ29LLEtBQUYsQ0FBUUcsV0FBUixHQUFvQixpQkFBeEcsRUFBMEh2SyxDQUFDLENBQUNvSyxLQUFGLENBQVFJLFNBQVIsR0FBa0IsWUFBNUk7QUFBeUosVUFBSS9DLENBQUMsR0FBQ2xGLFFBQVEsQ0FBQ2tJLElBQVQsSUFBZWxJLFFBQVEsQ0FBQ0MsZUFBOUI7QUFBOENpRixPQUFDLENBQUNpRCxXQUFGLENBQWMxSyxDQUFkO0FBQWlCLFVBQUk4SCxDQUFDLEdBQUNELENBQUMsQ0FBQzdILENBQUQsQ0FBUDtBQUFXZ0ksT0FBQyxHQUFDLE9BQUsyQyxJQUFJLENBQUNDLEtBQUwsQ0FBV3JELENBQUMsQ0FBQ08sQ0FBQyxDQUFDOEIsS0FBSCxDQUFaLENBQVAsRUFBOEJsQyxDQUFDLENBQUNtRCxjQUFGLEdBQWlCN0MsQ0FBL0MsRUFBaURQLENBQUMsQ0FBQ3FELFdBQUYsQ0FBYzlLLENBQWQsQ0FBakQ7QUFBa0U7QUFBQzs7QUFBQSxXQUFTMEgsQ0FBVCxDQUFXMUgsQ0FBWCxFQUFhO0FBQUMsUUFBRzhILENBQUMsSUFBRyxZQUFVLE9BQU85SCxDQUFqQixLQUFxQkEsQ0FBQyxHQUFDdUMsUUFBUSxDQUFDd0ksYUFBVCxDQUF1Qi9LLENBQXZCLENBQXZCLENBQUgsRUFBcURBLENBQUMsSUFBRSxvQkFBaUJBLENBQWpCLENBQUgsSUFBdUJBLENBQUMsQ0FBQ2dMLFFBQWxGLEVBQTJGO0FBQUMsVUFBSXRELENBQUMsR0FBQ0csQ0FBQyxDQUFDN0gsQ0FBRCxDQUFQO0FBQVcsVUFBRyxVQUFRMEgsQ0FBQyxDQUFDdUQsT0FBYixFQUFxQixPQUFPeEQsQ0FBQyxFQUFSO0FBQVcsVUFBSUUsQ0FBQyxHQUFDLEVBQU47QUFBU0EsT0FBQyxDQUFDaUMsS0FBRixHQUFRNUosQ0FBQyxDQUFDa0wsV0FBVixFQUFzQnZELENBQUMsQ0FBQ2tDLE1BQUYsR0FBUzdKLENBQUMsQ0FBQ21MLFlBQWpDOztBQUE4QyxXQUFJLElBQUlsRCxDQUFDLEdBQUNOLENBQUMsQ0FBQ3lELFdBQUYsR0FBYyxnQkFBYzFELENBQUMsQ0FBQzhDLFNBQXBDLEVBQThDckMsQ0FBQyxHQUFDLENBQXBELEVBQXNESixDQUFDLEdBQUNJLENBQXhELEVBQTBEQSxDQUFDLEVBQTNELEVBQThEO0FBQUMsWUFBSWtELENBQUMsR0FBQ3pELENBQUMsQ0FBQ08sQ0FBRCxDQUFQO0FBQUEsWUFBV21ELENBQUMsR0FBQzVELENBQUMsQ0FBQzJELENBQUQsQ0FBZDtBQUFBLFlBQWtCRSxDQUFDLEdBQUNsSCxVQUFVLENBQUNpSCxDQUFELENBQTlCO0FBQWtDM0QsU0FBQyxDQUFDMEQsQ0FBRCxDQUFELEdBQUsxQixLQUFLLENBQUM0QixDQUFELENBQUwsR0FBUyxDQUFULEdBQVdBLENBQWhCO0FBQWtCOztBQUFBLFVBQUlDLENBQUMsR0FBQzdELENBQUMsQ0FBQzhELFdBQUYsR0FBYzlELENBQUMsQ0FBQytELFlBQXRCO0FBQUEsVUFBbUNDLENBQUMsR0FBQ2hFLENBQUMsQ0FBQ2lFLFVBQUYsR0FBYWpFLENBQUMsQ0FBQ2tFLGFBQXBEO0FBQUEsVUFBa0VDLENBQUMsR0FBQ25FLENBQUMsQ0FBQ29FLFVBQUYsR0FBYXBFLENBQUMsQ0FBQ3FFLFdBQW5GO0FBQUEsVUFBK0ZDLENBQUMsR0FBQ3RFLENBQUMsQ0FBQ3VFLFNBQUYsR0FBWXZFLENBQUMsQ0FBQ3dFLFlBQS9HO0FBQUEsVUFBNEhDLENBQUMsR0FBQ3pFLENBQUMsQ0FBQzBFLGVBQUYsR0FBa0IxRSxDQUFDLENBQUMyRSxnQkFBbEo7QUFBQSxVQUFtS0MsQ0FBQyxHQUFDNUUsQ0FBQyxDQUFDNkUsY0FBRixHQUFpQjdFLENBQUMsQ0FBQzhFLGlCQUF4TDtBQUFBLFVBQTBNQyxDQUFDLEdBQUN6RSxDQUFDLElBQUVELENBQS9NO0FBQUEsVUFBaU4yRSxDQUFDLEdBQUNwRixDQUFDLENBQUNHLENBQUMsQ0FBQ2tDLEtBQUgsQ0FBcE47O0FBQThOK0MsT0FBQyxLQUFHLENBQUMsQ0FBTCxLQUFTaEYsQ0FBQyxDQUFDaUMsS0FBRixHQUFRK0MsQ0FBQyxJQUFFRCxDQUFDLEdBQUMsQ0FBRCxHQUFHbEIsQ0FBQyxHQUFDWSxDQUFSLENBQWxCO0FBQThCLFVBQUlRLENBQUMsR0FBQ3JGLENBQUMsQ0FBQ0csQ0FBQyxDQUFDbUMsTUFBSCxDQUFQO0FBQWtCLGFBQU8rQyxDQUFDLEtBQUcsQ0FBQyxDQUFMLEtBQVNqRixDQUFDLENBQUNrQyxNQUFGLEdBQVMrQyxDQUFDLElBQUVGLENBQUMsR0FBQyxDQUFELEdBQUdmLENBQUMsR0FBQ1ksQ0FBUixDQUFuQixHQUErQjVFLENBQUMsQ0FBQ21DLFVBQUYsR0FBYW5DLENBQUMsQ0FBQ2lDLEtBQUYsSUFBUzRCLENBQUMsR0FBQ1ksQ0FBWCxDQUE1QyxFQUEwRHpFLENBQUMsQ0FBQ29DLFdBQUYsR0FBY3BDLENBQUMsQ0FBQ2tDLE1BQUYsSUFBVThCLENBQUMsR0FBQ1ksQ0FBWixDQUF4RSxFQUF1RjVFLENBQUMsQ0FBQ3FDLFVBQUYsR0FBYXJDLENBQUMsQ0FBQ2lDLEtBQUYsR0FBUWtDLENBQTVHLEVBQThHbkUsQ0FBQyxDQUFDc0MsV0FBRixHQUFjdEMsQ0FBQyxDQUFDa0MsTUFBRixHQUFTb0MsQ0FBckksRUFBdUl0RSxDQUE5STtBQUFnSjtBQUFDOztBQUFBLE1BQUlLLENBQUo7QUFBQSxNQUFNTCxDQUFDLEdBQUMsZUFBYSxPQUFPbEssT0FBcEIsR0FBNEJ1QyxDQUE1QixHQUE4QixVQUFTdUgsQ0FBVCxFQUFXO0FBQUM5SixXQUFPLENBQUNrSixLQUFSLENBQWNZLENBQWQ7QUFBaUIsR0FBbkU7QUFBQSxNQUFvRUssQ0FBQyxHQUFDLENBQUMsYUFBRCxFQUFlLGNBQWYsRUFBOEIsWUFBOUIsRUFBMkMsZUFBM0MsRUFBMkQsWUFBM0QsRUFBd0UsYUFBeEUsRUFBc0YsV0FBdEYsRUFBa0csY0FBbEcsRUFBaUgsaUJBQWpILEVBQW1JLGtCQUFuSSxFQUFzSixnQkFBdEosRUFBdUssbUJBQXZLLENBQXRFO0FBQUEsTUFBa1FHLENBQUMsR0FBQ0gsQ0FBQyxDQUFDL0QsTUFBdFE7QUFBQSxNQUE2UW9FLENBQUMsR0FBQyxDQUFDLENBQWhSO0FBQWtSLFNBQU9QLENBQVA7QUFBUyxDQUFsNkQsQ0FBbmhFLEVBQXU3SCxVQUFTSCxDQUFULEVBQVd2SCxDQUFYLEVBQWE7QUFBQzs7QUFBYSxVQUFzQ3dILHVDQUFvRHhILENBQTlDLGtsQkFBNUMsR0FBNkYsU0FBN0Y7QUFBOEssQ0FBek0sQ0FBME1tQyxNQUExTSxFQUFpTixZQUFVO0FBQUM7O0FBQWEsTUFBSW9GLENBQUMsR0FBQyxZQUFVO0FBQUMsUUFBSUEsQ0FBQyxHQUFDcEYsTUFBTSxDQUFDMEssT0FBUCxDQUFlckUsU0FBckI7QUFBK0IsUUFBR2pCLENBQUMsQ0FBQ3VGLE9BQUwsRUFBYSxPQUFNLFNBQU47QUFBZ0IsUUFBR3ZGLENBQUMsQ0FBQ3dGLGVBQUwsRUFBcUIsT0FBTSxpQkFBTjs7QUFBd0IsU0FBSSxJQUFJL00sQ0FBQyxHQUFDLENBQUMsUUFBRCxFQUFVLEtBQVYsRUFBZ0IsSUFBaEIsRUFBcUIsR0FBckIsQ0FBTixFQUFnQ3lILENBQUMsR0FBQyxDQUF0QyxFQUF3Q0EsQ0FBQyxHQUFDekgsQ0FBQyxDQUFDNkQsTUFBNUMsRUFBbUQ0RCxDQUFDLEVBQXBELEVBQXVEO0FBQUMsVUFBSUksQ0FBQyxHQUFDN0gsQ0FBQyxDQUFDeUgsQ0FBRCxDQUFQO0FBQUEsVUFBV0ssQ0FBQyxHQUFDRCxDQUFDLEdBQUMsaUJBQWY7QUFBaUMsVUFBR04sQ0FBQyxDQUFDTyxDQUFELENBQUosRUFBUSxPQUFPQSxDQUFQO0FBQVM7QUFBQyxHQUEvTixFQUFOOztBQUF3TyxTQUFPLFVBQVM5SCxDQUFULEVBQVd5SCxDQUFYLEVBQWE7QUFBQyxXQUFPekgsQ0FBQyxDQUFDdUgsQ0FBRCxDQUFELENBQUtFLENBQUwsQ0FBUDtBQUFlLEdBQXBDO0FBQXFDLENBQXRmLENBQXY3SCxFQUErNkksVUFBU0YsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsVUFBc0N3SCxpQ0FBOEIsQ0FBQywwQkFBRCxDQUF4QixpQ0FBdUUsVUFBU0MsQ0FBVCxFQUFXO0FBQUMsV0FBT3pILENBQUMsQ0FBQ3VILENBQUQsRUFBR0UsQ0FBSCxDQUFSO0FBQWMsR0FBakcsZ0RBQTVDLEdBQStJLFNBQS9JO0FBQXNSLENBQXBTLENBQXFTdEYsTUFBclMsRUFBNFMsVUFBU29GLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLE1BQUl5SCxDQUFDLEdBQUMsRUFBTjtBQUFTQSxHQUFDLENBQUNrQixNQUFGLEdBQVMsVUFBU3BCLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFNBQUksSUFBSXlILENBQVIsSUFBYXpILENBQWI7QUFBZXVILE9BQUMsQ0FBQ0UsQ0FBRCxDQUFELEdBQUt6SCxDQUFDLENBQUN5SCxDQUFELENBQU47QUFBZjs7QUFBeUIsV0FBT0YsQ0FBUDtBQUFTLEdBQXpELEVBQTBERSxDQUFDLENBQUN1RixNQUFGLEdBQVMsVUFBU3pGLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFdBQU0sQ0FBQ3VILENBQUMsR0FBQ3ZILENBQUYsR0FBSUEsQ0FBTCxJQUFRQSxDQUFkO0FBQWdCLEdBQWpHO0FBQWtHLE1BQUk2SCxDQUFDLEdBQUNtQixLQUFLLENBQUNSLFNBQU4sQ0FBZ0JTLEtBQXRCO0FBQTRCeEIsR0FBQyxDQUFDd0YsU0FBRixHQUFZLFVBQVMxRixDQUFULEVBQVc7QUFBQyxRQUFHeUIsS0FBSyxDQUFDa0UsT0FBTixDQUFjM0YsQ0FBZCxDQUFILEVBQW9CLE9BQU9BLENBQVA7QUFBUyxRQUFHLFNBQU9BLENBQVAsSUFBVSxLQUFLLENBQUwsS0FBU0EsQ0FBdEIsRUFBd0IsT0FBTSxFQUFOO0FBQVMsUUFBSXZILENBQUMsR0FBQyxvQkFBaUJ1SCxDQUFqQixLQUFvQixZQUFVLE9BQU9BLENBQUMsQ0FBQzFELE1BQTdDO0FBQW9ELFdBQU83RCxDQUFDLEdBQUM2SCxDQUFDLENBQUNnQixJQUFGLENBQU90QixDQUFQLENBQUQsR0FBVyxDQUFDQSxDQUFELENBQW5CO0FBQXVCLEdBQWpLLEVBQWtLRSxDQUFDLENBQUMwRixVQUFGLEdBQWEsVUFBUzVGLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFFBQUl5SCxDQUFDLEdBQUNGLENBQUMsQ0FBQzRCLE9BQUYsQ0FBVW5KLENBQVYsQ0FBTjtBQUFtQixLQUFDLENBQUQsSUFBSXlILENBQUosSUFBT0YsQ0FBQyxDQUFDaUMsTUFBRixDQUFTL0IsQ0FBVCxFQUFXLENBQVgsQ0FBUDtBQUFxQixHQUFyTyxFQUFzT0EsQ0FBQyxDQUFDMkYsU0FBRixHQUFZLFVBQVM3RixDQUFULEVBQVdFLENBQVgsRUFBYTtBQUFDLFdBQUtGLENBQUMsQ0FBQzhGLFVBQUYsSUFBYzlGLENBQUMsSUFBRWhGLFFBQVEsQ0FBQ2tJLElBQS9CO0FBQXFDLFVBQUdsRCxDQUFDLEdBQUNBLENBQUMsQ0FBQzhGLFVBQUosRUFBZXJOLENBQUMsQ0FBQ3VILENBQUQsRUFBR0UsQ0FBSCxDQUFuQixFQUF5QixPQUFPRixDQUFQO0FBQTlEO0FBQXVFLEdBQXZVLEVBQXdVRSxDQUFDLENBQUM2RixlQUFGLEdBQWtCLFVBQVMvRixDQUFULEVBQVc7QUFBQyxXQUFNLFlBQVUsT0FBT0EsQ0FBakIsR0FBbUJoRixRQUFRLENBQUN3SSxhQUFULENBQXVCeEQsQ0FBdkIsQ0FBbkIsR0FBNkNBLENBQW5EO0FBQXFELEdBQTNaLEVBQTRaRSxDQUFDLENBQUM4RixXQUFGLEdBQWMsVUFBU2hHLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsT0FBS3VILENBQUMsQ0FBQzdGLElBQWI7QUFBa0IsU0FBSzFCLENBQUwsS0FBUyxLQUFLQSxDQUFMLEVBQVF1SCxDQUFSLENBQVQ7QUFBb0IsR0FBNWQsRUFBNmRFLENBQUMsQ0FBQytGLGtCQUFGLEdBQXFCLFVBQVNqRyxDQUFULEVBQVdNLENBQVgsRUFBYTtBQUFDTixLQUFDLEdBQUNFLENBQUMsQ0FBQ3dGLFNBQUYsQ0FBWTFGLENBQVosQ0FBRjtBQUFpQixRQUFJTyxDQUFDLEdBQUMsRUFBTjtBQUFTLFdBQU9QLENBQUMsQ0FBQ2tHLE9BQUYsQ0FBVSxVQUFTbEcsQ0FBVCxFQUFXO0FBQUMsVUFBR0EsQ0FBQyxZQUFZbUcsV0FBaEIsRUFBNEI7QUFBQyxZQUFHLENBQUM3RixDQUFKLEVBQU0sT0FBTyxLQUFLQyxDQUFDLENBQUNzQixJQUFGLENBQU83QixDQUFQLENBQVo7QUFBc0J2SCxTQUFDLENBQUN1SCxDQUFELEVBQUdNLENBQUgsQ0FBRCxJQUFRQyxDQUFDLENBQUNzQixJQUFGLENBQU83QixDQUFQLENBQVI7O0FBQWtCLGFBQUksSUFBSUUsQ0FBQyxHQUFDRixDQUFDLENBQUNvRyxnQkFBRixDQUFtQjlGLENBQW5CLENBQU4sRUFBNEJILENBQUMsR0FBQyxDQUFsQyxFQUFvQ0EsQ0FBQyxHQUFDRCxDQUFDLENBQUM1RCxNQUF4QyxFQUErQzZELENBQUMsRUFBaEQ7QUFBbURJLFdBQUMsQ0FBQ3NCLElBQUYsQ0FBTzNCLENBQUMsQ0FBQ0MsQ0FBRCxDQUFSO0FBQW5EO0FBQWdFO0FBQUMsS0FBbEssR0FBb0tJLENBQTNLO0FBQTZLLEdBQXZzQixFQUF3c0JMLENBQUMsQ0FBQ21HLGNBQUYsR0FBaUIsVUFBU3JHLENBQVQsRUFBV3ZILENBQVgsRUFBYXlILENBQWIsRUFBZTtBQUFDQSxLQUFDLEdBQUNBLENBQUMsSUFBRSxHQUFMO0FBQVMsUUFBSUksQ0FBQyxHQUFDTixDQUFDLENBQUNpQixTQUFGLENBQVl4SSxDQUFaLENBQU47QUFBQSxRQUFxQjhILENBQUMsR0FBQzlILENBQUMsR0FBQyxTQUF6Qjs7QUFBbUN1SCxLQUFDLENBQUNpQixTQUFGLENBQVl4SSxDQUFaLElBQWUsWUFBVTtBQUFDLFVBQUl1SCxDQUFDLEdBQUMsS0FBS08sQ0FBTCxDQUFOO0FBQWMrRixrQkFBWSxDQUFDdEcsQ0FBRCxDQUFaO0FBQWdCLFVBQUl2SCxDQUFDLEdBQUM4SSxTQUFOO0FBQUEsVUFBZ0JwQixDQUFDLEdBQUMsSUFBbEI7QUFBdUIsV0FBS0ksQ0FBTCxJQUFRYixVQUFVLENBQUMsWUFBVTtBQUFDWSxTQUFDLENBQUNPLEtBQUYsQ0FBUVYsQ0FBUixFQUFVMUgsQ0FBVixHQUFhLE9BQU8wSCxDQUFDLENBQUNJLENBQUQsQ0FBckI7QUFBeUIsT0FBckMsRUFBc0NMLENBQXRDLENBQWxCO0FBQTJELEtBQTFJO0FBQTJJLEdBQWg2QixFQUFpNkJBLENBQUMsQ0FBQ3FHLFFBQUYsR0FBVyxVQUFTdkcsQ0FBVCxFQUFXO0FBQUMsUUFBSXZILENBQUMsR0FBQ3VDLFFBQVEsQ0FBQ3dMLFVBQWY7QUFBMEIsa0JBQVkvTixDQUFaLElBQWUsaUJBQWVBLENBQTlCLEdBQWdDaUgsVUFBVSxDQUFDTSxDQUFELENBQTFDLEdBQThDaEYsUUFBUSxDQUFDSCxnQkFBVCxDQUEwQixrQkFBMUIsRUFBNkNtRixDQUE3QyxDQUE5QztBQUE4RixHQUFoakMsRUFBaWpDRSxDQUFDLENBQUN1RyxRQUFGLEdBQVcsVUFBU3pHLENBQVQsRUFBVztBQUFDLFdBQU9BLENBQUMsQ0FBQ0QsT0FBRixDQUFVLGFBQVYsRUFBd0IsVUFBU0MsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFheUgsQ0FBYixFQUFlO0FBQUMsYUFBT3pILENBQUMsR0FBQyxHQUFGLEdBQU15SCxDQUFiO0FBQWUsS0FBdkQsRUFBeUR3RyxXQUF6RCxFQUFQO0FBQThFLEdBQXRwQztBQUF1cEMsTUFBSW5HLENBQUMsR0FBQ1AsQ0FBQyxDQUFDOUosT0FBUjtBQUFnQixTQUFPZ0ssQ0FBQyxDQUFDeUcsUUFBRixHQUFXLFVBQVNsTyxDQUFULEVBQVc2SCxDQUFYLEVBQWE7QUFBQ0osS0FBQyxDQUFDcUcsUUFBRixDQUFXLFlBQVU7QUFBQyxVQUFJcEcsQ0FBQyxHQUFDRCxDQUFDLENBQUN1RyxRQUFGLENBQVduRyxDQUFYLENBQU47QUFBQSxVQUFvQkcsQ0FBQyxHQUFDLFVBQVFOLENBQTlCO0FBQUEsVUFBZ0NDLENBQUMsR0FBQ3BGLFFBQVEsQ0FBQ29MLGdCQUFULENBQTBCLE1BQUkzRixDQUFKLEdBQU0sR0FBaEMsQ0FBbEM7QUFBQSxVQUF1RUosQ0FBQyxHQUFDckYsUUFBUSxDQUFDb0wsZ0JBQVQsQ0FBMEIsU0FBT2pHLENBQWpDLENBQXpFO0FBQUEsVUFBNkdLLENBQUMsR0FBQ04sQ0FBQyxDQUFDd0YsU0FBRixDQUFZdEYsQ0FBWixFQUFld0csTUFBZixDQUFzQjFHLENBQUMsQ0FBQ3dGLFNBQUYsQ0FBWXJGLENBQVosQ0FBdEIsQ0FBL0c7QUFBQSxVQUFxSkssQ0FBQyxHQUFDRCxDQUFDLEdBQUMsVUFBeko7QUFBQSxVQUFvS0csQ0FBQyxHQUFDWixDQUFDLENBQUNnQixNQUF4SztBQUErS1IsT0FBQyxDQUFDMEYsT0FBRixDQUFVLFVBQVNsRyxDQUFULEVBQVc7QUFBQyxZQUFJRSxDQUFKO0FBQUEsWUFBTUMsQ0FBQyxHQUFDSCxDQUFDLENBQUM2RyxZQUFGLENBQWVwRyxDQUFmLEtBQW1CVCxDQUFDLENBQUM2RyxZQUFGLENBQWVuRyxDQUFmLENBQTNCOztBQUE2QyxZQUFHO0FBQUNSLFdBQUMsR0FBQ0MsQ0FBQyxJQUFFMUIsSUFBSSxDQUFDcUksS0FBTCxDQUFXM0csQ0FBWCxDQUFMO0FBQW1CLFNBQXZCLENBQXVCLE9BQU1DLENBQU4sRUFBUTtBQUFDLGlCQUFPLE1BQUtHLENBQUMsSUFBRUEsQ0FBQyxDQUFDbkIsS0FBRixDQUFRLG1CQUFpQnFCLENBQWpCLEdBQW1CLE1BQW5CLEdBQTBCVCxDQUFDLENBQUMrRyxTQUE1QixHQUFzQyxJQUF0QyxHQUEyQzNHLENBQW5ELENBQVIsQ0FBUDtBQUFzRTs7QUFBQSxZQUFJQyxDQUFDLEdBQUMsSUFBSTVILENBQUosQ0FBTXVILENBQU4sRUFBUUUsQ0FBUixDQUFOO0FBQWlCVSxTQUFDLElBQUVBLENBQUMsQ0FBQzlHLElBQUYsQ0FBT2tHLENBQVAsRUFBU00sQ0FBVCxFQUFXRCxDQUFYLENBQUg7QUFBaUIsT0FBM007QUFBNk0sS0FBbFo7QUFBb1osR0FBN2EsRUFBOGFILENBQXJiO0FBQXViLENBQS9oRSxDQUEvNkksRUFBZzlNLFVBQVNGLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFVBQXNDd0gsaUNBQXVCLENBQUMsMEJBQUQsRUFBeUIsMEJBQXpCLENBQWpCLG9DQUErRHhILENBQS9EO0FBQUE7QUFBQSxvSEFBNUMsR0FBOEcsU0FBOUc7QUFBMlEsQ0FBelIsQ0FBMFJtQyxNQUExUixFQUFpUyxVQUFTb0YsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUM7O0FBQWEsV0FBU3lILENBQVQsQ0FBV0YsQ0FBWCxFQUFhO0FBQUMsU0FBSSxJQUFJdkgsQ0FBUixJQUFhdUgsQ0FBYjtBQUFlLGFBQU0sQ0FBQyxDQUFQO0FBQWY7O0FBQXdCLFdBQU92SCxDQUFDLEdBQUMsSUFBRixFQUFPLENBQUMsQ0FBZjtBQUFpQjs7QUFBQSxXQUFTNkgsQ0FBVCxDQUFXTixDQUFYLEVBQWF2SCxDQUFiLEVBQWU7QUFBQ3VILEtBQUMsS0FBRyxLQUFLZ0gsT0FBTCxHQUFhaEgsQ0FBYixFQUFlLEtBQUtpSCxNQUFMLEdBQVl4TyxDQUEzQixFQUE2QixLQUFLeU8sUUFBTCxHQUFjO0FBQUM3QixPQUFDLEVBQUMsQ0FBSDtBQUFLZCxPQUFDLEVBQUM7QUFBUCxLQUEzQyxFQUFxRCxLQUFLNEMsT0FBTCxFQUF4RCxDQUFEO0FBQXlFOztBQUFBLFdBQVM1RyxDQUFULENBQVdQLENBQVgsRUFBYTtBQUFDLFdBQU9BLENBQUMsQ0FBQ0QsT0FBRixDQUFVLFVBQVYsRUFBcUIsVUFBU0MsQ0FBVCxFQUFXO0FBQUMsYUFBTSxNQUFJQSxDQUFDLENBQUMwRyxXQUFGLEVBQVY7QUFBMEIsS0FBM0QsQ0FBUDtBQUFvRTs7QUFBQSxNQUFJdkcsQ0FBQyxHQUFDbkYsUUFBUSxDQUFDQyxlQUFULENBQXlCNEgsS0FBL0I7QUFBQSxNQUFxQ3BDLENBQUMsR0FBQyxZQUFVLE9BQU9OLENBQUMsQ0FBQ2lILFVBQW5CLEdBQThCLFlBQTlCLEdBQTJDLGtCQUFsRjtBQUFBLE1BQXFHaEgsQ0FBQyxHQUFDLFlBQVUsT0FBT0QsQ0FBQyxDQUFDa0gsU0FBbkIsR0FBNkIsV0FBN0IsR0FBeUMsaUJBQWhKO0FBQUEsTUFBa0toSCxDQUFDLEdBQUM7QUFBQ2lILG9CQUFnQixFQUFDLHFCQUFsQjtBQUF3Q0YsY0FBVSxFQUFDO0FBQW5ELElBQW9FM0csQ0FBcEUsQ0FBcEs7QUFBQSxNQUEyT0QsQ0FBQyxHQUFDO0FBQUM2RyxhQUFTLEVBQUNqSCxDQUFYO0FBQWFnSCxjQUFVLEVBQUMzRyxDQUF4QjtBQUEwQjhHLHNCQUFrQixFQUFDOUcsQ0FBQyxHQUFDLFVBQS9DO0FBQTBEK0csc0JBQWtCLEVBQUMvRyxDQUFDLEdBQUMsVUFBL0U7QUFBMEZnSCxtQkFBZSxFQUFDaEgsQ0FBQyxHQUFDO0FBQTVHLEdBQTdPO0FBQUEsTUFBa1dDLENBQUMsR0FBQ0osQ0FBQyxDQUFDVyxTQUFGLEdBQVloRCxNQUFNLENBQUN5SixNQUFQLENBQWMxSCxDQUFDLENBQUNpQixTQUFoQixDQUFoWDtBQUEyWVAsR0FBQyxDQUFDaUgsV0FBRixHQUFjckgsQ0FBZCxFQUFnQkksQ0FBQyxDQUFDeUcsT0FBRixHQUFVLFlBQVU7QUFBQyxTQUFLUyxPQUFMLEdBQWE7QUFBQ0MsbUJBQWEsRUFBQyxFQUFmO0FBQWtCQyxXQUFLLEVBQUMsRUFBeEI7QUFBMkJDLFdBQUssRUFBQztBQUFqQyxLQUFiLEVBQWtELEtBQUs1TSxHQUFMLENBQVM7QUFBQytMLGNBQVEsRUFBQztBQUFWLEtBQVQsQ0FBbEQ7QUFBa0YsR0FBdkgsRUFBd0h4RyxDQUFDLENBQUNzRixXQUFGLEdBQWMsVUFBU2hHLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsT0FBS3VILENBQUMsQ0FBQzdGLElBQWI7QUFBa0IsU0FBSzFCLENBQUwsS0FBUyxLQUFLQSxDQUFMLEVBQVF1SCxDQUFSLENBQVQ7QUFBb0IsR0FBeEwsRUFBeUxVLENBQUMsQ0FBQ3NILE9BQUYsR0FBVSxZQUFVO0FBQUMsU0FBS0MsSUFBTCxHQUFVeFAsQ0FBQyxDQUFDLEtBQUt1TyxPQUFOLENBQVg7QUFBMEIsR0FBeE8sRUFBeU90RyxDQUFDLENBQUN2RixHQUFGLEdBQU0sVUFBUzZFLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsS0FBS3VPLE9BQUwsQ0FBYW5FLEtBQW5COztBQUF5QixTQUFJLElBQUkzQyxDQUFSLElBQWFGLENBQWIsRUFBZTtBQUFDLFVBQUlNLENBQUMsR0FBQ0UsQ0FBQyxDQUFDTixDQUFELENBQUQsSUFBTUEsQ0FBWjtBQUFjekgsT0FBQyxDQUFDNkgsQ0FBRCxDQUFELEdBQUtOLENBQUMsQ0FBQ0UsQ0FBRCxDQUFOO0FBQVU7QUFBQyxHQUE3VCxFQUE4VFEsQ0FBQyxDQUFDd0gsV0FBRixHQUFjLFlBQVU7QUFBQyxRQUFJbEksQ0FBQyxHQUFDMkMsZ0JBQWdCLENBQUMsS0FBS3FFLE9BQU4sQ0FBdEI7QUFBQSxRQUFxQ3ZPLENBQUMsR0FBQyxLQUFLd08sTUFBTCxDQUFZa0IsVUFBWixDQUF1QixZQUF2QixDQUF2QztBQUFBLFFBQTRFakksQ0FBQyxHQUFDLEtBQUsrRyxNQUFMLENBQVlrQixVQUFaLENBQXVCLFdBQXZCLENBQTlFO0FBQUEsUUFBa0g3SCxDQUFDLEdBQUNOLENBQUMsQ0FBQ3ZILENBQUMsR0FBQyxNQUFELEdBQVEsT0FBVixDQUFySDtBQUFBLFFBQXdJOEgsQ0FBQyxHQUFDUCxDQUFDLENBQUNFLENBQUMsR0FBQyxLQUFELEdBQU8sUUFBVCxDQUEzSTtBQUFBLFFBQThKQyxDQUFDLEdBQUNyRCxVQUFVLENBQUN3RCxDQUFELENBQTFLO0FBQUEsUUFBOEtHLENBQUMsR0FBQzNELFVBQVUsQ0FBQ3lELENBQUQsQ0FBMUw7QUFBQSxRQUE4TEgsQ0FBQyxHQUFDLEtBQUs2RyxNQUFMLENBQVlnQixJQUE1TTs7QUFBaU4sS0FBQyxDQUFELElBQUkzSCxDQUFDLENBQUNzQixPQUFGLENBQVUsR0FBVixDQUFKLEtBQXFCekIsQ0FBQyxHQUFDQSxDQUFDLEdBQUMsR0FBRixHQUFNQyxDQUFDLENBQUNpQyxLQUEvQixHQUFzQyxDQUFDLENBQUQsSUFBSTlCLENBQUMsQ0FBQ3FCLE9BQUYsQ0FBVSxHQUFWLENBQUosS0FBcUJuQixDQUFDLEdBQUNBLENBQUMsR0FBQyxHQUFGLEdBQU1MLENBQUMsQ0FBQ2tDLE1BQS9CLENBQXRDLEVBQTZFbkMsQ0FBQyxHQUFDaUMsS0FBSyxDQUFDakMsQ0FBRCxDQUFMLEdBQVMsQ0FBVCxHQUFXQSxDQUExRixFQUE0Rk0sQ0FBQyxHQUFDMkIsS0FBSyxDQUFDM0IsQ0FBRCxDQUFMLEdBQVMsQ0FBVCxHQUFXQSxDQUF6RyxFQUEyR04sQ0FBQyxJQUFFMUgsQ0FBQyxHQUFDMkgsQ0FBQyxDQUFDOEQsV0FBSCxHQUFlOUQsQ0FBQyxDQUFDK0QsWUFBaEksRUFBNkkxRCxDQUFDLElBQUVQLENBQUMsR0FBQ0UsQ0FBQyxDQUFDaUUsVUFBSCxHQUFjakUsQ0FBQyxDQUFDa0UsYUFBakssRUFBK0ssS0FBSzRDLFFBQUwsQ0FBYzdCLENBQWQsR0FBZ0JsRixDQUEvTCxFQUFpTSxLQUFLK0csUUFBTCxDQUFjM0MsQ0FBZCxHQUFnQjlELENBQWpOO0FBQW1OLEdBQTN2QixFQUE0dkJDLENBQUMsQ0FBQzBILGNBQUYsR0FBaUIsWUFBVTtBQUFDLFFBQUlwSSxDQUFDLEdBQUMsS0FBS2lILE1BQUwsQ0FBWWdCLElBQWxCO0FBQUEsUUFBdUJ4UCxDQUFDLEdBQUMsRUFBekI7QUFBQSxRQUE0QnlILENBQUMsR0FBQyxLQUFLK0csTUFBTCxDQUFZa0IsVUFBWixDQUF1QixZQUF2QixDQUE5QjtBQUFBLFFBQW1FN0gsQ0FBQyxHQUFDLEtBQUsyRyxNQUFMLENBQVlrQixVQUFaLENBQXVCLFdBQXZCLENBQXJFO0FBQUEsUUFBeUc1SCxDQUFDLEdBQUNMLENBQUMsR0FBQyxhQUFELEdBQWUsY0FBM0g7QUFBQSxRQUEwSUMsQ0FBQyxHQUFDRCxDQUFDLEdBQUMsTUFBRCxHQUFRLE9BQXJKO0FBQUEsUUFBNkpPLENBQUMsR0FBQ1AsQ0FBQyxHQUFDLE9BQUQsR0FBUyxNQUF6SztBQUFBLFFBQWdMRSxDQUFDLEdBQUMsS0FBSzhHLFFBQUwsQ0FBYzdCLENBQWQsR0FBZ0JyRixDQUFDLENBQUNPLENBQUQsQ0FBbk07O0FBQXVNOUgsS0FBQyxDQUFDMEgsQ0FBRCxDQUFELEdBQUssS0FBS2tJLFNBQUwsQ0FBZWpJLENBQWYsQ0FBTCxFQUF1QjNILENBQUMsQ0FBQ2dJLENBQUQsQ0FBRCxHQUFLLEVBQTVCO0FBQStCLFFBQUlKLENBQUMsR0FBQ0MsQ0FBQyxHQUFDLFlBQUQsR0FBYyxlQUFyQjtBQUFBLFFBQXFDRSxDQUFDLEdBQUNGLENBQUMsR0FBQyxLQUFELEdBQU8sUUFBL0M7QUFBQSxRQUF3REksQ0FBQyxHQUFDSixDQUFDLEdBQUMsUUFBRCxHQUFVLEtBQXJFO0FBQUEsUUFBMkVNLENBQUMsR0FBQyxLQUFLc0csUUFBTCxDQUFjM0MsQ0FBZCxHQUFnQnZFLENBQUMsQ0FBQ0ssQ0FBRCxDQUE5RjtBQUFrRzVILEtBQUMsQ0FBQytILENBQUQsQ0FBRCxHQUFLLEtBQUs4SCxTQUFMLENBQWUxSCxDQUFmLENBQUwsRUFBdUJuSSxDQUFDLENBQUNpSSxDQUFELENBQUQsR0FBSyxFQUE1QixFQUErQixLQUFLdkYsR0FBTCxDQUFTMUMsQ0FBVCxDQUEvQixFQUEyQyxLQUFLeUosU0FBTCxDQUFlLFFBQWYsRUFBd0IsQ0FBQyxJQUFELENBQXhCLENBQTNDO0FBQTJFLEdBQTNxQyxFQUE0cUN4QixDQUFDLENBQUMySCxTQUFGLEdBQVksVUFBU3JJLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsS0FBS3dPLE1BQUwsQ0FBWWtCLFVBQVosQ0FBdUIsWUFBdkIsQ0FBTjs7QUFBMkMsV0FBTyxLQUFLbEIsTUFBTCxDQUFZOUYsT0FBWixDQUFvQm9ILGVBQXBCLElBQXFDLENBQUM5UCxDQUF0QyxHQUF3Q3VILENBQUMsR0FBQyxLQUFLaUgsTUFBTCxDQUFZZ0IsSUFBWixDQUFpQjVGLEtBQW5CLEdBQXlCLEdBQXpCLEdBQTZCLEdBQXJFLEdBQXlFckMsQ0FBQyxHQUFDLElBQWxGO0FBQXVGLEdBQXQwQyxFQUF1MENVLENBQUMsQ0FBQzRILFNBQUYsR0FBWSxVQUFTdEksQ0FBVCxFQUFXO0FBQUMsUUFBSXZILENBQUMsR0FBQyxLQUFLd08sTUFBTCxDQUFZa0IsVUFBWixDQUF1QixZQUF2QixDQUFOOztBQUEyQyxXQUFPLEtBQUtsQixNQUFMLENBQVk5RixPQUFaLENBQW9Cb0gsZUFBcEIsSUFBcUM5UCxDQUFyQyxHQUF1Q3VILENBQUMsR0FBQyxLQUFLaUgsTUFBTCxDQUFZZ0IsSUFBWixDQUFpQjNGLE1BQW5CLEdBQTBCLEdBQTFCLEdBQThCLEdBQXJFLEdBQXlFdEMsQ0FBQyxHQUFDLElBQWxGO0FBQXVGLEdBQWorQyxFQUFrK0NVLENBQUMsQ0FBQzhILGFBQUYsR0FBZ0IsVUFBU3hJLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFNBQUt5UCxXQUFMO0FBQW1CLFFBQUloSSxDQUFDLEdBQUMsS0FBS2dILFFBQUwsQ0FBYzdCLENBQXBCO0FBQUEsUUFBc0IvRSxDQUFDLEdBQUMsS0FBSzRHLFFBQUwsQ0FBYzNDLENBQXRDO0FBQUEsUUFBd0NoRSxDQUFDLEdBQUNQLENBQUMsSUFBRSxLQUFLa0gsUUFBTCxDQUFjN0IsQ0FBakIsSUFBb0I1TSxDQUFDLElBQUUsS0FBS3lPLFFBQUwsQ0FBYzNDLENBQS9FO0FBQWlGLFFBQUcsS0FBS2tFLFdBQUwsQ0FBaUJ6SSxDQUFqQixFQUFtQnZILENBQW5CLEdBQXNCOEgsQ0FBQyxJQUFFLENBQUMsS0FBS21JLGVBQWxDLEVBQWtELE9BQU8sS0FBSyxLQUFLTixjQUFMLEVBQVo7QUFBa0MsUUFBSWpJLENBQUMsR0FBQ0gsQ0FBQyxHQUFDRSxDQUFSO0FBQUEsUUFBVU8sQ0FBQyxHQUFDaEksQ0FBQyxHQUFDNkgsQ0FBZDtBQUFBLFFBQWdCRixDQUFDLEdBQUMsRUFBbEI7QUFBcUJBLEtBQUMsQ0FBQ2lILFNBQUYsR0FBWSxLQUFLc0IsWUFBTCxDQUFrQnhJLENBQWxCLEVBQW9CTSxDQUFwQixDQUFaLEVBQW1DLEtBQUsyRyxVQUFMLENBQWdCO0FBQUN3QixRQUFFLEVBQUN4SSxDQUFKO0FBQU15SSxxQkFBZSxFQUFDO0FBQUN4QixpQkFBUyxFQUFDLEtBQUtlO0FBQWhCLE9BQXRCO0FBQXNEVSxnQkFBVSxFQUFDLENBQUM7QUFBbEUsS0FBaEIsQ0FBbkM7QUFBeUgsR0FBdDBELEVBQXUwRHBJLENBQUMsQ0FBQ2lJLFlBQUYsR0FBZSxVQUFTM0ksQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsUUFBSXlILENBQUMsR0FBQyxLQUFLK0csTUFBTCxDQUFZa0IsVUFBWixDQUF1QixZQUF2QixDQUFOO0FBQUEsUUFBMkM3SCxDQUFDLEdBQUMsS0FBSzJHLE1BQUwsQ0FBWWtCLFVBQVosQ0FBdUIsV0FBdkIsQ0FBN0M7O0FBQWlGLFdBQU9uSSxDQUFDLEdBQUNFLENBQUMsR0FBQ0YsQ0FBRCxHQUFHLENBQUNBLENBQVAsRUFBU3ZILENBQUMsR0FBQzZILENBQUMsR0FBQzdILENBQUQsR0FBRyxDQUFDQSxDQUFoQixFQUFrQixpQkFBZXVILENBQWYsR0FBaUIsTUFBakIsR0FBd0J2SCxDQUF4QixHQUEwQixRQUFuRDtBQUE0RCxHQUFqL0QsRUFBay9EaUksQ0FBQyxDQUFDcUksSUFBRixHQUFPLFVBQVMvSSxDQUFULEVBQVd2SCxDQUFYLEVBQWE7QUFBQyxTQUFLZ1EsV0FBTCxDQUFpQnpJLENBQWpCLEVBQW1CdkgsQ0FBbkIsR0FBc0IsS0FBSzJQLGNBQUwsRUFBdEI7QUFBNEMsR0FBbmpFLEVBQW9qRTFILENBQUMsQ0FBQ3NJLE1BQUYsR0FBU3RJLENBQUMsQ0FBQzhILGFBQS9qRSxFQUE2a0U5SCxDQUFDLENBQUMrSCxXQUFGLEdBQWMsVUFBU3pJLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFNBQUt5TyxRQUFMLENBQWM3QixDQUFkLEdBQWdCdkksVUFBVSxDQUFDa0QsQ0FBRCxDQUExQixFQUE4QixLQUFLa0gsUUFBTCxDQUFjM0MsQ0FBZCxHQUFnQnpILFVBQVUsQ0FBQ3JFLENBQUQsQ0FBeEQ7QUFBNEQsR0FBcnFFLEVBQXNxRWlJLENBQUMsQ0FBQ3VJLGNBQUYsR0FBaUIsVUFBU2pKLENBQVQsRUFBVztBQUFDLFNBQUs3RSxHQUFMLENBQVM2RSxDQUFDLENBQUM0SSxFQUFYLEdBQWU1SSxDQUFDLENBQUM4SSxVQUFGLElBQWMsS0FBS0ksYUFBTCxDQUFtQmxKLENBQUMsQ0FBQzRJLEVBQXJCLENBQTdCOztBQUFzRCxTQUFJLElBQUluUSxDQUFSLElBQWF1SCxDQUFDLENBQUM2SSxlQUFmO0FBQStCN0ksT0FBQyxDQUFDNkksZUFBRixDQUFrQnBRLENBQWxCLEVBQXFCNkksSUFBckIsQ0FBMEIsSUFBMUI7QUFBL0I7QUFBK0QsR0FBeHpFLEVBQXl6RVosQ0FBQyxDQUFDMEcsVUFBRixHQUFhLFVBQVNwSCxDQUFULEVBQVc7QUFBQyxRQUFHLENBQUNsRCxVQUFVLENBQUMsS0FBS21LLE1BQUwsQ0FBWTlGLE9BQVosQ0FBb0JvRyxrQkFBckIsQ0FBZCxFQUF1RCxPQUFPLEtBQUssS0FBSzBCLGNBQUwsQ0FBb0JqSixDQUFwQixDQUFaO0FBQW1DLFFBQUl2SCxDQUFDLEdBQUMsS0FBS21QLE9BQVg7O0FBQW1CLFNBQUksSUFBSTFILENBQVIsSUFBYUYsQ0FBQyxDQUFDNkksZUFBZjtBQUErQnBRLE9BQUMsQ0FBQ3NQLEtBQUYsQ0FBUTdILENBQVIsSUFBV0YsQ0FBQyxDQUFDNkksZUFBRixDQUFrQjNJLENBQWxCLENBQVg7QUFBL0I7O0FBQStELFNBQUlBLENBQUosSUFBU0YsQ0FBQyxDQUFDNEksRUFBWDtBQUFjblEsT0FBQyxDQUFDb1AsYUFBRixDQUFnQjNILENBQWhCLElBQW1CLENBQUMsQ0FBcEIsRUFBc0JGLENBQUMsQ0FBQzhJLFVBQUYsS0FBZXJRLENBQUMsQ0FBQ3FQLEtBQUYsQ0FBUTVILENBQVIsSUFBVyxDQUFDLENBQTNCLENBQXRCO0FBQWQ7O0FBQWtFLFFBQUdGLENBQUMsQ0FBQzFILElBQUwsRUFBVTtBQUFDLFdBQUs2QyxHQUFMLENBQVM2RSxDQUFDLENBQUMxSCxJQUFYO0FBQWlCLFVBQUlnSSxDQUFDLEdBQUMsS0FBSzBHLE9BQUwsQ0FBYXBELFlBQW5CO0FBQWdDdEQsT0FBQyxHQUFDLElBQUY7QUFBTzs7QUFBQSxTQUFLNkksZ0JBQUwsQ0FBc0JuSixDQUFDLENBQUM0SSxFQUF4QixHQUE0QixLQUFLek4sR0FBTCxDQUFTNkUsQ0FBQyxDQUFDNEksRUFBWCxDQUE1QixFQUEyQyxLQUFLRixlQUFMLEdBQXFCLENBQUMsQ0FBakU7QUFBbUUsR0FBdHNGO0FBQXVzRixNQUFJOUgsQ0FBQyxHQUFDLGFBQVdMLENBQUMsQ0FBQ0gsQ0FBRCxDQUFsQjtBQUFzQk0sR0FBQyxDQUFDeUksZ0JBQUYsR0FBbUIsWUFBVTtBQUFDLFFBQUcsQ0FBQyxLQUFLVCxlQUFULEVBQXlCO0FBQUMsVUFBSTFJLENBQUMsR0FBQyxLQUFLaUgsTUFBTCxDQUFZOUYsT0FBWixDQUFvQm9HLGtCQUExQjtBQUE2Q3ZILE9BQUMsR0FBQyxZQUFVLE9BQU9BLENBQWpCLEdBQW1CQSxDQUFDLEdBQUMsSUFBckIsR0FBMEJBLENBQTVCLEVBQThCLEtBQUs3RSxHQUFMLENBQVM7QUFBQ3FNLDBCQUFrQixFQUFDNUcsQ0FBcEI7QUFBc0IyRywwQkFBa0IsRUFBQ3ZILENBQXpDO0FBQTJDeUgsdUJBQWUsRUFBQyxLQUFLMkIsWUFBTCxJQUFtQjtBQUE5RSxPQUFULENBQTlCLEVBQXlILEtBQUtwQyxPQUFMLENBQWFuTSxnQkFBYixDQUE4QndGLENBQTlCLEVBQWdDLElBQWhDLEVBQXFDLENBQUMsQ0FBdEMsQ0FBekg7QUFBa0s7QUFBQyxHQUF4USxFQUF5UUssQ0FBQyxDQUFDMkkscUJBQUYsR0FBd0IsVUFBU3JKLENBQVQsRUFBVztBQUFDLFNBQUtzSixlQUFMLENBQXFCdEosQ0FBckI7QUFBd0IsR0FBclUsRUFBc1VVLENBQUMsQ0FBQzZJLGdCQUFGLEdBQW1CLFVBQVN2SixDQUFULEVBQVc7QUFBQyxTQUFLc0osZUFBTCxDQUFxQnRKLENBQXJCO0FBQXdCLEdBQTdYO0FBQThYLE1BQUk4RCxDQUFDLEdBQUM7QUFBQyx5QkFBb0I7QUFBckIsR0FBTjtBQUF3Q3BELEdBQUMsQ0FBQzRJLGVBQUYsR0FBa0IsVUFBU3RKLENBQVQsRUFBVztBQUFDLFFBQUdBLENBQUMsQ0FBQ3dKLE1BQUYsS0FBVyxLQUFLeEMsT0FBbkIsRUFBMkI7QUFBQyxVQUFJdk8sQ0FBQyxHQUFDLEtBQUttUCxPQUFYO0FBQUEsVUFBbUJ0SCxDQUFDLEdBQUN3RCxDQUFDLENBQUM5RCxDQUFDLENBQUN5SixZQUFILENBQUQsSUFBbUJ6SixDQUFDLENBQUN5SixZQUExQzs7QUFBdUQsVUFBRyxPQUFPaFIsQ0FBQyxDQUFDb1AsYUFBRixDQUFnQnZILENBQWhCLENBQVAsRUFBMEJKLENBQUMsQ0FBQ3pILENBQUMsQ0FBQ29QLGFBQUgsQ0FBRCxJQUFvQixLQUFLNkIsaUJBQUwsRUFBOUMsRUFBdUVwSixDQUFDLElBQUk3SCxDQUFDLENBQUNxUCxLQUFQLEtBQWUsS0FBS2QsT0FBTCxDQUFhbkUsS0FBYixDQUFtQjdDLENBQUMsQ0FBQ3lKLFlBQXJCLElBQW1DLEVBQW5DLEVBQXNDLE9BQU9oUixDQUFDLENBQUNxUCxLQUFGLENBQVF4SCxDQUFSLENBQTVELENBQXZFLEVBQStJQSxDQUFDLElBQUk3SCxDQUFDLENBQUNzUCxLQUF6SixFQUErSjtBQUFDLFlBQUl4SCxDQUFDLEdBQUM5SCxDQUFDLENBQUNzUCxLQUFGLENBQVF6SCxDQUFSLENBQU47QUFBaUJDLFNBQUMsQ0FBQ2UsSUFBRixDQUFPLElBQVAsR0FBYSxPQUFPN0ksQ0FBQyxDQUFDc1AsS0FBRixDQUFRekgsQ0FBUixDQUFwQjtBQUErQjs7QUFBQSxXQUFLNEIsU0FBTCxDQUFlLGVBQWYsRUFBK0IsQ0FBQyxJQUFELENBQS9CO0FBQXVDO0FBQUMsR0FBelcsRUFBMFd4QixDQUFDLENBQUNnSixpQkFBRixHQUFvQixZQUFVO0FBQUMsU0FBS0Msc0JBQUwsSUFBOEIsS0FBSzNDLE9BQUwsQ0FBYTRDLG1CQUFiLENBQWlDdkosQ0FBakMsRUFBbUMsSUFBbkMsRUFBd0MsQ0FBQyxDQUF6QyxDQUE5QixFQUEwRSxLQUFLcUksZUFBTCxHQUFxQixDQUFDLENBQWhHO0FBQWtHLEdBQTNlLEVBQTRlaEksQ0FBQyxDQUFDd0ksYUFBRixHQUFnQixVQUFTbEosQ0FBVCxFQUFXO0FBQUMsUUFBSXZILENBQUMsR0FBQyxFQUFOOztBQUFTLFNBQUksSUFBSXlILENBQVIsSUFBYUYsQ0FBYjtBQUFldkgsT0FBQyxDQUFDeUgsQ0FBRCxDQUFELEdBQUssRUFBTDtBQUFmOztBQUF1QixTQUFLL0UsR0FBTCxDQUFTMUMsQ0FBVDtBQUFZLEdBQXBqQjtBQUFxakIsTUFBSXNMLENBQUMsR0FBQztBQUFDeUQsc0JBQWtCLEVBQUMsRUFBcEI7QUFBdUJELHNCQUFrQixFQUFDLEVBQTFDO0FBQTZDRSxtQkFBZSxFQUFDO0FBQTdELEdBQU47QUFBdUUsU0FBTy9HLENBQUMsQ0FBQ2lKLHNCQUFGLEdBQXlCLFlBQVU7QUFBQyxTQUFLeE8sR0FBTCxDQUFTNEksQ0FBVDtBQUFZLEdBQWhELEVBQWlEckQsQ0FBQyxDQUFDbUosT0FBRixHQUFVLFVBQVM3SixDQUFULEVBQVc7QUFBQ0EsS0FBQyxHQUFDb0MsS0FBSyxDQUFDcEMsQ0FBRCxDQUFMLEdBQVMsQ0FBVCxHQUFXQSxDQUFiLEVBQWUsS0FBS29KLFlBQUwsR0FBa0JwSixDQUFDLEdBQUMsSUFBbkM7QUFBd0MsR0FBL0csRUFBZ0hVLENBQUMsQ0FBQ29KLFVBQUYsR0FBYSxZQUFVO0FBQUMsU0FBSzlDLE9BQUwsQ0FBYWxCLFVBQWIsQ0FBd0J2QyxXQUF4QixDQUFvQyxLQUFLeUQsT0FBekMsR0FBa0QsS0FBSzdMLEdBQUwsQ0FBUztBQUFDdUksYUFBTyxFQUFDO0FBQVQsS0FBVCxDQUFsRCxFQUF5RSxLQUFLeEIsU0FBTCxDQUFlLFFBQWYsRUFBd0IsQ0FBQyxJQUFELENBQXhCLENBQXpFO0FBQXlHLEdBQWpQLEVBQWtQeEIsQ0FBQyxDQUFDYixNQUFGLEdBQVMsWUFBVTtBQUFDLFdBQU9ZLENBQUMsSUFBRTNELFVBQVUsQ0FBQyxLQUFLbUssTUFBTCxDQUFZOUYsT0FBWixDQUFvQm9HLGtCQUFyQixDQUFiLElBQXVELEtBQUt6RixJQUFMLENBQVUsZUFBVixFQUEwQixZQUFVO0FBQUMsV0FBS2dJLFVBQUw7QUFBa0IsS0FBdkQsR0FBeUQsS0FBSyxLQUFLeFEsSUFBTCxFQUFySCxJQUFrSSxLQUFLLEtBQUt3USxVQUFMLEVBQTlJO0FBQWdLLEdBQXRhLEVBQXVhcEosQ0FBQyxDQUFDcUosTUFBRixHQUFTLFlBQVU7QUFBQyxXQUFPLEtBQUtDLFFBQVosRUFBcUIsS0FBSzdPLEdBQUwsQ0FBUztBQUFDdUksYUFBTyxFQUFDO0FBQVQsS0FBVCxDQUFyQjtBQUE0QyxRQUFJMUQsQ0FBQyxHQUFDLEtBQUtpSCxNQUFMLENBQVk5RixPQUFsQjtBQUFBLFFBQTBCMUksQ0FBQyxHQUFDLEVBQTVCO0FBQUEsUUFBK0J5SCxDQUFDLEdBQUMsS0FBSytKLGtDQUFMLENBQXdDLGNBQXhDLENBQWpDO0FBQXlGeFIsS0FBQyxDQUFDeUgsQ0FBRCxDQUFELEdBQUssS0FBS2dLLHFCQUFWLEVBQWdDLEtBQUs5QyxVQUFMLENBQWdCO0FBQUM5TyxVQUFJLEVBQUMwSCxDQUFDLENBQUNtSyxXQUFSO0FBQW9CdkIsUUFBRSxFQUFDNUksQ0FBQyxDQUFDb0ssWUFBekI7QUFBc0N0QixnQkFBVSxFQUFDLENBQUMsQ0FBbEQ7QUFBb0RELHFCQUFlLEVBQUNwUTtBQUFwRSxLQUFoQixDQUFoQztBQUF3SCxHQUF4ckIsRUFBeXJCaUksQ0FBQyxDQUFDd0oscUJBQUYsR0FBd0IsWUFBVTtBQUFDLFNBQUtGLFFBQUwsSUFBZSxLQUFLOUgsU0FBTCxDQUFlLFFBQWYsQ0FBZjtBQUF3QyxHQUFwd0IsRUFBcXdCeEIsQ0FBQyxDQUFDdUosa0NBQUYsR0FBcUMsVUFBU2pLLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsS0FBS3dPLE1BQUwsQ0FBWTlGLE9BQVosQ0FBb0JuQixDQUFwQixDQUFOO0FBQTZCLFFBQUd2SCxDQUFDLENBQUM0UixPQUFMLEVBQWEsT0FBTSxTQUFOOztBQUFnQixTQUFJLElBQUluSyxDQUFSLElBQWF6SCxDQUFiO0FBQWUsYUFBT3lILENBQVA7QUFBZjtBQUF3QixHQUF4NEIsRUFBeTRCUSxDQUFDLENBQUNwSCxJQUFGLEdBQU8sWUFBVTtBQUFDLFNBQUswUSxRQUFMLEdBQWMsQ0FBQyxDQUFmLEVBQWlCLEtBQUs3TyxHQUFMLENBQVM7QUFBQ3VJLGFBQU8sRUFBQztBQUFULEtBQVQsQ0FBakI7QUFBd0MsUUFBSTFELENBQUMsR0FBQyxLQUFLaUgsTUFBTCxDQUFZOUYsT0FBbEI7QUFBQSxRQUEwQjFJLENBQUMsR0FBQyxFQUE1QjtBQUFBLFFBQStCeUgsQ0FBQyxHQUFDLEtBQUsrSixrQ0FBTCxDQUF3QyxhQUF4QyxDQUFqQztBQUF3RnhSLEtBQUMsQ0FBQ3lILENBQUQsQ0FBRCxHQUFLLEtBQUtvSyxtQkFBVixFQUE4QixLQUFLbEQsVUFBTCxDQUFnQjtBQUFDOU8sVUFBSSxFQUFDMEgsQ0FBQyxDQUFDb0ssWUFBUjtBQUFxQnhCLFFBQUUsRUFBQzVJLENBQUMsQ0FBQ21LLFdBQTFCO0FBQXNDckIsZ0JBQVUsRUFBQyxDQUFDLENBQWxEO0FBQW9ERCxxQkFBZSxFQUFDcFE7QUFBcEUsS0FBaEIsQ0FBOUI7QUFBc0gsR0FBanBDLEVBQWtwQ2lJLENBQUMsQ0FBQzRKLG1CQUFGLEdBQXNCLFlBQVU7QUFBQyxTQUFLTixRQUFMLEtBQWdCLEtBQUs3TyxHQUFMLENBQVM7QUFBQ3VJLGFBQU8sRUFBQztBQUFULEtBQVQsR0FBMkIsS0FBS3hCLFNBQUwsQ0FBZSxNQUFmLENBQTNDO0FBQW1FLEdBQXR2QyxFQUF1dkN4QixDQUFDLENBQUM2SixPQUFGLEdBQVUsWUFBVTtBQUFDLFNBQUtwUCxHQUFMLENBQVM7QUFBQytMLGNBQVEsRUFBQyxFQUFWO0FBQWFzRCxVQUFJLEVBQUMsRUFBbEI7QUFBcUJDLFdBQUssRUFBQyxFQUEzQjtBQUE4QkMsU0FBRyxFQUFDLEVBQWxDO0FBQXFDQyxZQUFNLEVBQUMsRUFBNUM7QUFBK0N2RCxnQkFBVSxFQUFDLEVBQTFEO0FBQTZEQyxlQUFTLEVBQUM7QUFBdkUsS0FBVDtBQUFxRixHQUFqMkMsRUFBazJDL0csQ0FBejJDO0FBQTIyQyxDQUFuaE0sQ0FBaDlNLEVBQXErWSxVQUFTTixDQUFULEVBQVd2SCxDQUFYLEVBQWE7QUFBQzs7QUFBYSxVQUFzQ3dILGlDQUEyQixDQUFDLDBCQUFELEVBQXlCLDBCQUF6QixFQUE2QywwQkFBN0MsRUFBb0UsMEJBQXBFLENBQXJCLGlDQUFtRyxVQUFTQyxDQUFULEVBQVdJLENBQVgsRUFBYUMsQ0FBYixFQUFlSixDQUFmLEVBQWlCO0FBQUMsV0FBTzFILENBQUMsQ0FBQ3VILENBQUQsRUFBR0UsQ0FBSCxFQUFLSSxDQUFMLEVBQU9DLENBQVAsRUFBU0osQ0FBVCxDQUFSO0FBQW9CLEdBQXpJLGdEQUE1QyxHQUF1TCxTQUF2TDtBQUE4WSxDQUF6YSxDQUEwYXZGLE1BQTFhLEVBQWliLFVBQVNvRixDQUFULEVBQVd2SCxDQUFYLEVBQWF5SCxDQUFiLEVBQWVJLENBQWYsRUFBaUJDLENBQWpCLEVBQW1CO0FBQUM7O0FBQWEsV0FBU0osQ0FBVCxDQUFXSCxDQUFYLEVBQWF2SCxDQUFiLEVBQWU7QUFBQyxRQUFJeUgsQ0FBQyxHQUFDSSxDQUFDLENBQUN5RixlQUFGLENBQWtCL0YsQ0FBbEIsQ0FBTjtBQUEyQixRQUFHLENBQUNFLENBQUosRUFBTSxPQUFPLE1BQUtHLENBQUMsSUFBRUEsQ0FBQyxDQUFDakIsS0FBRixDQUFRLHFCQUFtQixLQUFLdUksV0FBTCxDQUFpQmlELFNBQXBDLEdBQThDLElBQTlDLElBQW9EMUssQ0FBQyxJQUFFRixDQUF2RCxDQUFSLENBQVIsQ0FBUDtBQUFtRixTQUFLZ0gsT0FBTCxHQUFhOUcsQ0FBYixFQUFlTSxDQUFDLEtBQUcsS0FBS2xELFFBQUwsR0FBY2tELENBQUMsQ0FBQyxLQUFLd0csT0FBTixDQUFsQixDQUFoQixFQUFrRCxLQUFLN0YsT0FBTCxHQUFhYixDQUFDLENBQUNjLE1BQUYsQ0FBUyxFQUFULEVBQVksS0FBS3VHLFdBQUwsQ0FBaUJrRCxRQUE3QixDQUEvRCxFQUFzRyxLQUFLL0osTUFBTCxDQUFZckksQ0FBWixDQUF0RztBQUFxSCxRQUFJOEgsQ0FBQyxHQUFDLEVBQUVLLENBQVI7QUFBVSxTQUFLb0csT0FBTCxDQUFhOEQsWUFBYixHQUEwQnZLLENBQTFCLEVBQTRCdUQsQ0FBQyxDQUFDdkQsQ0FBRCxDQUFELEdBQUssSUFBakMsRUFBc0MsS0FBSzRHLE9BQUwsRUFBdEM7O0FBQXFELFFBQUloSCxDQUFDLEdBQUMsS0FBS2dJLFVBQUwsQ0FBZ0IsWUFBaEIsQ0FBTjs7QUFBb0NoSSxLQUFDLElBQUUsS0FBSzhHLE1BQUwsRUFBSDtBQUFpQjs7QUFBQSxXQUFTeEcsQ0FBVCxDQUFXVCxDQUFYLEVBQWE7QUFBQyxhQUFTdkgsQ0FBVCxHQUFZO0FBQUN1SCxPQUFDLENBQUNhLEtBQUYsQ0FBUSxJQUFSLEVBQWFVLFNBQWI7QUFBd0I7O0FBQUEsV0FBTzlJLENBQUMsQ0FBQ3dJLFNBQUYsR0FBWWhELE1BQU0sQ0FBQ3lKLE1BQVAsQ0FBYzFILENBQUMsQ0FBQ2lCLFNBQWhCLENBQVosRUFBdUN4SSxDQUFDLENBQUN3SSxTQUFGLENBQVkwRyxXQUFaLEdBQXdCbFAsQ0FBL0QsRUFBaUVBLENBQXhFO0FBQTBFOztBQUFBLFdBQVMySCxDQUFULENBQVdKLENBQVgsRUFBYTtBQUFDLFFBQUcsWUFBVSxPQUFPQSxDQUFwQixFQUFzQixPQUFPQSxDQUFQO0FBQVMsUUFBSXZILENBQUMsR0FBQ3VILENBQUMsQ0FBQytLLEtBQUYsQ0FBUSxtQkFBUixDQUFOO0FBQUEsUUFBbUM3SyxDQUFDLEdBQUN6SCxDQUFDLElBQUVBLENBQUMsQ0FBQyxDQUFELENBQXpDO0FBQUEsUUFBNkM2SCxDQUFDLEdBQUM3SCxDQUFDLElBQUVBLENBQUMsQ0FBQyxDQUFELENBQW5EO0FBQXVELFFBQUcsQ0FBQ3lILENBQUMsQ0FBQzVELE1BQU4sRUFBYSxPQUFPLENBQVA7QUFBUzRELEtBQUMsR0FBQ3BELFVBQVUsQ0FBQ29ELENBQUQsQ0FBWjtBQUFnQixRQUFJSyxDQUFDLEdBQUN5RCxDQUFDLENBQUMxRCxDQUFELENBQUQsSUFBTSxDQUFaO0FBQWMsV0FBT0osQ0FBQyxHQUFDSyxDQUFUO0FBQVc7O0FBQUEsTUFBSUYsQ0FBQyxHQUFDTCxDQUFDLENBQUM5SixPQUFSO0FBQUEsTUFBZ0JzSyxDQUFDLEdBQUNSLENBQUMsQ0FBQ2dCLE1BQXBCO0FBQUEsTUFBMkJOLENBQUMsR0FBQyxTQUFGQSxDQUFFLEdBQVUsQ0FBRSxDQUF6QztBQUFBLE1BQTBDRSxDQUFDLEdBQUMsQ0FBNUM7QUFBQSxNQUE4Q2tELENBQUMsR0FBQyxFQUFoRDs7QUFBbUQzRCxHQUFDLENBQUN5SyxTQUFGLEdBQVksVUFBWixFQUF1QnpLLENBQUMsQ0FBQzZLLElBQUYsR0FBT3pLLENBQTlCLEVBQWdDSixDQUFDLENBQUMwSyxRQUFGLEdBQVc7QUFBQ0ksa0JBQWMsRUFBQztBQUFDL0QsY0FBUSxFQUFDO0FBQVYsS0FBaEI7QUFBc0NnRSxjQUFVLEVBQUMsQ0FBQyxDQUFsRDtBQUFvREMsY0FBVSxFQUFDLENBQUMsQ0FBaEU7QUFBa0VDLGFBQVMsRUFBQyxDQUFDLENBQTdFO0FBQStFQyxVQUFNLEVBQUMsQ0FBQyxDQUF2RjtBQUF5RkMsbUJBQWUsRUFBQyxDQUFDLENBQTFHO0FBQTRHL0Qsc0JBQWtCLEVBQUMsTUFBL0g7QUFBc0k0QyxlQUFXLEVBQUM7QUFBQ0UsYUFBTyxFQUFDLENBQVQ7QUFBV2hELGVBQVMsRUFBQztBQUFyQixLQUFsSjtBQUF1TCtDLGdCQUFZLEVBQUM7QUFBQ0MsYUFBTyxFQUFDLENBQVQ7QUFBV2hELGVBQVMsRUFBQztBQUFyQjtBQUFwTSxHQUEzQztBQUFpUixNQUFJdEQsQ0FBQyxHQUFDNUQsQ0FBQyxDQUFDYyxTQUFSO0FBQWtCWCxHQUFDLENBQUNjLE1BQUYsQ0FBUzJDLENBQVQsRUFBV3RMLENBQUMsQ0FBQ3dJLFNBQWIsR0FBd0I4QyxDQUFDLENBQUNqRCxNQUFGLEdBQVMsVUFBU2QsQ0FBVCxFQUFXO0FBQUNNLEtBQUMsQ0FBQ2MsTUFBRixDQUFTLEtBQUtELE9BQWQsRUFBc0JuQixDQUF0QjtBQUF5QixHQUF0RSxFQUF1RStELENBQUMsQ0FBQ29FLFVBQUYsR0FBYSxVQUFTbkksQ0FBVCxFQUFXO0FBQUMsUUFBSXZILENBQUMsR0FBQyxLQUFLa1AsV0FBTCxDQUFpQjRELGFBQWpCLENBQStCdkwsQ0FBL0IsQ0FBTjtBQUF3QyxXQUFPdkgsQ0FBQyxJQUFFLEtBQUssQ0FBTCxLQUFTLEtBQUswSSxPQUFMLENBQWExSSxDQUFiLENBQVosR0FBNEIsS0FBSzBJLE9BQUwsQ0FBYTFJLENBQWIsQ0FBNUIsR0FBNEMsS0FBSzBJLE9BQUwsQ0FBYW5CLENBQWIsQ0FBbkQ7QUFBbUUsR0FBM00sRUFBNE1HLENBQUMsQ0FBQ29MLGFBQUYsR0FBZ0I7QUFBQ0wsY0FBVSxFQUFDLGNBQVo7QUFBMkJNLGNBQVUsRUFBQyxjQUF0QztBQUFxREMsaUJBQWEsRUFBQyxpQkFBbkU7QUFBcUZOLGNBQVUsRUFBQyxjQUFoRztBQUErR0MsYUFBUyxFQUFDLGFBQXpIO0FBQXVJQyxVQUFNLEVBQUMsZUFBOUk7QUFBOEpDLG1CQUFlLEVBQUM7QUFBOUssR0FBNU4sRUFBaWF2SCxDQUFDLENBQUNvRCxPQUFGLEdBQVUsWUFBVTtBQUFDLFNBQUt1RSxXQUFMLElBQW1CLEtBQUtDLE1BQUwsR0FBWSxFQUEvQixFQUFrQyxLQUFLQyxLQUFMLENBQVcsS0FBS3pLLE9BQUwsQ0FBYXlLLEtBQXhCLENBQWxDLEVBQWlFdEwsQ0FBQyxDQUFDYyxNQUFGLENBQVMsS0FBSzRGLE9BQUwsQ0FBYW5FLEtBQXRCLEVBQTRCLEtBQUsxQixPQUFMLENBQWE4SixjQUF6QyxDQUFqRTs7QUFBMEgsUUFBSWpMLENBQUMsR0FBQyxLQUFLbUksVUFBTCxDQUFnQixRQUFoQixDQUFOOztBQUFnQ25JLEtBQUMsSUFBRSxLQUFLNkwsVUFBTCxFQUFIO0FBQXFCLEdBQXJtQixFQUFzbUI5SCxDQUFDLENBQUMySCxXQUFGLEdBQWMsWUFBVTtBQUFDLFNBQUt4UixLQUFMLEdBQVcsS0FBSzRSLFFBQUwsQ0FBYyxLQUFLOUUsT0FBTCxDQUFhK0UsUUFBM0IsQ0FBWDtBQUFnRCxHQUEvcUIsRUFBZ3JCaEksQ0FBQyxDQUFDK0gsUUFBRixHQUFXLFVBQVM5TCxDQUFULEVBQVc7QUFBQyxTQUFJLElBQUl2SCxDQUFDLEdBQUMsS0FBS3VULHVCQUFMLENBQTZCaE0sQ0FBN0IsQ0FBTixFQUFzQ0UsQ0FBQyxHQUFDLEtBQUt5SCxXQUFMLENBQWlCcUQsSUFBekQsRUFBOEQxSyxDQUFDLEdBQUMsRUFBaEUsRUFBbUVDLENBQUMsR0FBQyxDQUF6RSxFQUEyRUEsQ0FBQyxHQUFDOUgsQ0FBQyxDQUFDNkQsTUFBL0UsRUFBc0ZpRSxDQUFDLEVBQXZGLEVBQTBGO0FBQUMsVUFBSUosQ0FBQyxHQUFDMUgsQ0FBQyxDQUFDOEgsQ0FBRCxDQUFQO0FBQUEsVUFBV0UsQ0FBQyxHQUFDLElBQUlQLENBQUosQ0FBTUMsQ0FBTixFQUFRLElBQVIsQ0FBYjtBQUEyQkcsT0FBQyxDQUFDdUIsSUFBRixDQUFPcEIsQ0FBUDtBQUFVOztBQUFBLFdBQU9ILENBQVA7QUFBUyxHQUFoMUIsRUFBaTFCeUQsQ0FBQyxDQUFDaUksdUJBQUYsR0FBMEIsVUFBU2hNLENBQVQsRUFBVztBQUFDLFdBQU9NLENBQUMsQ0FBQzJGLGtCQUFGLENBQXFCakcsQ0FBckIsRUFBdUIsS0FBS21CLE9BQUwsQ0FBYTFGLFlBQXBDLENBQVA7QUFBeUQsR0FBaDdCLEVBQWk3QnNJLENBQUMsQ0FBQ2tJLGVBQUYsR0FBa0IsWUFBVTtBQUFDLFdBQU8sS0FBSy9SLEtBQUwsQ0FBV2dTLEdBQVgsQ0FBZSxVQUFTbE0sQ0FBVCxFQUFXO0FBQUMsYUFBT0EsQ0FBQyxDQUFDZ0gsT0FBVDtBQUFpQixLQUE1QyxDQUFQO0FBQXFELEdBQW5nQyxFQUFvZ0NqRCxDQUFDLENBQUNrRCxNQUFGLEdBQVMsWUFBVTtBQUFDLFNBQUtrRixZQUFMLElBQW9CLEtBQUtDLGFBQUwsRUFBcEI7O0FBQXlDLFFBQUlwTSxDQUFDLEdBQUMsS0FBS21JLFVBQUwsQ0FBZ0IsZUFBaEIsQ0FBTjtBQUFBLFFBQXVDMVAsQ0FBQyxHQUFDLEtBQUssQ0FBTCxLQUFTdUgsQ0FBVCxHQUFXQSxDQUFYLEdBQWEsQ0FBQyxLQUFLcU0sZUFBNUQ7O0FBQTRFLFNBQUtDLFdBQUwsQ0FBaUIsS0FBS3BTLEtBQXRCLEVBQTRCekIsQ0FBNUIsR0FBK0IsS0FBSzRULGVBQUwsR0FBcUIsQ0FBQyxDQUFyRDtBQUF1RCxHQUFwc0MsRUFBcXNDdEksQ0FBQyxDQUFDaEQsS0FBRixHQUFRZ0QsQ0FBQyxDQUFDa0QsTUFBL3NDLEVBQXN0Q2xELENBQUMsQ0FBQ29JLFlBQUYsR0FBZSxZQUFVO0FBQUMsU0FBS25FLE9BQUw7QUFBZSxHQUEvdkMsRUFBZ3dDakUsQ0FBQyxDQUFDaUUsT0FBRixHQUFVLFlBQVU7QUFBQyxTQUFLQyxJQUFMLEdBQVUvSCxDQUFDLENBQUMsS0FBSzhHLE9BQU4sQ0FBWDtBQUEwQixHQUEveUMsRUFBZ3pDakQsQ0FBQyxDQUFDd0ksZUFBRixHQUFrQixVQUFTdk0sQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsUUFBSTZILENBQUo7QUFBQSxRQUFNQyxDQUFDLEdBQUMsS0FBS1ksT0FBTCxDQUFhbkIsQ0FBYixDQUFSO0FBQXdCTyxLQUFDLElBQUUsWUFBVSxPQUFPQSxDQUFqQixHQUFtQkQsQ0FBQyxHQUFDLEtBQUswRyxPQUFMLENBQWF4RCxhQUFiLENBQTJCakQsQ0FBM0IsQ0FBckIsR0FBbURBLENBQUMsWUFBWTRGLFdBQWIsS0FBMkI3RixDQUFDLEdBQUNDLENBQTdCLENBQW5ELEVBQW1GLEtBQUtQLENBQUwsSUFBUU0sQ0FBQyxHQUFDSixDQUFDLENBQUNJLENBQUQsQ0FBRCxDQUFLN0gsQ0FBTCxDQUFELEdBQVM4SCxDQUF2RyxJQUEwRyxLQUFLUCxDQUFMLElBQVEsQ0FBbkg7QUFBcUgsR0FBNzlDLEVBQTg5QytELENBQUMsQ0FBQ3VJLFdBQUYsR0FBYyxVQUFTdE0sQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUN1SCxLQUFDLEdBQUMsS0FBS3dNLGtCQUFMLENBQXdCeE0sQ0FBeEIsQ0FBRixFQUE2QixLQUFLeU0sWUFBTCxDQUFrQnpNLENBQWxCLEVBQW9CdkgsQ0FBcEIsQ0FBN0IsRUFBb0QsS0FBS2lVLFdBQUwsRUFBcEQ7QUFBdUUsR0FBamtELEVBQWtrRDNJLENBQUMsQ0FBQ3lJLGtCQUFGLEdBQXFCLFVBQVN4TSxDQUFULEVBQVc7QUFBQyxXQUFPQSxDQUFDLENBQUMyTSxNQUFGLENBQVMsVUFBUzNNLENBQVQsRUFBVztBQUFDLGFBQU0sQ0FBQ0EsQ0FBQyxDQUFDNE0sU0FBVDtBQUFtQixLQUF4QyxDQUFQO0FBQWlELEdBQXBwRCxFQUFxcEQ3SSxDQUFDLENBQUMwSSxZQUFGLEdBQWUsVUFBU3pNLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFFBQUcsS0FBS29VLG9CQUFMLENBQTBCLFFBQTFCLEVBQW1DN00sQ0FBbkMsR0FBc0NBLENBQUMsSUFBRUEsQ0FBQyxDQUFDMUQsTUFBOUMsRUFBcUQ7QUFBQyxVQUFJNEQsQ0FBQyxHQUFDLEVBQU47QUFBU0YsT0FBQyxDQUFDa0csT0FBRixDQUFVLFVBQVNsRyxDQUFULEVBQVc7QUFBQyxZQUFJTSxDQUFDLEdBQUMsS0FBS3dNLHNCQUFMLENBQTRCOU0sQ0FBNUIsQ0FBTjs7QUFBcUNNLFNBQUMsQ0FBQ3lNLElBQUYsR0FBTy9NLENBQVAsRUFBU00sQ0FBQyxDQUFDME0sU0FBRixHQUFZdlUsQ0FBQyxJQUFFdUgsQ0FBQyxDQUFDaU4sZUFBMUIsRUFBMEMvTSxDQUFDLENBQUMyQixJQUFGLENBQU92QixDQUFQLENBQTFDO0FBQW9ELE9BQS9HLEVBQWdILElBQWhILEdBQXNILEtBQUs0TSxtQkFBTCxDQUF5QmhOLENBQXpCLENBQXRIO0FBQWtKO0FBQUMsR0FBcDRELEVBQXE0RDZELENBQUMsQ0FBQytJLHNCQUFGLEdBQXlCLFlBQVU7QUFBQyxXQUFNO0FBQUN6SCxPQUFDLEVBQUMsQ0FBSDtBQUFLZCxPQUFDLEVBQUM7QUFBUCxLQUFOO0FBQWdCLEdBQXo3RCxFQUEwN0RSLENBQUMsQ0FBQ21KLG1CQUFGLEdBQXNCLFVBQVNsTixDQUFULEVBQVc7QUFBQyxTQUFLbU4sYUFBTCxJQUFxQm5OLENBQUMsQ0FBQ2tHLE9BQUYsQ0FBVSxVQUFTbEcsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsV0FBSzJVLGFBQUwsQ0FBbUJwTixDQUFDLENBQUMrTSxJQUFyQixFQUEwQi9NLENBQUMsQ0FBQ3FGLENBQTVCLEVBQThCckYsQ0FBQyxDQUFDdUUsQ0FBaEMsRUFBa0N2RSxDQUFDLENBQUNnTixTQUFwQyxFQUE4Q3ZVLENBQTlDO0FBQWlELEtBQXpFLEVBQTBFLElBQTFFLENBQXJCO0FBQXFHLEdBQWprRSxFQUFra0VzTCxDQUFDLENBQUNvSixhQUFGLEdBQWdCLFlBQVU7QUFBQyxRQUFJbk4sQ0FBQyxHQUFDLEtBQUttQixPQUFMLENBQWEwSSxPQUFuQjtBQUEyQixXQUFPLFNBQU83SixDQUFQLElBQVUsS0FBSyxDQUFMLEtBQVNBLENBQW5CLEdBQXFCLE1BQUssS0FBSzZKLE9BQUwsR0FBYSxDQUFsQixDQUFyQixJQUEyQyxLQUFLQSxPQUFMLEdBQWF6SixDQUFDLENBQUNKLENBQUQsQ0FBZCxFQUFrQixLQUFLNkosT0FBbEUsQ0FBUDtBQUFrRixHQUExc0UsRUFBMnNFOUYsQ0FBQyxDQUFDcUosYUFBRixHQUFnQixVQUFTcE4sQ0FBVCxFQUFXdkgsQ0FBWCxFQUFheUgsQ0FBYixFQUFlSSxDQUFmLEVBQWlCQyxDQUFqQixFQUFtQjtBQUFDRCxLQUFDLEdBQUNOLENBQUMsQ0FBQytJLElBQUYsQ0FBT3RRLENBQVAsRUFBU3lILENBQVQsQ0FBRCxJQUFjRixDQUFDLENBQUM2SixPQUFGLENBQVV0SixDQUFDLEdBQUMsS0FBS3NKLE9BQWpCLEdBQTBCN0osQ0FBQyxDQUFDZ0osTUFBRixDQUFTdlEsQ0FBVCxFQUFXeUgsQ0FBWCxDQUF4QyxDQUFEO0FBQXdELEdBQXZ5RSxFQUF3eUU2RCxDQUFDLENBQUMySSxXQUFGLEdBQWMsWUFBVTtBQUFDLFNBQUtwQixlQUFMO0FBQXVCLEdBQXgxRSxFQUF5MUV2SCxDQUFDLENBQUN1SCxlQUFGLEdBQWtCLFlBQVU7QUFBQyxRQUFJdEwsQ0FBQyxHQUFDLEtBQUttSSxVQUFMLENBQWdCLGlCQUFoQixDQUFOOztBQUF5QyxRQUFHbkksQ0FBSCxFQUFLO0FBQUMsVUFBSXZILENBQUMsR0FBQyxLQUFLNFUsaUJBQUwsRUFBTjs7QUFBK0I1VSxPQUFDLEtBQUcsS0FBSzZVLG9CQUFMLENBQTBCN1UsQ0FBQyxDQUFDNEosS0FBNUIsRUFBa0MsQ0FBQyxDQUFuQyxHQUFzQyxLQUFLaUwsb0JBQUwsQ0FBMEI3VSxDQUFDLENBQUM2SixNQUE1QixFQUFtQyxDQUFDLENBQXBDLENBQXpDLENBQUQ7QUFBa0Y7QUFBQyxHQUF2aEYsRUFBd2hGeUIsQ0FBQyxDQUFDc0osaUJBQUYsR0FBb0IzTSxDQUE1aUYsRUFBOGlGcUQsQ0FBQyxDQUFDdUosb0JBQUYsR0FBdUIsVUFBU3ROLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFFBQUcsS0FBSyxDQUFMLEtBQVN1SCxDQUFaLEVBQWM7QUFBQyxVQUFJRSxDQUFDLEdBQUMsS0FBSytILElBQVg7QUFBZ0IvSCxPQUFDLENBQUMyRCxXQUFGLEtBQWdCN0QsQ0FBQyxJQUFFdkgsQ0FBQyxHQUFDeUgsQ0FBQyxDQUFDZ0UsV0FBRixHQUFjaEUsQ0FBQyxDQUFDaUUsWUFBaEIsR0FBNkJqRSxDQUFDLENBQUM0RSxlQUEvQixHQUErQzVFLENBQUMsQ0FBQzZFLGdCQUFsRCxHQUFtRTdFLENBQUMsQ0FBQ29FLGFBQUYsR0FBZ0JwRSxDQUFDLENBQUNtRSxVQUFsQixHQUE2Qm5FLENBQUMsQ0FBQytFLGNBQS9CLEdBQThDL0UsQ0FBQyxDQUFDZ0YsaUJBQXZJLEdBQTBKbEYsQ0FBQyxHQUFDb0QsSUFBSSxDQUFDL0ssR0FBTCxDQUFTMkgsQ0FBVCxFQUFXLENBQVgsQ0FBNUosRUFBMEssS0FBS2dILE9BQUwsQ0FBYW5FLEtBQWIsQ0FBbUJwSyxDQUFDLEdBQUMsT0FBRCxHQUFTLFFBQTdCLElBQXVDdUgsQ0FBQyxHQUFDLElBQW5OO0FBQXdOO0FBQUMsR0FBMzBGLEVBQTQwRitELENBQUMsQ0FBQzhJLG9CQUFGLEdBQXVCLFVBQVM3TSxDQUFULEVBQVd2SCxDQUFYLEVBQWE7QUFBQyxhQUFTeUgsQ0FBVCxHQUFZO0FBQUNLLE9BQUMsQ0FBQ2dOLGFBQUYsQ0FBZ0J2TixDQUFDLEdBQUMsVUFBbEIsRUFBNkIsSUFBN0IsRUFBa0MsQ0FBQ3ZILENBQUQsQ0FBbEM7QUFBdUM7O0FBQUEsYUFBUzZILENBQVQsR0FBWTtBQUFDRyxPQUFDLElBQUdBLENBQUMsSUFBRU4sQ0FBSCxJQUFNRCxDQUFDLEVBQVg7QUFBYzs7QUFBQSxRQUFJSyxDQUFDLEdBQUMsSUFBTjtBQUFBLFFBQVdKLENBQUMsR0FBQzFILENBQUMsQ0FBQzZELE1BQWY7QUFBc0IsUUFBRyxDQUFDN0QsQ0FBRCxJQUFJLENBQUMwSCxDQUFSLEVBQVUsT0FBTyxLQUFLRCxDQUFDLEVBQWI7QUFBZ0IsUUFBSU8sQ0FBQyxHQUFDLENBQU47QUFBUWhJLEtBQUMsQ0FBQ3lOLE9BQUYsQ0FBVSxVQUFTek4sQ0FBVCxFQUFXO0FBQUNBLE9BQUMsQ0FBQ3FKLElBQUYsQ0FBTzlCLENBQVAsRUFBU00sQ0FBVDtBQUFZLEtBQWxDO0FBQW9DLEdBQTVoRyxFQUE2aEd5RCxDQUFDLENBQUN3SixhQUFGLEdBQWdCLFVBQVN2TixDQUFULEVBQVd2SCxDQUFYLEVBQWF5SCxDQUFiLEVBQWU7QUFBQyxRQUFJSSxDQUFDLEdBQUM3SCxDQUFDLEdBQUMsQ0FBQ0EsQ0FBRCxFQUFJbU8sTUFBSixDQUFXMUcsQ0FBWCxDQUFELEdBQWVBLENBQXRCO0FBQXdCLFFBQUcsS0FBS2dDLFNBQUwsQ0FBZWxDLENBQWYsRUFBaUJNLENBQWpCLEdBQW9CRSxDQUF2QixFQUF5QixJQUFHLEtBQUtsRCxRQUFMLEdBQWMsS0FBS0EsUUFBTCxJQUFla0QsQ0FBQyxDQUFDLEtBQUt3RyxPQUFOLENBQTlCLEVBQTZDdk8sQ0FBaEQsRUFBa0Q7QUFBQyxVQUFJOEgsQ0FBQyxHQUFDQyxDQUFDLENBQUNnTixLQUFGLENBQVEvVSxDQUFSLENBQU47QUFBaUI4SCxPQUFDLENBQUNwRyxJQUFGLEdBQU82RixDQUFQLEVBQVMsS0FBSzFDLFFBQUwsQ0FBY0wsT0FBZCxDQUFzQnNELENBQXRCLEVBQXdCTCxDQUF4QixDQUFUO0FBQW9DLEtBQXhHLE1BQTZHLEtBQUs1QyxRQUFMLENBQWNMLE9BQWQsQ0FBc0IrQyxDQUF0QixFQUF3QkUsQ0FBeEI7QUFBMkIsR0FBdHZHLEVBQXV2RzZELENBQUMsQ0FBQzBKLE1BQUYsR0FBUyxVQUFTek4sQ0FBVCxFQUFXO0FBQUMsUUFBSXZILENBQUMsR0FBQyxLQUFLaVYsT0FBTCxDQUFhMU4sQ0FBYixDQUFOO0FBQXNCdkgsS0FBQyxLQUFHQSxDQUFDLENBQUNtVSxTQUFGLEdBQVksQ0FBQyxDQUFoQixDQUFEO0FBQW9CLEdBQXR6RyxFQUF1ekc3SSxDQUFDLENBQUM0SixRQUFGLEdBQVcsVUFBUzNOLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsS0FBS2lWLE9BQUwsQ0FBYTFOLENBQWIsQ0FBTjtBQUFzQnZILEtBQUMsSUFBRSxPQUFPQSxDQUFDLENBQUNtVSxTQUFaO0FBQXNCLEdBQTEzRyxFQUEyM0c3SSxDQUFDLENBQUM2SCxLQUFGLEdBQVEsVUFBUzVMLENBQVQsRUFBVztBQUFDQSxLQUFDLEdBQUMsS0FBSzROLEtBQUwsQ0FBVzVOLENBQVgsQ0FBRixFQUFnQkEsQ0FBQyxLQUFHLEtBQUsyTCxNQUFMLEdBQVksS0FBS0EsTUFBTCxDQUFZL0UsTUFBWixDQUFtQjVHLENBQW5CLENBQVosRUFBa0NBLENBQUMsQ0FBQ2tHLE9BQUYsQ0FBVSxLQUFLdUgsTUFBZixFQUFzQixJQUF0QixDQUFyQyxDQUFqQjtBQUFtRixHQUFsK0csRUFBbStHMUosQ0FBQyxDQUFDOEosT0FBRixHQUFVLFVBQVM3TixDQUFULEVBQVc7QUFBQ0EsS0FBQyxHQUFDLEtBQUs0TixLQUFMLENBQVc1TixDQUFYLENBQUYsRUFBZ0JBLENBQUMsSUFBRUEsQ0FBQyxDQUFDa0csT0FBRixDQUFVLFVBQVNsRyxDQUFULEVBQVc7QUFBQ00sT0FBQyxDQUFDc0YsVUFBRixDQUFhLEtBQUsrRixNQUFsQixFQUF5QjNMLENBQXpCLEdBQTRCLEtBQUsyTixRQUFMLENBQWMzTixDQUFkLENBQTVCO0FBQTZDLEtBQW5FLEVBQW9FLElBQXBFLENBQW5CO0FBQTZGLEdBQXRsSCxFQUF1bEgrRCxDQUFDLENBQUM2SixLQUFGLEdBQVEsVUFBUzVOLENBQVQsRUFBVztBQUFDLFdBQU9BLENBQUMsSUFBRSxZQUFVLE9BQU9BLENBQWpCLEtBQXFCQSxDQUFDLEdBQUMsS0FBS2dILE9BQUwsQ0FBYVosZ0JBQWIsQ0FBOEJwRyxDQUE5QixDQUF2QixHQUF5REEsQ0FBQyxHQUFDTSxDQUFDLENBQUNvRixTQUFGLENBQVkxRixDQUFaLENBQTdELElBQTZFLEtBQUssQ0FBMUY7QUFBNEYsR0FBdnNILEVBQXdzSCtELENBQUMsQ0FBQ3FJLGFBQUYsR0FBZ0IsWUFBVTtBQUFDLFNBQUtULE1BQUwsSUFBYSxLQUFLQSxNQUFMLENBQVlyUCxNQUF6QixLQUFrQyxLQUFLd1IsZ0JBQUwsSUFBd0IsS0FBS25DLE1BQUwsQ0FBWXpGLE9BQVosQ0FBb0IsS0FBSzZILFlBQXpCLEVBQXNDLElBQXRDLENBQTFEO0FBQXVHLEdBQTEwSCxFQUEyMEhoSyxDQUFDLENBQUMrSixnQkFBRixHQUFtQixZQUFVO0FBQUMsUUFBSTlOLENBQUMsR0FBQyxLQUFLZ0gsT0FBTCxDQUFhZ0gscUJBQWIsRUFBTjtBQUFBLFFBQTJDdlYsQ0FBQyxHQUFDLEtBQUt3UCxJQUFsRDtBQUF1RCxTQUFLZ0csYUFBTCxHQUFtQjtBQUFDekQsVUFBSSxFQUFDeEssQ0FBQyxDQUFDd0ssSUFBRixHQUFPL1IsQ0FBQyxDQUFDeUwsV0FBVCxHQUFxQnpMLENBQUMsQ0FBQ3FNLGVBQTdCO0FBQTZDNEYsU0FBRyxFQUFDMUssQ0FBQyxDQUFDMEssR0FBRixHQUFNalMsQ0FBQyxDQUFDNEwsVUFBUixHQUFtQjVMLENBQUMsQ0FBQ3dNLGNBQXRFO0FBQXFGd0YsV0FBSyxFQUFDekssQ0FBQyxDQUFDeUssS0FBRixJQUFTaFMsQ0FBQyxDQUFDMEwsWUFBRixHQUFlMUwsQ0FBQyxDQUFDc00sZ0JBQTFCLENBQTNGO0FBQXVJNEYsWUFBTSxFQUFDM0ssQ0FBQyxDQUFDMkssTUFBRixJQUFVbFMsQ0FBQyxDQUFDNkwsYUFBRixHQUFnQjdMLENBQUMsQ0FBQ3lNLGlCQUE1QjtBQUE5SSxLQUFuQjtBQUFpTixHQUFqbkksRUFBa25JbkIsQ0FBQyxDQUFDZ0ssWUFBRixHQUFlck4sQ0FBam9JLEVBQW1vSXFELENBQUMsQ0FBQ21LLGlCQUFGLEdBQW9CLFVBQVNsTyxDQUFULEVBQVc7QUFBQyxRQUFJdkgsQ0FBQyxHQUFDdUgsQ0FBQyxDQUFDZ08scUJBQUYsRUFBTjtBQUFBLFFBQWdDMU4sQ0FBQyxHQUFDLEtBQUsyTixhQUF2QztBQUFBLFFBQXFEMU4sQ0FBQyxHQUFDTCxDQUFDLENBQUNGLENBQUQsQ0FBeEQ7QUFBQSxRQUE0REcsQ0FBQyxHQUFDO0FBQUNxSyxVQUFJLEVBQUMvUixDQUFDLENBQUMrUixJQUFGLEdBQU9sSyxDQUFDLENBQUNrSyxJQUFULEdBQWNqSyxDQUFDLENBQUNpRSxVQUF0QjtBQUFpQ2tHLFNBQUcsRUFBQ2pTLENBQUMsQ0FBQ2lTLEdBQUYsR0FBTXBLLENBQUMsQ0FBQ29LLEdBQVIsR0FBWW5LLENBQUMsQ0FBQ29FLFNBQW5EO0FBQTZEOEYsV0FBSyxFQUFDbkssQ0FBQyxDQUFDbUssS0FBRixHQUFRaFMsQ0FBQyxDQUFDZ1MsS0FBVixHQUFnQmxLLENBQUMsQ0FBQ2tFLFdBQXJGO0FBQWlHa0csWUFBTSxFQUFDckssQ0FBQyxDQUFDcUssTUFBRixHQUFTbFMsQ0FBQyxDQUFDa1MsTUFBWCxHQUFrQnBLLENBQUMsQ0FBQ3FFO0FBQTVILEtBQTlEO0FBQXdNLFdBQU96RSxDQUFQO0FBQVMsR0FBcDNJLEVBQXEzSTRELENBQUMsQ0FBQ2lDLFdBQUYsR0FBYzFGLENBQUMsQ0FBQzBGLFdBQXI0SSxFQUFpNUlqQyxDQUFDLENBQUM4SCxVQUFGLEdBQWEsWUFBVTtBQUFDN0wsS0FBQyxDQUFDbkYsZ0JBQUYsQ0FBbUIsUUFBbkIsRUFBNEIsSUFBNUIsR0FBa0MsS0FBS3NULGFBQUwsR0FBbUIsQ0FBQyxDQUF0RDtBQUF3RCxHQUFqK0ksRUFBaytJcEssQ0FBQyxDQUFDcUssWUFBRixHQUFlLFlBQVU7QUFBQ3BPLEtBQUMsQ0FBQzRKLG1CQUFGLENBQXNCLFFBQXRCLEVBQStCLElBQS9CLEdBQXFDLEtBQUt1RSxhQUFMLEdBQW1CLENBQUMsQ0FBekQ7QUFBMkQsR0FBdmpKLEVBQXdqSnBLLENBQUMsQ0FBQ3NLLFFBQUYsR0FBVyxZQUFVO0FBQUMsU0FBS2hELE1BQUw7QUFBYyxHQUE1bEosRUFBNmxKL0ssQ0FBQyxDQUFDK0YsY0FBRixDQUFpQmxHLENBQWpCLEVBQW1CLFVBQW5CLEVBQThCLEdBQTlCLENBQTdsSixFQUFnb0o0RCxDQUFDLENBQUNzSCxNQUFGLEdBQVMsWUFBVTtBQUFDLFNBQUs4QyxhQUFMLElBQW9CLEtBQUtHLGlCQUFMLEVBQXBCLElBQThDLEtBQUtySCxNQUFMLEVBQTlDO0FBQTRELEdBQWh0SixFQUFpdEpsRCxDQUFDLENBQUN1SyxpQkFBRixHQUFvQixZQUFVO0FBQUMsUUFBSXRPLENBQUMsR0FBQ0UsQ0FBQyxDQUFDLEtBQUs4RyxPQUFOLENBQVA7QUFBQSxRQUFzQnZPLENBQUMsR0FBQyxLQUFLd1AsSUFBTCxJQUFXakksQ0FBbkM7QUFBcUMsV0FBT3ZILENBQUMsSUFBRXVILENBQUMsQ0FBQ3VDLFVBQUYsS0FBZSxLQUFLMEYsSUFBTCxDQUFVMUYsVUFBbkM7QUFBOEMsR0FBbjBKLEVBQW8wSndCLENBQUMsQ0FBQ3dLLFFBQUYsR0FBVyxVQUFTdk8sQ0FBVCxFQUFXO0FBQUMsUUFBSXZILENBQUMsR0FBQyxLQUFLcVQsUUFBTCxDQUFjOUwsQ0FBZCxDQUFOOztBQUF1QixXQUFPdkgsQ0FBQyxDQUFDNkQsTUFBRixLQUFXLEtBQUtwQyxLQUFMLEdBQVcsS0FBS0EsS0FBTCxDQUFXME0sTUFBWCxDQUFrQm5PLENBQWxCLENBQXRCLEdBQTRDQSxDQUFuRDtBQUFxRCxHQUF2NkosRUFBdzZKc0wsQ0FBQyxDQUFDeUssUUFBRixHQUFXLFVBQVN4TyxDQUFULEVBQVc7QUFBQyxRQUFJdkgsQ0FBQyxHQUFDLEtBQUs4VixRQUFMLENBQWN2TyxDQUFkLENBQU47QUFBdUJ2SCxLQUFDLENBQUM2RCxNQUFGLEtBQVcsS0FBS2dRLFdBQUwsQ0FBaUI3VCxDQUFqQixFQUFtQixDQUFDLENBQXBCLEdBQXVCLEtBQUtzUixNQUFMLENBQVl0UixDQUFaLENBQWxDO0FBQWtELEdBQXhnSyxFQUF5Z0tzTCxDQUFDLENBQUMwSyxTQUFGLEdBQVksVUFBU3pPLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsS0FBS3FULFFBQUwsQ0FBYzlMLENBQWQsQ0FBTjs7QUFBdUIsUUFBR3ZILENBQUMsQ0FBQzZELE1BQUwsRUFBWTtBQUFDLFVBQUk0RCxDQUFDLEdBQUMsS0FBS2hHLEtBQUwsQ0FBV3dILEtBQVgsQ0FBaUIsQ0FBakIsQ0FBTjtBQUEwQixXQUFLeEgsS0FBTCxHQUFXekIsQ0FBQyxDQUFDbU8sTUFBRixDQUFTMUcsQ0FBVCxDQUFYLEVBQXVCLEtBQUtpTSxZQUFMLEVBQXZCLEVBQTJDLEtBQUtDLGFBQUwsRUFBM0MsRUFBZ0UsS0FBS0UsV0FBTCxDQUFpQjdULENBQWpCLEVBQW1CLENBQUMsQ0FBcEIsQ0FBaEUsRUFBdUYsS0FBS3NSLE1BQUwsQ0FBWXRSLENBQVosQ0FBdkYsRUFBc0csS0FBSzZULFdBQUwsQ0FBaUJwTSxDQUFqQixDQUF0RztBQUEwSDtBQUFDLEdBQTF0SyxFQUEydEs2RCxDQUFDLENBQUNnRyxNQUFGLEdBQVMsVUFBUy9KLENBQVQsRUFBVztBQUFDLFFBQUcsS0FBSzZNLG9CQUFMLENBQTBCLFFBQTFCLEVBQW1DN00sQ0FBbkMsR0FBc0NBLENBQUMsSUFBRUEsQ0FBQyxDQUFDMUQsTUFBOUMsRUFBcUQ7QUFBQyxVQUFJN0QsQ0FBQyxHQUFDLEtBQUswVSxhQUFMLEVBQU47QUFBMkJuTixPQUFDLENBQUNrRyxPQUFGLENBQVUsVUFBU2xHLENBQVQsRUFBV0UsQ0FBWCxFQUFhO0FBQUNGLFNBQUMsQ0FBQzZKLE9BQUYsQ0FBVTNKLENBQUMsR0FBQ3pILENBQVosR0FBZXVILENBQUMsQ0FBQytKLE1BQUYsRUFBZjtBQUEwQixPQUFsRDtBQUFvRDtBQUFDLEdBQXQzSyxFQUF1M0toRyxDQUFDLENBQUN6SyxJQUFGLEdBQU8sVUFBUzBHLENBQVQsRUFBVztBQUFDLFFBQUcsS0FBSzZNLG9CQUFMLENBQTBCLE1BQTFCLEVBQWlDN00sQ0FBakMsR0FBb0NBLENBQUMsSUFBRUEsQ0FBQyxDQUFDMUQsTUFBNUMsRUFBbUQ7QUFBQyxVQUFJN0QsQ0FBQyxHQUFDLEtBQUswVSxhQUFMLEVBQU47QUFBMkJuTixPQUFDLENBQUNrRyxPQUFGLENBQVUsVUFBU2xHLENBQVQsRUFBV0UsQ0FBWCxFQUFhO0FBQUNGLFNBQUMsQ0FBQzZKLE9BQUYsQ0FBVTNKLENBQUMsR0FBQ3pILENBQVosR0FBZXVILENBQUMsQ0FBQzFHLElBQUYsRUFBZjtBQUF3QixPQUFoRDtBQUFrRDtBQUFDLEdBQTVnTCxFQUE2Z0x5SyxDQUFDLENBQUMySyxrQkFBRixHQUFxQixVQUFTMU8sQ0FBVCxFQUFXO0FBQUMsUUFBSXZILENBQUMsR0FBQyxLQUFLa1csUUFBTCxDQUFjM08sQ0FBZCxDQUFOO0FBQXVCLFNBQUsrSixNQUFMLENBQVl0UixDQUFaO0FBQWUsR0FBcGxMLEVBQXFsTHNMLENBQUMsQ0FBQzZLLGdCQUFGLEdBQW1CLFVBQVM1TyxDQUFULEVBQVc7QUFBQyxRQUFJdkgsQ0FBQyxHQUFDLEtBQUtrVyxRQUFMLENBQWMzTyxDQUFkLENBQU47QUFBdUIsU0FBSzFHLElBQUwsQ0FBVWIsQ0FBVjtBQUFhLEdBQXhwTCxFQUF5cExzTCxDQUFDLENBQUMySixPQUFGLEdBQVUsVUFBUzFOLENBQVQsRUFBVztBQUFDLFNBQUksSUFBSXZILENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQyxLQUFLeUIsS0FBTCxDQUFXb0MsTUFBekIsRUFBZ0M3RCxDQUFDLEVBQWpDLEVBQW9DO0FBQUMsVUFBSXlILENBQUMsR0FBQyxLQUFLaEcsS0FBTCxDQUFXekIsQ0FBWCxDQUFOO0FBQW9CLFVBQUd5SCxDQUFDLENBQUM4RyxPQUFGLElBQVdoSCxDQUFkLEVBQWdCLE9BQU9FLENBQVA7QUFBUztBQUFDLEdBQWx3TCxFQUFtd0w2RCxDQUFDLENBQUM0SyxRQUFGLEdBQVcsVUFBUzNPLENBQVQsRUFBVztBQUFDQSxLQUFDLEdBQUNNLENBQUMsQ0FBQ29GLFNBQUYsQ0FBWTFGLENBQVosQ0FBRjtBQUFpQixRQUFJdkgsQ0FBQyxHQUFDLEVBQU47QUFBUyxXQUFPdUgsQ0FBQyxDQUFDa0csT0FBRixDQUFVLFVBQVNsRyxDQUFULEVBQVc7QUFBQyxVQUFJRSxDQUFDLEdBQUMsS0FBS3dOLE9BQUwsQ0FBYTFOLENBQWIsQ0FBTjtBQUFzQkUsT0FBQyxJQUFFekgsQ0FBQyxDQUFDb0osSUFBRixDQUFPM0IsQ0FBUCxDQUFIO0FBQWEsS0FBekQsRUFBMEQsSUFBMUQsR0FBZ0V6SCxDQUF2RTtBQUF5RSxHQUE3M0wsRUFBODNMc0wsQ0FBQyxDQUFDbEUsTUFBRixHQUFTLFVBQVNHLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsS0FBS2tXLFFBQUwsQ0FBYzNPLENBQWQsQ0FBTjtBQUF1QixTQUFLNk0sb0JBQUwsQ0FBMEIsUUFBMUIsRUFBbUNwVSxDQUFuQyxHQUFzQ0EsQ0FBQyxJQUFFQSxDQUFDLENBQUM2RCxNQUFMLElBQWE3RCxDQUFDLENBQUN5TixPQUFGLENBQVUsVUFBU2xHLENBQVQsRUFBVztBQUFDQSxPQUFDLENBQUNILE1BQUYsSUFBV1MsQ0FBQyxDQUFDc0YsVUFBRixDQUFhLEtBQUsxTCxLQUFsQixFQUF3QjhGLENBQXhCLENBQVg7QUFBc0MsS0FBNUQsRUFBNkQsSUFBN0QsQ0FBbkQ7QUFBc0gsR0FBaGlNLEVBQWlpTStELENBQUMsQ0FBQ3dHLE9BQUYsR0FBVSxZQUFVO0FBQUMsUUFBSXZLLENBQUMsR0FBQyxLQUFLZ0gsT0FBTCxDQUFhbkUsS0FBbkI7QUFBeUI3QyxLQUFDLENBQUNzQyxNQUFGLEdBQVMsRUFBVCxFQUFZdEMsQ0FBQyxDQUFDa0gsUUFBRixHQUFXLEVBQXZCLEVBQTBCbEgsQ0FBQyxDQUFDcUMsS0FBRixHQUFRLEVBQWxDLEVBQXFDLEtBQUtuSSxLQUFMLENBQVdnTSxPQUFYLENBQW1CLFVBQVNsRyxDQUFULEVBQVc7QUFBQ0EsT0FBQyxDQUFDdUssT0FBRjtBQUFZLEtBQTNDLENBQXJDLEVBQWtGLEtBQUs2RCxZQUFMLEVBQWxGO0FBQXNHLFFBQUkzVixDQUFDLEdBQUMsS0FBS3VPLE9BQUwsQ0FBYThELFlBQW5CO0FBQWdDLFdBQU9oSCxDQUFDLENBQUNyTCxDQUFELENBQVIsRUFBWSxPQUFPLEtBQUt1TyxPQUFMLENBQWE4RCxZQUFoQyxFQUE2Q3RLLENBQUMsSUFBRUEsQ0FBQyxDQUFDcU8sVUFBRixDQUFhLEtBQUs3SCxPQUFsQixFQUEwQixLQUFLVyxXQUFMLENBQWlCaUQsU0FBM0MsQ0FBaEQ7QUFBc0csR0FBM3pNLEVBQTR6TXpLLENBQUMsQ0FBQ3JHLElBQUYsR0FBTyxVQUFTa0csQ0FBVCxFQUFXO0FBQUNBLEtBQUMsR0FBQ00sQ0FBQyxDQUFDeUYsZUFBRixDQUFrQi9GLENBQWxCLENBQUY7QUFBdUIsUUFBSXZILENBQUMsR0FBQ3VILENBQUMsSUFBRUEsQ0FBQyxDQUFDOEssWUFBWDtBQUF3QixXQUFPclMsQ0FBQyxJQUFFcUwsQ0FBQyxDQUFDckwsQ0FBRCxDQUFYO0FBQWUsR0FBNzRNLEVBQTg0TTBILENBQUMsQ0FBQ3VILE1BQUYsR0FBUyxVQUFTMUgsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsUUFBSXlILENBQUMsR0FBQ08sQ0FBQyxDQUFDTixDQUFELENBQVA7QUFBVyxXQUFPRCxDQUFDLENBQUMySyxRQUFGLEdBQVd2SyxDQUFDLENBQUNjLE1BQUYsQ0FBUyxFQUFULEVBQVlqQixDQUFDLENBQUMwSyxRQUFkLENBQVgsRUFBbUN2SyxDQUFDLENBQUNjLE1BQUYsQ0FBU2xCLENBQUMsQ0FBQzJLLFFBQVgsRUFBb0JwUyxDQUFwQixDQUFuQyxFQUEwRHlILENBQUMsQ0FBQ3FMLGFBQUYsR0FBZ0JqTCxDQUFDLENBQUNjLE1BQUYsQ0FBUyxFQUFULEVBQVlqQixDQUFDLENBQUNvTCxhQUFkLENBQTFFLEVBQXVHckwsQ0FBQyxDQUFDMEssU0FBRixHQUFZNUssQ0FBbkgsRUFBcUhFLENBQUMsQ0FBQ3BHLElBQUYsR0FBT3FHLENBQUMsQ0FBQ3JHLElBQTlILEVBQW1Jb0csQ0FBQyxDQUFDOEssSUFBRixHQUFPdkssQ0FBQyxDQUFDRixDQUFELENBQTNJLEVBQStJRCxDQUFDLENBQUNxRyxRQUFGLENBQVd6RyxDQUFYLEVBQWFGLENBQWIsQ0FBL0ksRUFBK0pRLENBQUMsSUFBRUEsQ0FBQyxDQUFDZ0IsT0FBTCxJQUFjaEIsQ0FBQyxDQUFDZ0IsT0FBRixDQUFVeEIsQ0FBVixFQUFZRSxDQUFaLENBQTdLLEVBQTRMQSxDQUFuTTtBQUFxTSxHQUFybk47QUFBc25OLE1BQUk4RCxDQUFDLEdBQUM7QUFBQzhLLE1BQUUsRUFBQyxDQUFKO0FBQU1yTyxLQUFDLEVBQUM7QUFBUixHQUFOO0FBQW1CLFNBQU9OLENBQUMsQ0FBQzZLLElBQUYsR0FBT3pLLENBQVAsRUFBU0osQ0FBaEI7QUFBa0IsQ0FBaGxRLENBQXIrWSxFQUF1anBCLFVBQVNILENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFVBQXNDd0gsaUNBQU8sQ0FBQywwQkFBRCxFQUFxQiwwQkFBckIsQ0FBRCxvQ0FBMkN4SCxDQUEzQztBQUFBO0FBQUE7QUFBQSxvR0FBNUMsR0FBMEYsU0FBMUY7QUFBOE4sQ0FBNU8sQ0FBNk9tQyxNQUE3TyxFQUFvUCxVQUFTb0YsQ0FBVCxFQUFXdkgsQ0FBWCxFQUFhO0FBQUMsTUFBSXlILENBQUMsR0FBQ0YsQ0FBQyxDQUFDMEgsTUFBRixDQUFTLFNBQVQsQ0FBTjtBQUEwQnhILEdBQUMsQ0FBQ3FMLGFBQUYsQ0FBZ0J3RCxRQUFoQixHQUF5QixZQUF6QjtBQUFzQyxNQUFJek8sQ0FBQyxHQUFDSixDQUFDLENBQUNlLFNBQVI7QUFBa0IsU0FBT1gsQ0FBQyxDQUFDNkwsWUFBRixHQUFlLFlBQVU7QUFBQyxTQUFLbkUsT0FBTCxJQUFlLEtBQUt1RSxlQUFMLENBQXFCLGFBQXJCLEVBQW1DLFlBQW5DLENBQWYsRUFBZ0UsS0FBS0EsZUFBTCxDQUFxQixRQUFyQixFQUE4QixZQUE5QixDQUFoRSxFQUE0RyxLQUFLeUMsY0FBTCxFQUE1RyxFQUFrSSxLQUFLQyxLQUFMLEdBQVcsRUFBN0k7O0FBQWdKLFNBQUksSUFBSWpQLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBQyxLQUFLa1AsSUFBbkIsRUFBd0JsUCxDQUFDLEVBQXpCO0FBQTRCLFdBQUtpUCxLQUFMLENBQVdwTixJQUFYLENBQWdCLENBQWhCO0FBQTVCOztBQUErQyxTQUFLc04sSUFBTCxHQUFVLENBQVYsRUFBWSxLQUFLQyxrQkFBTCxHQUF3QixDQUFwQztBQUFzQyxHQUEvUCxFQUFnUTlPLENBQUMsQ0FBQzBPLGNBQUYsR0FBaUIsWUFBVTtBQUFDLFFBQUcsS0FBS0ssaUJBQUwsSUFBeUIsQ0FBQyxLQUFLM1QsV0FBbEMsRUFBOEM7QUFBQyxVQUFJc0UsQ0FBQyxHQUFDLEtBQUs5RixLQUFMLENBQVcsQ0FBWCxDQUFOO0FBQUEsVUFBb0JnRyxDQUFDLEdBQUNGLENBQUMsSUFBRUEsQ0FBQyxDQUFDZ0gsT0FBM0I7QUFBbUMsV0FBS3RMLFdBQUwsR0FBaUJ3RSxDQUFDLElBQUV6SCxDQUFDLENBQUN5SCxDQUFELENBQUQsQ0FBS3VDLFVBQVIsSUFBb0IsS0FBSzZNLGNBQTFDO0FBQXlEOztBQUFBLFFBQUloUCxDQUFDLEdBQUMsS0FBSzVFLFdBQUwsSUFBa0IsS0FBSzZULE1BQTdCO0FBQUEsUUFBb0NoUCxDQUFDLEdBQUMsS0FBSytPLGNBQUwsR0FBb0IsS0FBS0MsTUFBL0Q7QUFBQSxRQUFzRXBQLENBQUMsR0FBQ0ksQ0FBQyxHQUFDRCxDQUExRTtBQUFBLFFBQTRFRyxDQUFDLEdBQUNILENBQUMsR0FBQ0MsQ0FBQyxHQUFDRCxDQUFsRjtBQUFBLFFBQW9GRixDQUFDLEdBQUNLLENBQUMsSUFBRSxJQUFFQSxDQUFMLEdBQU8sT0FBUCxHQUFlLE9BQXJHO0FBQTZHTixLQUFDLEdBQUNpRCxJQUFJLENBQUNoRCxDQUFELENBQUosQ0FBUUQsQ0FBUixDQUFGLEVBQWEsS0FBSytPLElBQUwsR0FBVTlMLElBQUksQ0FBQy9LLEdBQUwsQ0FBUzhILENBQVQsRUFBVyxDQUFYLENBQXZCO0FBQXFDLEdBQXpqQixFQUEwakJHLENBQUMsQ0FBQytPLGlCQUFGLEdBQW9CLFlBQVU7QUFBQyxRQUFJclAsQ0FBQyxHQUFDLEtBQUttSSxVQUFMLENBQWdCLFVBQWhCLENBQU47QUFBQSxRQUFrQ2pJLENBQUMsR0FBQ0YsQ0FBQyxHQUFDLEtBQUtnSCxPQUFMLENBQWFsQixVQUFkLEdBQXlCLEtBQUtrQixPQUFuRTtBQUFBLFFBQTJFMUcsQ0FBQyxHQUFDN0gsQ0FBQyxDQUFDeUgsQ0FBRCxDQUE5RTs7QUFBa0YsU0FBS29QLGNBQUwsR0FBb0JoUCxDQUFDLElBQUVBLENBQUMsQ0FBQ2lDLFVBQXpCO0FBQW9DLEdBQS9zQixFQUFndEJqQyxDQUFDLENBQUN3TSxzQkFBRixHQUF5QixVQUFTOU0sQ0FBVCxFQUFXO0FBQUNBLEtBQUMsQ0FBQ2dJLE9BQUY7QUFBWSxRQUFJdlAsQ0FBQyxHQUFDdUgsQ0FBQyxDQUFDaUksSUFBRixDQUFPeEYsVUFBUCxHQUFrQixLQUFLL0csV0FBN0I7QUFBQSxRQUF5Q3dFLENBQUMsR0FBQ3pILENBQUMsSUFBRSxJQUFFQSxDQUFMLEdBQU8sT0FBUCxHQUFlLE1BQTFEO0FBQUEsUUFBaUU2SCxDQUFDLEdBQUM4QyxJQUFJLENBQUNsRCxDQUFELENBQUosQ0FBUUYsQ0FBQyxDQUFDaUksSUFBRixDQUFPeEYsVUFBUCxHQUFrQixLQUFLL0csV0FBL0IsQ0FBbkU7QUFBK0c0RSxLQUFDLEdBQUM4QyxJQUFJLENBQUNoTCxHQUFMLENBQVNrSSxDQUFULEVBQVcsS0FBSzRPLElBQWhCLENBQUY7O0FBQXdCLFNBQUksSUFBSTNPLENBQUMsR0FBQyxLQUFLWSxPQUFMLENBQWFxTyxlQUFiLEdBQTZCLDJCQUE3QixHQUF5RCxvQkFBL0QsRUFBb0ZyUCxDQUFDLEdBQUMsS0FBS0ksQ0FBTCxFQUFRRCxDQUFSLEVBQVVOLENBQVYsQ0FBdEYsRUFBbUdTLENBQUMsR0FBQztBQUFDNEUsT0FBQyxFQUFDLEtBQUszSixXQUFMLEdBQWlCeUUsQ0FBQyxDQUFDc1AsR0FBdEI7QUFBMEJsTCxPQUFDLEVBQUNwRSxDQUFDLENBQUNvRTtBQUE5QixLQUFyRyxFQUFzSW5FLENBQUMsR0FBQ0QsQ0FBQyxDQUFDb0UsQ0FBRixHQUFJdkUsQ0FBQyxDQUFDaUksSUFBRixDQUFPdkYsV0FBbkosRUFBK0pyQyxDQUFDLEdBQUNDLENBQUMsR0FBQ0gsQ0FBQyxDQUFDc1AsR0FBckssRUFBeUtqUCxDQUFDLEdBQUNMLENBQUMsQ0FBQ3NQLEdBQWpMLEVBQXFMcFAsQ0FBQyxHQUFDRyxDQUF2TCxFQUF5TEEsQ0FBQyxFQUExTDtBQUE2TCxXQUFLeU8sS0FBTCxDQUFXek8sQ0FBWCxJQUFjSixDQUFkO0FBQTdMOztBQUE2TSxXQUFPSyxDQUFQO0FBQVMsR0FBOWxDLEVBQStsQ0gsQ0FBQyxDQUFDb1Asa0JBQUYsR0FBcUIsVUFBUzFQLENBQVQsRUFBVztBQUFDLFFBQUl2SCxDQUFDLEdBQUMsS0FBS2tYLGVBQUwsQ0FBcUIzUCxDQUFyQixDQUFOO0FBQUEsUUFBOEJFLENBQUMsR0FBQ2tELElBQUksQ0FBQ2hMLEdBQUwsQ0FBU3lJLEtBQVQsQ0FBZXVDLElBQWYsRUFBb0IzSyxDQUFwQixDQUFoQzs7QUFBdUQsV0FBTTtBQUFDZ1gsU0FBRyxFQUFDaFgsQ0FBQyxDQUFDbUosT0FBRixDQUFVMUIsQ0FBVixDQUFMO0FBQWtCcUUsT0FBQyxFQUFDckU7QUFBcEIsS0FBTjtBQUE2QixHQUFwdEMsRUFBcXRDSSxDQUFDLENBQUNxUCxlQUFGLEdBQWtCLFVBQVMzUCxDQUFULEVBQVc7QUFBQyxRQUFHLElBQUVBLENBQUwsRUFBTyxPQUFPLEtBQUtpUCxLQUFaOztBQUFrQixTQUFJLElBQUl4VyxDQUFDLEdBQUMsRUFBTixFQUFTeUgsQ0FBQyxHQUFDLEtBQUtnUCxJQUFMLEdBQVUsQ0FBVixHQUFZbFAsQ0FBdkIsRUFBeUJNLENBQUMsR0FBQyxDQUEvQixFQUFpQ0osQ0FBQyxHQUFDSSxDQUFuQyxFQUFxQ0EsQ0FBQyxFQUF0QztBQUF5QzdILE9BQUMsQ0FBQzZILENBQUQsQ0FBRCxHQUFLLEtBQUtzUCxhQUFMLENBQW1CdFAsQ0FBbkIsRUFBcUJOLENBQXJCLENBQUw7QUFBekM7O0FBQXNFLFdBQU92SCxDQUFQO0FBQVMsR0FBMzFDLEVBQTQxQzZILENBQUMsQ0FBQ3NQLGFBQUYsR0FBZ0IsVUFBUzVQLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFFBQUcsSUFBRUEsQ0FBTCxFQUFPLE9BQU8sS0FBS3dXLEtBQUwsQ0FBV2pQLENBQVgsQ0FBUDtBQUFxQixRQUFJRSxDQUFDLEdBQUMsS0FBSytPLEtBQUwsQ0FBV3ZOLEtBQVgsQ0FBaUIxQixDQUFqQixFQUFtQkEsQ0FBQyxHQUFDdkgsQ0FBckIsQ0FBTjtBQUE4QixXQUFPMkssSUFBSSxDQUFDL0ssR0FBTCxDQUFTd0ksS0FBVCxDQUFldUMsSUFBZixFQUFvQmxELENBQXBCLENBQVA7QUFBOEIsR0FBbDlDLEVBQW05Q0ksQ0FBQyxDQUFDdVAseUJBQUYsR0FBNEIsVUFBUzdQLENBQVQsRUFBV3ZILENBQVgsRUFBYTtBQUFDLFFBQUl5SCxDQUFDLEdBQUMsS0FBS2tQLGtCQUFMLEdBQXdCLEtBQUtGLElBQW5DO0FBQUEsUUFBd0M1TyxDQUFDLEdBQUNOLENBQUMsR0FBQyxDQUFGLElBQUtFLENBQUMsR0FBQ0YsQ0FBRixHQUFJLEtBQUtrUCxJQUF4RDtBQUE2RGhQLEtBQUMsR0FBQ0ksQ0FBQyxHQUFDLENBQUQsR0FBR0osQ0FBTjtBQUFRLFFBQUlLLENBQUMsR0FBQzlILENBQUMsQ0FBQ3dQLElBQUYsQ0FBT3hGLFVBQVAsSUFBbUJoSyxDQUFDLENBQUN3UCxJQUFGLENBQU92RixXQUFoQztBQUE0QyxXQUFPLEtBQUswTSxrQkFBTCxHQUF3QjdPLENBQUMsR0FBQ0wsQ0FBQyxHQUFDRixDQUFILEdBQUssS0FBS29QLGtCQUFuQyxFQUFzRDtBQUFDSyxTQUFHLEVBQUN2UCxDQUFMO0FBQU9xRSxPQUFDLEVBQUMsS0FBS3FMLGFBQUwsQ0FBbUIxUCxDQUFuQixFQUFxQkYsQ0FBckI7QUFBVCxLQUE3RDtBQUErRixHQUE3c0QsRUFBOHNETSxDQUFDLENBQUN5TixZQUFGLEdBQWUsVUFBUy9OLENBQVQsRUFBVztBQUFDLFFBQUlFLENBQUMsR0FBQ3pILENBQUMsQ0FBQ3VILENBQUQsQ0FBUDtBQUFBLFFBQVdNLENBQUMsR0FBQyxLQUFLNE4saUJBQUwsQ0FBdUJsTyxDQUF2QixDQUFiO0FBQUEsUUFBdUNPLENBQUMsR0FBQyxLQUFLNEgsVUFBTCxDQUFnQixZQUFoQixDQUF6QztBQUFBLFFBQXVFaEksQ0FBQyxHQUFDSSxDQUFDLEdBQUNELENBQUMsQ0FBQ2tLLElBQUgsR0FBUWxLLENBQUMsQ0FBQ21LLEtBQXBGO0FBQUEsUUFBMEZoSyxDQUFDLEdBQUNOLENBQUMsR0FBQ0QsQ0FBQyxDQUFDdUMsVUFBaEc7QUFBQSxRQUEyR3JDLENBQUMsR0FBQ2dELElBQUksQ0FBQzBNLEtBQUwsQ0FBVzNQLENBQUMsR0FBQyxLQUFLekUsV0FBbEIsQ0FBN0c7O0FBQTRJMEUsS0FBQyxHQUFDZ0QsSUFBSSxDQUFDL0ssR0FBTCxDQUFTLENBQVQsRUFBVytILENBQVgsQ0FBRjtBQUFnQixRQUFJQyxDQUFDLEdBQUMrQyxJQUFJLENBQUMwTSxLQUFMLENBQVdyUCxDQUFDLEdBQUMsS0FBSy9FLFdBQWxCLENBQU47QUFBcUMyRSxLQUFDLElBQUVJLENBQUMsR0FBQyxLQUFLL0UsV0FBUCxHQUFtQixDQUFuQixHQUFxQixDQUF4QixFQUEwQjJFLENBQUMsR0FBQytDLElBQUksQ0FBQ2hMLEdBQUwsQ0FBUyxLQUFLOFcsSUFBTCxHQUFVLENBQW5CLEVBQXFCN08sQ0FBckIsQ0FBNUI7O0FBQW9ELFNBQUksSUFBSUcsQ0FBQyxHQUFDLEtBQUsySCxVQUFMLENBQWdCLFdBQWhCLENBQU4sRUFBbUN6SCxDQUFDLEdBQUMsQ0FBQ0YsQ0FBQyxHQUFDRixDQUFDLENBQUNvSyxHQUFILEdBQU9wSyxDQUFDLENBQUNxSyxNQUFYLElBQW1CekssQ0FBQyxDQUFDd0MsV0FBMUQsRUFBc0U5QixDQUFDLEdBQUNSLENBQTVFLEVBQThFQyxDQUFDLElBQUVPLENBQWpGLEVBQW1GQSxDQUFDLEVBQXBGO0FBQXVGLFdBQUtxTyxLQUFMLENBQVdyTyxDQUFYLElBQWN3QyxJQUFJLENBQUMvSyxHQUFMLENBQVNxSSxDQUFULEVBQVcsS0FBS3VPLEtBQUwsQ0FBV3JPLENBQVgsQ0FBWCxDQUFkO0FBQXZGO0FBQStILEdBQTdsRSxFQUE4bEVOLENBQUMsQ0FBQytNLGlCQUFGLEdBQW9CLFlBQVU7QUFBQyxTQUFLOEIsSUFBTCxHQUFVL0wsSUFBSSxDQUFDL0ssR0FBTCxDQUFTd0ksS0FBVCxDQUFldUMsSUFBZixFQUFvQixLQUFLNkwsS0FBekIsQ0FBVjtBQUEwQyxRQUFJalAsQ0FBQyxHQUFDO0FBQUNzQyxZQUFNLEVBQUMsS0FBSzZNO0FBQWIsS0FBTjtBQUF5QixXQUFPLEtBQUtoSCxVQUFMLENBQWdCLFVBQWhCLE1BQThCbkksQ0FBQyxDQUFDcUMsS0FBRixHQUFRLEtBQUswTixxQkFBTCxFQUF0QyxHQUFvRS9QLENBQTNFO0FBQTZFLEdBQTd3RSxFQUE4d0VNLENBQUMsQ0FBQ3lQLHFCQUFGLEdBQXdCLFlBQVU7QUFBQyxTQUFJLElBQUkvUCxDQUFDLEdBQUMsQ0FBTixFQUFRdkgsQ0FBQyxHQUFDLEtBQUt5VyxJQUFuQixFQUF3QixFQUFFelcsQ0FBRixJQUFLLE1BQUksS0FBS3dXLEtBQUwsQ0FBV3hXLENBQVgsQ0FBakM7QUFBZ0R1SCxPQUFDO0FBQWpEOztBQUFvRCxXQUFNLENBQUMsS0FBS2tQLElBQUwsR0FBVWxQLENBQVgsSUFBYyxLQUFLdEUsV0FBbkIsR0FBK0IsS0FBSzZULE1BQTFDO0FBQWlELEdBQXQ1RSxFQUF1NUVqUCxDQUFDLENBQUNnTyxpQkFBRixHQUFvQixZQUFVO0FBQUMsUUFBSXRPLENBQUMsR0FBQyxLQUFLc1AsY0FBWDtBQUEwQixXQUFPLEtBQUtELGlCQUFMLElBQXlCclAsQ0FBQyxJQUFFLEtBQUtzUCxjQUF4QztBQUF1RCxHQUF2Z0YsRUFBd2dGcFAsQ0FBL2dGO0FBQWloRixDQUFyMkYsQ0FBdmpwQixDOzs7Ozs7Ozs7OztBQ1JBLHVDIiwiZmlsZSI6ImZyb250LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cbi8vIGFueSBDU1MgeW91IHJlcXVpcmUgd2lsbCBvdXRwdXQgaW50byBhIHNpbmdsZSBjc3MgZmlsZSAoYXBwLmNzcyBpbiB0aGlzIGNhc2UpXG5yZXF1aXJlKCcuLi9zY3NzL2Zyb250LnNjc3MnKTtcblxuLy8gTmVlZCBqUXVlcnk/IEluc3RhbGwgaXQgd2l0aCBcInlhcm4gYWRkIGpxdWVyeVwiLCB0aGVuIHVuY29tbWVudCB0byByZXF1aXJlIGl0LlxuLy8gY29uc3QgJCA9IHJlcXVpcmUoJ2pxdWVyeScpO1xuXG5jb25zb2xlLmxvZygnSGVsbG8gV2VicGFjayBFbmNvcmUhIEVkaXQgbWUgaW4gYXNzZXRzL2pzL2FwcC5qcycpO1xuXG5pbXBvcnQgJy4vZnJvbnQvZm9ybXMnO1xuaW1wb3J0ICcuL2Zyb250L2NvbW1vbic7IiwiaW1wb3J0ICdpb24tcmFuZ2VzbGlkZXInO1xuaW1wb3J0ICdpb24tcmFuZ2VzbGlkZXIvY3NzL2lvbi5yYW5nZVNsaWRlci5jc3MnXG5pbXBvcnQgJ21hZ25pZmljLXBvcHVwJztcbmltcG9ydCAnbWFnbmlmaWMtcG9wdXAvZGlzdC9tYWduaWZpYy1wb3B1cC5jc3MnXG5pbXBvcnQgJ3Ntb290aHNjcm9sbCc7XG5pbXBvcnQgJ21vYmlsZS1kZXRlY3QvbW9iaWxlLWRldGVjdC5taW4nO1xuLy9pbXBvcnQgJ2pxdWVyeS1jb2xvcmJveCc7XG4vL2ltcG9ydCAnanF1ZXJ5LWNvbG9yYm94L2V4YW1wbGUyL2NvbG9yYm94LmNzcyc7XG5pbXBvcnQgJ2xpZ2h0Ym94Mic7XG5pbXBvcnQgJ2xpZ2h0Ym94Mi9kaXN0L2Nzcy9saWdodGJveC5jc3MnO1xuaW1wb3J0ICcuL21hc29ucnkucGtnZC5taW4nXG5cbmxldCBjb21tb24gPSAoZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8vbGV0IG1kID0gbmV3IE1vYmlsZURldGVjdCh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudCk7XG4gICAgICAgICAgICBjb21tb24uaW5pdE1hcCgpO1xuICAgICAgICAgICAgY29tbW9uLmluaXRTbGlkZXIoKTtcbiAgICAgICAgICAgIGNvbW1vbi5pbml0Q2FsYygpO1xuICAgICAgICAgICAgY29tbW9uLmluaXRQb3B1cHMoKTtcbiAgICAgICAgICAgIGNvbW1vbi5pbml0U21vb3RoU2Nyb2xsKCk7XG4gICAgICAgICAgICBjb21tb24uaW5pdE1vYmlsZU1lbnUoKTtcbiAgICAgICAgICAgIGNvbW1vbi5pbml0U3RpY2t5SGVhZGVyKCk7XG4gICAgICAgICAgICBjb21tb24uaW5pdEFudGlTcGFtKCk7XG4gICAgICAgICAgICAvL2NvbW1vbi5pbml0Q29sb3JCb3goKTtcbiAgICAgICAgICAgIC8vY29tbW9uLmluaXRNYXNvbnJ5KCk7XG4gICAgICAgIH0sXG4gICAgICAgIGluaXRNYXA6IGZ1bmN0aW9uICgpIHtcblxuXG4gICAgICAgICAgICBpZih0eXBlb2YoeW1hcHMpICE9PSAndW5kZWZpbmVkJyl7XG4gICAgICAgICAgICAgICAgeW1hcHMucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgY2VudGVyID0gWzU5Ljk4NDQ1OCwgMzAuMzYwNjQwXTtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgbXlNYXAgPSBuZXcgeW1hcHMuTWFwKCdtYXAnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjZW50ZXI6IGNlbnRlcixcbiAgICAgICAgICAgICAgICAgICAgICAgIHpvb206IDE2LFxuICAgICAgICAgICAgICAgICAgICAgICAgY29udHJvbHM6IFtdXG4gICAgICAgICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaENvbnRyb2xQcm92aWRlcjogJ3lhbmRleCNzZWFyY2gnXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB2YXIgbWFya2VyID0gbmV3IHltYXBzLlBsYWNlbWFyayhbNTkuOTg0NDU4LCAzMC4zNjA2NDBdLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBoaW50Q29udGVudDogJzE5NTE5Nywg0KDQvtGB0YHQuNGPLCDQodCw0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQsywg0J/QvtC70Y7RgdGC0YDQvtCy0YHQutC40Lkg0L/RgNC+0YHQv9C10LrRgiwgNzLQkSAsINC+0YQuIDEwOCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWxsb29uQ29udGVudDogJzE5NTE5Nywg0KDQvtGB0YHQuNGPLCDQodCw0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQsywg0J/QvtC70Y7RgdGC0YDQvtCy0YHQutC40Lkg0L/RgNC+0YHQv9C10LrRgiwgNzLQkSAsINC+0YQuIDEwOCcsXG4gICAgICAgICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb25MYXlvdXQ6ICdkZWZhdWx0I2ltYWdlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb25JbWFnZUhyZWY6ICdidWlsZC9pbWcvbWFwLnN2ZycsXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uSW1hZ2VTaXplOiBbNDEsIDQ4XSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb25JbWFnZU9mZnNldDogWy01LCAtMzhdXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIG15TWFwLmJlaGF2aW9ycy5kaXNhYmxlKCdzY3JvbGxab29tJyk7XG4gICAgICAgICAgICAgICAgICAgIG15TWFwLmdlb09iamVjdHMuYWRkKG1hcmtlcik7XG5cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIGluaXRTbGlkZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoXCIuanMtc2xpZGVyXCIpLmlvblJhbmdlU2xpZGVyKHtcbiAgICAgICAgICAgICAgICBtaW46IDEwLFxuICAgICAgICAgICAgICAgIG1heDogMTUwLFxuICAgICAgICAgICAgICAgIGZyb206IDUwLFxuICAgICAgICAgICAgICAgIHNraW46IFwicm91bmRcIlxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5pdENhbGM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoJy5qcy1jYWxjX19uZXh0JykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgbGV0ICRjYWxjID0gJCh0aGlzKS5jbG9zZXN0KCcuanMtY2FsYycpLFxuICAgICAgICAgICAgICAgICAgICBzdGVwID0gcGFyc2VJbnQoJGNhbGMuYXR0cignZGF0YS1zdGVwJykpLFxuICAgICAgICAgICAgICAgICAgICBuZXh0U3RlcElkID0gc3RlcCArIDEsXG4gICAgICAgICAgICAgICAgICAgICRuZXh0U3RlcCA9ICRjYWxjLmZpbmQoJ1tkYXRhLWlkPVwiJytTdHJpbmcobmV4dFN0ZXBJZCkrJ1wiXScpXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAgICQoJy5qcy1zdGVwJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXG4gICAgICAgICAgICAgICAgJG5leHRTdGVwLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICBpZihuZXh0U3RlcElkID4gNSl7XG4gICAgICAgICAgICAgICAgICAgICQoJy5qcy1jYWxjX19uYXYnKS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICQoJy5qcy1jYWxjX19iYWNrJykucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgJCgnLmpzLXN0ZXAtY3VycmVudCcpLnRleHQobmV4dFN0ZXBJZCk7XG4gICAgICAgICAgICAgICAgJGNhbGMuYXR0cignZGF0YS1zdGVwJyxuZXh0U3RlcElkKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkKCcuanMtY2FsY19fYmFjaycpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAgICAgdmFyICRjYWxjID0gJCh0aGlzKS5jbG9zZXN0KCcuanMtY2FsYycpLFxuICAgICAgICAgICAgICAgICAgICBzdGVwID0gcGFyc2VJbnQoJGNhbGMuYXR0cignZGF0YS1zdGVwJykpLFxuICAgICAgICAgICAgICAgICAgICBwcmV2U3RlcElkID0gc3RlcCAtIDEsXG4gICAgICAgICAgICAgICAgICAgICRwcmV2U3RlcCA9ICRjYWxjLmZpbmQoJ1tkYXRhLWlkPVwiJytTdHJpbmcocHJldlN0ZXBJZCkrJ1wiXScpXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAgIGlmKHN0ZXA9PT0xKXtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGVwPT09Mil7XG4gICAgICAgICAgICAgICAgICAgICQoJy5qcy1jYWxjX19iYWNrJykuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICQoJy5qcy1zdGVwJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICRwcmV2U3RlcC5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgJGNhbGMuYXR0cignZGF0YS1zdGVwJyxwcmV2U3RlcElkKTtcbiAgICAgICAgICAgICAgICAkKCcuanMtc3RlcC1jdXJyZW50JykudGV4dChwcmV2U3RlcElkKTtcbiAgICAgICAgICAgICAgICBpZihzdGVwIDw9IDYpe1xuICAgICAgICAgICAgICAgICAgICAkKCcuanMtY2FsY19fbmF2Jykuc2hvdygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkKCcuanMtY2FsYy1mb3JtJykub24oJ2Zvcm1fc3VjY2VzcycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICQoJy5qcy1mb3JtX19jb250ZW50JykuaGlkZSgpO1xuICAgICAgICAgICAgICAgICQodGhpcykuZmluZCgnLmpzLWZvcm1fX3N1Y2Nlc3MnKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgJCgnLmpzLWZvcm0tc3VjY2Vzcy1waG9uZScpLnRleHQoJCh0aGlzKS5maW5kKCcuanMtcGhvbmUnKS52YWwoKSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBpbml0UG9wdXBzOiBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgICQoJy5qcy1wb3B1cCcpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIHZhciBzcmMgPSAkKHRoaXMpLmF0dHIoJ2hyZWYnKTtcbiAgICAgICAgICAgICAgICBpZighKHR5cGVvZiAoJCh0aGlzKS5kYXRhKCdmb3JtJykpPT09IHVuZGVmaW5lZCkpe1xuICAgICAgICAgICAgICAgICAgICAkKCcuanMtZm9ybS1uYW1lJykuYXR0cigndmFsdWUnLCQodGhpcykuZGF0YSgnZm9ybScpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgJC5tYWduaWZpY1BvcHVwLm9wZW4oe1xuICAgICAgICAgICAgICAgICAgICBpdGVtczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBzcmMsIC8vIGNhbiBiZSBhIEhUTUwgc3RyaW5nLCBqUXVlcnkgb2JqZWN0LCBvciBDU1Mgc2VsZWN0b3JcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdpbmxpbmUnXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICQoJy5qcy1tZnAtY2xvc2UnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIG1hZ25pZmljUG9wdXAgPSAkLm1hZ25pZmljUG9wdXAuaW5zdGFuY2U7XG4gICAgICAgICAgICAgICAgbWFnbmlmaWNQb3B1cC5jbG9zZSgpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICQoJy5qcy1wb3B1cC1iYWNrJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgdmFyICRmb3JtID0gJCh0aGlzKS5jbG9zZXN0KCcuanMtZm9ybScpLFxuICAgICAgICAgICAgICAgICAgICAkc3VjY2VzcyA9ICRmb3JtLmZpbmQoJy5qcy1mb3JtX19zdWNjZXNzJyksXG4gICAgICAgICAgICAgICAgICAgICRjb250ZW50ID0gJGZvcm0uZmluZCgnLmpzLWZvcm1fX2NvbnRlbnQnKTtcbiAgICAgICAgICAgICAgICAkc3VjY2Vzcy5oaWRlKCk7XG4gICAgICAgICAgICAgICAgJGNvbnRlbnQuc2hvdygpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICQoJy5qcy1wb3B1cF9fZm9ybScpLm9uKCdmb3JtX3N1Y2Nlc3MnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmZpbmQoJy5qcy1mb3JtX19jb250ZW50JykuaGlkZSgpO1xuICAgICAgICAgICAgICAgICQodGhpcykuZmluZCgnLmpzLWZvcm1fX3N1Y2Nlc3MnKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgJCgnLmpzLWZvcm0tc3VjY2Vzcy1waG9uZScpLnRleHQoJCh0aGlzKS5maW5kKCcuanMtcGhvbmUnKS52YWwoKSk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB9LFxuXG4gICAgICAgIGluaXRTbW9vdGhTY3JvbGw6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8vdmFyIHNjcm9sbCA9IG5ldyBTbW9vdGhTY3JvbGwoJ2FbaHJlZio9XCIjXCJdJyk7XG4gICAgICAgIH0sXG4gICAgICAgIGluaXRNb2JpbGVNZW51OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKCcuanMtbWVudS10b2dnbGVyJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgJCgnLmpzLW1vYmlsZS1tZW51JykudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAkKCcuanMtbW9iaWxlLW1lbnVfX2xpbmsnKS5jbGljayhmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICQoJy5qcy1tb2JpbGUtbWVudScpLnRvZ2dsZUNsYXNzKCdvcGVuJyk7XG5cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5pdFN0aWNreUhlYWRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgbmF2ID0gJCgnLmpzLWhlYWRlcicpO1xuICAgICAgICAgICAgdmFyIGxhc3RTY3JvbGxUb3A7XG4gICAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLCBmdW5jdGlvbigpIHsgIC8vIGxpc3RlbiBmb3IgdGhlIHNjcm9sbFxuICAgICAgICAgICAgICAgIHZhciBzdCA9IHdpbmRvdy5wYWdlWU9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xuICAgICAgICAgICAgICAgIGlmIChzdCA+IGxhc3RTY3JvbGxUb3Ape1xuICAgICAgICAgICAgICAgICAgICBuYXYuY3NzKCdvcGFjaXR5JywnMCcpOyAvLyBoaWRlIHRoZSBuYXYtYmFyIHdoZW4gZ29pbmcgZG93blxuICAgICAgICAgICAgICAgICAgICBuYXYucmVtb3ZlQ2xhc3MoJ3N0aWNreScpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBuYXYuY3NzKCdvcGFjaXR5JywgMSk7IC8vIGRpc3BsYXkgdGhlIG5hdi1iYXIgd2hlbiBnb2luZyB1cFxuICAgICAgICAgICAgICAgICAgICBuYXYuYWRkQ2xhc3MoJ3N0aWNreScpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBsYXN0U2Nyb2xsVG9wID0gc3Q7XG4gICAgICAgICAgICAgICAgaWYoc3QgPCAxMCl7XG4gICAgICAgICAgICAgICAgICAgIG5hdi5yZW1vdmVDbGFzcygnc3RpY2t5Jyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwgZmFsc2UpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGluaXRBbnRpU3BhbTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICQoJy5qcy1hbnRpc3BhbScpLnZhbCgnbm8tYm90cy1hbnRpc3BhbScpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfSxcblxuICAgICAgICBpbml0Q29sb3JCb3g6IGZ1bmN0aW9uICgpIHtcblx0XHRcdCQoJy5qcy1jb2xvcmJveCcpLmNvbG9yYm94KHtyZWw6J2dhbCd9KTtcblx0XHR9LFxuXHRcdGluaXRNYXNvbnJ5OiBmdW5jdGlvbiAoKSB7XG5cdFx0XHQkKCcuanMtbWFzb25yeS1ncmlkJykubWFzb25yeSh7XG5cdFx0XHRcdC8vIG9wdGlvbnNcblx0XHRcdFx0aXRlbVNlbGVjdG9yOiAnLmpzLWdyaWQtaXRlbScsXG5cdFx0XHRcdGNvbHVtbldpZHRoOiAyMDBcblx0XHRcdH0pO1xuXHRcdH1cbiAgICB9XG59KSgpO1xuY29tbW9uLmluaXQoKTsiLCJpbXBvcnQgJ2pxdWVyeS5tYXNrZWRpbnB1dC9zcmMvanF1ZXJ5Lm1hc2tlZGlucHV0JztcbnZhciBmb3JtcyA9IChmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGVtYWlsUmUgPSAvXihbYS16MC05X1xcLlxcLVxcK10rKUAoW1xcZGEtelxcLlxcLV0rKVxcLihbYS16XFwuXXsyLDZ9KSQvaTtcbiAgICB2YXIgcGhvbmVSZSA9IC9eWy0gKygpMC05XSskL2k7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgZm9ybXMucGhvbmVNYXNrKCk7XG4gICAgICAgICAgICBmb3Jtcy5jcmVhdGVOdW1iZXJUeXBlKCk7XG4gICAgICAgICAgICBmb3Jtcy5wbGFjZWhvbGRlck1vdmUoKTtcbiAgICAgICAgICAgIGZvcm1zLmlucHV0Q2hhbmdlKCk7XG4gICAgICAgICAgICBmb3Jtcy5zdWJtaXQoKTtcbiAgICAgICAgICAgIGZvcm1zLnBsYWNlaG9sZGVySW5pdCgpO1xuICAgICAgICAgICAgZm9ybXMucHJvY2Vzc2luZygpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDQlNC+0LHQsNCy0LvRj9C10YIg0LzQsNGB0LrRgyDQvdCwINC40L3Qv9GD0YLRiyDRgSDRgtC40L/QvtC8IHBob25lXG4gICAgICAgICAqL1xuICAgICAgICBwaG9uZU1hc2s6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciAkcGhvbmVJbnB1dCA9ICQoXCIuanMtcGhvbmVcIik7XG4gICAgICAgICAgICBpZiAoJHBob25lSW5wdXQubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgJHBob25lSW5wdXQubWFzayhcIis3ICg5OTkpIDk5OS05OS05OVwiLCB7YXV0b2NsZWFyOmZhbHNlfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqINCY0L3QuNGG0LjQsNC70LjQt9C40YDRg9C10YIg0LjQvdC/0YPRgtGLINGBINGC0LjQv9C+0LwgTnVtYmVyXG4gICAgICAgICAqL1xuICAgICAgICBjcmVhdGVOdW1iZXJUeXBlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKCcuanMtZm9ybV9fbnVtYmVyJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyICRpbnB1dCA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgICAgICRwbHVzID0gJGlucHV0LnNpYmxpbmdzKCcuanMtZm9ybV9fbnVtYmVyLXBsdXMnKSxcbiAgICAgICAgICAgICAgICAgICAgJG1pbnVzID0gJGlucHV0LnNpYmxpbmdzKCcuanMtZm9ybV9fbnVtYmVyLW1pbnVzJyksXG4gICAgICAgICAgICAgICAgICAgIG1pbiA9IHBhcnNlRmxvYXQoJGlucHV0LmF0dHIoJ21pbicpKSxcbiAgICAgICAgICAgICAgICAgICAgbWF4ID0gcGFyc2VGbG9hdCgkaW5wdXQuYXR0cignbWF4JykpLFxuICAgICAgICAgICAgICAgICAgICBzdGVwID0gJGlucHV0LmF0dHIoJ3N0ZXAnKSA/IHBhcnNlRmxvYXQoJGlucHV0LmF0dHIoJ3N0ZXAnKSkgOiAxLFxuICAgICAgICAgICAgICAgICAgICBvbGRWYWx1ZSA9IHBhcnNlRmxvYXQoJGlucHV0LnZhbCgpKTtcbiAgICAgICAgICAgICAgICAkaW5wdXQuZGF0YSgnc3RhcnQtdmFsdWUnLCAkaW5wdXQudmFsKCkpO1xuICAgICAgICAgICAgICAgICRpbnB1dC5kYXRhKCdwcmV2LXZhbHVlJywgJGlucHV0LnZhbCgpKTtcbiAgICAgICAgICAgICAgICBpZiAob2xkVmFsdWUgPj0gbWF4KSB7XG4gICAgICAgICAgICAgICAgICAgICRpbnB1dC52YWwobWF4KTtcbiAgICAgICAgICAgICAgICAgICAgJHBsdXMuYWRkQ2xhc3MoJ21heGVkJyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChvbGRWYWx1ZSA8PSBtaW4pIHtcbiAgICAgICAgICAgICAgICAgICAgJGlucHV0LnZhbChtaW4pO1xuICAgICAgICAgICAgICAgICAgICAkbWludXMuYWRkQ2xhc3MoJ21heGVkJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICRpbnB1dC5jaGFuZ2UoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBvbGRWYWx1ZSA9IHBhcnNlRmxvYXQoJCh0aGlzKS52YWwoKSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChvbGRWYWx1ZSA+PSBtYXgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRpbnB1dC52YWwobWF4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRwbHVzLmFkZENsYXNzKCdtYXhlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJG1pbnVzLnJlbW92ZUNsYXNzKCdtYXhlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgb2xkVmFsdWUgPSBtYXg7XG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChvbGRWYWx1ZSA8PSBtaW4pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRpbnB1dC52YWwobWluKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRtaW51cy5hZGRDbGFzcygnbWF4ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRwbHVzLnJlbW92ZUNsYXNzKCdtYXhlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgb2xkVmFsdWUgPSBtaW47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgb2xkVmFsdWUgPSAoICQodGhpcykudmFsKCkgLSAkKHRoaXMpLmRhdGEoJ3ByZXYtdmFsdWUnKSApIDtcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5kYXRhKCdwcmV2LXZhbHVlJywgJCh0aGlzKS52YWwoKSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgJHBsdXMuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBvbGRWYWx1ZSA9IHBhcnNlRmxvYXQoJGlucHV0LnZhbCgpKTtcbiAgICAgICAgICAgICAgICAgICAgJG1pbnVzLnJlbW92ZUNsYXNzKCdtYXhlZCcpO1xuICAgICAgICAgICAgICAgICAgICBpZiAob2xkVmFsdWUgPj0gbWF4KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQudmFsKG9sZFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRwbHVzLmFkZENsYXNzKCdtYXhlZCcpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHBsdXMucmVtb3ZlQ2xhc3MoJ21heGVkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQudmFsKG9sZFZhbHVlICsgc3RlcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQuZGF0YSgncHJldi12YWx1ZScsICskaW5wdXQuZGF0YSgncHJldi12YWx1ZScpICsgc3RlcCApO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9sZFZhbHVlICsgc3RlcCA+PSBtYXgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkcGx1cy5hZGRDbGFzcygnbWF4ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICRpbnB1dC50cmlnZ2VyKCdjaGFuZ2UnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICRtaW51cy5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIG9sZFZhbHVlID0gcGFyc2VGbG9hdCgkaW5wdXQudmFsKCkpO1xuICAgICAgICAgICAgICAgICAgICAkcGx1cy5yZW1vdmVDbGFzcygnbWF4ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9sZFZhbHVlIDw9IG1pbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgJGlucHV0LnZhbChvbGRWYWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkbWludXMuYWRkQ2xhc3MoJ21heGVkJyk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkbWludXMucmVtb3ZlQ2xhc3MoJ21heGVkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQudmFsKG9sZFZhbHVlIC0gc3RlcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQuZGF0YSgncHJldi12YWx1ZScsICskaW5wdXQuZGF0YSgncHJldi12YWx1ZScpIC0gc3RlcCApO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9sZFZhbHVlIC0gc3RlcCA8PSBtaW4pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbWludXMuYWRkQ2xhc3MoJ21heGVkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQudHJpZ2dlcignY2hhbmdlJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIHBsYWNlaG9sZGVySW5pdDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkKCcuanMtaW5wdXQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciAkaW5wdXQgPSAkKHRoaXMpO1xuXG4gICAgICAgICAgICAgICAgaWYgKCRpbnB1dC52YWwoKSAhPT0gJycpIHtcbiAgICAgICAgICAgICAgICAgICAgJGlucHV0LmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkaW5wdXQucmVtb3ZlQ2xhc3MoJ2FjdGl2ZSB2YWxpZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDQodC00LLQuNCz0LDQtdGCINC/0LvQtdC50YHRhdC+0LvQtNC10YAg0L/RgNC4INGE0L7QutGD0YHQtSDQtNC+0LHQsNCy0LvRj9GPINC60LvQsNGB0YFcbiAgICAgICAgICovXG4gICAgICAgIHBsYWNlaG9sZGVyTW92ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJCgnLmpzLWlucHV0JykuY2hhbmdlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS52YWwoKSAhPT0gJycpIHtcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcygnYWN0aXZlIHZhbGlkJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqINCj0LHQuNGA0LDQtdGCINC60LvQsNGB0YEg0L7RiNC40LHQutC4INC/0YDQuCDRhNC+0LrRg9GB0LUg0LhcbiAgICAgICAgICog0LjQvdC40YbQuNCw0LvQuNC30LjRgNGD0LXRgiDQstCw0LvQuNC00LDRhtC40Y4g0L3QsCDRgdC+0LHRi9GC0LjQtSBjaGFuZ2VcbiAgICAgICAgICovXG4gICAgICAgIGlucHV0Q2hhbmdlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgJGlucHV0cyA9ICQoJy5qcy1pbnB1dCcpO1xuICAgICAgICAgICAgJGlucHV0cy5mb2N1cyhmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcygnZXJyb3InKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgJGlucHV0cy5vbignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLmhhc0NsYXNzKCdqcy1yZXF1aXJlZCcpIHx8ICQodGhpcykuaGFzQ2xhc3MoJ2pzLXZhbGlkYXRlJykpIHtcbiAgICAgICAgICAgICAgICAgICAgZm9ybXMuaW5wdXRWYWxpZGF0ZSgkKHRoaXMpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICog0JLQsNC70LjQtNCw0YbQuNGPINC40L3Qv9GD0YLQsFxuICAgICAgICAgKi9cbiAgICAgICAgaW5wdXRWYWxpZGF0ZTogZnVuY3Rpb24gKCRlbGVtZW50KSB7XG4gICAgICAgICAgICB2YXIgdmFsdWUgPSAkZWxlbWVudC52YWwoKSxcbiAgICAgICAgICAgICAgICB2YWxpZCA9IGZhbHNlLFxuICAgICAgICAgICAgICAgICRwYXJlbnQgPSAkKCRlbGVtZW50KS5wYXJlbnQoJy5mb3JtX190ZXh0LWlucHV0LWIsIC5mb3JtX19jaGVja2JveC1pbnB1dC1iJyk7XG4gICAgICAgICAgICBpZiAodmFsdWUubGVuZ3RoID09PSAwICYmICEkZWxlbWVudC5oYXNDbGFzcygnanMtcmVxdWlyZWQnKSkge1xuICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coJGVsZW1lbnQpO1xuICAgICAgICAgICAgICAgICRlbGVtZW50LnJlbW92ZUNsYXNzKCdlcnJvcicpLnJlbW92ZUNsYXNzKCd2YWxpZCcpO1xuICAgICAgICAgICAgICAgIGlmICgkcGFyZW50Lmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAkcGFyZW50LnJlbW92ZUNsYXNzKCd2YWxpZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzd2l0Y2ggKCRlbGVtZW50LmF0dHIoJ3R5cGUnKSkge1xuICAgICAgICAgICAgICAgIGNhc2UgJ2VtYWlsJzpcbiAgICAgICAgICAgICAgICAgICAgdmFsaWQgPSBlbWFpbFJlLnRlc3QodmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICd0ZWwnOlxuICAgICAgICAgICAgICAgICAgICB2YWxpZCA9IHBob25lUmUudGVzdCh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ2NoZWNrYm94JzpcbiAgICAgICAgICAgICAgICAgICAgdmFsaWQgPSAkZWxlbWVudC5wcm9wKCdjaGVja2VkJyk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIHZhbGlkID0gKHZhbHVlICE9PSBcIlwiKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodmFsaWQpIHtcbiAgICAgICAgICAgICAgICAkZWxlbWVudC5yZW1vdmVDbGFzcygnZXJyb3InKS5hZGRDbGFzcygndmFsaWQnKTtcbiAgICAgICAgICAgICAgICBpZiAoJHBhcmVudC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgJHBhcmVudC5hZGRDbGFzcygndmFsaWQnKS5yZW1vdmVDbGFzcygnZXJyb3InKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYgKCRlbGVtZW50Lmhhc0NsYXNzKCdqcy1yZXF1aXJlZCcpIHx8ICRlbGVtZW50Lmhhc0NsYXNzKCdqcy12YWxpZGF0ZScpKSB7XG4gICAgICAgICAgICAgICAgJGVsZW1lbnQucmVtb3ZlQ2xhc3MoJ3ZhbGlkJykuYWRkQ2xhc3MoJ2Vycm9yJyk7XG4gICAgICAgICAgICAgICAgaWYgKCRwYXJlbnQubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coJ3BhcmVudCBlcnJvcicpXG4gICAgICAgICAgICAgICAgICAgICRwYXJlbnQucmVtb3ZlQ2xhc3MoJ3ZhbGlkJykuYWRkQ2xhc3MoJ2Vycm9yJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDQntCx0YDQsNCx0L7RgtCw0YLRjCDQvtGC0LLQtdGCINGB0YLRgNCw0L3QuNGG0YtcbiAgICAgICAgICovXG4gICAgICAgIGlzRXJvcnJzUmV0dXJuQW5kUmVuZGVyOiBmdW5jdGlvbigkZm9ybSwgcmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciBlcnJvck1lc3NhZ2UsXG4gICAgICAgICAgICAgICAgaGFzRXJvcnJzID0gZmFsc2U7XG5cbiAgICAgICAgICAgIGlmICghcmVzcG9uc2VbJ2Vycm9ycyddIHx8ICFPYmplY3Qua2V5cyhyZXNwb25zZVsnZXJyb3JzJ10pLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICRmb3JtLmZpbmQoJy5qcy1pbnB1dCcpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciAkaW5wdXQgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgJHBhcmVudCA9ICRpbnB1dC5wYXJlbnQoKTtcblxuICAgICAgICAgICAgICAgICAgICAkaW5wdXQucmVtb3ZlQ2xhc3MoJ2Vycm9yJykuYWRkQ2xhc3MoJ3ZhbGlkJyk7XG4gICAgICAgICAgICAgICAgICAgICRwYXJlbnQuYWRkQ2xhc3MoJ3ZhbGlkJyk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB2YXIgJHN1Y2Nlc3MgPSAkZm9ybS5maW5kKCcuanMtZm9ybV9fc3VjY2VzcycpLFxuICAgICAgICAgICAgICAgICAgICAkY29udGVudCA9ICRmb3JtLmZpbmQoJy5qcy1mb3JtX19jb250ZW50Jyk7XG4gICAgICAgICAgICAgICAgaWYgKCRzdWNjZXNzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAkc3VjY2Vzcy5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICgkY29udGVudC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgJGNvbnRlbnQuaGlkZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2VbJ2Vycm9ycyddWydhbGwnXSkge1xuICAgICAgICAgICAgICAgIGVycm9yTWVzc2FnZSA9IHJlc3BvbnNlWydlcnJvcnMnXVsnYWxsJ107XG4gICAgICAgICAgICAgICAgZm9ybXMuc2hvd0Vycm9yTWVzc2FnZSgkZm9ybSwgZXJyb3JNZXNzYWdlKTtcbiAgICAgICAgICAgICAgICBoYXNFcm9ycnMgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihyZXNwb25zZVsnZXJyb3JzJ10pIHtcbiAgICAgICAgICAgICAgICAkZm9ybS5maW5kKCcuanMtaW5wdXQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgJGlucHV0ID0gJCh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyICRwYXJlbnQgPSAkaW5wdXQucGFyZW50KCk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlWydlcnJvcnMnXVskaW5wdXQuYXR0cignbmFtZScpXSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JNZXNzYWdlID0gcmVzcG9uc2VbJ2Vycm9ycyddWyRpbnB1dC5hdHRyKCduYW1lJyldO1xuICAgICAgICAgICAgICAgICAgICAgICAgJHBhcmVudC5yZW1vdmVDbGFzcygndmFsaWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRpbnB1dC5hZGRDbGFzcygnZXJyb3InKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1zLnNob3dFcnJvck1lc3NhZ2UoJHBhcmVudCwgZXJyb3JNZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlWydlcnJvcnMnXVsnYWxsLW5vbWVzc2FnZSddKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkcGFyZW50LnJlbW92ZUNsYXNzKCd2YWxpZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJGlucHV0LnJlbW92ZUNsYXNzKCd2YWxpZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJGlucHV0LmFkZENsYXNzKCdlcnJvcicpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgJGlucHV0LnJlbW92ZUNsYXNzKCdlcnJvcicpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICBoYXNFcm9ycnMgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGhhc0Vyb3JycztcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICog0J7RgtC/0YDQsNCy0LrQsCDRhNC+0YDQvNGLXG4gICAgICAgICAqL1xuICAgICAgICBzdWJtaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoJy5qcy1mb3JtJykuc3VibWl0KGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgICAgICB2YXIgJGZvcm0gPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgICAgICBkYXRhQ29ycmVjdCA9IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZCxcbiAgICAgICAgICAgICAgICAgICAgY29udGVudFR5cGUsXG4gICAgICAgICAgICAgICAgICAgIHByb2Nlc3NEYXRhLFxuICAgICAgICAgICAgICAgICAgICBkYXRhO1xuICAgICAgICAgICAgICAgICRmb3JtLmZpbmQoJy5qcy1pbnB1dC5qcy1yZXF1aXJlZCcpLmVhY2goZnVuY3Rpb24gKCkgeyAvLzp2aXNpYmxlXG4gICAgICAgICAgICAgICAgICAgIGZvcm1zLmlucHV0VmFsaWRhdGUoJCh0aGlzKSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgJGZvcm0uZmluZCgnLmpzLXJlcXVpcmVkJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLmhhc0NsYXNzKCdlcnJvcicpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhQ29ycmVjdCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGFDb3JyZWN0KSB7XG4gICAgICAgICAgICAgICAgICAgICRmb3JtLnRyaWdnZXIoJ2Zvcm1fc3RhcnQnKTtcbiAgICAgICAgICAgICAgICAgICAgaWYoJGZvcm0uaGFzQ2xhc3MoJ2pzLWxvZ2luLWZvcm0nKSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJuYW1lOiAkKCcuanMtdXNlcm5hbWUnKS52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXNzd29yZDogJCgnLmpzLXBhc3N3b3JkJykudmFsKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9rZW46ICQoJy5qcy10b2tlbicpLnZhbCgpXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlID0gJ2FwcGxpY2F0aW9uL2pzb24nO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc0RhdGEgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2V7XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhID0gbmV3IEZvcm1EYXRhKCRmb3JtWzBdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9jZXNzRGF0YSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgbWV0aG9kID0gJGZvcm0uYXR0cignbWV0aG9kJyk7XG4gICAgICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBtZXRob2QgPyBtZXRob2QgOiAnUE9TVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICB1cmw6ICRmb3JtLmF0dHIoJ2FjdGlvbicpLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFUeXBlOiBcImpzb25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlOiBjb250ZW50VHlwZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NEYXRhOiBwcm9jZXNzRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3ApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZighZm9ybXMuaXNFcm9ycnNSZXR1cm5BbmRSZW5kZXIoJGZvcm0sIHJlc3ApKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRmb3JtLnRyaWdnZXIoJ2Zvcm1fc3VjY2VzcycsIHJlc3ApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGZvcm0udHJpZ2dlcignZm9ybV9lcnJvcicsIHJlc3ApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybXMuc2hvd0Vycm9yTWVzc2FnZSgkZm9ybSwgWyRmb3JtLmRhdGEoJ2Vycm9yLW1zZycpXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGZvcm0udHJpZ2dlcignZm9ybV9lcnJvcicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkKCcuanMtc3VibWl0JykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5jbG9zZXN0KCcuanMtZm9ybScpLnN1Ym1pdCgpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICog0JLRi9Cy0LXRgdGC0Lgg0YHQvtC+0LHRidC10L3QuNC1INC+0LEg0L7RiNC40LHQutC1INCyINGE0L7RgNC80LVcbiAgICAgICAgICovXG4gICAgICAgIHNob3dFcnJvck1lc3NhZ2U6IGZ1bmN0aW9uKCRlbGVtZW50LCAkbWVzc2FnZXMpIHtcbiAgICAgICAgICAgIGZvcih2YXIgJGtleSBpbiAkbWVzc2FnZXMpIHtcbiAgICAgICAgICAgICAgICB2YXIgJGVycm9yID0gJCgnPGRpdiBjbGFzcz1cImZvcm1fX3NlbmQtZmFpbFwiIHN0eWxlPVwib3BhY2l0eTogMFwiPicgKyAkbWVzc2FnZXNbJGtleV0gKyAnPC9kaXY+Jyk7XG4gICAgICAgICAgICAgICAgLy9pZiAoJGVsZW1lbnQuZGF0YSgnZXJyb3ItbXNnJykpIHtcbiAgICAgICAgICAgICAgICAkZWxlbWVudC5hcHBlbmQoJGVycm9yKTtcbiAgICAgICAgICAgICAgICAkZXJyb3IuZmFkZVRvKFwiZmFzdFwiICwgMSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICRlcnJvci5maXJzdCgpLmZhZGVPdXQoXCJmYXN0XCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRlbGVtZW50LmZpbmQoJy5mb3JtX19zZW5kLWZhaWwnKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9LCAxNTAwKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAvL31cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBwcm9jZXNzaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICQoJy5qcy1mb3JtJykub24oJ2Zvcm1fc3RhcnQnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdwcm9jZXNzaW5nJyk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJCgnLmpzLWZvcm0nKS5vbignZm9ybV9zdWNjZXNzIGZvcm1fZXJyb3InLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdwcm9jZXNzaW5nJyk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJCgnLmpzLWxvZ2luLWZvcm0nKS5vbignZm9ybV9zdWNjZXNzJywgZnVuY3Rpb24oZSxyZXNwKSB7XG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlcGxhY2UoJCh0aGlzKS5kYXRhKCdyZWRpcmVjdCcpKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxufSkoKTtcbmZvcm1zLmluaXQoKTsiLCIvKiFcbiAqIE1hc29ucnkgUEFDS0FHRUQgdjQuMi4yXG4gKiBDYXNjYWRpbmcgZ3JpZCBsYXlvdXQgbGlicmFyeVxuICogaHR0cHM6Ly9tYXNvbnJ5LmRlc2FuZHJvLmNvbVxuICogTUlUIExpY2Vuc2VcbiAqIGJ5IERhdmlkIERlU2FuZHJvXG4gKi9cblxuIWZ1bmN0aW9uKHQsZSl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcImpxdWVyeS1icmlkZ2V0L2pxdWVyeS1icmlkZ2V0XCIsW1wianF1ZXJ5XCJdLGZ1bmN0aW9uKGkpe3JldHVybiBlKHQsaSl9KTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1lKHQscmVxdWlyZShcImpxdWVyeVwiKSk6dC5qUXVlcnlCcmlkZ2V0PWUodCx0LmpRdWVyeSl9KHdpbmRvdyxmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkoaSxyLGEpe2Z1bmN0aW9uIGgodCxlLG4pe3ZhciBvLHI9XCIkKCkuXCIraSsnKFwiJytlKydcIiknO3JldHVybiB0LmVhY2goZnVuY3Rpb24odCxoKXt2YXIgdT1hLmRhdGEoaCxpKTtpZighdSlyZXR1cm4gdm9pZCBzKGkrXCIgbm90IGluaXRpYWxpemVkLiBDYW5ub3QgY2FsbCBtZXRob2RzLCBpLmUuIFwiK3IpO3ZhciBkPXVbZV07aWYoIWR8fFwiX1wiPT1lLmNoYXJBdCgwKSlyZXR1cm4gdm9pZCBzKHIrXCIgaXMgbm90IGEgdmFsaWQgbWV0aG9kXCIpO3ZhciBsPWQuYXBwbHkodSxuKTtvPXZvaWQgMD09PW8/bDpvfSksdm9pZCAwIT09bz9vOnR9ZnVuY3Rpb24gdSh0LGUpe3QuZWFjaChmdW5jdGlvbih0LG4pe3ZhciBvPWEuZGF0YShuLGkpO28/KG8ub3B0aW9uKGUpLG8uX2luaXQoKSk6KG89bmV3IHIobixlKSxhLmRhdGEobixpLG8pKX0pfWE9YXx8ZXx8dC5qUXVlcnksYSYmKHIucHJvdG90eXBlLm9wdGlvbnx8KHIucHJvdG90eXBlLm9wdGlvbj1mdW5jdGlvbih0KXthLmlzUGxhaW5PYmplY3QodCkmJih0aGlzLm9wdGlvbnM9YS5leHRlbmQoITAsdGhpcy5vcHRpb25zLHQpKX0pLGEuZm5baV09ZnVuY3Rpb24odCl7aWYoXCJzdHJpbmdcIj09dHlwZW9mIHQpe3ZhciBlPW8uY2FsbChhcmd1bWVudHMsMSk7cmV0dXJuIGgodGhpcyx0LGUpfXJldHVybiB1KHRoaXMsdCksdGhpc30sbihhKSl9ZnVuY3Rpb24gbih0KXshdHx8dCYmdC5icmlkZ2V0fHwodC5icmlkZ2V0PWkpfXZhciBvPUFycmF5LnByb3RvdHlwZS5zbGljZSxyPXQuY29uc29sZSxzPVwidW5kZWZpbmVkXCI9PXR5cGVvZiByP2Z1bmN0aW9uKCl7fTpmdW5jdGlvbih0KXtyLmVycm9yKHQpfTtyZXR1cm4gbihlfHx0LmpRdWVyeSksaX0pLGZ1bmN0aW9uKHQsZSl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcImV2LWVtaXR0ZXIvZXYtZW1pdHRlclwiLGUpOlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGUmJm1vZHVsZS5leHBvcnRzP21vZHVsZS5leHBvcnRzPWUoKTp0LkV2RW1pdHRlcj1lKCl9KFwidW5kZWZpbmVkXCIhPXR5cGVvZiB3aW5kb3c/d2luZG93OnRoaXMsZnVuY3Rpb24oKXtmdW5jdGlvbiB0KCl7fXZhciBlPXQucHJvdG90eXBlO3JldHVybiBlLm9uPWZ1bmN0aW9uKHQsZSl7aWYodCYmZSl7dmFyIGk9dGhpcy5fZXZlbnRzPXRoaXMuX2V2ZW50c3x8e30sbj1pW3RdPWlbdF18fFtdO3JldHVybi0xPT1uLmluZGV4T2YoZSkmJm4ucHVzaChlKSx0aGlzfX0sZS5vbmNlPWZ1bmN0aW9uKHQsZSl7aWYodCYmZSl7dGhpcy5vbih0LGUpO3ZhciBpPXRoaXMuX29uY2VFdmVudHM9dGhpcy5fb25jZUV2ZW50c3x8e30sbj1pW3RdPWlbdF18fHt9O3JldHVybiBuW2VdPSEwLHRoaXN9fSxlLm9mZj1mdW5jdGlvbih0LGUpe3ZhciBpPXRoaXMuX2V2ZW50cyYmdGhpcy5fZXZlbnRzW3RdO2lmKGkmJmkubGVuZ3RoKXt2YXIgbj1pLmluZGV4T2YoZSk7cmV0dXJuLTEhPW4mJmkuc3BsaWNlKG4sMSksdGhpc319LGUuZW1pdEV2ZW50PWZ1bmN0aW9uKHQsZSl7dmFyIGk9dGhpcy5fZXZlbnRzJiZ0aGlzLl9ldmVudHNbdF07aWYoaSYmaS5sZW5ndGgpe2k9aS5zbGljZSgwKSxlPWV8fFtdO2Zvcih2YXIgbj10aGlzLl9vbmNlRXZlbnRzJiZ0aGlzLl9vbmNlRXZlbnRzW3RdLG89MDtvPGkubGVuZ3RoO28rKyl7dmFyIHI9aVtvXSxzPW4mJm5bcl07cyYmKHRoaXMub2ZmKHQsciksZGVsZXRlIG5bcl0pLHIuYXBwbHkodGhpcyxlKX1yZXR1cm4gdGhpc319LGUuYWxsT2ZmPWZ1bmN0aW9uKCl7ZGVsZXRlIHRoaXMuX2V2ZW50cyxkZWxldGUgdGhpcy5fb25jZUV2ZW50c30sdH0pLGZ1bmN0aW9uKHQsZSl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcImdldC1zaXplL2dldC1zaXplXCIsZSk6XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9ZSgpOnQuZ2V0U2l6ZT1lKCl9KHdpbmRvdyxmdW5jdGlvbigpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHQodCl7dmFyIGU9cGFyc2VGbG9hdCh0KSxpPS0xPT10LmluZGV4T2YoXCIlXCIpJiYhaXNOYU4oZSk7cmV0dXJuIGkmJmV9ZnVuY3Rpb24gZSgpe31mdW5jdGlvbiBpKCl7Zm9yKHZhciB0PXt3aWR0aDowLGhlaWdodDowLGlubmVyV2lkdGg6MCxpbm5lckhlaWdodDowLG91dGVyV2lkdGg6MCxvdXRlckhlaWdodDowfSxlPTA7dT5lO2UrKyl7dmFyIGk9aFtlXTt0W2ldPTB9cmV0dXJuIHR9ZnVuY3Rpb24gbih0KXt2YXIgZT1nZXRDb21wdXRlZFN0eWxlKHQpO3JldHVybiBlfHxhKFwiU3R5bGUgcmV0dXJuZWQgXCIrZStcIi4gQXJlIHlvdSBydW5uaW5nIHRoaXMgY29kZSBpbiBhIGhpZGRlbiBpZnJhbWUgb24gRmlyZWZveD8gU2VlIGh0dHBzOi8vYml0Lmx5L2dldHNpemVidWcxXCIpLGV9ZnVuY3Rpb24gbygpe2lmKCFkKXtkPSEwO3ZhciBlPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7ZS5zdHlsZS53aWR0aD1cIjIwMHB4XCIsZS5zdHlsZS5wYWRkaW5nPVwiMXB4IDJweCAzcHggNHB4XCIsZS5zdHlsZS5ib3JkZXJTdHlsZT1cInNvbGlkXCIsZS5zdHlsZS5ib3JkZXJXaWR0aD1cIjFweCAycHggM3B4IDRweFwiLGUuc3R5bGUuYm94U2l6aW5nPVwiYm9yZGVyLWJveFwiO3ZhciBpPWRvY3VtZW50LmJvZHl8fGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtpLmFwcGVuZENoaWxkKGUpO3ZhciBvPW4oZSk7cz0yMDA9PU1hdGgucm91bmQodChvLndpZHRoKSksci5pc0JveFNpemVPdXRlcj1zLGkucmVtb3ZlQ2hpbGQoZSl9fWZ1bmN0aW9uIHIoZSl7aWYobygpLFwic3RyaW5nXCI9PXR5cGVvZiBlJiYoZT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKGUpKSxlJiZcIm9iamVjdFwiPT10eXBlb2YgZSYmZS5ub2RlVHlwZSl7dmFyIHI9bihlKTtpZihcIm5vbmVcIj09ci5kaXNwbGF5KXJldHVybiBpKCk7dmFyIGE9e307YS53aWR0aD1lLm9mZnNldFdpZHRoLGEuaGVpZ2h0PWUub2Zmc2V0SGVpZ2h0O2Zvcih2YXIgZD1hLmlzQm9yZGVyQm94PVwiYm9yZGVyLWJveFwiPT1yLmJveFNpemluZyxsPTA7dT5sO2wrKyl7dmFyIGM9aFtsXSxmPXJbY10sbT1wYXJzZUZsb2F0KGYpO2FbY109aXNOYU4obSk/MDptfXZhciBwPWEucGFkZGluZ0xlZnQrYS5wYWRkaW5nUmlnaHQsZz1hLnBhZGRpbmdUb3ArYS5wYWRkaW5nQm90dG9tLHk9YS5tYXJnaW5MZWZ0K2EubWFyZ2luUmlnaHQsdj1hLm1hcmdpblRvcCthLm1hcmdpbkJvdHRvbSxfPWEuYm9yZGVyTGVmdFdpZHRoK2EuYm9yZGVyUmlnaHRXaWR0aCx6PWEuYm9yZGVyVG9wV2lkdGgrYS5ib3JkZXJCb3R0b21XaWR0aCxFPWQmJnMsYj10KHIud2lkdGgpO2IhPT0hMSYmKGEud2lkdGg9YisoRT8wOnArXykpO3ZhciB4PXQoci5oZWlnaHQpO3JldHVybiB4IT09ITEmJihhLmhlaWdodD14KyhFPzA6Zyt6KSksYS5pbm5lcldpZHRoPWEud2lkdGgtKHArXyksYS5pbm5lckhlaWdodD1hLmhlaWdodC0oZyt6KSxhLm91dGVyV2lkdGg9YS53aWR0aCt5LGEub3V0ZXJIZWlnaHQ9YS5oZWlnaHQrdixhfX12YXIgcyxhPVwidW5kZWZpbmVkXCI9PXR5cGVvZiBjb25zb2xlP2U6ZnVuY3Rpb24odCl7Y29uc29sZS5lcnJvcih0KX0saD1bXCJwYWRkaW5nTGVmdFwiLFwicGFkZGluZ1JpZ2h0XCIsXCJwYWRkaW5nVG9wXCIsXCJwYWRkaW5nQm90dG9tXCIsXCJtYXJnaW5MZWZ0XCIsXCJtYXJnaW5SaWdodFwiLFwibWFyZ2luVG9wXCIsXCJtYXJnaW5Cb3R0b21cIixcImJvcmRlckxlZnRXaWR0aFwiLFwiYm9yZGVyUmlnaHRXaWR0aFwiLFwiYm9yZGVyVG9wV2lkdGhcIixcImJvcmRlckJvdHRvbVdpZHRoXCJdLHU9aC5sZW5ndGgsZD0hMTtyZXR1cm4gcn0pLGZ1bmN0aW9uKHQsZSl7XCJ1c2Ugc3RyaWN0XCI7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcImRlc2FuZHJvLW1hdGNoZXMtc2VsZWN0b3IvbWF0Y2hlcy1zZWxlY3RvclwiLGUpOlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGUmJm1vZHVsZS5leHBvcnRzP21vZHVsZS5leHBvcnRzPWUoKTp0Lm1hdGNoZXNTZWxlY3Rvcj1lKCl9KHdpbmRvdyxmdW5jdGlvbigpe1widXNlIHN0cmljdFwiO3ZhciB0PWZ1bmN0aW9uKCl7dmFyIHQ9d2luZG93LkVsZW1lbnQucHJvdG90eXBlO2lmKHQubWF0Y2hlcylyZXR1cm5cIm1hdGNoZXNcIjtpZih0Lm1hdGNoZXNTZWxlY3RvcilyZXR1cm5cIm1hdGNoZXNTZWxlY3RvclwiO2Zvcih2YXIgZT1bXCJ3ZWJraXRcIixcIm1velwiLFwibXNcIixcIm9cIl0saT0wO2k8ZS5sZW5ndGg7aSsrKXt2YXIgbj1lW2ldLG89bitcIk1hdGNoZXNTZWxlY3RvclwiO2lmKHRbb10pcmV0dXJuIG99fSgpO3JldHVybiBmdW5jdGlvbihlLGkpe3JldHVybiBlW3RdKGkpfX0pLGZ1bmN0aW9uKHQsZSl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcImZpenp5LXVpLXV0aWxzL3V0aWxzXCIsW1wiZGVzYW5kcm8tbWF0Y2hlcy1zZWxlY3Rvci9tYXRjaGVzLXNlbGVjdG9yXCJdLGZ1bmN0aW9uKGkpe3JldHVybiBlKHQsaSl9KTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1lKHQscmVxdWlyZShcImRlc2FuZHJvLW1hdGNoZXMtc2VsZWN0b3JcIikpOnQuZml6enlVSVV0aWxzPWUodCx0Lm1hdGNoZXNTZWxlY3Rvcil9KHdpbmRvdyxmdW5jdGlvbih0LGUpe3ZhciBpPXt9O2kuZXh0ZW5kPWZ1bmN0aW9uKHQsZSl7Zm9yKHZhciBpIGluIGUpdFtpXT1lW2ldO3JldHVybiB0fSxpLm1vZHVsbz1mdW5jdGlvbih0LGUpe3JldHVybih0JWUrZSklZX07dmFyIG49QXJyYXkucHJvdG90eXBlLnNsaWNlO2kubWFrZUFycmF5PWZ1bmN0aW9uKHQpe2lmKEFycmF5LmlzQXJyYXkodCkpcmV0dXJuIHQ7aWYobnVsbD09PXR8fHZvaWQgMD09PXQpcmV0dXJuW107dmFyIGU9XCJvYmplY3RcIj09dHlwZW9mIHQmJlwibnVtYmVyXCI9PXR5cGVvZiB0Lmxlbmd0aDtyZXR1cm4gZT9uLmNhbGwodCk6W3RdfSxpLnJlbW92ZUZyb209ZnVuY3Rpb24odCxlKXt2YXIgaT10LmluZGV4T2YoZSk7LTEhPWkmJnQuc3BsaWNlKGksMSl9LGkuZ2V0UGFyZW50PWZ1bmN0aW9uKHQsaSl7Zm9yKDt0LnBhcmVudE5vZGUmJnQhPWRvY3VtZW50LmJvZHk7KWlmKHQ9dC5wYXJlbnROb2RlLGUodCxpKSlyZXR1cm4gdH0saS5nZXRRdWVyeUVsZW1lbnQ9ZnVuY3Rpb24odCl7cmV0dXJuXCJzdHJpbmdcIj09dHlwZW9mIHQ/ZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0KTp0fSxpLmhhbmRsZUV2ZW50PWZ1bmN0aW9uKHQpe3ZhciBlPVwib25cIit0LnR5cGU7dGhpc1tlXSYmdGhpc1tlXSh0KX0saS5maWx0ZXJGaW5kRWxlbWVudHM9ZnVuY3Rpb24odCxuKXt0PWkubWFrZUFycmF5KHQpO3ZhciBvPVtdO3JldHVybiB0LmZvckVhY2goZnVuY3Rpb24odCl7aWYodCBpbnN0YW5jZW9mIEhUTUxFbGVtZW50KXtpZighbilyZXR1cm4gdm9pZCBvLnB1c2godCk7ZSh0LG4pJiZvLnB1c2godCk7Zm9yKHZhciBpPXQucXVlcnlTZWxlY3RvckFsbChuKSxyPTA7cjxpLmxlbmd0aDtyKyspby5wdXNoKGlbcl0pfX0pLG99LGkuZGVib3VuY2VNZXRob2Q9ZnVuY3Rpb24odCxlLGkpe2k9aXx8MTAwO3ZhciBuPXQucHJvdG90eXBlW2VdLG89ZStcIlRpbWVvdXRcIjt0LnByb3RvdHlwZVtlXT1mdW5jdGlvbigpe3ZhciB0PXRoaXNbb107Y2xlYXJUaW1lb3V0KHQpO3ZhciBlPWFyZ3VtZW50cyxyPXRoaXM7dGhpc1tvXT1zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7bi5hcHBseShyLGUpLGRlbGV0ZSByW29dfSxpKX19LGkuZG9jUmVhZHk9ZnVuY3Rpb24odCl7dmFyIGU9ZG9jdW1lbnQucmVhZHlTdGF0ZTtcImNvbXBsZXRlXCI9PWV8fFwiaW50ZXJhY3RpdmVcIj09ZT9zZXRUaW1lb3V0KHQpOmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsdCl9LGkudG9EYXNoZWQ9ZnVuY3Rpb24odCl7cmV0dXJuIHQucmVwbGFjZSgvKC4pKFtBLVpdKS9nLGZ1bmN0aW9uKHQsZSxpKXtyZXR1cm4gZStcIi1cIitpfSkudG9Mb3dlckNhc2UoKX07dmFyIG89dC5jb25zb2xlO3JldHVybiBpLmh0bWxJbml0PWZ1bmN0aW9uKGUsbil7aS5kb2NSZWFkeShmdW5jdGlvbigpe3ZhciByPWkudG9EYXNoZWQobikscz1cImRhdGEtXCIrcixhPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbXCIrcytcIl1cIiksaD1kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmpzLVwiK3IpLHU9aS5tYWtlQXJyYXkoYSkuY29uY2F0KGkubWFrZUFycmF5KGgpKSxkPXMrXCItb3B0aW9uc1wiLGw9dC5qUXVlcnk7dS5mb3JFYWNoKGZ1bmN0aW9uKHQpe3ZhciBpLHI9dC5nZXRBdHRyaWJ1dGUocyl8fHQuZ2V0QXR0cmlidXRlKGQpO3RyeXtpPXImJkpTT04ucGFyc2Uocil9Y2F0Y2goYSl7cmV0dXJuIHZvaWQobyYmby5lcnJvcihcIkVycm9yIHBhcnNpbmcgXCIrcytcIiBvbiBcIit0LmNsYXNzTmFtZStcIjogXCIrYSkpfXZhciBoPW5ldyBlKHQsaSk7bCYmbC5kYXRhKHQsbixoKX0pfSl9LGl9KSxmdW5jdGlvbih0LGUpe1wiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoXCJvdXRsYXllci9pdGVtXCIsW1wiZXYtZW1pdHRlci9ldi1lbWl0dGVyXCIsXCJnZXQtc2l6ZS9nZXQtc2l6ZVwiXSxlKTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1lKHJlcXVpcmUoXCJldi1lbWl0dGVyXCIpLHJlcXVpcmUoXCJnZXQtc2l6ZVwiKSk6KHQuT3V0bGF5ZXI9e30sdC5PdXRsYXllci5JdGVtPWUodC5FdkVtaXR0ZXIsdC5nZXRTaXplKSl9KHdpbmRvdyxmdW5jdGlvbih0LGUpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGkodCl7Zm9yKHZhciBlIGluIHQpcmV0dXJuITE7cmV0dXJuIGU9bnVsbCwhMH1mdW5jdGlvbiBuKHQsZSl7dCYmKHRoaXMuZWxlbWVudD10LHRoaXMubGF5b3V0PWUsdGhpcy5wb3NpdGlvbj17eDowLHk6MH0sdGhpcy5fY3JlYXRlKCkpfWZ1bmN0aW9uIG8odCl7cmV0dXJuIHQucmVwbGFjZSgvKFtBLVpdKS9nLGZ1bmN0aW9uKHQpe3JldHVyblwiLVwiK3QudG9Mb3dlckNhc2UoKX0pfXZhciByPWRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZSxzPVwic3RyaW5nXCI9PXR5cGVvZiByLnRyYW5zaXRpb24/XCJ0cmFuc2l0aW9uXCI6XCJXZWJraXRUcmFuc2l0aW9uXCIsYT1cInN0cmluZ1wiPT10eXBlb2Ygci50cmFuc2Zvcm0/XCJ0cmFuc2Zvcm1cIjpcIldlYmtpdFRyYW5zZm9ybVwiLGg9e1dlYmtpdFRyYW5zaXRpb246XCJ3ZWJraXRUcmFuc2l0aW9uRW5kXCIsdHJhbnNpdGlvbjpcInRyYW5zaXRpb25lbmRcIn1bc10sdT17dHJhbnNmb3JtOmEsdHJhbnNpdGlvbjpzLHRyYW5zaXRpb25EdXJhdGlvbjpzK1wiRHVyYXRpb25cIix0cmFuc2l0aW9uUHJvcGVydHk6cytcIlByb3BlcnR5XCIsdHJhbnNpdGlvbkRlbGF5OnMrXCJEZWxheVwifSxkPW4ucHJvdG90eXBlPU9iamVjdC5jcmVhdGUodC5wcm90b3R5cGUpO2QuY29uc3RydWN0b3I9bixkLl9jcmVhdGU9ZnVuY3Rpb24oKXt0aGlzLl90cmFuc249e2luZ1Byb3BlcnRpZXM6e30sY2xlYW46e30sb25FbmQ6e319LHRoaXMuY3NzKHtwb3NpdGlvbjpcImFic29sdXRlXCJ9KX0sZC5oYW5kbGVFdmVudD1mdW5jdGlvbih0KXt2YXIgZT1cIm9uXCIrdC50eXBlO3RoaXNbZV0mJnRoaXNbZV0odCl9LGQuZ2V0U2l6ZT1mdW5jdGlvbigpe3RoaXMuc2l6ZT1lKHRoaXMuZWxlbWVudCl9LGQuY3NzPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuZWxlbWVudC5zdHlsZTtmb3IodmFyIGkgaW4gdCl7dmFyIG49dVtpXXx8aTtlW25dPXRbaV19fSxkLmdldFBvc2l0aW9uPWZ1bmN0aW9uKCl7dmFyIHQ9Z2V0Q29tcHV0ZWRTdHlsZSh0aGlzLmVsZW1lbnQpLGU9dGhpcy5sYXlvdXQuX2dldE9wdGlvbihcIm9yaWdpbkxlZnRcIiksaT10aGlzLmxheW91dC5fZ2V0T3B0aW9uKFwib3JpZ2luVG9wXCIpLG49dFtlP1wibGVmdFwiOlwicmlnaHRcIl0sbz10W2k/XCJ0b3BcIjpcImJvdHRvbVwiXSxyPXBhcnNlRmxvYXQobikscz1wYXJzZUZsb2F0KG8pLGE9dGhpcy5sYXlvdXQuc2l6ZTstMSE9bi5pbmRleE9mKFwiJVwiKSYmKHI9ci8xMDAqYS53aWR0aCksLTEhPW8uaW5kZXhPZihcIiVcIikmJihzPXMvMTAwKmEuaGVpZ2h0KSxyPWlzTmFOKHIpPzA6cixzPWlzTmFOKHMpPzA6cyxyLT1lP2EucGFkZGluZ0xlZnQ6YS5wYWRkaW5nUmlnaHQscy09aT9hLnBhZGRpbmdUb3A6YS5wYWRkaW5nQm90dG9tLHRoaXMucG9zaXRpb24ueD1yLHRoaXMucG9zaXRpb24ueT1zfSxkLmxheW91dFBvc2l0aW9uPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5sYXlvdXQuc2l6ZSxlPXt9LGk9dGhpcy5sYXlvdXQuX2dldE9wdGlvbihcIm9yaWdpbkxlZnRcIiksbj10aGlzLmxheW91dC5fZ2V0T3B0aW9uKFwib3JpZ2luVG9wXCIpLG89aT9cInBhZGRpbmdMZWZ0XCI6XCJwYWRkaW5nUmlnaHRcIixyPWk/XCJsZWZ0XCI6XCJyaWdodFwiLHM9aT9cInJpZ2h0XCI6XCJsZWZ0XCIsYT10aGlzLnBvc2l0aW9uLngrdFtvXTtlW3JdPXRoaXMuZ2V0WFZhbHVlKGEpLGVbc109XCJcIjt2YXIgaD1uP1wicGFkZGluZ1RvcFwiOlwicGFkZGluZ0JvdHRvbVwiLHU9bj9cInRvcFwiOlwiYm90dG9tXCIsZD1uP1wiYm90dG9tXCI6XCJ0b3BcIixsPXRoaXMucG9zaXRpb24ueSt0W2hdO2VbdV09dGhpcy5nZXRZVmFsdWUobCksZVtkXT1cIlwiLHRoaXMuY3NzKGUpLHRoaXMuZW1pdEV2ZW50KFwibGF5b3V0XCIsW3RoaXNdKX0sZC5nZXRYVmFsdWU9ZnVuY3Rpb24odCl7dmFyIGU9dGhpcy5sYXlvdXQuX2dldE9wdGlvbihcImhvcml6b250YWxcIik7cmV0dXJuIHRoaXMubGF5b3V0Lm9wdGlvbnMucGVyY2VudFBvc2l0aW9uJiYhZT90L3RoaXMubGF5b3V0LnNpemUud2lkdGgqMTAwK1wiJVwiOnQrXCJweFwifSxkLmdldFlWYWx1ZT1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmxheW91dC5fZ2V0T3B0aW9uKFwiaG9yaXpvbnRhbFwiKTtyZXR1cm4gdGhpcy5sYXlvdXQub3B0aW9ucy5wZXJjZW50UG9zaXRpb24mJmU/dC90aGlzLmxheW91dC5zaXplLmhlaWdodCoxMDArXCIlXCI6dCtcInB4XCJ9LGQuX3RyYW5zaXRpb25Ubz1mdW5jdGlvbih0LGUpe3RoaXMuZ2V0UG9zaXRpb24oKTt2YXIgaT10aGlzLnBvc2l0aW9uLngsbj10aGlzLnBvc2l0aW9uLnksbz10PT10aGlzLnBvc2l0aW9uLngmJmU9PXRoaXMucG9zaXRpb24ueTtpZih0aGlzLnNldFBvc2l0aW9uKHQsZSksbyYmIXRoaXMuaXNUcmFuc2l0aW9uaW5nKXJldHVybiB2b2lkIHRoaXMubGF5b3V0UG9zaXRpb24oKTt2YXIgcj10LWkscz1lLW4sYT17fTthLnRyYW5zZm9ybT10aGlzLmdldFRyYW5zbGF0ZShyLHMpLHRoaXMudHJhbnNpdGlvbih7dG86YSxvblRyYW5zaXRpb25FbmQ6e3RyYW5zZm9ybTp0aGlzLmxheW91dFBvc2l0aW9ufSxpc0NsZWFuaW5nOiEwfSl9LGQuZ2V0VHJhbnNsYXRlPWZ1bmN0aW9uKHQsZSl7dmFyIGk9dGhpcy5sYXlvdXQuX2dldE9wdGlvbihcIm9yaWdpbkxlZnRcIiksbj10aGlzLmxheW91dC5fZ2V0T3B0aW9uKFwib3JpZ2luVG9wXCIpO3JldHVybiB0PWk/dDotdCxlPW4/ZTotZSxcInRyYW5zbGF0ZTNkKFwiK3QrXCJweCwgXCIrZStcInB4LCAwKVwifSxkLmdvVG89ZnVuY3Rpb24odCxlKXt0aGlzLnNldFBvc2l0aW9uKHQsZSksdGhpcy5sYXlvdXRQb3NpdGlvbigpfSxkLm1vdmVUbz1kLl90cmFuc2l0aW9uVG8sZC5zZXRQb3NpdGlvbj1mdW5jdGlvbih0LGUpe3RoaXMucG9zaXRpb24ueD1wYXJzZUZsb2F0KHQpLHRoaXMucG9zaXRpb24ueT1wYXJzZUZsb2F0KGUpfSxkLl9ub25UcmFuc2l0aW9uPWZ1bmN0aW9uKHQpe3RoaXMuY3NzKHQudG8pLHQuaXNDbGVhbmluZyYmdGhpcy5fcmVtb3ZlU3R5bGVzKHQudG8pO2Zvcih2YXIgZSBpbiB0Lm9uVHJhbnNpdGlvbkVuZCl0Lm9uVHJhbnNpdGlvbkVuZFtlXS5jYWxsKHRoaXMpfSxkLnRyYW5zaXRpb249ZnVuY3Rpb24odCl7aWYoIXBhcnNlRmxvYXQodGhpcy5sYXlvdXQub3B0aW9ucy50cmFuc2l0aW9uRHVyYXRpb24pKXJldHVybiB2b2lkIHRoaXMuX25vblRyYW5zaXRpb24odCk7dmFyIGU9dGhpcy5fdHJhbnNuO2Zvcih2YXIgaSBpbiB0Lm9uVHJhbnNpdGlvbkVuZCllLm9uRW5kW2ldPXQub25UcmFuc2l0aW9uRW5kW2ldO2ZvcihpIGluIHQudG8pZS5pbmdQcm9wZXJ0aWVzW2ldPSEwLHQuaXNDbGVhbmluZyYmKGUuY2xlYW5baV09ITApO2lmKHQuZnJvbSl7dGhpcy5jc3ModC5mcm9tKTt2YXIgbj10aGlzLmVsZW1lbnQub2Zmc2V0SGVpZ2h0O249bnVsbH10aGlzLmVuYWJsZVRyYW5zaXRpb24odC50byksdGhpcy5jc3ModC50byksdGhpcy5pc1RyYW5zaXRpb25pbmc9ITB9O3ZhciBsPVwib3BhY2l0eSxcIitvKGEpO2QuZW5hYmxlVHJhbnNpdGlvbj1mdW5jdGlvbigpe2lmKCF0aGlzLmlzVHJhbnNpdGlvbmluZyl7dmFyIHQ9dGhpcy5sYXlvdXQub3B0aW9ucy50cmFuc2l0aW9uRHVyYXRpb247dD1cIm51bWJlclwiPT10eXBlb2YgdD90K1wibXNcIjp0LHRoaXMuY3NzKHt0cmFuc2l0aW9uUHJvcGVydHk6bCx0cmFuc2l0aW9uRHVyYXRpb246dCx0cmFuc2l0aW9uRGVsYXk6dGhpcy5zdGFnZ2VyRGVsYXl8fDB9KSx0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihoLHRoaXMsITEpfX0sZC5vbndlYmtpdFRyYW5zaXRpb25FbmQ9ZnVuY3Rpb24odCl7dGhpcy5vbnRyYW5zaXRpb25lbmQodCl9LGQub25vdHJhbnNpdGlvbmVuZD1mdW5jdGlvbih0KXt0aGlzLm9udHJhbnNpdGlvbmVuZCh0KX07dmFyIGM9e1wiLXdlYmtpdC10cmFuc2Zvcm1cIjpcInRyYW5zZm9ybVwifTtkLm9udHJhbnNpdGlvbmVuZD1mdW5jdGlvbih0KXtpZih0LnRhcmdldD09PXRoaXMuZWxlbWVudCl7dmFyIGU9dGhpcy5fdHJhbnNuLG49Y1t0LnByb3BlcnR5TmFtZV18fHQucHJvcGVydHlOYW1lO2lmKGRlbGV0ZSBlLmluZ1Byb3BlcnRpZXNbbl0saShlLmluZ1Byb3BlcnRpZXMpJiZ0aGlzLmRpc2FibGVUcmFuc2l0aW9uKCksbiBpbiBlLmNsZWFuJiYodGhpcy5lbGVtZW50LnN0eWxlW3QucHJvcGVydHlOYW1lXT1cIlwiLGRlbGV0ZSBlLmNsZWFuW25dKSxuIGluIGUub25FbmQpe3ZhciBvPWUub25FbmRbbl07by5jYWxsKHRoaXMpLGRlbGV0ZSBlLm9uRW5kW25dfXRoaXMuZW1pdEV2ZW50KFwidHJhbnNpdGlvbkVuZFwiLFt0aGlzXSl9fSxkLmRpc2FibGVUcmFuc2l0aW9uPWZ1bmN0aW9uKCl7dGhpcy5yZW1vdmVUcmFuc2l0aW9uU3R5bGVzKCksdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoaCx0aGlzLCExKSx0aGlzLmlzVHJhbnNpdGlvbmluZz0hMX0sZC5fcmVtb3ZlU3R5bGVzPWZ1bmN0aW9uKHQpe3ZhciBlPXt9O2Zvcih2YXIgaSBpbiB0KWVbaV09XCJcIjt0aGlzLmNzcyhlKX07dmFyIGY9e3RyYW5zaXRpb25Qcm9wZXJ0eTpcIlwiLHRyYW5zaXRpb25EdXJhdGlvbjpcIlwiLHRyYW5zaXRpb25EZWxheTpcIlwifTtyZXR1cm4gZC5yZW1vdmVUcmFuc2l0aW9uU3R5bGVzPWZ1bmN0aW9uKCl7dGhpcy5jc3MoZil9LGQuc3RhZ2dlcj1mdW5jdGlvbih0KXt0PWlzTmFOKHQpPzA6dCx0aGlzLnN0YWdnZXJEZWxheT10K1wibXNcIn0sZC5yZW1vdmVFbGVtPWZ1bmN0aW9uKCl7dGhpcy5lbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5lbGVtZW50KSx0aGlzLmNzcyh7ZGlzcGxheTpcIlwifSksdGhpcy5lbWl0RXZlbnQoXCJyZW1vdmVcIixbdGhpc10pfSxkLnJlbW92ZT1mdW5jdGlvbigpe3JldHVybiBzJiZwYXJzZUZsb2F0KHRoaXMubGF5b3V0Lm9wdGlvbnMudHJhbnNpdGlvbkR1cmF0aW9uKT8odGhpcy5vbmNlKFwidHJhbnNpdGlvbkVuZFwiLGZ1bmN0aW9uKCl7dGhpcy5yZW1vdmVFbGVtKCl9KSx2b2lkIHRoaXMuaGlkZSgpKTp2b2lkIHRoaXMucmVtb3ZlRWxlbSgpfSxkLnJldmVhbD1mdW5jdGlvbigpe2RlbGV0ZSB0aGlzLmlzSGlkZGVuLHRoaXMuY3NzKHtkaXNwbGF5OlwiXCJ9KTt2YXIgdD10aGlzLmxheW91dC5vcHRpb25zLGU9e30saT10aGlzLmdldEhpZGVSZXZlYWxUcmFuc2l0aW9uRW5kUHJvcGVydHkoXCJ2aXNpYmxlU3R5bGVcIik7ZVtpXT10aGlzLm9uUmV2ZWFsVHJhbnNpdGlvbkVuZCx0aGlzLnRyYW5zaXRpb24oe2Zyb206dC5oaWRkZW5TdHlsZSx0bzp0LnZpc2libGVTdHlsZSxpc0NsZWFuaW5nOiEwLG9uVHJhbnNpdGlvbkVuZDplfSl9LGQub25SZXZlYWxUcmFuc2l0aW9uRW5kPWZ1bmN0aW9uKCl7dGhpcy5pc0hpZGRlbnx8dGhpcy5lbWl0RXZlbnQoXCJyZXZlYWxcIil9LGQuZ2V0SGlkZVJldmVhbFRyYW5zaXRpb25FbmRQcm9wZXJ0eT1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmxheW91dC5vcHRpb25zW3RdO2lmKGUub3BhY2l0eSlyZXR1cm5cIm9wYWNpdHlcIjtmb3IodmFyIGkgaW4gZSlyZXR1cm4gaX0sZC5oaWRlPWZ1bmN0aW9uKCl7dGhpcy5pc0hpZGRlbj0hMCx0aGlzLmNzcyh7ZGlzcGxheTpcIlwifSk7dmFyIHQ9dGhpcy5sYXlvdXQub3B0aW9ucyxlPXt9LGk9dGhpcy5nZXRIaWRlUmV2ZWFsVHJhbnNpdGlvbkVuZFByb3BlcnR5KFwiaGlkZGVuU3R5bGVcIik7ZVtpXT10aGlzLm9uSGlkZVRyYW5zaXRpb25FbmQsdGhpcy50cmFuc2l0aW9uKHtmcm9tOnQudmlzaWJsZVN0eWxlLHRvOnQuaGlkZGVuU3R5bGUsaXNDbGVhbmluZzohMCxvblRyYW5zaXRpb25FbmQ6ZX0pfSxkLm9uSGlkZVRyYW5zaXRpb25FbmQ9ZnVuY3Rpb24oKXt0aGlzLmlzSGlkZGVuJiYodGhpcy5jc3Moe2Rpc3BsYXk6XCJub25lXCJ9KSx0aGlzLmVtaXRFdmVudChcImhpZGVcIikpfSxkLmRlc3Ryb3k9ZnVuY3Rpb24oKXt0aGlzLmNzcyh7cG9zaXRpb246XCJcIixsZWZ0OlwiXCIscmlnaHQ6XCJcIix0b3A6XCJcIixib3R0b206XCJcIix0cmFuc2l0aW9uOlwiXCIsdHJhbnNmb3JtOlwiXCJ9KX0sbn0pLGZ1bmN0aW9uKHQsZSl7XCJ1c2Ugc3RyaWN0XCI7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShcIm91dGxheWVyL291dGxheWVyXCIsW1wiZXYtZW1pdHRlci9ldi1lbWl0dGVyXCIsXCJnZXQtc2l6ZS9nZXQtc2l6ZVwiLFwiZml6enktdWktdXRpbHMvdXRpbHNcIixcIi4vaXRlbVwiXSxmdW5jdGlvbihpLG4sbyxyKXtyZXR1cm4gZSh0LGksbixvLHIpfSk6XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9ZSh0LHJlcXVpcmUoXCJldi1lbWl0dGVyXCIpLHJlcXVpcmUoXCJnZXQtc2l6ZVwiKSxyZXF1aXJlKFwiZml6enktdWktdXRpbHNcIikscmVxdWlyZShcIi4vaXRlbVwiKSk6dC5PdXRsYXllcj1lKHQsdC5FdkVtaXR0ZXIsdC5nZXRTaXplLHQuZml6enlVSVV0aWxzLHQuT3V0bGF5ZXIuSXRlbSl9KHdpbmRvdyxmdW5jdGlvbih0LGUsaSxuLG8pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIHIodCxlKXt2YXIgaT1uLmdldFF1ZXJ5RWxlbWVudCh0KTtpZighaSlyZXR1cm4gdm9pZChoJiZoLmVycm9yKFwiQmFkIGVsZW1lbnQgZm9yIFwiK3RoaXMuY29uc3RydWN0b3IubmFtZXNwYWNlK1wiOiBcIisoaXx8dCkpKTt0aGlzLmVsZW1lbnQ9aSx1JiYodGhpcy4kZWxlbWVudD11KHRoaXMuZWxlbWVudCkpLHRoaXMub3B0aW9ucz1uLmV4dGVuZCh7fSx0aGlzLmNvbnN0cnVjdG9yLmRlZmF1bHRzKSx0aGlzLm9wdGlvbihlKTt2YXIgbz0rK2w7dGhpcy5lbGVtZW50Lm91dGxheWVyR1VJRD1vLGNbb109dGhpcyx0aGlzLl9jcmVhdGUoKTt2YXIgcj10aGlzLl9nZXRPcHRpb24oXCJpbml0TGF5b3V0XCIpO3ImJnRoaXMubGF5b3V0KCl9ZnVuY3Rpb24gcyh0KXtmdW5jdGlvbiBlKCl7dC5hcHBseSh0aGlzLGFyZ3VtZW50cyl9cmV0dXJuIGUucHJvdG90eXBlPU9iamVjdC5jcmVhdGUodC5wcm90b3R5cGUpLGUucHJvdG90eXBlLmNvbnN0cnVjdG9yPWUsZX1mdW5jdGlvbiBhKHQpe2lmKFwibnVtYmVyXCI9PXR5cGVvZiB0KXJldHVybiB0O3ZhciBlPXQubWF0Y2goLyheXFxkKlxcLj9cXGQqKShcXHcqKS8pLGk9ZSYmZVsxXSxuPWUmJmVbMl07aWYoIWkubGVuZ3RoKXJldHVybiAwO2k9cGFyc2VGbG9hdChpKTt2YXIgbz1tW25dfHwxO3JldHVybiBpKm99dmFyIGg9dC5jb25zb2xlLHU9dC5qUXVlcnksZD1mdW5jdGlvbigpe30sbD0wLGM9e307ci5uYW1lc3BhY2U9XCJvdXRsYXllclwiLHIuSXRlbT1vLHIuZGVmYXVsdHM9e2NvbnRhaW5lclN0eWxlOntwb3NpdGlvbjpcInJlbGF0aXZlXCJ9LGluaXRMYXlvdXQ6ITAsb3JpZ2luTGVmdDohMCxvcmlnaW5Ub3A6ITAscmVzaXplOiEwLHJlc2l6ZUNvbnRhaW5lcjohMCx0cmFuc2l0aW9uRHVyYXRpb246XCIwLjRzXCIsaGlkZGVuU3R5bGU6e29wYWNpdHk6MCx0cmFuc2Zvcm06XCJzY2FsZSgwLjAwMSlcIn0sdmlzaWJsZVN0eWxlOntvcGFjaXR5OjEsdHJhbnNmb3JtOlwic2NhbGUoMSlcIn19O3ZhciBmPXIucHJvdG90eXBlO24uZXh0ZW5kKGYsZS5wcm90b3R5cGUpLGYub3B0aW9uPWZ1bmN0aW9uKHQpe24uZXh0ZW5kKHRoaXMub3B0aW9ucyx0KX0sZi5fZ2V0T3B0aW9uPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuY29uc3RydWN0b3IuY29tcGF0T3B0aW9uc1t0XTtyZXR1cm4gZSYmdm9pZCAwIT09dGhpcy5vcHRpb25zW2VdP3RoaXMub3B0aW9uc1tlXTp0aGlzLm9wdGlvbnNbdF19LHIuY29tcGF0T3B0aW9ucz17aW5pdExheW91dDpcImlzSW5pdExheW91dFwiLGhvcml6b250YWw6XCJpc0hvcml6b250YWxcIixsYXlvdXRJbnN0YW50OlwiaXNMYXlvdXRJbnN0YW50XCIsb3JpZ2luTGVmdDpcImlzT3JpZ2luTGVmdFwiLG9yaWdpblRvcDpcImlzT3JpZ2luVG9wXCIscmVzaXplOlwiaXNSZXNpemVCb3VuZFwiLHJlc2l6ZUNvbnRhaW5lcjpcImlzUmVzaXppbmdDb250YWluZXJcIn0sZi5fY3JlYXRlPWZ1bmN0aW9uKCl7dGhpcy5yZWxvYWRJdGVtcygpLHRoaXMuc3RhbXBzPVtdLHRoaXMuc3RhbXAodGhpcy5vcHRpb25zLnN0YW1wKSxuLmV4dGVuZCh0aGlzLmVsZW1lbnQuc3R5bGUsdGhpcy5vcHRpb25zLmNvbnRhaW5lclN0eWxlKTt2YXIgdD10aGlzLl9nZXRPcHRpb24oXCJyZXNpemVcIik7dCYmdGhpcy5iaW5kUmVzaXplKCl9LGYucmVsb2FkSXRlbXM9ZnVuY3Rpb24oKXt0aGlzLml0ZW1zPXRoaXMuX2l0ZW1pemUodGhpcy5lbGVtZW50LmNoaWxkcmVuKX0sZi5faXRlbWl6ZT1mdW5jdGlvbih0KXtmb3IodmFyIGU9dGhpcy5fZmlsdGVyRmluZEl0ZW1FbGVtZW50cyh0KSxpPXRoaXMuY29uc3RydWN0b3IuSXRlbSxuPVtdLG89MDtvPGUubGVuZ3RoO28rKyl7dmFyIHI9ZVtvXSxzPW5ldyBpKHIsdGhpcyk7bi5wdXNoKHMpfXJldHVybiBufSxmLl9maWx0ZXJGaW5kSXRlbUVsZW1lbnRzPWZ1bmN0aW9uKHQpe3JldHVybiBuLmZpbHRlckZpbmRFbGVtZW50cyh0LHRoaXMub3B0aW9ucy5pdGVtU2VsZWN0b3IpfSxmLmdldEl0ZW1FbGVtZW50cz1mdW5jdGlvbigpe3JldHVybiB0aGlzLml0ZW1zLm1hcChmdW5jdGlvbih0KXtyZXR1cm4gdC5lbGVtZW50fSl9LGYubGF5b3V0PWZ1bmN0aW9uKCl7dGhpcy5fcmVzZXRMYXlvdXQoKSx0aGlzLl9tYW5hZ2VTdGFtcHMoKTt2YXIgdD10aGlzLl9nZXRPcHRpb24oXCJsYXlvdXRJbnN0YW50XCIpLGU9dm9pZCAwIT09dD90OiF0aGlzLl9pc0xheW91dEluaXRlZDt0aGlzLmxheW91dEl0ZW1zKHRoaXMuaXRlbXMsZSksdGhpcy5faXNMYXlvdXRJbml0ZWQ9ITB9LGYuX2luaXQ9Zi5sYXlvdXQsZi5fcmVzZXRMYXlvdXQ9ZnVuY3Rpb24oKXt0aGlzLmdldFNpemUoKX0sZi5nZXRTaXplPWZ1bmN0aW9uKCl7dGhpcy5zaXplPWkodGhpcy5lbGVtZW50KX0sZi5fZ2V0TWVhc3VyZW1lbnQ9ZnVuY3Rpb24odCxlKXt2YXIgbixvPXRoaXMub3B0aW9uc1t0XTtvPyhcInN0cmluZ1wiPT10eXBlb2Ygbz9uPXRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKG8pOm8gaW5zdGFuY2VvZiBIVE1MRWxlbWVudCYmKG49byksdGhpc1t0XT1uP2kobilbZV06byk6dGhpc1t0XT0wfSxmLmxheW91dEl0ZW1zPWZ1bmN0aW9uKHQsZSl7dD10aGlzLl9nZXRJdGVtc0ZvckxheW91dCh0KSx0aGlzLl9sYXlvdXRJdGVtcyh0LGUpLHRoaXMuX3Bvc3RMYXlvdXQoKX0sZi5fZ2V0SXRlbXNGb3JMYXlvdXQ9ZnVuY3Rpb24odCl7cmV0dXJuIHQuZmlsdGVyKGZ1bmN0aW9uKHQpe3JldHVybiF0LmlzSWdub3JlZH0pfSxmLl9sYXlvdXRJdGVtcz1mdW5jdGlvbih0LGUpe2lmKHRoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJsYXlvdXRcIix0KSx0JiZ0Lmxlbmd0aCl7dmFyIGk9W107dC5mb3JFYWNoKGZ1bmN0aW9uKHQpe3ZhciBuPXRoaXMuX2dldEl0ZW1MYXlvdXRQb3NpdGlvbih0KTtuLml0ZW09dCxuLmlzSW5zdGFudD1lfHx0LmlzTGF5b3V0SW5zdGFudCxpLnB1c2gobil9LHRoaXMpLHRoaXMuX3Byb2Nlc3NMYXlvdXRRdWV1ZShpKX19LGYuX2dldEl0ZW1MYXlvdXRQb3NpdGlvbj1mdW5jdGlvbigpe3JldHVybnt4OjAseTowfX0sZi5fcHJvY2Vzc0xheW91dFF1ZXVlPWZ1bmN0aW9uKHQpe3RoaXMudXBkYXRlU3RhZ2dlcigpLHQuZm9yRWFjaChmdW5jdGlvbih0LGUpe3RoaXMuX3Bvc2l0aW9uSXRlbSh0Lml0ZW0sdC54LHQueSx0LmlzSW5zdGFudCxlKX0sdGhpcyl9LGYudXBkYXRlU3RhZ2dlcj1mdW5jdGlvbigpe3ZhciB0PXRoaXMub3B0aW9ucy5zdGFnZ2VyO3JldHVybiBudWxsPT09dHx8dm9pZCAwPT09dD92b2lkKHRoaXMuc3RhZ2dlcj0wKToodGhpcy5zdGFnZ2VyPWEodCksdGhpcy5zdGFnZ2VyKX0sZi5fcG9zaXRpb25JdGVtPWZ1bmN0aW9uKHQsZSxpLG4sbyl7bj90LmdvVG8oZSxpKToodC5zdGFnZ2VyKG8qdGhpcy5zdGFnZ2VyKSx0Lm1vdmVUbyhlLGkpKX0sZi5fcG9zdExheW91dD1mdW5jdGlvbigpe3RoaXMucmVzaXplQ29udGFpbmVyKCl9LGYucmVzaXplQ29udGFpbmVyPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5fZ2V0T3B0aW9uKFwicmVzaXplQ29udGFpbmVyXCIpO2lmKHQpe3ZhciBlPXRoaXMuX2dldENvbnRhaW5lclNpemUoKTtlJiYodGhpcy5fc2V0Q29udGFpbmVyTWVhc3VyZShlLndpZHRoLCEwKSx0aGlzLl9zZXRDb250YWluZXJNZWFzdXJlKGUuaGVpZ2h0LCExKSl9fSxmLl9nZXRDb250YWluZXJTaXplPWQsZi5fc2V0Q29udGFpbmVyTWVhc3VyZT1mdW5jdGlvbih0LGUpe2lmKHZvaWQgMCE9PXQpe3ZhciBpPXRoaXMuc2l6ZTtpLmlzQm9yZGVyQm94JiYodCs9ZT9pLnBhZGRpbmdMZWZ0K2kucGFkZGluZ1JpZ2h0K2kuYm9yZGVyTGVmdFdpZHRoK2kuYm9yZGVyUmlnaHRXaWR0aDppLnBhZGRpbmdCb3R0b20raS5wYWRkaW5nVG9wK2kuYm9yZGVyVG9wV2lkdGgraS5ib3JkZXJCb3R0b21XaWR0aCksdD1NYXRoLm1heCh0LDApLHRoaXMuZWxlbWVudC5zdHlsZVtlP1wid2lkdGhcIjpcImhlaWdodFwiXT10K1wicHhcIn19LGYuX2VtaXRDb21wbGV0ZU9uSXRlbXM9ZnVuY3Rpb24odCxlKXtmdW5jdGlvbiBpKCl7by5kaXNwYXRjaEV2ZW50KHQrXCJDb21wbGV0ZVwiLG51bGwsW2VdKX1mdW5jdGlvbiBuKCl7cysrLHM9PXImJmkoKX12YXIgbz10aGlzLHI9ZS5sZW5ndGg7aWYoIWV8fCFyKXJldHVybiB2b2lkIGkoKTt2YXIgcz0wO2UuZm9yRWFjaChmdW5jdGlvbihlKXtlLm9uY2UodCxuKX0pfSxmLmRpc3BhdGNoRXZlbnQ9ZnVuY3Rpb24odCxlLGkpe3ZhciBuPWU/W2VdLmNvbmNhdChpKTppO2lmKHRoaXMuZW1pdEV2ZW50KHQsbiksdSlpZih0aGlzLiRlbGVtZW50PXRoaXMuJGVsZW1lbnR8fHUodGhpcy5lbGVtZW50KSxlKXt2YXIgbz11LkV2ZW50KGUpO28udHlwZT10LHRoaXMuJGVsZW1lbnQudHJpZ2dlcihvLGkpfWVsc2UgdGhpcy4kZWxlbWVudC50cmlnZ2VyKHQsaSl9LGYuaWdub3JlPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuZ2V0SXRlbSh0KTtlJiYoZS5pc0lnbm9yZWQ9ITApfSxmLnVuaWdub3JlPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuZ2V0SXRlbSh0KTtlJiZkZWxldGUgZS5pc0lnbm9yZWR9LGYuc3RhbXA9ZnVuY3Rpb24odCl7dD10aGlzLl9maW5kKHQpLHQmJih0aGlzLnN0YW1wcz10aGlzLnN0YW1wcy5jb25jYXQodCksdC5mb3JFYWNoKHRoaXMuaWdub3JlLHRoaXMpKX0sZi51bnN0YW1wPWZ1bmN0aW9uKHQpe3Q9dGhpcy5fZmluZCh0KSx0JiZ0LmZvckVhY2goZnVuY3Rpb24odCl7bi5yZW1vdmVGcm9tKHRoaXMuc3RhbXBzLHQpLHRoaXMudW5pZ25vcmUodCl9LHRoaXMpfSxmLl9maW5kPWZ1bmN0aW9uKHQpe3JldHVybiB0PyhcInN0cmluZ1wiPT10eXBlb2YgdCYmKHQ9dGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwodCkpLHQ9bi5tYWtlQXJyYXkodCkpOnZvaWQgMH0sZi5fbWFuYWdlU3RhbXBzPWZ1bmN0aW9uKCl7dGhpcy5zdGFtcHMmJnRoaXMuc3RhbXBzLmxlbmd0aCYmKHRoaXMuX2dldEJvdW5kaW5nUmVjdCgpLHRoaXMuc3RhbXBzLmZvckVhY2godGhpcy5fbWFuYWdlU3RhbXAsdGhpcykpfSxmLl9nZXRCb3VuZGluZ1JlY3Q9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksZT10aGlzLnNpemU7dGhpcy5fYm91bmRpbmdSZWN0PXtsZWZ0OnQubGVmdCtlLnBhZGRpbmdMZWZ0K2UuYm9yZGVyTGVmdFdpZHRoLHRvcDp0LnRvcCtlLnBhZGRpbmdUb3ArZS5ib3JkZXJUb3BXaWR0aCxyaWdodDp0LnJpZ2h0LShlLnBhZGRpbmdSaWdodCtlLmJvcmRlclJpZ2h0V2lkdGgpLGJvdHRvbTp0LmJvdHRvbS0oZS5wYWRkaW5nQm90dG9tK2UuYm9yZGVyQm90dG9tV2lkdGgpfX0sZi5fbWFuYWdlU3RhbXA9ZCxmLl9nZXRFbGVtZW50T2Zmc2V0PWZ1bmN0aW9uKHQpe3ZhciBlPXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksbj10aGlzLl9ib3VuZGluZ1JlY3Qsbz1pKHQpLHI9e2xlZnQ6ZS5sZWZ0LW4ubGVmdC1vLm1hcmdpbkxlZnQsdG9wOmUudG9wLW4udG9wLW8ubWFyZ2luVG9wLHJpZ2h0Om4ucmlnaHQtZS5yaWdodC1vLm1hcmdpblJpZ2h0LGJvdHRvbTpuLmJvdHRvbS1lLmJvdHRvbS1vLm1hcmdpbkJvdHRvbX07cmV0dXJuIHJ9LGYuaGFuZGxlRXZlbnQ9bi5oYW5kbGVFdmVudCxmLmJpbmRSZXNpemU9ZnVuY3Rpb24oKXt0LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIix0aGlzKSx0aGlzLmlzUmVzaXplQm91bmQ9ITB9LGYudW5iaW5kUmVzaXplPWZ1bmN0aW9uKCl7dC5yZW1vdmVFdmVudExpc3RlbmVyKFwicmVzaXplXCIsdGhpcyksdGhpcy5pc1Jlc2l6ZUJvdW5kPSExfSxmLm9ucmVzaXplPWZ1bmN0aW9uKCl7dGhpcy5yZXNpemUoKX0sbi5kZWJvdW5jZU1ldGhvZChyLFwib25yZXNpemVcIiwxMDApLGYucmVzaXplPWZ1bmN0aW9uKCl7dGhpcy5pc1Jlc2l6ZUJvdW5kJiZ0aGlzLm5lZWRzUmVzaXplTGF5b3V0KCkmJnRoaXMubGF5b3V0KCl9LGYubmVlZHNSZXNpemVMYXlvdXQ9ZnVuY3Rpb24oKXt2YXIgdD1pKHRoaXMuZWxlbWVudCksZT10aGlzLnNpemUmJnQ7cmV0dXJuIGUmJnQuaW5uZXJXaWR0aCE9PXRoaXMuc2l6ZS5pbm5lcldpZHRofSxmLmFkZEl0ZW1zPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuX2l0ZW1pemUodCk7cmV0dXJuIGUubGVuZ3RoJiYodGhpcy5pdGVtcz10aGlzLml0ZW1zLmNvbmNhdChlKSksZX0sZi5hcHBlbmRlZD1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmFkZEl0ZW1zKHQpO2UubGVuZ3RoJiYodGhpcy5sYXlvdXRJdGVtcyhlLCEwKSx0aGlzLnJldmVhbChlKSl9LGYucHJlcGVuZGVkPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuX2l0ZW1pemUodCk7aWYoZS5sZW5ndGgpe3ZhciBpPXRoaXMuaXRlbXMuc2xpY2UoMCk7dGhpcy5pdGVtcz1lLmNvbmNhdChpKSx0aGlzLl9yZXNldExheW91dCgpLHRoaXMuX21hbmFnZVN0YW1wcygpLHRoaXMubGF5b3V0SXRlbXMoZSwhMCksdGhpcy5yZXZlYWwoZSksdGhpcy5sYXlvdXRJdGVtcyhpKX19LGYucmV2ZWFsPWZ1bmN0aW9uKHQpe2lmKHRoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJyZXZlYWxcIix0KSx0JiZ0Lmxlbmd0aCl7dmFyIGU9dGhpcy51cGRhdGVTdGFnZ2VyKCk7dC5mb3JFYWNoKGZ1bmN0aW9uKHQsaSl7dC5zdGFnZ2VyKGkqZSksdC5yZXZlYWwoKX0pfX0sZi5oaWRlPWZ1bmN0aW9uKHQpe2lmKHRoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJoaWRlXCIsdCksdCYmdC5sZW5ndGgpe3ZhciBlPXRoaXMudXBkYXRlU3RhZ2dlcigpO3QuZm9yRWFjaChmdW5jdGlvbih0LGkpe3Quc3RhZ2dlcihpKmUpLHQuaGlkZSgpfSl9fSxmLnJldmVhbEl0ZW1FbGVtZW50cz1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmdldEl0ZW1zKHQpO3RoaXMucmV2ZWFsKGUpfSxmLmhpZGVJdGVtRWxlbWVudHM9ZnVuY3Rpb24odCl7dmFyIGU9dGhpcy5nZXRJdGVtcyh0KTt0aGlzLmhpZGUoZSl9LGYuZ2V0SXRlbT1mdW5jdGlvbih0KXtmb3IodmFyIGU9MDtlPHRoaXMuaXRlbXMubGVuZ3RoO2UrKyl7dmFyIGk9dGhpcy5pdGVtc1tlXTtpZihpLmVsZW1lbnQ9PXQpcmV0dXJuIGl9fSxmLmdldEl0ZW1zPWZ1bmN0aW9uKHQpe3Q9bi5tYWtlQXJyYXkodCk7dmFyIGU9W107cmV0dXJuIHQuZm9yRWFjaChmdW5jdGlvbih0KXt2YXIgaT10aGlzLmdldEl0ZW0odCk7aSYmZS5wdXNoKGkpfSx0aGlzKSxlfSxmLnJlbW92ZT1mdW5jdGlvbih0KXt2YXIgZT10aGlzLmdldEl0ZW1zKHQpO3RoaXMuX2VtaXRDb21wbGV0ZU9uSXRlbXMoXCJyZW1vdmVcIixlKSxlJiZlLmxlbmd0aCYmZS5mb3JFYWNoKGZ1bmN0aW9uKHQpe3QucmVtb3ZlKCksbi5yZW1vdmVGcm9tKHRoaXMuaXRlbXMsdCl9LHRoaXMpfSxmLmRlc3Ryb3k9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmVsZW1lbnQuc3R5bGU7dC5oZWlnaHQ9XCJcIix0LnBvc2l0aW9uPVwiXCIsdC53aWR0aD1cIlwiLHRoaXMuaXRlbXMuZm9yRWFjaChmdW5jdGlvbih0KXt0LmRlc3Ryb3koKX0pLHRoaXMudW5iaW5kUmVzaXplKCk7dmFyIGU9dGhpcy5lbGVtZW50Lm91dGxheWVyR1VJRDtkZWxldGUgY1tlXSxkZWxldGUgdGhpcy5lbGVtZW50Lm91dGxheWVyR1VJRCx1JiZ1LnJlbW92ZURhdGEodGhpcy5lbGVtZW50LHRoaXMuY29uc3RydWN0b3IubmFtZXNwYWNlKX0sci5kYXRhPWZ1bmN0aW9uKHQpe3Q9bi5nZXRRdWVyeUVsZW1lbnQodCk7dmFyIGU9dCYmdC5vdXRsYXllckdVSUQ7cmV0dXJuIGUmJmNbZV19LHIuY3JlYXRlPWZ1bmN0aW9uKHQsZSl7dmFyIGk9cyhyKTtyZXR1cm4gaS5kZWZhdWx0cz1uLmV4dGVuZCh7fSxyLmRlZmF1bHRzKSxuLmV4dGVuZChpLmRlZmF1bHRzLGUpLGkuY29tcGF0T3B0aW9ucz1uLmV4dGVuZCh7fSxyLmNvbXBhdE9wdGlvbnMpLGkubmFtZXNwYWNlPXQsaS5kYXRhPXIuZGF0YSxpLkl0ZW09cyhvKSxuLmh0bWxJbml0KGksdCksdSYmdS5icmlkZ2V0JiZ1LmJyaWRnZXQodCxpKSxpfTt2YXIgbT17bXM6MSxzOjFlM307cmV0dXJuIHIuSXRlbT1vLHJ9KSxmdW5jdGlvbih0LGUpe1wiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW1wib3V0bGF5ZXIvb3V0bGF5ZXJcIixcImdldC1zaXplL2dldC1zaXplXCJdLGUpOlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGUmJm1vZHVsZS5leHBvcnRzP21vZHVsZS5leHBvcnRzPWUocmVxdWlyZShcIm91dGxheWVyXCIpLHJlcXVpcmUoXCJnZXQtc2l6ZVwiKSk6dC5NYXNvbnJ5PWUodC5PdXRsYXllcix0LmdldFNpemUpfSh3aW5kb3csZnVuY3Rpb24odCxlKXt2YXIgaT10LmNyZWF0ZShcIm1hc29ucnlcIik7aS5jb21wYXRPcHRpb25zLmZpdFdpZHRoPVwiaXNGaXRXaWR0aFwiO3ZhciBuPWkucHJvdG90eXBlO3JldHVybiBuLl9yZXNldExheW91dD1mdW5jdGlvbigpe3RoaXMuZ2V0U2l6ZSgpLHRoaXMuX2dldE1lYXN1cmVtZW50KFwiY29sdW1uV2lkdGhcIixcIm91dGVyV2lkdGhcIiksdGhpcy5fZ2V0TWVhc3VyZW1lbnQoXCJndXR0ZXJcIixcIm91dGVyV2lkdGhcIiksdGhpcy5tZWFzdXJlQ29sdW1ucygpLHRoaXMuY29sWXM9W107Zm9yKHZhciB0PTA7dDx0aGlzLmNvbHM7dCsrKXRoaXMuY29sWXMucHVzaCgwKTt0aGlzLm1heFk9MCx0aGlzLmhvcml6b250YWxDb2xJbmRleD0wfSxuLm1lYXN1cmVDb2x1bW5zPWZ1bmN0aW9uKCl7aWYodGhpcy5nZXRDb250YWluZXJXaWR0aCgpLCF0aGlzLmNvbHVtbldpZHRoKXt2YXIgdD10aGlzLml0ZW1zWzBdLGk9dCYmdC5lbGVtZW50O3RoaXMuY29sdW1uV2lkdGg9aSYmZShpKS5vdXRlcldpZHRofHx0aGlzLmNvbnRhaW5lcldpZHRofXZhciBuPXRoaXMuY29sdW1uV2lkdGgrPXRoaXMuZ3V0dGVyLG89dGhpcy5jb250YWluZXJXaWR0aCt0aGlzLmd1dHRlcixyPW8vbixzPW4tbyVuLGE9cyYmMT5zP1wicm91bmRcIjpcImZsb29yXCI7cj1NYXRoW2FdKHIpLHRoaXMuY29scz1NYXRoLm1heChyLDEpfSxuLmdldENvbnRhaW5lcldpZHRoPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5fZ2V0T3B0aW9uKFwiZml0V2lkdGhcIiksaT10P3RoaXMuZWxlbWVudC5wYXJlbnROb2RlOnRoaXMuZWxlbWVudCxuPWUoaSk7dGhpcy5jb250YWluZXJXaWR0aD1uJiZuLmlubmVyV2lkdGh9LG4uX2dldEl0ZW1MYXlvdXRQb3NpdGlvbj1mdW5jdGlvbih0KXt0LmdldFNpemUoKTt2YXIgZT10LnNpemUub3V0ZXJXaWR0aCV0aGlzLmNvbHVtbldpZHRoLGk9ZSYmMT5lP1wicm91bmRcIjpcImNlaWxcIixuPU1hdGhbaV0odC5zaXplLm91dGVyV2lkdGgvdGhpcy5jb2x1bW5XaWR0aCk7bj1NYXRoLm1pbihuLHRoaXMuY29scyk7Zm9yKHZhciBvPXRoaXMub3B0aW9ucy5ob3Jpem9udGFsT3JkZXI/XCJfZ2V0SG9yaXpvbnRhbENvbFBvc2l0aW9uXCI6XCJfZ2V0VG9wQ29sUG9zaXRpb25cIixyPXRoaXNbb10obix0KSxzPXt4OnRoaXMuY29sdW1uV2lkdGgqci5jb2wseTpyLnl9LGE9ci55K3Quc2l6ZS5vdXRlckhlaWdodCxoPW4rci5jb2wsdT1yLmNvbDtoPnU7dSsrKXRoaXMuY29sWXNbdV09YTtyZXR1cm4gc30sbi5fZ2V0VG9wQ29sUG9zaXRpb249ZnVuY3Rpb24odCl7dmFyIGU9dGhpcy5fZ2V0VG9wQ29sR3JvdXAodCksaT1NYXRoLm1pbi5hcHBseShNYXRoLGUpO3JldHVybntjb2w6ZS5pbmRleE9mKGkpLHk6aX19LG4uX2dldFRvcENvbEdyb3VwPWZ1bmN0aW9uKHQpe2lmKDI+dClyZXR1cm4gdGhpcy5jb2xZcztmb3IodmFyIGU9W10saT10aGlzLmNvbHMrMS10LG49MDtpPm47bisrKWVbbl09dGhpcy5fZ2V0Q29sR3JvdXBZKG4sdCk7cmV0dXJuIGV9LG4uX2dldENvbEdyb3VwWT1mdW5jdGlvbih0LGUpe2lmKDI+ZSlyZXR1cm4gdGhpcy5jb2xZc1t0XTt2YXIgaT10aGlzLmNvbFlzLnNsaWNlKHQsdCtlKTtyZXR1cm4gTWF0aC5tYXguYXBwbHkoTWF0aCxpKX0sbi5fZ2V0SG9yaXpvbnRhbENvbFBvc2l0aW9uPWZ1bmN0aW9uKHQsZSl7dmFyIGk9dGhpcy5ob3Jpem9udGFsQ29sSW5kZXgldGhpcy5jb2xzLG49dD4xJiZpK3Q+dGhpcy5jb2xzO2k9bj8wOmk7dmFyIG89ZS5zaXplLm91dGVyV2lkdGgmJmUuc2l6ZS5vdXRlckhlaWdodDtyZXR1cm4gdGhpcy5ob3Jpem9udGFsQ29sSW5kZXg9bz9pK3Q6dGhpcy5ob3Jpem9udGFsQ29sSW5kZXgse2NvbDppLHk6dGhpcy5fZ2V0Q29sR3JvdXBZKGksdCl9fSxuLl9tYW5hZ2VTdGFtcD1mdW5jdGlvbih0KXt2YXIgaT1lKHQpLG49dGhpcy5fZ2V0RWxlbWVudE9mZnNldCh0KSxvPXRoaXMuX2dldE9wdGlvbihcIm9yaWdpbkxlZnRcIikscj1vP24ubGVmdDpuLnJpZ2h0LHM9citpLm91dGVyV2lkdGgsYT1NYXRoLmZsb29yKHIvdGhpcy5jb2x1bW5XaWR0aCk7YT1NYXRoLm1heCgwLGEpO3ZhciBoPU1hdGguZmxvb3Iocy90aGlzLmNvbHVtbldpZHRoKTtoLT1zJXRoaXMuY29sdW1uV2lkdGg/MDoxLGg9TWF0aC5taW4odGhpcy5jb2xzLTEsaCk7Zm9yKHZhciB1PXRoaXMuX2dldE9wdGlvbihcIm9yaWdpblRvcFwiKSxkPSh1P24udG9wOm4uYm90dG9tKStpLm91dGVySGVpZ2h0LGw9YTtoPj1sO2wrKyl0aGlzLmNvbFlzW2xdPU1hdGgubWF4KGQsdGhpcy5jb2xZc1tsXSl9LG4uX2dldENvbnRhaW5lclNpemU9ZnVuY3Rpb24oKXt0aGlzLm1heFk9TWF0aC5tYXguYXBwbHkoTWF0aCx0aGlzLmNvbFlzKTt2YXIgdD17aGVpZ2h0OnRoaXMubWF4WX07cmV0dXJuIHRoaXMuX2dldE9wdGlvbihcImZpdFdpZHRoXCIpJiYodC53aWR0aD10aGlzLl9nZXRDb250YWluZXJGaXRXaWR0aCgpKSx0fSxuLl9nZXRDb250YWluZXJGaXRXaWR0aD1mdW5jdGlvbigpe2Zvcih2YXIgdD0wLGU9dGhpcy5jb2xzOy0tZSYmMD09PXRoaXMuY29sWXNbZV07KXQrKztyZXR1cm4odGhpcy5jb2xzLXQpKnRoaXMuY29sdW1uV2lkdGgtdGhpcy5ndXR0ZXJ9LG4ubmVlZHNSZXNpemVMYXlvdXQ9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmNvbnRhaW5lcldpZHRoO3JldHVybiB0aGlzLmdldENvbnRhaW5lcldpZHRoKCksdCE9dGhpcy5jb250YWluZXJXaWR0aH0saX0pOyIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=