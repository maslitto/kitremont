(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["admin"],{

/***/ "./assets/js/admin.js":
/*!****************************!*\
  !*** ./assets/js/admin.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var toastr_toastr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! toastr/toastr */ "./node_modules/toastr/toastr.js");
/* harmony import */ var toastr_toastr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(toastr_toastr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bootstrap3_dist_js_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap3/dist/js/bootstrap */ "./node_modules/bootstrap3/dist/js/bootstrap.js");
/* harmony import */ var bootstrap3_dist_js_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bootstrap3_dist_js_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _admin_dashboard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin/dashboard */ "./assets/js/admin/dashboard.js");
/* harmony import */ var _admin_gallery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin/gallery */ "./assets/js/admin/gallery.js");
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you require will output into a single css file (app.css in this case)
__webpack_require__(/*! ../scss/admin.scss */ "./assets/scss/admin.scss"); // Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
//const $ = require('jquery');







/***/ }),

/***/ "./assets/js/admin/dashboard.js":
/*!**************************************!*\
  !*** ./assets/js/admin/dashboard.js ***!
  \**************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.function.bind */ "./node_modules/core-js/modules/es.function.bind.js");
/* harmony import */ var core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert_dist_sweetalert_min__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert/dist/sweetalert.min */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert_dist_sweetalert_min__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert_dist_sweetalert_min__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var metismenu_dist_metisMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! metismenu/dist/metisMenu */ "./node_modules/metismenu/dist/metisMenu.js");
/* harmony import */ var metismenu_dist_metisMenu__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(metismenu_dist_metisMenu__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var metismenu_dist_metisMenu_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! metismenu/dist/metisMenu.css */ "./node_modules/metismenu/dist/metisMenu.css");
/* harmony import */ var metismenu_dist_metisMenu_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(metismenu_dist_metisMenu_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var jquery_nestable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jquery-nestable */ "./node_modules/jquery-nestable/jquery.nestable.js");
/* harmony import */ var jquery_nestable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jquery_nestable__WEBPACK_IMPORTED_MODULE_5__);







var dashboard = function () {
  return {
    init: function init() {
      dashboard.initMetisMenu();
      dashboard.initSweetAlert();
      dashboard.smoothlyMenu();
      dashboard.initSideMenuEvents();
      dashboard.nestable();
    },
    initMetisMenu: function initMetisMenu() {
      // MetisMenu
      var sideMenu = $('#side-menu').metisMenu({
        preventDefault: false
      });
      sideMenu.on('shown.metisMenu', function (e) {
        dashboard.menuFixHeight();
      });
    },
    initSweetAlert: function initSweetAlert() {
      $('.js-delete-btn').click(function (e) {
        e.preventDefault();
        var currentElement = $(this);
        swal({
          title: "Точно удалить?",
          text: "Удаленная запись не подлежит восстановлению!",
          icon: "warning",
          buttons: ["Отмена", "Удалить"],
          dangerMode: true
        }).then(function (willDelete) {
          if (willDelete) {
            swal({
              text: "Успешное удаление",
              icon: "info",
              buttons: false
            });
            window.location.href = currentElement.attr('href');
          } else {
            swal("Может и правильно");
          }
        });
      });
      $('.js-clone-btn').click(function (e) {
        e.preventDefault();
        var currentElement = $(this);
        swal({
          title: "Точно копировать?",
          icon: "warning",
          buttons: ["Отмена", "Копировать"],
          dangerMode: true
        }).then(function (willDelete) {
          if (willDelete) {
            swal({
              text: "Успешное копирование",
              icon: "info",
              buttons: false
            });
            window.location.href = currentElement.attr('href');
          } else {
            swal("Может и правильно");
          }
        });
      });
    },
    menuFixHeight: function menuFixHeight() {
      var heightWithoutNavbar = $("body > #wrapper").height() - 62;
      $(".sidebar-panel").css("min-height", heightWithoutNavbar + "px");
      var navbarheight = $('nav.navbar-default').height();
      var wrapperHeight = $('#page-wrapper').height();

      if (navbarheight > wrapperHeight) {
        $('#page-wrapper').css("min-height", navbarheight + "px");
      }

      if (navbarheight < wrapperHeight) {
        $('#page-wrapper').css("min-height", $(window).height() + "px");
      }

      if ($('body').hasClass('fixed-nav')) {
        if (navbarheight > wrapperHeight) {
          $('#page-wrapper').css("min-height", navbarheight + "px");
        } else {
          $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
        }
      }
    },
    smoothlyMenu: function smoothlyMenu() {
      if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide(); // For smoothly turn on menu

        setTimeout(function () {
          $('#side-menu').fadeIn(400);
        }, 200);
      } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(function () {
          $('#side-menu').fadeIn(400);
        }, 100);
      } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
      }
    },
    initSideMenuEvents: function initSideMenuEvents() {
      // Fixed Sidebar
      $(window).bind("load", function () {
        if ($("body").hasClass('fixed-sidebar')) {
          $('.sidebar-collapse').slimScroll({
            height: '100%',
            railOpacity: 0.9
          });
        }
      });
      $(window).bind("load resize scroll", function () {
        // Full height of sidebar
        setTimeout(function () {
          if (!$("body").hasClass('body-small')) {
            dashboard.menuFixHeight();
          }
        });
      }); // Minimalize menu when screen is less than 768px

      $(window).bind("resize", function () {
        if ($(this).width() < 769) {
          $('body').addClass('body-small');
        } else {
          $('body').removeClass('body-small');
        }
      });
    },
    nestable: function nestable() {
      $(document).ready(function () {
        $('#nestable').nestable({
          group: 1
        }).on('change', function (e) {
          $.ajax({
            url: $(this).data('action'),
            method: 'POST',
            dataType: 'json',
            data: {
              tree: $(this).nestable('serialize')
            },
            success: function success(resp) {
              console.log(resp);
            },
            error: function error() {}
          });
        });
        $('.js-nestable-menu button').on('click', function (e) {
          var target = $(e.target),
              action = target.data('action');

          if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
          }

          if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
          }
        });
      });
    }
  };
}();

dashboard.init();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/admin/gallery.js":
/*!************************************!*\
  !*** ./assets/js/admin/gallery.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var dropzone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! dropzone */ "./node_modules/dropzone/dist/dropzone.js");
/* harmony import */ var dropzone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(dropzone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var dropzone_dist_dropzone_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dropzone/dist/dropzone.css */ "./node_modules/dropzone/dist/dropzone.css");
/* harmony import */ var dropzone_dist_dropzone_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(dropzone_dist_dropzone_css__WEBPACK_IMPORTED_MODULE_1__);



var gallery = function () {
  var $gallery = $('.js-gallery');
  return {
    init: function init() {
      gallery.initDropZone();
      gallery.removeImage();
    },
    initDropZone: function initDropZone() {
      var $dropzone = $('.js-dropzone'),
          url = $dropzone.data('url'),
          ownerId = $dropzone.data('owner-id');
      $dropzone.dropzone({
        url: url,
        acceptedFiles: "image/*",
        dictDefaultMessage: 'Перетащите в это окно файлы для загрузки',
        init: function init() {
          this.on('sending', function (file, xhr, formData) {
            formData.append('ownerId', ownerId);
          });
        },
        success: function success(file, response) {
          $gallery.append('<div class="col-sm-1">\n' + '<div class="gallery__image">\n' + '<a href="#" class="js-remove-image gallery__image-remove" data-url="' + response.url + '"><i class="fa fa-close"></i></a>\n' + '<img src="' + response.image + '" alt="" class="img-responsive thumbnail">\n' + '</div>\n' + '</div>');
        }
      });
    },
    removeImage: function removeImage() {
      $(document).on('click', '.js-remove-image', function (e) {
        e.preventDefault();
        var id = $(this).data('id'),
            url = $(this).data('url'),
            $image = $(this).closest('.js-gallery-image');
        $.ajax({
          url: url,
          method: 'POST',
          dataType: 'json',
          data: {
            id: id
          },
          success: function success(response) {
            $image.remove();
          }
        });
      });
    }
  };
}();

gallery.init();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/scss/admin.scss":
/*!********************************!*\
  !*** ./assets/scss/admin.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/js/admin.js","runtime","vendors~admin~front","vendors~admin"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYWRtaW4uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FkbWluL2Rhc2hib2FyZC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYWRtaW4vZ2FsbGVyeS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvc2Nzcy9hZG1pbi5zY3NzIl0sIm5hbWVzIjpbInJlcXVpcmUiLCJkYXNoYm9hcmQiLCJpbml0IiwiaW5pdE1ldGlzTWVudSIsImluaXRTd2VldEFsZXJ0Iiwic21vb3RobHlNZW51IiwiaW5pdFNpZGVNZW51RXZlbnRzIiwibmVzdGFibGUiLCJzaWRlTWVudSIsIiQiLCJtZXRpc01lbnUiLCJwcmV2ZW50RGVmYXVsdCIsIm9uIiwiZSIsIm1lbnVGaXhIZWlnaHQiLCJjbGljayIsImN1cnJlbnRFbGVtZW50Iiwic3dhbCIsInRpdGxlIiwidGV4dCIsImljb24iLCJidXR0b25zIiwiZGFuZ2VyTW9kZSIsInRoZW4iLCJ3aWxsRGVsZXRlIiwid2luZG93IiwibG9jYXRpb24iLCJocmVmIiwiYXR0ciIsImhlaWdodFdpdGhvdXROYXZiYXIiLCJoZWlnaHQiLCJjc3MiLCJuYXZiYXJoZWlnaHQiLCJ3cmFwcGVySGVpZ2h0IiwiaGFzQ2xhc3MiLCJoaWRlIiwic2V0VGltZW91dCIsImZhZGVJbiIsInJlbW92ZUF0dHIiLCJiaW5kIiwic2xpbVNjcm9sbCIsInJhaWxPcGFjaXR5Iiwid2lkdGgiLCJhZGRDbGFzcyIsInJlbW92ZUNsYXNzIiwiZG9jdW1lbnQiLCJyZWFkeSIsImdyb3VwIiwiYWpheCIsInVybCIsImRhdGEiLCJtZXRob2QiLCJkYXRhVHlwZSIsInRyZWUiLCJzdWNjZXNzIiwicmVzcCIsImNvbnNvbGUiLCJsb2ciLCJlcnJvciIsInRhcmdldCIsImFjdGlvbiIsImdhbGxlcnkiLCIkZ2FsbGVyeSIsImluaXREcm9wWm9uZSIsInJlbW92ZUltYWdlIiwiJGRyb3B6b25lIiwib3duZXJJZCIsImRyb3B6b25lIiwiYWNjZXB0ZWRGaWxlcyIsImRpY3REZWZhdWx0TWVzc2FnZSIsImZpbGUiLCJ4aHIiLCJmb3JtRGF0YSIsImFwcGVuZCIsInJlc3BvbnNlIiwiaW1hZ2UiLCJpZCIsIiRpbWFnZSIsImNsb3Nlc3QiLCJyZW1vdmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7QUFPQTtBQUNBQSxtQkFBTyxDQUFDLG9EQUFELENBQVAsQyxDQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2RBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQUlDLFNBQVMsR0FBSSxZQUFZO0FBQzVCLFNBQU87QUFDTkMsUUFBSSxFQUFFLGdCQUFZO0FBQ2pCRCxlQUFTLENBQUNFLGFBQVY7QUFDQUYsZUFBUyxDQUFDRyxjQUFWO0FBQ0FILGVBQVMsQ0FBQ0ksWUFBVjtBQUNBSixlQUFTLENBQUNLLGtCQUFWO0FBQ0FMLGVBQVMsQ0FBQ00sUUFBVjtBQUNBLEtBUEs7QUFTTkosaUJBQWEsRUFBRSx5QkFBWTtBQUMxQjtBQUNBLFVBQUlLLFFBQVEsR0FBR0MsQ0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQkMsU0FBaEIsQ0FBMEI7QUFDeENDLHNCQUFjLEVBQUU7QUFEd0IsT0FBMUIsQ0FBZjtBQUlBSCxjQUFRLENBQUNJLEVBQVQsQ0FBWSxpQkFBWixFQUErQixVQUFVQyxDQUFWLEVBQWE7QUFDM0NaLGlCQUFTLENBQUNhLGFBQVY7QUFDQSxPQUZEO0FBR0EsS0FsQks7QUFvQk5WLGtCQUFjLEVBQUUsMEJBQVk7QUFDM0JLLE9BQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CTSxLQUFwQixDQUEwQixVQUFVRixDQUFWLEVBQWE7QUFDdENBLFNBQUMsQ0FBQ0YsY0FBRjtBQUNBLFlBQUlLLGNBQWMsR0FBR1AsQ0FBQyxDQUFDLElBQUQsQ0FBdEI7QUFDQVEsWUFBSSxDQUFDO0FBQ0pDLGVBQUssRUFBRSxnQkFESDtBQUVKQyxjQUFJLEVBQUUsOENBRkY7QUFHSkMsY0FBSSxFQUFFLFNBSEY7QUFJSkMsaUJBQU8sRUFBRSxDQUFDLFFBQUQsRUFBVyxTQUFYLENBSkw7QUFLSkMsb0JBQVUsRUFBRTtBQUxSLFNBQUQsQ0FBSixDQU9FQyxJQVBGLENBT08sVUFBQ0MsVUFBRCxFQUFnQjtBQUNyQixjQUFJQSxVQUFKLEVBQWdCO0FBQ2ZQLGdCQUFJLENBQUM7QUFDSkUsa0JBQUksRUFBRSxtQkFERjtBQUVKQyxrQkFBSSxFQUFFLE1BRkY7QUFHSkMscUJBQU8sRUFBRTtBQUhMLGFBQUQsQ0FBSjtBQUtBSSxrQkFBTSxDQUFDQyxRQUFQLENBQWdCQyxJQUFoQixHQUF1QlgsY0FBYyxDQUFDWSxJQUFmLENBQW9CLE1BQXBCLENBQXZCO0FBRUEsV0FSRCxNQVFPO0FBQ05YLGdCQUFJLENBQUMsbUJBQUQsQ0FBSjtBQUNBO0FBQ0QsU0FuQkY7QUFvQkEsT0F2QkQ7QUF3QkFSLE9BQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJNLEtBQW5CLENBQXlCLFVBQVVGLENBQVYsRUFBYTtBQUNyQ0EsU0FBQyxDQUFDRixjQUFGO0FBQ0EsWUFBSUssY0FBYyxHQUFHUCxDQUFDLENBQUMsSUFBRCxDQUF0QjtBQUNBUSxZQUFJLENBQUM7QUFDSkMsZUFBSyxFQUFFLG1CQURIO0FBRUpFLGNBQUksRUFBRSxTQUZGO0FBR0pDLGlCQUFPLEVBQUUsQ0FBQyxRQUFELEVBQVcsWUFBWCxDQUhMO0FBSUpDLG9CQUFVLEVBQUU7QUFKUixTQUFELENBQUosQ0FNRUMsSUFORixDQU1PLFVBQUNDLFVBQUQsRUFBZ0I7QUFDckIsY0FBSUEsVUFBSixFQUFnQjtBQUNmUCxnQkFBSSxDQUFDO0FBQ0pFLGtCQUFJLEVBQUUsc0JBREY7QUFFSkMsa0JBQUksRUFBRSxNQUZGO0FBR0pDLHFCQUFPLEVBQUU7QUFITCxhQUFELENBQUo7QUFLQUksa0JBQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsSUFBaEIsR0FBdUJYLGNBQWMsQ0FBQ1ksSUFBZixDQUFvQixNQUFwQixDQUF2QjtBQUVBLFdBUkQsTUFRTztBQUNOWCxnQkFBSSxDQUFDLG1CQUFELENBQUo7QUFDQTtBQUNELFNBbEJGO0FBbUJBLE9BdEJEO0FBdUJBLEtBcEVLO0FBc0VOSCxpQkFBYSxFQUFFLHlCQUFhO0FBQzNCLFVBQUllLG1CQUFtQixHQUFHcEIsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJxQixNQUFyQixLQUFnQyxFQUExRDtBQUNBckIsT0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JzQixHQUFwQixDQUF3QixZQUF4QixFQUFzQ0YsbUJBQW1CLEdBQUcsSUFBNUQ7QUFFQSxVQUFJRyxZQUFZLEdBQUd2QixDQUFDLENBQUMsb0JBQUQsQ0FBRCxDQUF3QnFCLE1BQXhCLEVBQW5CO0FBQ0EsVUFBSUcsYUFBYSxHQUFHeEIsQ0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQnFCLE1BQW5CLEVBQXBCOztBQUVBLFVBQUlFLFlBQVksR0FBR0MsYUFBbkIsRUFBa0M7QUFDakN4QixTQUFDLENBQUMsZUFBRCxDQUFELENBQW1Cc0IsR0FBbkIsQ0FBdUIsWUFBdkIsRUFBcUNDLFlBQVksR0FBRyxJQUFwRDtBQUNBOztBQUVELFVBQUlBLFlBQVksR0FBR0MsYUFBbkIsRUFBa0M7QUFDakN4QixTQUFDLENBQUMsZUFBRCxDQUFELENBQW1Cc0IsR0FBbkIsQ0FBdUIsWUFBdkIsRUFBcUN0QixDQUFDLENBQUNnQixNQUFELENBQUQsQ0FBVUssTUFBVixLQUFxQixJQUExRDtBQUNBOztBQUVELFVBQUlyQixDQUFDLENBQUMsTUFBRCxDQUFELENBQVV5QixRQUFWLENBQW1CLFdBQW5CLENBQUosRUFBcUM7QUFDcEMsWUFBSUYsWUFBWSxHQUFHQyxhQUFuQixFQUFrQztBQUNqQ3hCLFdBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJzQixHQUFuQixDQUF1QixZQUF2QixFQUFxQ0MsWUFBWSxHQUFHLElBQXBEO0FBQ0EsU0FGRCxNQUVPO0FBQ052QixXQUFDLENBQUMsZUFBRCxDQUFELENBQW1Cc0IsR0FBbkIsQ0FBdUIsWUFBdkIsRUFBcUN0QixDQUFDLENBQUNnQixNQUFELENBQUQsQ0FBVUssTUFBVixLQUFxQixFQUFyQixHQUEwQixJQUEvRDtBQUNBO0FBQ0Q7QUFDRixLQTVGTTtBQThGSHpCLGdCQUFZLEVBQUUsd0JBQVc7QUFDM0IsVUFBSSxDQUFDSSxDQUFDLENBQUMsTUFBRCxDQUFELENBQVV5QixRQUFWLENBQW1CLGFBQW5CLENBQUQsSUFBc0N6QixDQUFDLENBQUMsTUFBRCxDQUFELENBQVV5QixRQUFWLENBQW1CLFlBQW5CLENBQTFDLEVBQTRFO0FBQzNFO0FBQ0F6QixTQUFDLENBQUMsWUFBRCxDQUFELENBQWdCMEIsSUFBaEIsR0FGMkUsQ0FHM0U7O0FBQ0FDLGtCQUFVLENBQ1QsWUFBWTtBQUNYM0IsV0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQjRCLE1BQWhCLENBQXVCLEdBQXZCO0FBQ0EsU0FIUSxFQUdOLEdBSE0sQ0FBVjtBQUlBLE9BUkQsTUFRTyxJQUFJNUIsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVeUIsUUFBVixDQUFtQixlQUFuQixDQUFKLEVBQXlDO0FBQy9DekIsU0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQjBCLElBQWhCO0FBQ0FDLGtCQUFVLENBQ1QsWUFBWTtBQUNYM0IsV0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQjRCLE1BQWhCLENBQXVCLEdBQXZCO0FBQ0EsU0FIUSxFQUdOLEdBSE0sQ0FBVjtBQUlBLE9BTk0sTUFNQTtBQUNOO0FBQ0E1QixTQUFDLENBQUMsWUFBRCxDQUFELENBQWdCNkIsVUFBaEIsQ0FBMkIsT0FBM0I7QUFDQTtBQUNELEtBakhLO0FBbUhOaEMsc0JBQWtCLEVBQUUsOEJBQVk7QUFDL0I7QUFDQUcsT0FBQyxDQUFDZ0IsTUFBRCxDQUFELENBQVVjLElBQVYsQ0FBZSxNQUFmLEVBQXVCLFlBQVk7QUFDbEMsWUFBSTlCLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVXlCLFFBQVYsQ0FBbUIsZUFBbkIsQ0FBSixFQUF5QztBQUN4Q3pCLFdBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCK0IsVUFBdkIsQ0FBa0M7QUFDakNWLGtCQUFNLEVBQUUsTUFEeUI7QUFFakNXLHVCQUFXLEVBQUU7QUFGb0IsV0FBbEM7QUFJQTtBQUNELE9BUEQ7QUFTQWhDLE9BQUMsQ0FBQ2dCLE1BQUQsQ0FBRCxDQUFVYyxJQUFWLENBQWUsb0JBQWYsRUFBcUMsWUFBWTtBQUNoRDtBQUNBSCxrQkFBVSxDQUFDLFlBQVU7QUFDcEIsY0FBSSxDQUFDM0IsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVeUIsUUFBVixDQUFtQixZQUFuQixDQUFMLEVBQXVDO0FBQ3RDakMscUJBQVMsQ0FBQ2EsYUFBVjtBQUNBO0FBQ0QsU0FKUyxDQUFWO0FBTUEsT0FSRCxFQVgrQixDQW9CL0I7O0FBQ0FMLE9BQUMsQ0FBQ2dCLE1BQUQsQ0FBRCxDQUFVYyxJQUFWLENBQWUsUUFBZixFQUF5QixZQUFZO0FBQ3BDLFlBQUk5QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpQyxLQUFSLEtBQWtCLEdBQXRCLEVBQTJCO0FBQzFCakMsV0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVa0MsUUFBVixDQUFtQixZQUFuQjtBQUNBLFNBRkQsTUFFTztBQUNObEMsV0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVbUMsV0FBVixDQUFzQixZQUF0QjtBQUNBO0FBQ0QsT0FORDtBQVFBLEtBaEpLO0FBa0pOckMsWUFBUSxFQUFFLG9CQUFZO0FBQ3JCRSxPQUFDLENBQUNvQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFVO0FBQzNCckMsU0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlRixRQUFmLENBQXdCO0FBQUN3QyxlQUFLLEVBQUU7QUFBUixTQUF4QixFQUFvQ25DLEVBQXBDLENBQXVDLFFBQXZDLEVBQWlELFVBQVVDLENBQVYsRUFBYTtBQUM3REosV0FBQyxDQUFDdUMsSUFBRixDQUFPO0FBQ05DLGVBQUcsRUFBRXhDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXlDLElBQVIsQ0FBYSxRQUFiLENBREM7QUFFTkMsa0JBQU0sRUFBRSxNQUZGO0FBR05DLG9CQUFRLEVBQUUsTUFISjtBQUtORixnQkFBSSxFQUFFO0FBQUNHLGtCQUFJLEVBQUM1QyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFGLFFBQVIsQ0FBaUIsV0FBakI7QUFBTixhQUxBO0FBTU4rQyxtQkFBTyxFQUFFLGlCQUFTQyxJQUFULEVBQWU7QUFDdkJDLHFCQUFPLENBQUNDLEdBQVIsQ0FBWUYsSUFBWjtBQUNBLGFBUks7QUFTTkcsaUJBQUssRUFBRSxpQkFBVyxDQUVqQjtBQVhLLFdBQVA7QUFhQSxTQWREO0FBZUFqRCxTQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QkcsRUFBOUIsQ0FBaUMsT0FBakMsRUFBMEMsVUFBVUMsQ0FBVixFQUFhO0FBQ3RELGNBQUk4QyxNQUFNLEdBQUdsRCxDQUFDLENBQUNJLENBQUMsQ0FBQzhDLE1BQUgsQ0FBZDtBQUFBLGNBQ0NDLE1BQU0sR0FBR0QsTUFBTSxDQUFDVCxJQUFQLENBQVksUUFBWixDQURWOztBQUVBLGNBQUlVLE1BQU0sS0FBSyxZQUFmLEVBQTZCO0FBQzVCbkQsYUFBQyxDQUFDLEtBQUQsQ0FBRCxDQUFTRixRQUFULENBQWtCLFdBQWxCO0FBQ0E7O0FBQ0QsY0FBSXFELE1BQU0sS0FBSyxjQUFmLEVBQStCO0FBQzlCbkQsYUFBQyxDQUFDLEtBQUQsQ0FBRCxDQUFTRixRQUFULENBQWtCLGFBQWxCO0FBQ0E7QUFDRCxTQVREO0FBVUEsT0ExQkQ7QUEyQkE7QUE5S0ssR0FBUDtBQWlMQSxDQWxMZSxFQUFoQjs7QUFtTEFOLFNBQVMsQ0FBQ0MsSUFBVixHOzs7Ozs7Ozs7Ozs7O0FDeExBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOztBQUNBLElBQUkyRCxPQUFPLEdBQUksWUFBWTtBQUN2QixNQUFJQyxRQUFRLEdBQUdyRCxDQUFDLENBQUMsYUFBRCxDQUFoQjtBQUNILFNBQU87QUFDTlAsUUFBSSxFQUFFLGdCQUFXO0FBQ1AyRCxhQUFPLENBQUNFLFlBQVI7QUFDQUYsYUFBTyxDQUFDRyxXQUFSO0FBQ1QsS0FKSztBQUtBRCxnQkFBWSxFQUFFLHdCQUFXO0FBQzlCLFVBQUlFLFNBQVMsR0FBR3hELENBQUMsQ0FBQyxjQUFELENBQWpCO0FBQUEsVUFDVXdDLEdBQUcsR0FBR2dCLFNBQVMsQ0FBQ2YsSUFBVixDQUFlLEtBQWYsQ0FEaEI7QUFBQSxVQUVhZ0IsT0FBTyxHQUFHRCxTQUFTLENBQUNmLElBQVYsQ0FBZSxVQUFmLENBRnZCO0FBR0FlLGVBQVMsQ0FBQ0UsUUFBVixDQUFtQjtBQUNsQmxCLFdBQUcsRUFBRUEsR0FEYTtBQUVObUIscUJBQWEsRUFBRSxTQUZUO0FBR05DLDBCQUFrQixFQUFFLDBDQUhkO0FBSU5uRSxZQUFJLEVBQUUsZ0JBQVc7QUFDYixlQUFLVSxFQUFMLENBQVEsU0FBUixFQUFtQixVQUFTMEQsSUFBVCxFQUFlQyxHQUFmLEVBQW9CQyxRQUFwQixFQUE2QjtBQUM1Q0Esb0JBQVEsQ0FBQ0MsTUFBVCxDQUFnQixTQUFoQixFQUEyQlAsT0FBM0I7QUFDSCxXQUZEO0FBR0gsU0FSSztBQVNOWixlQUFPLEVBQUUsaUJBQVVnQixJQUFWLEVBQWdCSSxRQUFoQixFQUEwQjtBQUMvQlosa0JBQVEsQ0FBQ1csTUFBVCxDQUFnQiw2QkFDWixnQ0FEWSxHQUVaLHNFQUZZLEdBRTJEQyxRQUFRLENBQUN6QixHQUZwRSxHQUV3RSxxQ0FGeEUsR0FHWixZQUhZLEdBR0N5QixRQUFRLENBQUNDLEtBSFYsR0FHZ0IsOENBSGhCLEdBSVosVUFKWSxHQUtaLFFBTEo7QUFNSDtBQWhCSyxPQUFuQjtBQW1CQSxLQTVCSztBQTZCQVgsZUFBVyxFQUFFLHVCQUFZO0FBQ3JCdkQsT0FBQyxDQUFDb0MsUUFBRCxDQUFELENBQVlqQyxFQUFaLENBQWUsT0FBZixFQUF1QixrQkFBdkIsRUFBMEMsVUFBU0MsQ0FBVCxFQUFXO0FBQ2pEQSxTQUFDLENBQUNGLGNBQUY7QUFDQSxZQUFJaUUsRUFBRSxHQUFHbkUsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFReUMsSUFBUixDQUFhLElBQWIsQ0FBVDtBQUFBLFlBQ0lELEdBQUcsR0FBR3hDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXlDLElBQVIsQ0FBYSxLQUFiLENBRFY7QUFBQSxZQUVJMkIsTUFBTSxHQUFHcEUsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRcUUsT0FBUixDQUFnQixtQkFBaEIsQ0FGYjtBQUlBckUsU0FBQyxDQUFDdUMsSUFBRixDQUFPO0FBQ0hDLGFBQUcsRUFBRUEsR0FERjtBQUVIRSxnQkFBTSxFQUFFLE1BRkw7QUFHSEMsa0JBQVEsRUFBRSxNQUhQO0FBSUhGLGNBQUksRUFBRTtBQUNGMEIsY0FBRSxFQUFFQTtBQURGLFdBSkg7QUFPSHRCLGlCQUFPLEVBQUUsaUJBQVVvQixRQUFWLEVBQW9CO0FBQ3pCRyxrQkFBTSxDQUFDRSxNQUFQO0FBQ0g7QUFURSxTQUFQO0FBV0gsT0FqQkQ7QUFrQkg7QUFoREQsR0FBUDtBQWtEQSxDQXBEYSxFQUFkOztBQXFEQWxCLE9BQU8sQ0FBQzNELElBQVIsRzs7Ozs7Ozs7Ozs7O0FDdkRBLHVDIiwiZmlsZSI6ImFkbWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cbi8vIGFueSBDU1MgeW91IHJlcXVpcmUgd2lsbCBvdXRwdXQgaW50byBhIHNpbmdsZSBjc3MgZmlsZSAoYXBwLmNzcyBpbiB0aGlzIGNhc2UpXG5yZXF1aXJlKCcuLi9zY3NzL2FkbWluLnNjc3MnKTtcblxuLy8gTmVlZCBqUXVlcnk/IEluc3RhbGwgaXQgd2l0aCBcInlhcm4gYWRkIGpxdWVyeVwiLCB0aGVuIHVuY29tbWVudCB0byByZXF1aXJlIGl0LlxuLy9jb25zdCAkID0gcmVxdWlyZSgnanF1ZXJ5Jyk7XG5pbXBvcnQgJ3RvYXN0ci90b2FzdHInXG5pbXBvcnQgJ2Jvb3RzdHJhcDMvZGlzdC9qcy9ib290c3RyYXAnO1xuaW1wb3J0ICcuL2FkbWluL2Rhc2hib2FyZCc7XG5pbXBvcnQgJy4vYWRtaW4vZ2FsbGVyeSc7XG4iLCJpbXBvcnQgJ3N3ZWV0YWxlcnQvZGlzdC9zd2VldGFsZXJ0Lm1pbic7XG5pbXBvcnQgJ21ldGlzbWVudS9kaXN0L21ldGlzTWVudSc7XG5pbXBvcnQgJ21ldGlzbWVudS9kaXN0L21ldGlzTWVudS5jc3MnO1xuaW1wb3J0ICdqcXVlcnktbmVzdGFibGUnO1xuXG5sZXQgZGFzaGJvYXJkID0gKGZ1bmN0aW9uICgpIHtcblx0cmV0dXJuIHtcblx0XHRpbml0OiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRkYXNoYm9hcmQuaW5pdE1ldGlzTWVudSgpO1xuXHRcdFx0ZGFzaGJvYXJkLmluaXRTd2VldEFsZXJ0KCk7XG5cdFx0XHRkYXNoYm9hcmQuc21vb3RobHlNZW51KCk7XG5cdFx0XHRkYXNoYm9hcmQuaW5pdFNpZGVNZW51RXZlbnRzKCk7XG5cdFx0XHRkYXNoYm9hcmQubmVzdGFibGUoKTtcblx0XHR9LFxuXG5cdFx0aW5pdE1ldGlzTWVudTogZnVuY3Rpb24gKCkge1xuXHRcdFx0Ly8gTWV0aXNNZW51XG5cdFx0XHRsZXQgc2lkZU1lbnUgPSAkKCcjc2lkZS1tZW51JykubWV0aXNNZW51KHtcblx0XHRcdFx0cHJldmVudERlZmF1bHQ6IGZhbHNlLFxuXHRcdFx0fSk7XG5cblx0XHRcdHNpZGVNZW51Lm9uKCdzaG93bi5tZXRpc01lbnUnLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRkYXNoYm9hcmQubWVudUZpeEhlaWdodCgpO1xuXHRcdFx0fSk7XG5cdFx0fSxcblxuXHRcdGluaXRTd2VldEFsZXJ0OiBmdW5jdGlvbiAoKSB7XG5cdFx0XHQkKCcuanMtZGVsZXRlLWJ0bicpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0bGV0IGN1cnJlbnRFbGVtZW50ID0gJCh0aGlzKTtcblx0XHRcdFx0c3dhbCh7XG5cdFx0XHRcdFx0dGl0bGU6IFwi0KLQvtGH0L3QviDRg9C00LDQu9C40YLRjD9cIixcblx0XHRcdFx0XHR0ZXh0OiBcItCj0LTQsNC70LXQvdC90LDRjyDQt9Cw0L/QuNGB0Ywg0L3QtSDQv9C+0LTQu9C10LbQuNGCINCy0L7RgdGB0YLQsNC90L7QstC70LXQvdC40Y4hXCIsXG5cdFx0XHRcdFx0aWNvbjogXCJ3YXJuaW5nXCIsXG5cdFx0XHRcdFx0YnV0dG9uczogW1wi0J7RgtC80LXQvdCwXCIsIFwi0KPQtNCw0LvQuNGC0YxcIl0sXG5cdFx0XHRcdFx0ZGFuZ2VyTW9kZTogdHJ1ZSxcblx0XHRcdFx0fSlcblx0XHRcdFx0XHQudGhlbigod2lsbERlbGV0ZSkgPT4ge1xuXHRcdFx0XHRcdFx0aWYgKHdpbGxEZWxldGUpIHtcblx0XHRcdFx0XHRcdFx0c3dhbCh7XG5cdFx0XHRcdFx0XHRcdFx0dGV4dDogXCLQo9GB0L/QtdGI0L3QvtC1INGD0LTQsNC70LXQvdC40LVcIixcblx0XHRcdFx0XHRcdFx0XHRpY29uOiBcImluZm9cIixcblx0XHRcdFx0XHRcdFx0XHRidXR0b25zOiBmYWxzZSxcblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gY3VycmVudEVsZW1lbnQuYXR0cignaHJlZicpO1xuXG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRzd2FsKFwi0JzQvtC20LXRgiDQuCDQv9GA0LDQstC40LvRjNC90L5cIik7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHRcdCQoJy5qcy1jbG9uZS1idG4nKS5jbGljayhmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdGxldCBjdXJyZW50RWxlbWVudCA9ICQodGhpcyk7XG5cdFx0XHRcdHN3YWwoe1xuXHRcdFx0XHRcdHRpdGxlOiBcItCi0L7Rh9C90L4g0LrQvtC/0LjRgNC+0LLQsNGC0Yw/XCIsXG5cdFx0XHRcdFx0aWNvbjogXCJ3YXJuaW5nXCIsXG5cdFx0XHRcdFx0YnV0dG9uczogW1wi0J7RgtC80LXQvdCwXCIsIFwi0JrQvtC/0LjRgNC+0LLQsNGC0YxcIl0sXG5cdFx0XHRcdFx0ZGFuZ2VyTW9kZTogdHJ1ZSxcblx0XHRcdFx0fSlcblx0XHRcdFx0XHQudGhlbigod2lsbERlbGV0ZSkgPT4ge1xuXHRcdFx0XHRcdFx0aWYgKHdpbGxEZWxldGUpIHtcblx0XHRcdFx0XHRcdFx0c3dhbCh7XG5cdFx0XHRcdFx0XHRcdFx0dGV4dDogXCLQo9GB0L/QtdGI0L3QvtC1INC60L7Qv9C40YDQvtCy0LDQvdC40LVcIixcblx0XHRcdFx0XHRcdFx0XHRpY29uOiBcImluZm9cIixcblx0XHRcdFx0XHRcdFx0XHRidXR0b25zOiBmYWxzZSxcblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gY3VycmVudEVsZW1lbnQuYXR0cignaHJlZicpO1xuXG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRzd2FsKFwi0JzQvtC20LXRgiDQuCDQv9GA0LDQstC40LvRjNC90L5cIik7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHR9LFxuXG5cdFx0bWVudUZpeEhlaWdodDogZnVuY3Rpb24gKCkgIHtcblx0XHRcdGxldCBoZWlnaHRXaXRob3V0TmF2YmFyID0gJChcImJvZHkgPiAjd3JhcHBlclwiKS5oZWlnaHQoKSAtIDYyO1xuXHRcdFx0JChcIi5zaWRlYmFyLXBhbmVsXCIpLmNzcyhcIm1pbi1oZWlnaHRcIiwgaGVpZ2h0V2l0aG91dE5hdmJhciArIFwicHhcIik7XG5cblx0XHRcdGxldCBuYXZiYXJoZWlnaHQgPSAkKCduYXYubmF2YmFyLWRlZmF1bHQnKS5oZWlnaHQoKTtcblx0XHRcdGxldCB3cmFwcGVySGVpZ2h0ID0gJCgnI3BhZ2Utd3JhcHBlcicpLmhlaWdodCgpO1xuXG5cdFx0XHRpZiAobmF2YmFyaGVpZ2h0ID4gd3JhcHBlckhlaWdodCkge1xuXHRcdFx0XHQkKCcjcGFnZS13cmFwcGVyJykuY3NzKFwibWluLWhlaWdodFwiLCBuYXZiYXJoZWlnaHQgKyBcInB4XCIpO1xuXHRcdFx0fVxuXG5cdFx0XHRpZiAobmF2YmFyaGVpZ2h0IDwgd3JhcHBlckhlaWdodCkge1xuXHRcdFx0XHQkKCcjcGFnZS13cmFwcGVyJykuY3NzKFwibWluLWhlaWdodFwiLCAkKHdpbmRvdykuaGVpZ2h0KCkgKyBcInB4XCIpO1xuXHRcdFx0fVxuXG5cdFx0XHRpZiAoJCgnYm9keScpLmhhc0NsYXNzKCdmaXhlZC1uYXYnKSkge1xuXHRcdFx0XHRpZiAobmF2YmFyaGVpZ2h0ID4gd3JhcHBlckhlaWdodCkge1xuXHRcdFx0XHRcdCQoJyNwYWdlLXdyYXBwZXInKS5jc3MoXCJtaW4taGVpZ2h0XCIsIG5hdmJhcmhlaWdodCArIFwicHhcIik7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0JCgnI3BhZ2Utd3JhcHBlcicpLmNzcyhcIm1pbi1oZWlnaHRcIiwgJCh3aW5kb3cpLmhlaWdodCgpIC0gNjAgKyBcInB4XCIpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdH0sXG5cblx0ICAgIHNtb290aGx5TWVudTogZnVuY3Rpb24oKSB7XG5cdFx0XHRpZiAoISQoJ2JvZHknKS5oYXNDbGFzcygnbWluaS1uYXZiYXInKSB8fCAkKCdib2R5JykuaGFzQ2xhc3MoJ2JvZHktc21hbGwnKSkge1xuXHRcdFx0XHQvLyBIaWRlIG1lbnUgaW4gb3JkZXIgdG8gc21vb3RobHkgdHVybiBvbiB3aGVuIG1heGltaXplIG1lbnVcblx0XHRcdFx0JCgnI3NpZGUtbWVudScpLmhpZGUoKTtcblx0XHRcdFx0Ly8gRm9yIHNtb290aGx5IHR1cm4gb24gbWVudVxuXHRcdFx0XHRzZXRUaW1lb3V0KFxuXHRcdFx0XHRcdGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRcdCQoJyNzaWRlLW1lbnUnKS5mYWRlSW4oNDAwKTtcblx0XHRcdFx0XHR9LCAyMDApO1xuXHRcdFx0fSBlbHNlIGlmICgkKCdib2R5JykuaGFzQ2xhc3MoJ2ZpeGVkLXNpZGViYXInKSkge1xuXHRcdFx0XHQkKCcjc2lkZS1tZW51JykuaGlkZSgpO1xuXHRcdFx0XHRzZXRUaW1lb3V0KFxuXHRcdFx0XHRcdGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRcdCQoJyNzaWRlLW1lbnUnKS5mYWRlSW4oNDAwKTtcblx0XHRcdFx0XHR9LCAxMDApO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8gUmVtb3ZlIGFsbCBpbmxpbmUgc3R5bGUgZnJvbSBqcXVlcnkgZmFkZUluIGZ1bmN0aW9uIHRvIHJlc2V0IG1lbnUgc3RhdGVcblx0XHRcdFx0JCgnI3NpZGUtbWVudScpLnJlbW92ZUF0dHIoJ3N0eWxlJyk7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdGluaXRTaWRlTWVudUV2ZW50czogZnVuY3Rpb24gKCkge1xuXHRcdFx0Ly8gRml4ZWQgU2lkZWJhclxuXHRcdFx0JCh3aW5kb3cpLmJpbmQoXCJsb2FkXCIsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0aWYgKCQoXCJib2R5XCIpLmhhc0NsYXNzKCdmaXhlZC1zaWRlYmFyJykpIHtcblx0XHRcdFx0XHQkKCcuc2lkZWJhci1jb2xsYXBzZScpLnNsaW1TY3JvbGwoe1xuXHRcdFx0XHRcdFx0aGVpZ2h0OiAnMTAwJScsXG5cdFx0XHRcdFx0XHRyYWlsT3BhY2l0eTogMC45XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHQkKHdpbmRvdykuYmluZChcImxvYWQgcmVzaXplIHNjcm9sbFwiLCBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdC8vIEZ1bGwgaGVpZ2h0IG9mIHNpZGViYXJcblx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xuXHRcdFx0XHRcdGlmICghJChcImJvZHlcIikuaGFzQ2xhc3MoJ2JvZHktc21hbGwnKSkge1xuXHRcdFx0XHRcdFx0ZGFzaGJvYXJkLm1lbnVGaXhIZWlnaHQoKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pXG5cblx0XHRcdH0pO1xuXHRcdFx0Ly8gTWluaW1hbGl6ZSBtZW51IHdoZW4gc2NyZWVuIGlzIGxlc3MgdGhhbiA3NjhweFxuXHRcdFx0JCh3aW5kb3cpLmJpbmQoXCJyZXNpemVcIiwgZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRpZiAoJCh0aGlzKS53aWR0aCgpIDwgNzY5KSB7XG5cdFx0XHRcdFx0JCgnYm9keScpLmFkZENsYXNzKCdib2R5LXNtYWxsJylcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHQkKCdib2R5JykucmVtb3ZlQ2xhc3MoJ2JvZHktc21hbGwnKVxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdH0sXG5cblx0XHRuZXN0YWJsZTogZnVuY3Rpb24gKCkge1xuXHRcdFx0JChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcblx0XHRcdFx0JCgnI25lc3RhYmxlJykubmVzdGFibGUoe2dyb3VwOiAxfSkub24oJ2NoYW5nZScsIGZ1bmN0aW9uIChlKSB7XG5cdFx0XHRcdFx0JC5hamF4KHtcblx0XHRcdFx0XHRcdHVybDogJCh0aGlzKS5kYXRhKCdhY3Rpb24nKSxcblx0XHRcdFx0XHRcdG1ldGhvZDogJ1BPU1QnLFxuXHRcdFx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJyxcblxuXHRcdFx0XHRcdFx0ZGF0YToge3RyZWU6JCh0aGlzKS5uZXN0YWJsZSgnc2VyaWFsaXplJyl9LFxuXHRcdFx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24ocmVzcCkge1xuXHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhyZXNwKTtcblx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRlcnJvcjogZnVuY3Rpb24oKSB7XG5cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9KVxuXHRcdFx0XHR9KTtcblx0XHRcdFx0JCgnLmpzLW5lc3RhYmxlLW1lbnUgYnV0dG9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0XHR2YXIgdGFyZ2V0ID0gJChlLnRhcmdldCksXG5cdFx0XHRcdFx0XHRhY3Rpb24gPSB0YXJnZXQuZGF0YSgnYWN0aW9uJyk7XG5cdFx0XHRcdFx0aWYgKGFjdGlvbiA9PT0gJ2V4cGFuZC1hbGwnKSB7XG5cdFx0XHRcdFx0XHQkKCcuZGQnKS5uZXN0YWJsZSgnZXhwYW5kQWxsJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGlmIChhY3Rpb24gPT09ICdjb2xsYXBzZS1hbGwnKSB7XG5cdFx0XHRcdFx0XHQkKCcuZGQnKS5uZXN0YWJsZSgnY29sbGFwc2VBbGwnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0fVxuXG5cdH1cbn0pKCk7XG5kYXNoYm9hcmQuaW5pdCgpOyIsImltcG9ydCAnZHJvcHpvbmUnO1xuaW1wb3J0ICdkcm9wem9uZS9kaXN0L2Ryb3B6b25lLmNzcydcbnZhciBnYWxsZXJ5ID0gKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgJGdhbGxlcnkgPSAkKCcuanMtZ2FsbGVyeScpO1xuXHRyZXR1cm4ge1xuXHRcdGluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgZ2FsbGVyeS5pbml0RHJvcFpvbmUoKTtcbiAgICAgICAgICAgIGdhbGxlcnkucmVtb3ZlSW1hZ2UoKTtcblx0XHR9LFxuICAgICAgICBpbml0RHJvcFpvbmU6IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyICRkcm9wem9uZSA9ICQoJy5qcy1kcm9wem9uZScpLFxuICAgICAgICAgICAgXHR1cmwgPSAkZHJvcHpvbmUuZGF0YSgndXJsJyksXG4gICAgICAgICAgICAgICAgb3duZXJJZCA9ICRkcm9wem9uZS5kYXRhKCdvd25lci1pZCcpO1xuXHRcdFx0JGRyb3B6b25lLmRyb3B6b25lKHtcblx0XHRcdFx0dXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgYWNjZXB0ZWRGaWxlczogXCJpbWFnZS8qXCIsXG4gICAgICAgICAgICAgICAgZGljdERlZmF1bHRNZXNzYWdlOiAn0J/QtdGA0LXRgtCw0YnQuNGC0LUg0LIg0Y3RgtC+INC+0LrQvdC+INGE0LDQudC70Ysg0LTQu9GPINC30LDQs9GA0YPQt9C60LgnLFxuICAgICAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uKCdzZW5kaW5nJywgZnVuY3Rpb24oZmlsZSwgeGhyLCBmb3JtRGF0YSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQoJ293bmVySWQnLCBvd25lcklkKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZmlsZSwgcmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgJGdhbGxlcnkuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiY29sLXNtLTFcIj5cXG4nICtcbiAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiZ2FsbGVyeV9faW1hZ2VcIj5cXG4nICtcbiAgICAgICAgICAgICAgICAgICAgICAgICc8YSBocmVmPVwiI1wiIGNsYXNzPVwianMtcmVtb3ZlLWltYWdlIGdhbGxlcnlfX2ltYWdlLXJlbW92ZVwiIGRhdGEtdXJsPVwiJytyZXNwb25zZS51cmwrJ1wiPjxpIGNsYXNzPVwiZmEgZmEtY2xvc2VcIj48L2k+PC9hPlxcbicgK1xuICAgICAgICAgICAgICAgICAgICAgICAgJzxpbWcgc3JjPVwiJytyZXNwb25zZS5pbWFnZSsnXCIgYWx0PVwiXCIgY2xhc3M9XCJpbWctcmVzcG9uc2l2ZSB0aHVtYm5haWxcIj5cXG4nICtcbiAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj5cXG4nICtcbiAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcblxuXHRcdH0sXG4gICAgICAgIHJlbW92ZUltYWdlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCcuanMtcmVtb3ZlLWltYWdlJyxmdW5jdGlvbihlKXtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgdmFyIGlkID0gJCh0aGlzKS5kYXRhKCdpZCcpLFxuICAgICAgICAgICAgICAgICAgICB1cmwgPSAkKHRoaXMpLmRhdGEoJ3VybCcpLFxuICAgICAgICAgICAgICAgICAgICAkaW1hZ2UgPSAkKHRoaXMpLmNsb3Nlc3QoJy5qcy1nYWxsZXJ5LWltYWdlJylcbiAgICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IGlkXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJGltYWdlLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG5cdH1cbn0pKCk7XG5nYWxsZXJ5LmluaXQoKTsiLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9