<?php

namespace App\Repository;

use App\Entity\Bid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bid|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bid|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bid[]    findAll()
 * @method Bid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BidRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bid::class);
    }

    public function getBidsQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('b');
        $qb = $queryBuilder->addSelect('b');
        if(isset($filter['query'])){
            $qb
                ->where('b.title LIKE :query')
                ->setParameter('query', '%'.$filter['query'].'%');
        }
        return $qb->orderBy('b.created','DESC');
    }


    public function getBidsPaginated(array $filter, int $pageNumber)
    {
        $qb = $this->getBidsQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $pageNumber, Bid::PER_PAGE);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function getActiveBids()
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.active = true')
            ->getQuery()
            ->getResult();
    }
}
