<?php

namespace App\Repository;

use App\Entity\Work;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Work|null find($id, $lockMode = null, $lockVersion = null)
 * @method Work|null findOneBy(array $criteria, array $orderBy = null)
 * @method Work[]    findAll()
 * @method Work[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Work::class);
    }

    public function getWorksQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('b');
        $qb = $queryBuilder->addSelect('b');
        if(isset($filter['query'])){
            $qb
                ->where('b.title LIKE :query')
                ->setParameter('query', '%'.$filter['query'].'%');
        }
        return $qb->orderBy('b.created','DESC');
    }


    public function getWorksPaginated(array $filter, int $pageNumber)
    {
        $qb = $this->getWorksQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $pageNumber, Work::PER_PAGE);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function getActiveWorks()
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.active = true')
            ->getQuery()
            ->getResult();
    }
}
