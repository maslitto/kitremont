<?php

namespace App\Controller\Front;

use App\Entity\Bid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="bid_")
 */
class BidController extends AbstractController
{

    private $mailer;
    private $managerMail;
    private $emailSender;

    public function __construct(\Swift_Mailer $mailer,string $emailSender, string $managerMail)
    {

        $this->mailer = $mailer;
        $this->emailSender = $emailSender;
        $this->managerMail = $managerMail;
    }
    /**
     * @Route("save/", name="save")
     */
    public function saveBid(Request $request)
    {
        $antispam = $request->get('antispam',NULL);
        if($antispam === 'no-bots-antispam'){
            $bid = new Bid();
            $bid->setName($request->get('name',NULL));
            $bid->setPhone($request->get('phone',NULL));
            $bid->setEmail($request->get('email',NULL));
            $bid->setType($request->get('type',NULL));
            $bid->setForm($request->get('form',NULL));
            $bid->setOtdelka($request->get('otdelka',NULL));
            $bid->setRooms($request->get('rooms',NULL));
            $bid->setSquare($request->get('square',NULL));
            $bid->setStyle($request->get('style',NULL));
            $bid->setCreated(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($bid);
            $em->flush();

            $this->sendReport("Телефон:".$bid->getPhone()." \nИмя:" .$bid->getName(),$this->managerMail);

            return $this->json([
                'success' => true
            ]);
        } else {
            return $this->json([
                'success' => false
            ],Response::HTTP_UNPROCESSABLE_ENTITY);
        }

    }
    /**
     * Sends the given $contents to the $recipient email address.
     */
    private function sendReport(string $contents, string $recipient): void
    {
        // See https://symfony.com/doc/current/email.html
        $message = $this->mailer->createMessage()
            ->setSubject('Новая заявка на сайте Кит ремонт')
            ->setFrom($this->emailSender)
            ->setTo($recipient)
            ->setBody($contents, 'text/plain')
        ;

        $this->mailer->send($message);
    }
}
