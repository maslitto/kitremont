<?php

namespace App\Controller\Front;

use App\Repository\WorkRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(WorkRepository $workRepository)
    {
        $works = $workRepository->findAll();
        return $this->render('front/index/index.html.twig',[
            'works' => $works,
            'prefix' => ''
        ]);
    }
}
