<?php

namespace App\Controller\Front;

use App\Entity\Work;
use App\Repository\WorkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="work_")
 */
class WorkController extends AbstractController
{

    /**
     * @Route("/show/{work}/", name="show")
     */
    public function showWork(Work $work,WorkRepository $workRepository)
    {
        if(!$work){
            return $this->createNotFoundException('album not found');
        }
        /** @var Work[] $works */
        $works = $workRepository->findAll();
        $prev = $workRepository->find($work->getId()-1);
        $next = $workRepository->find($work->getId()+1);
        if(!$prev){
            $prev = $works[sizeOf($works)-1];
        }
        if(!$next){
            $next = $works[0];
        }
        return $this->render('front/work/show.html.twig',[
            'work' => $work,
            'prev' => $prev,
            'next' => $next,
        ]);
    }
}
