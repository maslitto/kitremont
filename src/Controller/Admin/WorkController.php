<?php

namespace App\Controller\Admin;

use App\Entity\Photo;
use App\Entity\Work;
use App\Repository\WorkRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/work", name="admin_work_")
 */
class WorkController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(WorkRepository $workRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page', 1);
        $works = $workRepository->getWorksPaginated($filter, $page);
        return $this->render('admin/work/index.html.twig', [
            'works' => $works,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="edit")
     * @Route("/add/", name="add")
     *
     */
    public function edit(Request $request, int $id = NULL, WorkRepository $workRepository)
    {
        if($id){
            $work = $workRepository->findOneBy(['id' => $id]);
        } else{
            $work = new Work();
        }
        $form = $this->createFormBuilder($work)
            //->add('id', HiddenType::class,['label'=>'id','required' => false])
            ->add('title', TextType::class,['label'=>'Название объекта','required' => false])
            ->add('dateStart', DateType::class,['label'=>'Дата начала','required' => true])
            ->add('dateFinish', DateType::class,['label'=>'Дата окончания','required' => true])
            ->add('type', ChoiceType::class,['label'=>'Тип здания','required' => false,
                'choices'=>[
                    'Старый фонд'=>'Старый фонд',
                    'Новостройка'=>'Новостройка'
                ]])
            ->add('square', TextType::class,['label'=>'Площадь','required' => false])
            ->add('rooms', TextType::class,['label'=>'Кол-во комнат','required' => false])
            ->add('clientName', TextType::class,['label'=>'Имя клиента','required' => false])
            ->add('review', TextareaType::class,['label'=>'Отзыв','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $work = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($work);
            $em->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_work_edit',['id' => $work->getId()]);
        }

        return $this->render('admin/work/edit.html.twig',[
            'form' => $form->createView(),
            'work' => $work
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="delete")
     */
    public function delete(int $id, WorkRepository $workRepository)
    {
        $work = $workRepository->findOneBy(['id' => $id]);
        $em = $this->getDoctrine()->getManager();
        $photos = $work->getPhotos();
        foreach ($photos as $photo) {
            $filesystem = new Filesystem();
            if($photo->getFilepath()){
                $filesystem->remove($this->getParameter('publicPath').$photo->getFilepath());
            }
            $em->remove($photo);
        }
        $em->remove($work);
        $em->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_work_index');
    }

    /**
     * @Route("/copy/{work}/", name="copy")
     */
    public function copy(Work $work)
    {
        $em = $this->getDoctrine()->getManager();
        $newWork = clone $work;
        $newWork->setCreated(new \DateTime());
        $em->persist($newWork);
        $em->flush();
        $this->addFlash('success','Запись успешно скопирована');
        return $this->redirectToRoute('admin_work_index');
    }

    /**
     * @Route("/upload-photo/{work}/", name="upload_photo")
     */
    public function uploadPhoto(Work $work, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $photo = new Photo();
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        if ($file) {
            $newFilename = uniqid().'.'.$file->guessExtension();
            try {
                $webPath = '/uploads/'.$work->getId().'/';
                $file = $file->move($this->getParameter('publicPath').$webPath, $newFilename);

                //запишем новую и сохраним
                $photo->setFilepath($webPath.$file->getFilename());
                $photo->setFilename($file->getFilename());
                $photo->setWork($work);
                $em->persist($photo);
                $em->flush();
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
                return $this->json([
                    'errors'=> [
                        'file'=> $e->getMessage()
                    ]
                ],Response::HTTP_UNPROCESSABLE_ENTITY);
            }

        }
        $em->persist($photo);
        $em->flush();
        $response = [
            'image' => $photo->getFilepath(),
            'url' => $this->generateUrl('admin_work_remove_photo',['photo' => $photo->getId()])
        ];
        return $this->json($response);
    }

    /**
     * @Route("/remove-photo/{photo}", name="remove_photo")
     */
    public function removePhoto(Photo $photo,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if($photo->getFilepath()){
            $filesystem = new Filesystem();
            $filesystem->remove($this->getParameter('publicPath').$photo->getFilepath());
            $em->remove($photo);
            $em->flush();
        }
        $response = [
            'success' => true,
        ];
        return $this->json($response);
    }


}
