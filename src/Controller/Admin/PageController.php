<?php

namespace App\Controller\Admin;

use App\Entity\Photo;
use App\Entity\Page;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/page", name="admin_page_")
 */
class PageController extends AbstractController
{
    /** @var EntityManagerInterface $em */
    private $em;

    /** @var PageRepository $pageRepository */
    private $pageRepository;

    public function __construct(EntityManagerInterface $em,PageRepository $pageRepository)
    {
        $this->em = $em;
        $this->pageRepository = $pageRepository;
    }
    /**
     * @Route("/", name="index")
     */
    public function index(PageRepository $pageRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page', 1);
        $pages = $pageRepository->getRootNodes();
        return $this->render('admin/page/index.html.twig', [
            'pages' => $pages,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="edit")
     * @Route("/add/", name="add")
     *
     */
    public function edit(Request $request, int $id = NULL, PageRepository $pageRepository)
    {
        if($id){
            $page = $pageRepository->findOneBy(['id' => $id]);
        } else{
            $page = new Page();
        }
        $form = $this->createFormBuilder($page)
            ->add('title', TextType::class,['label'=>'Название','required' => true])
            ->add('metaTitle', TextType::class,['label'=>'Meta title','required' => false])
            ->add('metaKeywords', TextType::class,['label'=>'Meta keywords','required' => false])
            ->add('metaDescription', TextType::class,['label'=>'Meta description','required' => false])
            ->add('parent', EntityType::class, [
                // looks for choices from this entity
                'class' => Page::class,
                'label'=>'Родительская страница',
                // uses the User.username property as the visible option string
                'choice_label' => 'title',
                'required' => false
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Page $page */
            $page = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_page_edit',['id' => $page->getId()]);
        }

        return $this->render('admin/page/edit.html.twig',[
            'form' => $form->createView(),
            'page' => $page
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="delete")
     */
    public function delete(int $id, PageRepository $pageRepository)
    {
        $page = $pageRepository->findOneBy(['id' => $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($page);
        $em->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_page_index');
    }

    /**
     * @Route("/copy/{page}/", name="copy")
     */
    public function copy(Page $page)
    {
        $em = $this->getDoctrine()->getManager();
        $newPage = clone $page;
        $newPage->setCreated(new \DateTime());
        $em->persist($newPage);
        $em->flush();
        $this->addFlash('success','Запись успешно скопирована');
        return $this->redirectToRoute('admin_page_index');
    }

    /**
     * @Route("/update-tree/", name="update_tree")
     */
    public function updateTree(Request $request, PageRepository $pageRepository)
    {
        //dd($request);die();
        $tree = $request->get('tree',NULL);
        foreach ($tree as $rootElement) {
            //dd($root);die();
            $root = $pageRepository->find($rootElement['id']);
            $root->setParent(NULL);
            if(isset($rootElement['children'])){
                foreach ($rootElement['children'] as $child) {
                    $this->updateNode($child, $root);
                }
            }
            $this->em->persist($root);
        }
        $this->em->flush();
        return $this->json([
            'success' => true
        ]);
    }
    private function updateNode($nodeElement,Page $parent){
        $node = $this->pageRepository->find($nodeElement['id']);
        $node->setParent($parent);
        if(isset($nodeElement['children'])){
            foreach ($nodeElement['children'] as $child) {
                $this->updateNode($child, $node);
            }
            $this->em->persist($node);
        }
    }
}
