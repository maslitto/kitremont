<?php

namespace App\Controller\Admin;

use App\Entity\Bid;
use App\Repository\BidRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="bid_index")
     */
    public function index(BidRepository $bidRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page', 1);
        $bids = $bidRepository->getBidsPaginated($filter, $page);
        return $this->render('admin/dashboard/index.html.twig', [
            'bids' => $bids,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="bid_edit")
     * @Route("/add/", name="bid_add")
     *
     */
    public function edit(Request $request, int $id = NULL, BidRepository $bidRepository)
    {
        if($id){
            $bid = $bidRepository->findOneBy(['id' => $id]);
        } else{
            $bid = new Bid();
        }
        $form = $this->createFormBuilder($bid)
            ->add('active', CheckboxType::class,['label'=>'Активный','required' => false])
            ->add('dateStart', DateType::class,['label'=>'Дата начала','required' => true])
            ->add('title', TextType::class,['label'=>'Название','required' => false])
            ->add('teachers', TextType::class,['label'=>'Наставник(и)','required' => false])
            ->add('link', TextType::class,['label'=>'Ссылка','required' => false])
            //->add('image', TextType::class,['label'=>'Картинка','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bid = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bid);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_bid_edit',['id' => $bid->getId()]);
        }

        return $this->render('admin/bid/edit.html.twig',[
            'form' => $form->createView(),
            'bid' => $bid
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="bid_delete")
     */
    public function delete(int $id, BidRepository $bidRepository)
    {
        $bid = $bidRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($bid);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_bid_index');
    }

    /**
     * @Route("/copy/{bid}/", name="bid_copy")
     */
    public function copy(Bid $bid)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $newBid = clone $bid;
        $newBid->setCreated(new \DateTime());
        $entityManager->persist($newBid);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно скопирована');
        return $this->redirectToRoute('admin_bid_index');
    }
}
