<?php

namespace App\Controller\Admin;

use App\Entity\Photo;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/user", name="admin_user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(UserRepository $userRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page', 1);
        $users = $userRepository->getUsersPaginated($filter, $page);
        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="edit")
     * @Route("/add/", name="add")
     *
     */
    public function edit(Request $request, int $id = NULL, UserRepository $userRepository,UserPasswordEncoderInterface $passwordEncoder)
    {
        if($id){
            $user = $userRepository->findOneBy(['id' => $id]);
        } else{
            $user = new User();
        }
        $form = $this->createFormBuilder($user)
            ->add('email', TextType::class,['label'=>'Email','required' => false])
            ->add('password', TextType::class,['label'=>'Password','required' => false])
            ->add('roles', ChoiceType::class,[
                'label'=>'Роли',
                'required' => false,
                'multiple' =>true,
                'choices'=> [
                    "User" => "ROLE_USER",
                    "Admin" => "ROLE_ADMIN",
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $password = $user->getPassword();
            $user->setPassword($passwordEncoder->encodePassword($user,$password));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_user_edit',['id' => $user->getId()]);
        }

        return $this->render('admin/user/edit.html.twig',[
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="delete")
     */
    public function delete(int $id, UserRepository $userRepository)
    {
        $user = $userRepository->findOneBy(['id' => $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * @Route("/copy/{user}/", name="copy")
     */
    public function copy(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $newUser = clone $user;
        $newUser->setCreated(new \DateTime());
        $em->persist($newUser);
        $em->flush();
        $this->addFlash('success','Запись успешно скопирована');
        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * @Route("/upload-photo/{user}/", name="upload_photo")
     */
    public function uploadPhoto(User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $photo = new Photo();
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        if ($file) {
            $newFilename = uniqid().'.'.$file->guessExtension();
            try {
                $webPath = '/uploads/'.$user->getId().'/';
                $file = $file->move($this->getParameter('publicPath').$webPath, $newFilename);

                //запишем новую и сохраним
                $photo->setFilepath($webPath.$file->getFilename());
                $photo->setFilename($file->getFilename());
                $photo->setUser($user);
                $em->persist($photo);
                $em->flush();
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
                return $this->json([
                    'errors'=> [
                        'file'=> $e->getMessage()
                    ]
                ],Response::HTTP_UNPROCESSABLE_ENTITY);
            }

        }
        $em->persist($photo);
        $em->flush();
        $response = [
            'image' => $photo->getFilepath(),
            'url' => $this->generateUrl('admin_user_remove_photo',['photo' => $photo->getId()])
        ];
        return $this->json($response);
    }

    /**
     * @Route("/remove-photo/{photo}", name="remove_photo")
     */
    public function removePhoto(Photo $photo,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if($photo->getFilepath()){
            $filesystem = new Filesystem();
            $filesystem->remove($this->getParameter('publicPath').$photo->getFilepath());
            $em->remove($photo);
            $em->flush();
        }
        $response = [
            'success' => true,
        ];
        return $this->json($response);
    }


}
