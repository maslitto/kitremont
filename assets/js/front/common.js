import 'ion-rangeslider';
import 'ion-rangeslider/css/ion.rangeSlider.css'
import 'magnific-popup';
import 'magnific-popup/dist/magnific-popup.css'
import 'smoothscroll';
import 'mobile-detect/mobile-detect.min';
//import 'jquery-colorbox';
//import 'jquery-colorbox/example2/colorbox.css';
import 'lightbox2';
import 'lightbox2/dist/css/lightbox.css';
import './masonry.pkgd.min'

let common = (function () {
    return {
        init: function () {
            //let md = new MobileDetect(window.navigator.userAgent);
            common.initMap();
            common.initSlider();
            common.initCalc();
            common.initPopups();
            common.initSmoothScroll();
            common.initMobileMenu();
            common.initStickyHeader();
            common.initAntiSpam();
            //common.initColorBox();
            //common.initMasonry();
        },
        initMap: function () {


            if(typeof(ymaps) !== 'undefined'){
                ymaps.ready(function () {
                    var center = [59.984458, 30.360640];

                    var myMap = new ymaps.Map('map', {
                        center: center,
                        zoom: 16,
                        controls: []
                    }, {
                        searchControlProvider: 'yandex#search'
                    });
                    var marker = new ymaps.Placemark([59.984458, 30.360640], {
                        hintContent: '195197, Россия, Санкт-Петербург, Полюстровский проспект, 72Б , оф. 108',
                        balloonContent: '195197, Россия, Санкт-Петербург, Полюстровский проспект, 72Б , оф. 108',
                    }, {
                        iconLayout: 'default#image',
                        iconImageHref: 'build/img/map.svg',
                        iconImageSize: [41, 48],
                        iconImageOffset: [-5, -38]
                    });

                    myMap.behaviors.disable('scrollZoom');
                    myMap.geoObjects.add(marker);

                });
            }

        },

        initSlider: function () {
            $(".js-slider").ionRangeSlider({
                min: 10,
                max: 150,
                from: 50,
                skin: "round"
            });
        },

        initCalc: function () {
            $('.js-calc__next').click(function (e) {
                e.preventDefault();
                let $calc = $(this).closest('.js-calc'),
                    step = parseInt($calc.attr('data-step')),
                    nextStepId = step + 1,
                    $nextStep = $calc.find('[data-id="'+String(nextStepId)+'"]')
                ;
                $('.js-step').removeClass('active');

                $nextStep.addClass('active');
                if(nextStepId > 5){
                    $('.js-calc__nav').hide();
                }
                $('.js-calc__back').removeClass('disabled');
                $('.js-step-current').text(nextStepId);
                $calc.attr('data-step',nextStepId);
            });

            $('.js-calc__back').click(function (e) {
                e.preventDefault();

                var $calc = $(this).closest('.js-calc'),
                    step = parseInt($calc.attr('data-step')),
                    prevStepId = step - 1,
                    $prevStep = $calc.find('[data-id="'+String(prevStepId)+'"]')
                ;
                if(step===1){
                    return
                } else if (step===2){
                    $('.js-calc__back').addClass('disabled');
                }
                $('.js-step').removeClass('active');
                $prevStep.addClass('active');
                $calc.attr('data-step',prevStepId);
                $('.js-step-current').text(prevStepId);
                if(step <= 6){
                    $('.js-calc__nav').show();
                }
            });

            $('.js-calc-form').on('form_success', function() {
                $('.js-form__content').hide();
                $(this).find('.js-form__success').show();
                $('.js-form-success-phone').text($(this).find('.js-phone').val());
            });
        },

        initPopups: function () {

            $('.js-popup').click(function (e) {
                e.preventDefault();
                var src = $(this).attr('href');
                if(!(typeof ($(this).data('form'))=== undefined)){
                    $('.js-form-name').attr('value',$(this).data('form'));
                }
                $.magnificPopup.open({
                    items: {
                        src: src, // can be a HTML string, jQuery object, or CSS selector
                        type: 'inline'
                    }
                })
            });

            $('.js-mfp-close').click(function () {
                var magnificPopup = $.magnificPopup.instance;
                magnificPopup.close();
            });

            $('.js-popup-back').click(function (e) {
                e.preventDefault();
                var $form = $(this).closest('.js-form'),
                    $success = $form.find('.js-form__success'),
                    $content = $form.find('.js-form__content');
                $success.hide();
                $content.show();
            });

            $('.js-popup__form').on('form_success', function() {
                $(this).find('.js-form__content').hide();
                $(this).find('.js-form__success').show();
                $('.js-form-success-phone').text($(this).find('.js-phone').val());
            });

        },

        initSmoothScroll: function () {
            //var scroll = new SmoothScroll('a[href*="#"]');
        },
        initMobileMenu: function () {
            $('.js-menu-toggler').click(function (e) {
                e.preventDefault();
                $('.js-mobile-menu').toggleClass('open');
            })
            $('.js-mobile-menu__link').click(function (e) {
                $('.js-mobile-menu').toggleClass('open');

            })
        },

        initStickyHeader: function() {
            var nav = $('.js-header');
            var lastScrollTop;
            window.addEventListener("scroll", function() {  // listen for the scroll
                var st = window.pageYOffset || document.documentElement.scrollTop;
                if (st > lastScrollTop){
                    nav.css('opacity','0'); // hide the nav-bar when going down
                    nav.removeClass('sticky');
                }

                else {
                    nav.css('opacity', 1); // display the nav-bar when going up
                    nav.addClass('sticky');
                }
                lastScrollTop = st;
                if(st < 10){
                    nav.removeClass('sticky');
                }
            }, false);
        },

        initAntiSpam: function () {
            $(document).ready(function () {
                $('.js-antispam').val('no-bots-antispam');
            })
        },

        initColorBox: function () {
			$('.js-colorbox').colorbox({rel:'gal'});
		},
		initMasonry: function () {
			$('.js-masonry-grid').masonry({
				// options
				itemSelector: '.js-grid-item',
				columnWidth: 200
			});
		}
    }
})();
common.init();