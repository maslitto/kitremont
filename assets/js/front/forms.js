import 'jquery.maskedinput/src/jquery.maskedinput';
var forms = (function () {
    var emailRe = /^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i;
    var phoneRe = /^[- +()0-9]+$/i;
    return {
        init: function () {
            forms.phoneMask();
            forms.createNumberType();
            forms.placeholderMove();
            forms.inputChange();
            forms.submit();
            forms.placeholderInit();
            forms.processing();
        },

        /**
         * Добавляет маску на инпуты с типом phone
         */
        phoneMask: function () {
            var $phoneInput = $(".js-phone");
            if ($phoneInput.length) {
                $phoneInput.mask("+7 (999) 999-99-99", {autoclear:false});
            }
        },

        /**
         * Инициализирует инпуты с типом Number
         */
        createNumberType: function () {
            $('.js-form__number').each(function () {
                var $input = $(this),
                    $plus = $input.siblings('.js-form__number-plus'),
                    $minus = $input.siblings('.js-form__number-minus'),
                    min = parseFloat($input.attr('min')),
                    max = parseFloat($input.attr('max')),
                    step = $input.attr('step') ? parseFloat($input.attr('step')) : 1,
                    oldValue = parseFloat($input.val());
                $input.data('start-value', $input.val());
                $input.data('prev-value', $input.val());
                if (oldValue >= max) {
                    $input.val(max);
                    $plus.addClass('maxed');
                } else if (oldValue <= min) {
                    $input.val(min);
                    $minus.addClass('maxed');
                }
                $input.change(function () {
                    oldValue = parseFloat($(this).val());
                    if (oldValue >= max) {
                        $input.val(max);
                        $plus.addClass('maxed');
                        $minus.removeClass('maxed');
                        oldValue = max;

                    } else if (oldValue <= min) {
                        $input.val(min);
                        $minus.addClass('maxed');
                        $plus.removeClass('maxed');
                        oldValue = min;
                    }
                    oldValue = ( $(this).val() - $(this).data('prev-value') ) ;
                    $(this).data('prev-value', $(this).val());
                });
                $plus.click(function () {
                    oldValue = parseFloat($input.val());
                    $minus.removeClass('maxed');
                    if (oldValue >= max) {
                        $input.val(oldValue);
                        $plus.addClass('maxed');
                    } else {
                        $plus.removeClass('maxed');
                        $input.val(oldValue + step);
                        $input.data('prev-value', +$input.data('prev-value') + step );
                        if (oldValue + step >= max) {
                            $plus.addClass('maxed');
                        }
                        $input.trigger('change');
                    }
                });
                $minus.click(function () {
                    oldValue = parseFloat($input.val());
                    $plus.removeClass('maxed');
                    if (oldValue <= min) {
                        $input.val(oldValue);
                        $minus.addClass('maxed');
                    } else {
                        $minus.removeClass('maxed');
                        $input.val(oldValue - step);
                        $input.data('prev-value', +$input.data('prev-value') - step );
                        if (oldValue - step <= min) {
                            $minus.addClass('maxed');
                        }
                        $input.trigger('change');
                    }
                });
            });
        },

        placeholderInit: function() {
            $('.js-input').each(function() {
                var $input = $(this);

                if ($input.val() !== '') {
                    $input.addClass('active');
                } else {
                    $input.removeClass('active valid');
                }
            });
        },

        /**
         * Сдвигает плейсхолдер при фокусе добавляя класс
         */
        placeholderMove: function () {
            $('.js-input').change(function () {
                if ($(this).val() !== '') {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active valid');
                }
            });
        },

        /**
         * Убирает класс ошибки при фокусе и
         * инициализирует валидацию на событие change
         */
        inputChange: function () {
            var $inputs = $('.js-input');
            $inputs.focus(function () {
                $(this).removeClass('error');
            });
            $inputs.on('change', function () {
                if ($(this).hasClass('js-required') || $(this).hasClass('js-validate')) {
                    forms.inputValidate($(this));
                }
            });
        },

        /**
         * Валидация инпута
         */
        inputValidate: function ($element) {
            var value = $element.val(),
                valid = false,
                $parent = $($element).parent('.form__text-input-b, .form__checkbox-input-b');
            if (value.length === 0 && !$element.hasClass('js-required')) {
                //console.log($element);
                $element.removeClass('error').removeClass('valid');
                if ($parent.length) {
                    $parent.removeClass('valid');
                }
                return;
            }
            switch ($element.attr('type')) {
                case 'email':
                    valid = emailRe.test(value);
                    break;
                case 'tel':
                    valid = phoneRe.test(value);
                    break;
                case 'checkbox':
                    valid = $element.prop('checked');
                    break;
                default:
                    valid = (value !== "");
                    break;
            }
            if (valid) {
                $element.removeClass('error').addClass('valid');
                if ($parent.length) {
                    $parent.addClass('valid').removeClass('error');
                }
            } else if ($element.hasClass('js-required') || $element.hasClass('js-validate')) {
                $element.removeClass('valid').addClass('error');
                if ($parent.length) {
                    //console.log('parent error')
                    $parent.removeClass('valid').addClass('error');
                }
            }
        },

        /**
         * Обработать ответ страницы
         */
        isErorrsReturnAndRender: function($form, response) {
            var errorMessage,
                hasErorrs = false;

            if (!response['errors'] || !Object.keys(response['errors']).length) {
                $form.find('.js-input').each(function() {
                    var $input = $(this);
                    var $parent = $input.parent();

                    $input.removeClass('error').addClass('valid');
                    $parent.addClass('valid');
                });

                var $success = $form.find('.js-form__success'),
                    $content = $form.find('.js-form__content');
                if ($success.length) {
                    $success.addClass('active');
                }
                if ($content.length) {
                    $content.hide();
                }
            }
            else if(response['errors']['all']) {
                errorMessage = response['errors']['all'];
                forms.showErrorMessage($form, errorMessage);
                hasErorrs = true;
            }
            else if(response['errors']) {
                $form.find('.js-input').each(function() {
                    var $input = $(this);
                    var $parent = $input.parent();

                    if (response['errors'][$input.attr('name')]) {
                        errorMessage = response['errors'][$input.attr('name')];
                        $parent.removeClass('valid');
                        $input.addClass('error');
                        forms.showErrorMessage($parent, errorMessage);
                    }
                    else if(response['errors']['all-nomessage']) {
                        $parent.removeClass('valid');
                        $input.removeClass('valid');
                        $input.addClass('error');
                    }
                    else {
                        $input.removeClass('error');
                    }
                });

                hasErorrs = true;
            }
            return hasErorrs;
        },

        /**
         * Отправка формы
         */
        submit: function () {
            $('.js-form').submit(function (event) {
                event.preventDefault();

                var $form = $(this),
                    dataCorrect = true,
                    method,
                    contentType,
                    processData,
                    data;
                $form.find('.js-input.js-required').each(function () { //:visible
                    forms.inputValidate($(this));
                });
                $form.find('.js-required').each(function () {
                    if ($(this).hasClass('error')) {
                        dataCorrect = false;
                    }
                });
                if (dataCorrect) {
                    $form.trigger('form_start');
                    if($form.hasClass('js-login-form')){
                        data = JSON.stringify({
                            username: $('.js-username').val(),
                            password: $('.js-password').val(),
                            token: $('.js-token').val()
                        });
                        contentType = 'application/json';
                        processData = true;
                    } else{
                        data = new FormData($form[0]);
                        contentType = false;
                        processData = false;
                    }

                    method = $form.attr('method');
                    $.ajax({
                        type: method ? method : 'POST',
                        url: $form.attr('action'),
                        data: data,
                        dataType: "json",
                        contentType: contentType,
                        processData: processData,
                        success: function(resp) {
                            if(!forms.isErorrsReturnAndRender($form, resp)) {
                                $form.trigger('form_success', resp);
                            }
                            else {
                                $form.trigger('form_error', resp);
                            }
                        },
                        error: function() {
                            forms.showErrorMessage($form, [$form.data('error-msg')]);
                            $form.trigger('form_error');
                        }
                    });
                } else {
                    return false;
                }
            });
            $('.js-submit').click(function (e) {
                e.preventDefault();
                $(this).closest('.js-form').submit();
            })
        },

        /**
         * Вывести сообщение об ошибке в форме
         */
        showErrorMessage: function($element, $messages) {
            for(var $key in $messages) {
                var $error = $('<div class="form__send-fail" style="opacity: 0">' + $messages[$key] + '</div>');
                //if ($element.data('error-msg')) {
                $element.append($error);
                $error.fadeTo("fast" , 1, function() {
                    setTimeout(function(){
                        $error.first().fadeOut("fast", function() {
                            $element.find('.form__send-fail').remove();
                        });
                    }, 1500);
                });
                //}
            }
        },

        processing: function() {
            $('.js-form').on('form_start', function() {
                $(this).addClass('processing');
            });

            $('.js-form').on('form_success form_error', function() {
                $(this).removeClass('processing');
            });

            $('.js-login-form').on('form_success', function(e,resp) {
                window.location.replace($(this).data('redirect'));
            });
        }
    }
})();
forms.init();