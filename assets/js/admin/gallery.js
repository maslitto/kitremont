import 'dropzone';
import 'dropzone/dist/dropzone.css'
var gallery = (function () {
    var $gallery = $('.js-gallery');
	return {
		init: function() {
            gallery.initDropZone();
            gallery.removeImage();
		},
        initDropZone: function() {
			var $dropzone = $('.js-dropzone'),
            	url = $dropzone.data('url'),
                ownerId = $dropzone.data('owner-id');
			$dropzone.dropzone({
				url: url,
                acceptedFiles: "image/*",
                dictDefaultMessage: 'Перетащите в это окно файлы для загрузки',
                init: function() {
                    this.on('sending', function(file, xhr, formData){
                        formData.append('ownerId', ownerId);
                    });
                },
                success: function (file, response) {
                    $gallery.append('<div class="col-sm-1">\n' +
                        '<div class="gallery__image">\n' +
                        '<a href="#" class="js-remove-image gallery__image-remove" data-url="'+response.url+'"><i class="fa fa-close"></i></a>\n' +
                        '<img src="'+response.image+'" alt="" class="img-responsive thumbnail">\n' +
                        '</div>\n' +
                        '</div>');
                }
            });

		},
        removeImage: function () {
            $(document).on('click','.js-remove-image',function(e){
                e.preventDefault();
                var id = $(this).data('id'),
                    url = $(this).data('url'),
                    $image = $(this).closest('.js-gallery-image')
                 ;
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: id
                    },
                    success: function (response) {
                        $image.remove();
                    }
                });
            })
        }
	}
})();
gallery.init();