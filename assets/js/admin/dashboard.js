import 'sweetalert/dist/sweetalert.min';
import 'metismenu/dist/metisMenu';
import 'metismenu/dist/metisMenu.css';
import 'jquery-nestable';

let dashboard = (function () {
	return {
		init: function () {
			dashboard.initMetisMenu();
			dashboard.initSweetAlert();
			dashboard.smoothlyMenu();
			dashboard.initSideMenuEvents();
			dashboard.nestable();
		},

		initMetisMenu: function () {
			// MetisMenu
			let sideMenu = $('#side-menu').metisMenu({
				preventDefault: false,
			});

			sideMenu.on('shown.metisMenu', function (e) {
				dashboard.menuFixHeight();
			});
		},

		initSweetAlert: function () {
			$('.js-delete-btn').click(function (e) {
				e.preventDefault();
				let currentElement = $(this);
				swal({
					title: "Точно удалить?",
					text: "Удаленная запись не подлежит восстановлению!",
					icon: "warning",
					buttons: ["Отмена", "Удалить"],
					dangerMode: true,
				})
					.then((willDelete) => {
						if (willDelete) {
							swal({
								text: "Успешное удаление",
								icon: "info",
								buttons: false,
							});
							window.location.href = currentElement.attr('href');

						} else {
							swal("Может и правильно");
						}
					});
			});
			$('.js-clone-btn').click(function (e) {
				e.preventDefault();
				let currentElement = $(this);
				swal({
					title: "Точно копировать?",
					icon: "warning",
					buttons: ["Отмена", "Копировать"],
					dangerMode: true,
				})
					.then((willDelete) => {
						if (willDelete) {
							swal({
								text: "Успешное копирование",
								icon: "info",
								buttons: false,
							});
							window.location.href = currentElement.attr('href');

						} else {
							swal("Может и правильно");
						}
					});
			});
		},

		menuFixHeight: function ()  {
			let heightWithoutNavbar = $("body > #wrapper").height() - 62;
			$(".sidebar-panel").css("min-height", heightWithoutNavbar + "px");

			let navbarheight = $('nav.navbar-default').height();
			let wrapperHeight = $('#page-wrapper').height();

			if (navbarheight > wrapperHeight) {
				$('#page-wrapper').css("min-height", navbarheight + "px");
			}

			if (navbarheight < wrapperHeight) {
				$('#page-wrapper').css("min-height", $(window).height() + "px");
			}

			if ($('body').hasClass('fixed-nav')) {
				if (navbarheight > wrapperHeight) {
					$('#page-wrapper').css("min-height", navbarheight + "px");
				} else {
					$('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
				}
			}
	},

	    smoothlyMenu: function() {
			if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
				// Hide menu in order to smoothly turn on when maximize menu
				$('#side-menu').hide();
				// For smoothly turn on menu
				setTimeout(
					function () {
						$('#side-menu').fadeIn(400);
					}, 200);
			} else if ($('body').hasClass('fixed-sidebar')) {
				$('#side-menu').hide();
				setTimeout(
					function () {
						$('#side-menu').fadeIn(400);
					}, 100);
			} else {
				// Remove all inline style from jquery fadeIn function to reset menu state
				$('#side-menu').removeAttr('style');
			}
		},

		initSideMenuEvents: function () {
			// Fixed Sidebar
			$(window).bind("load", function () {
				if ($("body").hasClass('fixed-sidebar')) {
					$('.sidebar-collapse').slimScroll({
						height: '100%',
						railOpacity: 0.9
					});
				}
			});

			$(window).bind("load resize scroll", function () {
				// Full height of sidebar
				setTimeout(function(){
					if (!$("body").hasClass('body-small')) {
						dashboard.menuFixHeight();
					}
				})

			});
			// Minimalize menu when screen is less than 768px
			$(window).bind("resize", function () {
				if ($(this).width() < 769) {
					$('body').addClass('body-small')
				} else {
					$('body').removeClass('body-small')
				}
			});

		},

		nestable: function () {
			$(document).ready(function(){
				$('#nestable').nestable({group: 1}).on('change', function (e) {
					$.ajax({
						url: $(this).data('action'),
						method: 'POST',
						dataType: 'json',

						data: {tree:$(this).nestable('serialize')},
						success: function(resp) {
							console.log(resp);
						},
						error: function() {

						}
					})
				});
				$('.js-nestable-menu button').on('click', function (e) {
					var target = $(e.target),
						action = target.data('action');
					if (action === 'expand-all') {
						$('.dd').nestable('expandAll');
					}
					if (action === 'collapse-all') {
						$('.dd').nestable('collapseAll');
					}
				});
			});
		}

	}
})();
dashboard.init();


